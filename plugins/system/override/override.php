<?php
defined('_JEXEC') or die;


class plgSystemOverride extends JPlugin
{
    public function onAfterRoute()
    {
        // JLoader::register('ContentViewArticle', 'code/view.html.php', true);
        JLoader::register('ContentModelArticles', 'code/articles.php', true);
        JLoader::register('ContentModelCategory', 'code/category.php', true);
    } 
}