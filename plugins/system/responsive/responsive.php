<?php
/**
 * @version		:  2012-09-30 00:10:12$
 * @author		 Efatek
 * @package		reponsive
 * @copyright	Copyright (C) 2012- . All rights reserved.
 * @license		
 */

defined('_JEXEC') or die;

class  plgSystemResponsive extends JPlugin
{
	private $PLATFORM_PC = 1;
	private $PLATFORM_TABLET = 2;
	private $PLATFORM_MOBILE = 3;
	private $reponsive = array();
	private $changeTmpl;
	
	function __construct(& $subject, $config)
	{
		$this->reponsive[$this->PLATFORM_PC] = 'large';
		$this->reponsive[$this->PLATFORM_TABLET] = 'middle';
		$this->reponsive[$this->PLATFORM_MOBILE] = 'small';
		
		$this->changeTmpl = false;
		
		parent::__construct($subject, $config);
	}

	function onAfterInitialise()
	{
		$app = JFactory::getApplication();
		if ($app->isAdmin() || JDEBUG) {
			return;
		}
		// load library
		// see: http://code.google.com/p/php-mobile-detect/
		include_once (JPATH_BASE.DIRECTORY_SEPARATOR.'rd'.DIRECTORY_SEPARATOR.'Mobile_Detect.php');
		
		$doc = JFactory::getDocument();
		
		$platform = new Mobile_Detect();
		$platformId = JRequest::getInt('platform', $this->PLATFORM_PC);
		$doc = JFactory::getDocument();
		$this->reponsive[$this->PLATFORM_PC] = $doc->template;
		
		// 使用者手動切換版本
		$prevVersion = $app->getUserState('responsive.userSwitch', $this->PLATFORM_MOBILE);
		$userSwitch = JRequest::getInt('userswitch', $prevVersion);
		$app->setUserState('responsive.userSwitch', $userSwitch);
		
		if($prevVersion != $userSwitch)
		{
			$app->redirect(JURI::getInstance());
		}
		
		// 平板
		if($platform->isTablet()){
			$app->setUserState('responsive.platformId', $this->PLATFORM_TABLET);
			$platformId = $this->PLATFORM_TABLET;
			if($this->changeTmpl){
				JRequest::setVar('platform', $this->PLATFORM_TABLET);
				JRequest::setVar('template', $this->reponsive[$this->PLATFORM_TABLET]);
			}
		// 行動板
		} else if($platform->isMobile()){
			$app->setUserState('responsive.platformId', $this->PLATFORM_MOBILE);
			$platformId = $this->PLATFORM_MOBILE;
			if($this->changeTmpl){
				JRequest::setVar('platform', $this->PLATFORM_MOBILE);
				JRequest::setVar('template', $this->reponsive[$this->PLATFORM_MOBILE]);
			}
		// PC
		} else {
			$app->setUserState('responsive.platformId', $platformId);
			if($this->changeTmpl){
				JRequest::setVar('template', $this->reponsive[$platformId] );
			}
		}
		
		// 是否為行動裝置且以行動版觀看
		$phone = ($userSwitch == $this->PLATFORM_MOBILE && $platformId == $this->PLATFORM_MOBILE);
		$app->setUserState('responsive.layout.phone', $phone);

		return true;
	}
}
