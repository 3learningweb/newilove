<?php
class plgContentJVCommentInstallerScript {

   function install($parent) {
   
	  echo '<p>Installed plugin JV Comment 3.0.1</p>';
	  
	  $db = JFactory::getDBO();
	  $query = "update #__extensions set enabled = 1 where type='plugin' and element='jvcomment' and folder='content'";
	 
	  $db->setquery($query);
	  if(!$db->query()){
		echo 'Please enable plugin jvcomment!';
	  }
	  
   }

   function uninstall($parent) {
      echo '<p>Uninstalled plugin JV Comment 3.0.1</p>';
   }

   function update($parent) {
      echo '<p>'. JText::_('1.6 Custom update script') .'</p>';
   }

   function preflight($type, $parent) {
      echo '<p>'. JText::sprintf('1.6 Preflight for %s', $type) .'</p>';
   }

   function postflight($type, $parent) {
      echo '<p>'. JText::sprintf('1.6 Postflight for %s', $type) .'</p>';
   }
}

?>