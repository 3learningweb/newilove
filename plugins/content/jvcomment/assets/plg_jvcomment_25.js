/*
 # Component	JV Comment
 # @version		2.5.1
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright © 2008-2012 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/
window.addEvent('domready',function(){

	var myjvcomment = new JVCOMMENT({el:'plgjvcomment'});
	myjvcomment.getCommentHTML();
	
});

var JVCOMMENT = new Class({
	Implements: [Options, Events],
	options:{
		el:''
	},
	initialize:function(options){
		this.setOptions(options);
		
	},
	getCommentHTML:function(){
		var 
			This 	= this,
			el 	 	= This.options.el,
			group 	= $(el).getProperty('objectgroup'),
			id		= $(el).getProperty('objectid')
		;
		var myrequest = new Request.HTML({
			url: 'index.php',
			method:'post',
			data:{
				'option':'com_jvcomment',
				'view':'comments',
				'tmpl':'component',
				'objectgroup':group,
				'objectid':id
			},
		    onRequest: function(){
		    	$(el).set('text', 'loading...');
		      },

		      onSuccess:function(responseText, responseXML){
		    	console.log(responseText);
		    	var list = responseText[1];
		    	
		    	$(el).empty().adopt(list);
		    	This.addEventButtons();
		    	
				This.setFormSend(list.getElement('#formjvcomment'));
		      }
		}).send();
		
	},
	setFormSend:function(form){
		var This = this;
		form.set('send',{
			onSuccess:function(res){
				form.getElements('.inputbox').each(function(el){
					el.value = '';
				});
				form.parent_id.value = 0;
				This.refesh();
			}
		});
	},
	addEventButtons:function(){
		var 
			
			This = this
		;
		$(this.options.el).addEvent('click',function(e){
			var target = e.target;
			
			if(target.hasClass('btn-trash')){
				This.remove(target);
			}else if(target.hasClass('btn-reply')){
				This.reply(target);
			}else if(target.hasClass('btn-edit')){
				This.edit(target);
			}else if(target.hasClass('btn-like')){
				if(target.hasClass('disabled')) return;
				This.like(target);
			}else if(target.hasClass('btn-refesh')){
				This.refesh();
			}else if(target.hasClass('submit')){
				This.submit(target.getParent());
			}else if(target.hasClass('addcomment')){
				var form = $('formjvcomment');
				form.removeClass('hidden');
				form.getElements('.inputbox')[0].focus();
			}
			
		});
		return;
		
	},
	submit:function(form){
		var 
			This = this,
			flag = true,
			reg = /^(\w+@\w+\.\w{2,4})$/
		;
		
		form.getElements('.required').each(function(el){
			if(el.value==''){
				el.getParent().getParent().addClass('error');
				flag = false;
			}else{
				if(el.hasClass('email')){
					if(reg.test(el.value)==false){
						el.getParent().getParent().addClass('error');
						flag = false;
					}else{
						el.getParent().getParent().removeClass('error');
					}
				}else{
					el.getParent().getParent().removeClass('error');
				}
				
			} 
			
		});
		if(flag){
			form.send();
			form.addClass('hidden');
		}
	},
	refesh:function(){
		var
			el	   = $(this.options.el),
			childs = null
		;
		//jvcommentlist
		new Request.HTML({
			url: 'index.php',
			method:'post',
			data:{
				option:'com_jvcomment',
				view:'comments',
				layout:'default_comments',
				tmpl:'component',
				objectgroup:el.getProperty('objectgroup'),
				objectid:el.getProperty('objectid')
			},
			onRequest:function(){
				
				$('commentlist').addClass('loading');
			
			},
			onSuccess: function(res){
				
				$('commentlist').empty().adopt(res[1]);
				$('commentlist').removeClass('loading');
		      }
		}).send();
	},
	like:function(el){
		var 
			span = el.getElement('.numberlike')
		;
		new Request({
			url: 'index.php',
			method:'post',
			data:{
				option:'com_jvcomment',
				task:'comment.like',
				id:el.getProperty('data-id')
			},
		   
			onSuccess: function(res){
				if(res){
					span.set('text',res);
				}
				span.getParent().addClass('disabled');
		      }
		}).send();
	},
	edit:function(el){
		console.log('edit');
	},
	reply:function(el){
		var form = $('formjvcomment');
		form.removeClass('hidden');
		new Fx.Scroll(window).toElement(form.getPrevious()).chain(function(){
			form.parent_id.value = el.getProperty('data-id');
			form.getElements('.inputbox')[0].focus();
		});
		
	},
	remove:function(el){
		new Request({
			url: 'index.php',
			method:'post',
			data:{
				option:'com_jvcomment',
				task:'comment.delete',
				id:el.getProperty('data-id')
			},
		   
			onSuccess: function(res){
				
		    	if(res=='ok'){
					el.getParent().getParent().getParent().destroy();
				}
		      }
		}).send();
		
	}
	
});

