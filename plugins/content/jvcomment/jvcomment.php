<?php
/**
 # Plugin	    JV Comment
 # @version		2.5.1
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright © 2008-2012 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/
// No direct access to this file
defined( '_JEXEC' ) or die( 'Restricted access' );
/**
 * Vote plugin.
 *
 * @package		Joomla.Plugin
 * @subpackage	Content.jvcomment
 */
class plgContentJVComment extends JPlugin
{
	private $commentlog;
	
	function onContentAfterDisplay($context, &$article, &$params, $limitstart = 0){
		
		switch($context){

			case 'com_virtuemart.productdetails':
				$id = JRequest::getInt('virtuemart_product_id');
				break;
			default:
				$id = JRequest::getInt('id');
		}
		
		if(!$id){
			//remove bbcode {jvcomment}
			$article->introtext = str_replace('{jvcomment}', '', $article->introtext);
		}else{
			//insert comment list
			$option = explode('.',$context);
			$objectgroup = $option[0];
			
			$html = $this->_addAssets($objectgroup,$id);
			
			if($context=='com_virtuemart.productdetails'){
				$article->text = str_replace('{jvcomment}', $html, $article->text);
			}else{
				return $html;
			}
		}

		return;
	}


	private function _addAssets($objectgroup,$id){
		$params = $this->params;
		$bootstrap = $params->get('bootstrap',0);
		$typecomment = $params->get('type','jvcomment');
		
		$document = JFactory::getDocument();
		
		switch ($typecomment){
			case 'jvcomment':
				JHTML::_('behavior.framework');
				$version = new JVersion();		
				$ver = $version->RELEASE;		
				if($ver == '2.5'){
					$document->addScript(JURI::root().'plugins/content/jvcomment/assets/plg_jvcomment_25.js');
					if($bootstrap){
						$document->addStyleSheet(JURI::root().'media/com_jvcomment/bootstrap/css/bootstrap.min.css');
					}
				}else{
					$document->addScript(JURI::root().'plugins/content/jvcomment/assets/plg_jvcomment_30.js');
					if($bootstrap){
// 						$document->addStyleSheet('media/jui/css/bootstrap.css');
// 						JHtml::_('bootstrap.framework');
					}
					
				}
				
				
				$document->addStyleSheet(JURI::root().'plugins/content/jvcomment/assets/plg_jvcomment.css');
				$document->addStyleSheet(JURI::root().'media/com_jvcomment/css/com_jvcomment.css');
				
				$html = '<div id="plgjvcomment" objectgroup="'.$objectgroup.'" objectid="'.$id.'"></div>';
				
				break;
			case 'facebook':
				$fbappid = $params->get('facebookappid','184212361704812');
				$fbwidth = $params->get('facebookwidth','470');
				$fbnum = $params->get('facebooknumpost','2');
				$fbtheme = $params->get('facebooktheme','');
				
				
				if($fbappid){
					$html = '<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId='.$fbappid.'";
  fjs.parentNode.insertBefore(js, fjs);
}(document, "script", "facebook-jssdk"));</script>';
					
					$url = JFactory::getURI()->toString();
					$html .= '<div class="fb-comments" data-href="'.$url.'" data-width="'.$fbwidth.'" data-num-posts="'.$fbnum.'" data-colorscheme="'.$fbtheme.'"></div>';
				}
				
				break;
		}
		
		return $html;
	}

	/**
	 * for delete chidrens jvcomment
	 * @param unknown_type $context
	 * @param unknown_type $table
	 * @return void|mixed
	 */
	public function onContentAfterDelete($context, $table){
		//use for com com_jvcomment
		if($context != 'com_jvcomment.comment') return ;
		
		$value['comment'] = array();
		
		$id = $table->id;
		$db = JFactory::getdbo();
		$userid = JFactory::getUser()->id;
		$datecreated = JFactory::getdate()->tosql();
		$ip = $_SERVER['REMOTE_ADDR'];

		$value['object_group'] = $table->object_group;
		$value['object_id'] = $table->object_id;
		
		$value['comment'][] = $table->comment;
		
		

		//get child comment ids
		$childids = array();
		$childids = $this->_getCommentChilds($childids,$id);
		$ids = $childids;
		
		$ids[] =$id;
		//delete child comments
		if($childids){
			$childids = implode(',',$childids);
			
			$query = "select comment from #__jvcomment_comment where id in ($childids)";
			$db->setquery($query);
			$deleteids = $db->loadColumn();
			$value['comment'] = array_merge($value['comment'],$deleteids);
			
			$query = "delete from #__jvcomment_comment where id in ($childids)";
			$db->setquery($query);
			if(!$db->query()){
				echo 'error on delete comment childs';
			}
		}
		//delete like from log
		$ids = implode(',',$ids);
		$query = "delete from #__jvcomment_log where type='like' and comment_id in ($ids)";
		$db->setquery($query);
		if(!$db->query()){
			echo 'error on delete log';
		}

		//insert log delete
		$value = json_encode($value);
		$query = "insert into #__jvcomment_log (comment_id,type,user_id,ip,datecreated,value)
		values ($id,'delete',$userid,'$ip','$datecreated','$value')";
		$db->setquery($query);
		return $db->query();

	}

	private function _getCommentChilds(&$childids,$parentid){
		$db = JFactory::getdbo();
		if(is_array($parentid)){
			$parentid = implode(',',$parentid);
		}

		$query = "select id from #__jvcomment_comment where parent_id in ($parentid) ";
		$db->setquery($query);
		$ids = $db->loadColumn();
		if($ids){
			$childids = array_merge($childids,$ids);
			$childids = $this->_getCommentChilds($childids,$ids);
		}

		return $childids;

	}
}
