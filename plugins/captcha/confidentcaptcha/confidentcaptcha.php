<?php
/**
 * @package Joomla.Plugin
 * @subpackage Captcha
 * @copyright Copyright (C) 2012 Confident Technologies.  All rights reserved.
 * @license   GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.environment.browser');
require_once(JPATH_BASE . "/plugins/captcha/confidentcaptcha/confidentcaptcha/ConfidentCaptchaClient.php");

/**
 * Confident CAPTCHA Plugin.
 *
 * @package Joomla.Plugin
 * @subpackage Captcha
 * @since 2.5
 */
  
class plgCaptchaConfidentCaptcha extends JPlugin
{
   private $client;
   private $_sessionNamespace = "CONFIDENTCAPTCHA_PROPERTIES";

   public function __construct($subject, $config)
   {
      parent::__construct($subject, $config);
	  $this->loadLanguage();
       $this->client = new ConfidentCaptchaClient();
   }
   
   /**
    * Initialize the captcha
	*
	* @param string $id The id of the field.
	*
	* @return Boolean True on success, false otherwise
	*
	* @since 2.5
	*/
    public function onInit($id)
	{


        //Switch to file based session
        $temp_session = $_SESSION; // backup all session data
        session_write_close();
        ini_set("session.save_handler","files"); // set session saved handler on file
        session_start();
        //Save properties to session
        $callback = JURI::base() . "plugins/captcha/confidentcaptcha/callback.php?session_id=" . session_id();
        $_SESSION[$this->_sessionNamespace]["api_username"] = $this->params->get("api_username");
        $_SESSION[$this->_sessionNamespace]["api_password"] = $this->params->get("api_password");
        $_SESSION[$this->_sessionNamespace]["customer_id"] = $this->params->get("customer_id");
        $_SESSION[$this->_sessionNamespace]["site_id"] = $this->params->get("site_id");
        $_SESSION[$this->_sessionNamespace]["display_style"] = $this->params->get("display_style");
        $_SESSION[$this->_sessionNamespace]["width"] = $this->params->get("width");
        $_SESSION[$this->_sessionNamespace]["height"] = $this->params->get("height");
        $_SESSION[$this->_sessionNamespace]["captcha_length"] = $this->params->get("captcha_length");
        $_SESSION[$this->_sessionNamespace]["captcha_color"] = $this->params->get("captcha_color");
        $_SESSION[$this->_sessionNamespace]["image_code_color"] = $this->params->get("image_code_color");
        $_SESSION[$this->_sessionNamespace]["logo_name"] = $this->params->get("logo_name");
        $_SESSION[$this->_sessionNamespace]["billboard_name"] = $this->params->get("billboard_name");
        $_SESSION[$this->_sessionNamespace]["noise_level"] = $this->params->get("noise_level");
        $_SESSION[$this->_sessionNamespace]["failure_policy_math"] = $this->params->get("failure_policy_math");
        $_SESSION[$this->_sessionNamespace]["show_letters"] = $this->params->get("show_letters");
        $_SESSION[$this->_sessionNamespace]["ajax_verify"] = $this->params->get("ajax_verify");
        $_SESSION[$this->_sessionNamespace]["max_tries"] = $this->params->get("max_tries");
        $_SESSION[$this->_sessionNamespace]["callback_url"] = $callback;
        session_write_close();
        //Switch back to Joomla session
        ini_set("session.save_handler","user"); // put back session saved handler on database
        $jd = new JSessionStorageDatabase();
        $jd->register(); // set required parameters
        session_start(); // restart //
        $_SESSION = $temp_session; // restore last session data

	   //Initialize variables
       $customerID = $this->params->get('customer_id');
	   $siteID = $this->params->get('site_id');
	   $apiUsername = $this->params->get('api_username');
	   $apiPassword = $this->params->get('api_password');
	   if( $customerID == NULL || $customerID == '' || $siteID == NULL || $siteID == '' || $apiUsername == NULL || $apiUsername == '' || $apiPassword == null || $apiPassword == '' )
	   {
	      throw new Exception('Mandatory field missing, please recheck your Confident Captcha credentials.');
	   }

       //Request an AJAX verified captcha if the property is set
       if($this->params->get('ajax_verify') == "TRUE"){
           $blockResponse = $this->getClient()->createBlock();
           $this->CAPTCHACode = $this->getClient()->createCaptcha($blockResponse->getBlockId());
       }
       else{
           $this->CAPTCHACode = $this->getClient()->createCaptcha();
       }

	   //Add jQuery library
	   JHtml::_('script', 'http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js');
	   $document = JFactory::getDocument();
	   $document->addStyleSheet(JURI::base(). "plugins/captcha/confidentcaptcha/styleOverride.css");

	   return true;
    }
	public function onDisplay($name, $id, $class)
	{
	   return $this->CAPTCHACode;
    }
	public function onCheckAnswer($code)
	{
        //Switch to file based session
        //Start session if not already started
        $temp_session = $_SESSION; // backup all session data
        session_write_close();
        ini_set("session.save_handler","files"); // set session saved handler on file
        session_start();

        //Check the captcha result
        $result = $this->getClient()->checkCaptcha($_POST);

        //Reset the session
        if(isset($_SESSION[$this->_sessionNamespace])){
            unset($_SESSION[$this->_sessionNamespace]);
        }


        //Switch back to Joomla session
        session_write_close();
        ini_set("session.save_handler","user"); // put back session saved handler on database
        $jd = new JSessionStorageDatabase();
        $jd->register(); // set required parameters
        session_start(); // restart //
        $_SESSION = $temp_session; // restore last session data

        if($result->wasCaptchaSolved() == true){
            return true;
        }
        else{
            $this->_subject->setError(JText::_('Captcha solution incorrect, try again.'));
            return false;
        }
	}

    private function getClient(){
        $client = new ConfidentCaptchaClient();
        $callback = JURI::base() . "plugins/captcha/confidentcaptcha/callback.php?session_id=" . session_id();
        //Set CAPTCHA credentials
        $credentials = new ConfidentCaptchaCredentials($this->params->get("api_username"), $this->params->get("api_password"), $this->params->get("customer_id"), $this->params->get("site_id"));
        $client->setCredentials($credentials);

        //Set CAPTCHA properties
        $confident_captcha_properties = new ConfidentCaptchaProperties();
        $confident_captcha_properties->setProperty('display_style', $this->params->get("display_style"));
        $confident_captcha_properties->setProperty('width', $this->params->get("width"));
        $confident_captcha_properties->setProperty('height', $this->params->get("height"));
        $confident_captcha_properties->setProperty('captcha_length', $this->params->get("captcha_length"));
        $confident_captcha_properties->setProperty('captcha_color', $this->params->get("captcha_color"));
        $confident_captcha_properties->setProperty('image_code_color', $this->params->get("image_code_color"));
        $confident_captcha_properties->setProperty('logo_name', $this->params->get("logo_name", ""));
        $confident_captcha_properties->setProperty('billboard_name', $this->params->get("billboard_name", ""));
        $confident_captcha_properties->setProperty('noise_level', $this->params->get("noise_level"));
        $confident_captcha_properties->setProperty('failure_policy_math', $this->params->get("failure_policy_math"));
        $confident_captcha_properties->setProperty('show_letters', $this->params->get("show_letters"));
        $confident_captcha_properties->setProperty('ajax_verify', $this->params->get("ajax_verify"));
        $confident_captcha_properties->setProperty('max_tries', $this->params->get("max_tries"));
        $confident_captcha_properties->setProperty('callback_url', $callback);
        $client->setCaptchaProperties($confident_captcha_properties);



        return $client;
    }

}
	