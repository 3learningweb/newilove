<?php
define('JPATH_BASE', dirname(dirname(dirname(dirname(__FILE__)))) );
require_once(JPATH_BASE . "/plugins/captcha/confidentcaptcha/confidentcaptcha/ConfidentCaptchaClient.php");

if(isset($_REQUEST['endpoint']) && isset($_REQUEST['confidentcaptcha_block_id'])){
    session_id($_REQUEST["session_id"]);
    session_start();
    $_sessionNamespace = "CONFIDENTCAPTCHA_PROPERTIES";
    $client = new ConfidentCaptchaClient();
    //Set CAPTCHA credentials
    $credentials = new ConfidentCaptchaCredentials($_SESSION[$_sessionNamespace]["api_username"], $_SESSION[$_sessionNamespace]["api_password"], $_SESSION[$_sessionNamespace]["customer_id"], $_SESSION[$_sessionNamespace]["site_id"]);
    $client->setCredentials($credentials);

    //Set CAPTCHA properties
    $confident_captcha_properties = new ConfidentCaptchaProperties();
    $confident_captcha_properties->setProperty('display_style', $_SESSION[$_sessionNamespace]["display_style"]);
    $confident_captcha_properties->setProperty('width', $_SESSION[$_sessionNamespace]["width"]);
    $confident_captcha_properties->setProperty('height', $_SESSION[$_sessionNamespace]["height"]);
    $confident_captcha_properties->setProperty('captcha_length', $_SESSION[$_sessionNamespace]["captcha_length"]);
    $confident_captcha_properties->setProperty('captcha_color', $_SESSION[$_sessionNamespace]["captcha_color"]);
    $confident_captcha_properties->setProperty('image_code_color', $_SESSION[$_sessionNamespace]["image_code_color"]);
    $confident_captcha_properties->setProperty('logo_name', $_SESSION[$_sessionNamespace]["logo_name"]);
    $confident_captcha_properties->setProperty('billboard_name', $_SESSION[$_sessionNamespace]["billboard_name"]);
    $confident_captcha_properties->setProperty('noise_level', $_SESSION[$_sessionNamespace]["noise_level"]);
    $confident_captcha_properties->setProperty('failure_policy_math', $_SESSION[$_sessionNamespace]["failure_policy_math"]);
    $confident_captcha_properties->setProperty('show_letters', $_SESSION[$_sessionNamespace]["show_letters"]);
    $confident_captcha_properties->setProperty('ajax_verify', $_SESSION[$_sessionNamespace]["ajax_verify"]);
    $confident_captcha_properties->setProperty('max_tries', $_SESSION[$_sessionNamespace]["max_tries"]);
    $confident_captcha_properties->setProperty('callback_url', $_SESSION[$_sessionNamespace]["callback_url"]);
    $client->setCaptchaProperties($confident_captcha_properties);


    $return = $client->callback($_REQUEST);
    header($return[0]);
    echo $return[1];
}
