<?php
/**
 * @version		:  2015-08-27 05:07:21$
 * @author		 
 * @package		Tabs
 * @copyright	Copyright (C) 2011- . All rights reserved.
 * @license		
 */

// no direct access
defined('_JEXEC') or die;
?>
<script>
(function($) {
	$(document).ready(function() {		
		var questionNumber=0;
		var questionBank=new Array();
		var stage="#game1";
		var stage2=new Object;
		var questionLock=false;
		var numberOfQuestions;
		var answerArray=new Array();
		var score=0;
		var currentAnswer=0;
		var answerRef="";
		// var click_num=0;
 	 
 		$.getJSON('<?php echo JURI::base(); ?>components/com_truelove/assets/json/activity.json', function(data) {
			for(i=0;i<data.quizlist.length;i++) { 
				questionBank[i]=new Array;
				questionBank[i][0]=data.quizlist[i].question;
				questionBank[i]['answer'] = data.quizlist[i].answer;
				var numberOfOptions=(data.quizlist[i].option.length);
				for(ii=0;ii<numberOfOptions;ii++){
					questionBank[i][ii+1]=data.quizlist[i].option[ii];
				}//ii
			}//i
			numberOfQuestions=questionBank.length; 
			 
			displayQuestion();
		});//gtjson
 

		function displayQuestion() {
			var numberOfOptions=questionBank[questionNumber].length-1;
			answerArray=[];
			
			for(i=0;i<numberOfOptions;i++) {
				answerArray[i]=questionBank[questionNumber][i+1];
			}
		 
			answerRef=questionBank[questionNumber]['answer'];
			scramble(answerArray);
	
			$(stage).append('<div class="questionText">'+questionBank[questionNumber][0]+'</div>');
	
			for(i=0;i<answerArray.length;i++){
				$(stage).append('<div class="option_block"><img class="option_img" src="<?php echo JURI::base(); ?>filesys/images/com_truelove/option_img.gif" alt="" /><div id="'+[i+1]+'" class="option"><img src="<?php echo JURI::base(); ?>filesys/images/com_truelove/option_active.png" alt="" style="display: none;" />'+answerArray[i]+'</div></div>');
			}
	 
	
			$(document).one("click", ".option", function() {
				if(questionLock==false){
					questionLock=true;
					//correct answer
					if(this.id==currentAnswer) {
				   		$(".result_img").html('<img src="<?php echo JURI::base(); ?>filesys/images/com_truelove/correct.gif" alt="" />');
				   		score++;
				   		$(".score span").text(score*4);
				 	}
				  	//wrong answer
				  	if(this.id!=currentAnswer) {
				 		$("#"+currentAnswer).css("background-image", "url(<?php echo JURI::base(); ?>filesys/images/com_truelove/option_correct.png)");
				   		$(".result_img").html('<img src="<?php echo JURI::base(); ?>filesys/images/com_truelove/wrong.gif" alt="" />');
				  	}
				  	$(".next").html('<img src="<?php echo JURI::base(); ?>filesys/images/com_truelove/next.gif" alt="" />'); // 下一題
			 	}
			 	
				$(document).one("click", ".next", function() {
					$(".result_img").html('');
					$(".next").html('');
					changeQuestion();
				});
			});


		$(".option").mouseover(function() {
			$(this).addClass("active");
			$(this).children("img").show();
		});
		
		$(".option").mouseout(function() {
			$(this).removeClass("active");
			$(this).children("img").hide();
		});

		}//display question

		function changeQuestion(){
			questionNumber++;
		
			if(stage=="#game1") {
				stage2="#game1";stage="#game2";
			}else{
				stage2="#game2";stage="#game1";
			}
		
			if(questionNumber<numberOfQuestions) {
				displayQuestion();
			}else{
				displayFinalSlide();
			}
		
		 	$(stage2).animate({"right": "+=410px"},"slow", function() {
		 		$(stage2).css('right','-410px');$(stage2).empty();
		 	});
		 	$(stage).animate({"right": "+=410px"},"slow", function() {
		 		questionLock=false;
		 	});
		}//change question
	

	
	
		function displayFinalSlide() {
			var value = $(".score span").text();
			var msg = "";
			if(parseInt(value) > 60) {
				msg = "哇！你真是小矮人村的英雄，小矮人村洋溢著滿滿的愛心與幸福喔，小矮人們非常感謝你呢！";
			}else{
				msg = "小矮人們的愛心蛋都破了，正在哭泣呢！再重來一次幫幫小矮人們吧！";
			}
			$(stage).append('<div class="resultText">'+ msg +'</div>');
			$(stage).append('<div class="submit_block"><input type="button" id="submit_btn" value="重玩一次" /></div>');
			
			$("#navContent").addClass("resultContent");
			
			// change game_img
			$(".game_img").addClass("result_bg");
			$(".game_img img").attr("src", "<?php echo JURI::base(); ?>filesys/images/com_truelove/result.png");
		}//display final slide
	
	
	
		function scramble(scrambleArray) {
			var temp="";var rnd2=0;var rnd3=0;
			
			for(i=0 ; i<30 ; i++) {
				// 選項打亂
				// rnd2=Math.floor(Math.random()*scrambleArray.length);
				// rnd3=Math.floor(Math.random()*scrambleArray.length);
			 
				// temp=scrambleArray[rnd3];
				// scrambleArray[rnd3]=scrambleArray[rnd2];
				// scrambleArray[rnd2]=temp;
			}
			
			for(i=0 ; i<answerArray.length ; i++){
				answerArray[i]=scrambleArray[i]; 
				if(answerArray[i]==answerRef) {
					currentAnswer=i+1;
				}
			}
		}//scramble

	});//doc ready
})(jQuery);
