<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
defined( '_JEXEC' ) or die( 'Restricted access' );

$document = JFactory::getDocument();
$app=JFactory::getApplication();
$title=JRequest::getInt('id')!=0?$this->category_info->quiztitle: JText::_('Quizzes');
$document->setTitle($title.' - '.$app->getCfg( 'sitename' ) );
JHtml::_('behavior.framework');
JHtml::_('behavior.tooltip');
JHTML::_('behavior.modal');

/*$document->addScript(JURI::root().'components/com_vquiz/assets/js/library.js');
$document->addScript(JURI::root().'components/com_vquiz/assets/js/jquery-ui.js');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/jquery-ui.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/smoothness.css');*/
 $document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/style.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/responsive_layout.css');

 ?>
<form action="<?php echo JRoute::_('index.php?option=com_vquiz&view=quizmanager')?>" method="post" name="adminForm" id="adminForm">
          <div id="maindiv" class="board maindiv">

               <div class="inner_board"> 

				<h1 class="play_quiz"><?php echo (JRequest::getInt('id')!=0?$this->category_info->quiztitle: JText::_('All Quizzes'));?></h1>	

                <div class="cat_text"><?php echo JText::_('CHOOSE_CATEGORY'); ?></div>	  

				<?php

			   if(empty($this->quizzes))

			    echo JText::_('EMPTY_QUIZZES');

				$k = 0;

				for ($i=0, $n=count( $this->quizzes ); $i < $n; $i++)	{

				$row = &$this->quizzes[$i];

				$link 		= JRoute::_( 'index.php?option=com_vquiz&view=quizmanager&layout=description&id='. $row->id );

				?>
                  
                <div class="<?php echo "row$k"; ?> vcat">
                <a class="cat_img" href="<?php echo $link;?>">               
				<?php 
				$thumbail_width=!empty($this->configuration->categorythumbnailwidth)?$this->configuration->categorythumbnailwidth:'';
				$thumbail_heigh=!empty($this->configuration->categorythumbnailheight)?$this->configuration->categorythumbnailheight:'';
                if(!empty($row->image)){ 
                echo '<img src="'.JURI::root().'/media/com_vquiz/vquiz/images/photoupload/quizzes/thumbs/thumb_'.$row->image. '" alt="'.$row->quizzes_title.'" title="'.$row->quizzes_title.'" width="'.$thumbail_width.'" height="'.$thumbail_heigh.'" / >'; 
                }else { echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/no_image.png" alt="Image Not available" border="1" 
				 width="'.$thumbail_width.'" height="'.$thumbail_heigh.'"/>';} 
                
                ?>
				</a>   
                <a class="title" href="<?php echo $link; ?>"><?php echo $row->quizzes_title; ?></a>
				</div>

				<?php

				$k = 1 - $k;

				}

				?>           

			    </div>

 
	       </div>
    <tfoot>
    <tr>
    <td colspan="0"><?php echo $this->pagination->getListFooter(); ?></td>
    </tr>
    </tfoot>
 
<div class="clr"></div> 
<input type="hidden" name="option" value="com_vquiz" /> 
<input type="hidden" name="view" value="quizmanager" />
<input type="hidden" name="task" value=""/>  
<input type="hidden" name="category_id" value="<?php echo $categoryid = JRequest::getInt('id');?>" />  
</form>