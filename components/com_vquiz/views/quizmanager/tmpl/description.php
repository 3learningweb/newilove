<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
defined( '_JEXEC' ) or die( 'Restricted access' );

$document = JFactory::getDocument();
$app=JFactory::getApplication();
$title=$this->description->quizzes_title;
$document->setTitle($title.' - '.$app->getCfg( 'sitename' ) );

$document->addScript(JURI::root().'components/com_vquiz/assets/js/library.js');
//$document->addScript(JURI::root().'components/com_vquiz/assets/js/jquery-ui.js');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/style.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/responsive_layout.css');
//$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/jquery-ui.css');
//$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/smoothness.css');
 
 ?>

 <script type="text/javascript"> 
 var vg=jQuery.noConflict();
vg(document).ready(function(){

			 vg('#countinue_button').click(function(){
			  var quizzesid=vg(this).attr('qid');
			  quizzesdesc(quizzesid);
			 });

			
function quizzesdesc(quizzesid)
{

		 jQuery.ajax(
			{
			url:"<?php echo JURI::base() ; ?>",
			type: "POST",
			dataType:"json",
			data: {'option':'com_vquiz', 'view':'quizmanager', 'task':'quizzesdesc', 'tmpl':'component', 'quizzesid':quizzesid, "<?php echo JSession::getFormToken(); ?>":1},  
			
			 beforeSend: function()	{
							jQuery(".poploadingbox").css('display','inline-block');
						  },

						  complete: function()	{
							jQuery(".poploadingbox").hide();
						  },

				success: function(data)	
				{	 

					var access=data.access;
					var linkurl=data.link_url;
				    if(access!=0){
					alert('<?php echo JText::_('NOTAUTHORISHED');?>');	
					return false;
					}
					else{
				  // var newulr=linkurl+'?Itemid=284';
					 window.open(linkurl,'_self');
					return false;
					}
 
				}

	       });

}
 
});

</script> 
 <div class="poploadingbox">
<?php echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/loading.gif"   />' ?>
</div>
    <div id="maindiv" class="board desc" >
    <h1><?php echo $this->description->quizzes_title; ?></h1>
    <?php echo $this->description->description; ?>
    <div id="description" style="text-align:center;">
  <button id="countinue_button" class="btn btn-success" qid="<?php echo $this->description->id; ?>"><?php echo JText::_('Continue..');?></button>
    </div>
    </div>
 
<div class="clr"></div> 
<input type="hidden" name="option" value="com_vquiz" /> 
<input type="hidden" name="view" value="quizmanager" />
<input type="hidden" name="task" value="" />  

 