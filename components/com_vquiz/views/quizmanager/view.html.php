<?php

/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class VquizViewQuizmanager extends JViewLegacy {

	function display($tpl = null) {

		$layout = JRequest::getCmd('layout', '');
		$doc = JFactory::getDocument();


		if ($layout == 'quizzes') {
			$this->quizzes = $this->get('Quizzes');
			$this->category_info = $this->get('Category_info');

			if (!empty($this->category_info)) {

				$metadesc = $this->category_info->meta_desc;
				$metakey = $this->category_info->meta_keyword;
				$doc->setMetadata('keywords', $metakey);
				$doc->setDescription($metadesc);
			}

			$pagination = $this->get('Pagination');
			$this->assignRef('pagination', $pagination);

			if (empty($this->quizzes)) {
				JFactory::getApplication()->enqueueMessage(JText::_('NO_QUIZ_FOUND'));
				return false;
			}
		} else if ($layout == 'description') {
			$this->description = $this->get('Description');

			if (empty($this->description)) {
				jerror::raiseWarning('403', JText::_('UNAUTH_ACCESS'));
				return false;
			}
		} else if ($layout == 'quizresult') {
			$this->showresult = $this->get('Showresult');
		} else {
			// 一般測驗
			$this->configuration = $this->get('Configuration');

			$this->fitem = $this->get('Fitem');
			$this->resultcats = $this->get('Resultcats');


			$metadesc = $this->fitem->quizzes->meta_desc;
			$metakey = $this->fitem->quizzes->meta_keyword;
			$doc->setMetadata('keywords', $metakey);
			$doc->setDescription($metadesc);


			if (empty($this->fitem->ques)) {
				JFactory::getApplication()->enqueueMessage(JText::_('NO_QUESTION_FOUND'));
				return false;
			}
		}

		parent::display($tpl);
	}

}

