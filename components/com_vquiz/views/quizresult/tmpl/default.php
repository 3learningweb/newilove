<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/ 
defined( '_JEXEC' ) or die( 'Restricted access' );

$document = JFactory::getDocument();

$document->addScript('components/com_vquiz/assets/js/library.js');

$document->addStyleSheet('components/com_vquiz/assets/css/style.css');

$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/adminpanel.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/icomoon.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/responsive_layout.css');

$document->addScript('components/com_vquiz/assets/js/jquery-ui.js');

 

?>

 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
 
  <script type="text/javascript">
Joomla.submitbutton = function(task) {
 
 				if (task == 'cancel') {
				Joomla.submitform(task, document.getElementById('adminForm'));
				} 
 				  else if(task=='export') {
					var form = document.adminForm;
					if (document.adminForm.boxchecked.value==0)  {
					alert("<?php echo JText::_('PLZ_CHOSE_RESULT'); ?>");
					return false;
					}
				}
 
 				Joomla.submitform(task, document.getElementById('adminForm'));

}

</script>

<div class="manage_quizzes"> 

<div id="toolbar" class="btn-toolbar">

<div id="toolbar-delete" class="btn-wrapper">

	<button class="btn btn-small" onclick="if (document.adminForm.boxchecked.value==0){alert('Please first make a selection from the list');}else{if (confirm('Do you want to delete the record(s)')){Joomla.submitbutton('remove');}}">

	<span class="icon-delete"></span> <?php echo JText::_('DELETE');?></button>

</div>

<div id="toolbar-download" class="btn-wrapper">

	<button class="btn btn-small" onclick="Joomla.submitbutton('export')">

	<span class="icon-download"></span> <?php echo JText::_('CSVEXPORT');?></button>

</div>
</div>

<form action="<?php echo JRoute::_('index.php?option=com_vquiz&view=quizresult');?>" method="post" name="adminForm" id="adminForm">
<div class="filter">

		<div class="filter_left">

			<div class="search_buttons">

            <div class="btn-wrapper input-append">

			<input placeholder="Search" type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />

			<button class="btn" onclick="this.form.submit();"><i class="icon-search"></i></button>

			<button class="btn" onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'RESET' ); ?></button>

            </div></div>

			<!--document.getElementById('filter_user').value='';this.form.submit();--> 

	</div>

    

    <div class="filter_right">

			<input placeholder="Start Date" type="text" name="startdatesearch" id="startdatesearch" value="<?php echo $this->lists['startdatesearch'];?>"  class="text_area" />

			<script type="text/javascript">

            $(function(){

            $('*[name=startdatesearch]').datepicker({

			 dateFormat: "yy-mm-dd"

			});

            });

            </script>


			<input placeholder="End Date" type="text" name="enddatesearch" id="enddatesearch" value="<?php echo $this->lists['enddatesearch'];?>" class="text_area" />

			<script type="text/javascript">

            $(function(){

            $('*[name=enddatesearch]').datepicker({

			 dateFormat: "yy-mm-dd"

			});

            });

            </script>

    <button class="btn" onclick="this.form.submit();"><?php echo JText::_( 'GO' ); ?></button>

	<button class="btn" onclick="document.getElementById('startdatesearch').value='';document.getElementById('enddatesearch').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
    </div>
    </div>



<div id="editcell">

	<table class="adminlist table table-striped table-hover">

	<thead>

		<tr>

        	<th width="5">

                 <?php echo JText::_( 'Num' ); ?>

			</th>



			<th width="20">

	<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />

			</th>			

			<th  >

				 

                 <?php echo JHTML::_('grid.sort', 'QUIZ', 'i.quiztitle', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

                

			</th>

		    	<th>

                <?php echo JHTML::_('grid.sort', 'User', 'i.userid', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

				<?php /*?><?php echo JText::_( 'User' ); ?><?php */?>

			</th>

             <th>

           <?php /*?>   <?php echo JHTML::_('grid.sort', 'STARTTIME', 'i.startdatetime', @$this->lists['order_Dir'], @$this->lists['order'] ); ?><?php */?>
				<?php echo JHTML::_('grid.sort', 'PLay Date', 'i.start_datetime', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

			</th>

              <th>

              <?php echo JHTML::_('grid.sort', 'ENDTIME', 'i.enddatetime', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

			<?php /*?>	<?php echo JText::_( 'ENDTIME' ); ?><?php */?>

			</th>

             <th> 

                 <?php echo JHTML::_('grid.sort', 'SCORE', 'i.score', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

			</th>

             <th>

               <?php echo JHTML::_('grid.sort', 'STATUS', 'i.score', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

				<?php /*?><?php echo JText::_( 'STATUS' ); ?><?php */?>

			</th>

             <th>

             <?php echo JHTML::_('grid.sort', 'Flag', 'i.flag', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

				<?php /*?><?php echo JText::_( 'Flag' ); ?><?php */?>

			</th>

 

            

            <th width="5">

				 <?php echo JHTML::_('grid.sort', 'ID', 'i.id', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

			</th>

		</tr>

	</thead>

	<?php

	

	$k = 0;

	for ($i=0, $n=count( $this->items ); $i < $n; $i++)	{

		$row = &$this->items[$i];

		$maxscore=$row->maxscore;

		$score=$row->score;

		$passed_score=$row->passed_score;

		//$persentagescore=$score/$maxscore*100;
		
		if($maxscore>0)
		$persentagescore=$score/$maxscore*100;
		else
		$persentagescore=$score;

		$checked 	= JHTML::_('grid.id',   $i, $row->id );
		

$link 		= JRoute::_( 'index.php?option=com_vquiz&view=quizresult&task=edit&cid[]='. $row->id );

		?>

		<tr class="<?php echo "row$k"; ?>">

 		<td><?php echo $this->pagination->getRowOffset($i); ?></td>

			<td>

				<?php echo $checked; ?>

			</td>

			<td>

		    <a href="<?php echo $link; ?>"> <?php echo $row->quiztitle;  ?></a>

			</td> 

			<td align="center">

				<?php 					

                if($row->userid==0)

                echo 'Guest';

                else

                echo $row->username; 

                ?>  

            </td>

            <?php /*?><td align="center"><?php echo $row->startdatetime;  ?></td><?php */?>
             <td align="center"><?php echo $row->start_datetime;  ?></td>

            <td align="center"><?php echo $row->enddatetime;  ?></td>

            <td align="center"><?php echo $row->score;  ?></td>

            <td align="center">

            <?php if($persentagescore>=$passed_score)

            echo '<span style="color:green">Passed</span>';	

            else

            echo '<span style="color:red">Failed</span>'; ?>

            </td>

            <td align="center"><?php echo $row->flag;  ?></td>



             <td>

				<?php echo $row->id; ?>

			</td>

		</tr>

		<?php

		$k = 1 - $k;

	}

	?>



	</table>

<div class="pagination pagination-toolbar">
 <?php echo $this->pagination->getListFooter(); ?>
 </div>





</div>

<input type="hidden" name="option" value="com_vquiz" />

<input type="hidden" name="task" value="" />

<input type="hidden" name="boxchecked" value="0" />

<input type="hidden" name="view" value="quizresult" />



<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />

<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />



</form>

</div>



