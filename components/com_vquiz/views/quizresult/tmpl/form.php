<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/ 
defined( '_JEXEC' ) or die( 'Restricted access' );

JHTML::_('behavior.tooltip');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_vquiz/assets/css/style.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/adminpanel.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/responsive_layout.css');
$document->addScript('components/com_vquiz/assets/js/script.js');
?>
<div class="manage_quizzes">
<div id="toolbar" class="btn-toolbar">
<div id="toolbar-cancel" class="btn-wrapper">
	<button class="btn btn-small" onclick="Joomla.submitbutton('cancel')">
	<span class="icon-cancel"></span> <?php echo JText::_('CANCEL');?></button>
</div>
</div>
<form action="<?php echo JRoute::_('index.php?option=com_vquiz');?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">

        <div class="col101">

                <fieldset class="adminform">

                    <legend><?php echo JText::_( 'DETAILS' ); ?></legend>

                            <div align="center">

                            </div>

                            <table width="100%" class="result_table">

                            <tbody>

                            <tr><th class="sectiontableheader" style="text-align: center;" colspan="2"><?php echo JText::_( 'RESULT' ); ?></th></tr>

                            <tr>

                            <td><strong><?php echo JText::_( 'QUIZ_NAME' ); ?></strong></td>

                            <td><?php  echo $this->item->quiztitle;?></td>

                            </tr>

                            <tr>

                             <tr>

                            <td><strong><?php echo JText::_( 'USER_NAME' ); ?></strong></td>

                            <td>	

							<?php 					

                            if($this->item->userid==0)

                            echo 'Guest';

                            else

                            echo $this->item->username; 

                            ?> 

               				 </td>

                            </tr>

                            <tr>

                            <td><strong><?php echo JText::_( 'RESULT' ); ?></strong></td>

                            <td><?php echo $this->item->score ?> / <?php echo $this->item->maxscore ?></td>

                            </tr>

                            <tr>

                            <td><strong><?php echo JText::_( 'PERCENTAGE' ); ?></strong></td>

                            <td>
							<?php
							 $maxscores=$this->item->maxscore>0?$this->item->maxscore:1;
							 echo ($this->item->score/$maxscores*100);?> %
                            </td>

                            </tr>

                            <tr>

                            <td><strong><?php echo JText::_( 'STATUS' ); ?></strong></td>

                            <td>           

							<?php 
							$maxscores=$this->item->maxscore>0?$this->item->maxscore:1;
							if(($this->item->score/$maxscores*100)>=$this->item->passed_score)

                            echo '<span style="color:green">Passed</span>';	

                            else

                            echo '<span style="color:red">Failed</span>'; ?>

          					  </td>

                            </tr>

                            <tr>

                            <td><strong><?php echo JText::_( 'STARTTIME' );?></strong></td>

                            <td><?php echo $this->item->startdatetime; ?></td>

                            </tr>

                            <tr>

                            <td><strong><?php echo JText::_( 'ENDTIME' );?></strong></td>

                            <td><?php echo $this->item->enddatetime; ?></td>

                            </tr>

                            <tr>

                            <td><strong><?php echo JText::_( 'SPENT_TIME' );?></strong></td>

                            <td><?php echo $this->item->quiz_spentdtime;?> <?php echo JText::_( 'SECONDS' );?></td>

                            </tr>

                            <tr>

                            <td><strong><?php echo JText::_( 'PASSED_PERCENTAGE' );?></strong></td>

                            <td><?php echo $this->item->passed_score;?>  %</td>

                            </tr>
                            <tr>

                            <td><strong><?php echo JText::_( 'FLAG' );?></strong></td>

                            <td><?php echo $this->item->flag;?></td>

                            </tr>

                            </tbody>

                            </table>

                </fieldset>
                
                
<div class="all all_quizzes">
					<?php for( $i=0;$i<count($this->showresult->question_array);$i++){
                    $given_answer=explode(',',$this->showresult->givenanswer[$i]);
                    ?>
    <div class="quiz_all">
    
            <div class="qu_list">
                <h4><?php echo $this->showresult->question[$i]->qtitle;?> </h4>
            </div>
            
            <div class="all-quiz-table">
            <table border="0" width="100%" cellpadding="5" cellspacing="0">
                <tr>
                <th><?php echo JText::_('COUNT');?></th><th><?php echo JText::_('OPTIONS');?></th>
                
                <th>
				<?php
                 if($this->showresult->optiontypescore==1)
					 echo JText::_('YOUR_ANSWER');
				 elseif($this->showresult->optiontypescore==2){
					 echo JText::_("(Point)");
				 	 echo JText::_('YOUR_ANSWER');
				 }

					for($k=0;$k<count($given_answer);$k++){
						if ($given_answer[$k]==0)
						echo '<label class="skip_quest">('.JText::_('SKIP_QUESTION').')</label>';
					}
				 ?>
                
                </th>
                </tr>
                
                
                <?php for( $j=0;$j<count($this->showresult->options[$i]);$j++){?>
                  
                    <tr>
                        <td><p><?php echo $j+1;?></p></td>
                        
                        <td><p><?php echo $this->showresult->options[$i][$j]->qoption;?></p></td>
                        
                        <td>
                        <p style="text-align:center;">
                         
                        
                        <?php 
						
						if($this->showresult->optiontypescore==1)
						{
 							if($this->showresult->options[$i][$j]->correct_ans==1){
							echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/Ok-icon.png" />';
							}
							for($k=0;$k<count($given_answer);$k++){
							if ($given_answer[$k]==$this->showresult->options[$i][$j]->id and ($this->showresult->options[$i][$j]->correct_ans!=1) )
							echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/delete-icon.png" />';
							}
				 
						}
						
						elseif($this->showresult->optiontypescore==2)
						{
							for($k=0;$k<count($given_answer);$k++){
							echo $this->showresult->options[$i][$j]->options_score;
							if($this->showresult->options[$i][$j]->id==$given_answer[$k])
							echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/Ok-icon.png" />';
							}
						}
                        ?>
                        
                        </p>
                        </td>
                        
                    </tr> 
                <?php }?>
            </table>    
            </div>
            </div>    
            <?php }?>
    </div>


</div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="view" value="quizresult" />

</form>
</div>



