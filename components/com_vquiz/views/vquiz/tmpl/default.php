<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author Team WDMtech
# copyright Copyright (C) 2015 www.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech.com
# Technical Support: Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

JHTML::_('behavior.modal');
$document = JFactory::getDocument();
$app=JFactory::getApplication();
$document->setTitle('Quiz Categories'.' - '.$app->getCfg( 'sitename' ) );
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/style.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/responsive_layout.css');
$document->addStyleSheet(JURI::root().'components/com_vquiz/assets/css/popupcss.css');
$document->addScript(JURI::root().'components/com_vquiz/assets/js/library.js');
$user=JFactory::getUser();
JHtml::_('behavior.framework');
JHtml::_('behavior.tooltip');
 ?>
<script type="text/javascript">
var vg=jQuery.noConflict();
		vg(document).ready(function() {
 			vg(".count_header").mouseenter(function () {
			//vg(".count_header").on('mouseenter mouseleave', function() {
				//vg('.subcategory', this).slideToggle('slow'); 
				var parentid=vg(this).attr("parent-id");	
				var id=vg(this).attr("item-id");
				var checkajex=vg(this).attr("check-ajex");
				if(checkajex!=0){	
					child_category(parentid,id);
				}
			});

		function child_category(parentid,id) 
			{
 
					jQuery.ajax(
					{
					url: "index.php",
					type: "POST",
					dataType:"json",
					data: {'option':'com_vquiz', 'view':'vquiz', 'task':'child_category','parentid':parentid,'itemid':id,'tmpl':'component',"<?php echo JSession::getFormToken(); ?>":1},

						 beforeSend: function()	{
							vg('.small_loading'+id+'').css('display','inline-block');
						  },
						  complete: function()	{
							vg('.small_loading'+id+'').hide();
						  },
						  
					success: function(data)	
					{	 
					

							var html='<option value=""><?php echo JText::_('SUB_CATEGORY');?></option>';
								
 							for(var i=0;i<data.length;i++){
								
 						 	 html +='<option value="'+data[i].id+'">';
							 if(data[i].level>0){
								for(var j=0;j<data[i].level-1;j++)
								 html +='-';
  								 html +=data[i].quiztitle;
							 }
							 html +='</option>';
							}
							
							if(data=='')
							{
								vg('.sub_category_div'+id+'').html('<?php echo JText::_('No subcategory');?>');
							}else{
							vg('.parent_lsit'+id+'').find('select').html(html);
							vg('.parent_lsit'+id+'').find('select').css('display','inline-block');	
							vg('.count_header').find('.count_sub_category'+id+'').html(data.length);	
  							}
							vg('.sub_category_div'+id+'').find('.count_header').attr('check-ajex',0);	
 
					}
 				

				});

			}
});
</script>
 
 

 <form action="<?php echo JRoute::_('index.php?option=com_vquiz&view=vquiz')?>" method="post" name="adminForm" id="adminForm">
<div class="adminform">

	<div class="cpanel-left">
                <div class="board"> 
                <h1 class="play_quiz"><?php echo JText::_('PLAY_QUIZ'); ?></h1> 

				 <div class="cat_text"><?php echo JText::_('CHOOSE_QUIZ_CATEGORY'); ?></div>	  
				<?php
				$k = 0;
				for ($i=0, $n=count( $this->quizcategory ); $i < $n; $i++)	{
				$row = &$this->quizcategory[$i];
				$link 		= JRoute::_( 'index.php?option=com_vquiz&view=quizmanager&layout=quizzes&id='. $row->id );
				?>
				<div class="<?php echo "row$k"; ?> vcat">
                <a class="cat_img" href="<?php echo $link;?>">               
				<?php 
                if(!empty($row->photopath)){ 
                echo '<img src="'.JURI::root().'/media/com_vquiz/vquiz/images/photoupload/thumbs/thumb_'.$row->photopath. '" alt="'.$row->quiztitle.'" title="'.$row->quiztitle.'" width="'.$this->configuration->categorythumbnailwidth.'" height="'.$this->configuration->categorythumbnailheight.'" / >'; 
                }else { echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/no_image.png" alt="Image Not available" border="1" width="'.$this->configuration->categorythumbnailwidth.'" height="'.$this->configuration->categorythumbnailheight.'" />';} 
                
                ?>
				</a>   
                <a class="title" href="<?php echo $link; ?>"><?php echo $row->quiztitle; ?></a>
                
                <div class="sub_category_list">
             
                    <div class="small_loading<?php echo $row->id;?>" style="display:none;">
                    <?php echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/loading.gif"/>' ?>
                    </div>
                    
                    <div class="sub_category_div<?php echo $row->id;?>">
                    
                           <div class="count_header" parent-id="<?php echo $row->parent_id;?>" item-id="<?php echo $row->id;?>" check-ajex="1">
                                <span class="count_sub_category<?php echo $row->id;?>"></span>
                                <span><?php echo JText::_('subcategory');?></span>
                            </div>
                    
                             <div class="parent_lsit<?php echo $row->id;?>">
                                <select class="subcategory" name="subcategory" onchange="Joomla.submitbutton('subcategory')">
                                </select>
                            </div>
                           
                     </div>
 
                </div>
 
                    
                    
                
				</div
				><?php
				$k = 1 - $k;
				}
				?>           
 
			    </div>

	</div>
    </div>
    <tfoot>
    <tr>
    <td colspan="0"><?php echo $this->pagination->getListFooter(); ?></td>
    </tr>
    </tfoot>
<div class="clr"></div> 
<input type="hidden" name="option" value="com_vquiz" /> 
<input type="hidden" name="view" value="vquiz" />
<input type="hidden" name="task" value=""/>  
</form>
 