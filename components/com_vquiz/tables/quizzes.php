<?php 
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/ 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
class VquizTableQuizzes extends JTable
{
     
	var $id = null;
	var $quizzes_title = null;
	var $quiz_categoryid = null;
	var $description = null;
	var $published = null;
	function __construct(& $db) {
		$db = JFactory::getDBO();
		parent::__construct('#__vquiz_quizzes', 'id', $db);

	}

 

}