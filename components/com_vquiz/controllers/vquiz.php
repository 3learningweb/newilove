<?php
/*------------------------------------------------------------------------
# com_vquiz - vquiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2014 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class VquizControllerVquiz extends VquizController
{
		function __construct()
		{
		parent::__construct();
		$this->registerTask( 'add'  , 	'edit' );
		}

			function edit()
			{
			JRequest::setVar( 'view', 'vquiz' );
			JRequest::setVar('hidemainmenu', 1);
			parent::display();
	
			}
				
			function subcategory()
			{
			   $subcategoryid = JRequest::getInt('subcategory','post');
			   if($subcategoryid){
					$link= JRoute::_( 'index.php?option=com_vquiz&view=quizmanager&layout=quizzes&id='. $subcategoryid);
					$mainframe = JFactory::getApplication();
					$mainframe->redirect($link);
					return ;
			   }
			   else
					$msg = JText::_('NO_SUBCATEGORY');
					$link= JRoute::_( 'index.php?option=com_vquiz&view=vquiz');
					$this->setRedirect($link, $msg );
			
			}
			
			
			function child_category()
			{ 
				JRequest::checkToken() or jexit( '{"result":"error", "error":"'.JText::_('INVALID_TOKEN').'"}' );
				$model = $this->getModel('vquiz');
 				$obj = $model->child_category();		
				jexit(json_encode($obj));
			} 
			
			
 
		} 
  