<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech.com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
defined( '_JEXEC' ) or die( 'Restricted access' );

 

class VquizControllerQuizzes extends VquizController

{



	function __construct()

	{

		parent::__construct();

		

		$this->registerTask( 'add'  , 	'edit' );

		$this->registerTask( 'unpublish',	'publish' );

		$this->registerTask( 'orderup', 		'reorder' );

		$this->registerTask( 'orderdown', 		'reorder' );

		$this->registerTask( 'unfeatured','featured' );	

	

	}

	

	function edit()

	{

		JRequest::setVar( 'view', 'quizzes' );

		JRequest::setVar( 'layout', 'form'  );

		JRequest::setVar('hidemainmenu', 1);

		

		parent::display();

	}

	

	

 	function publish()

	{

		$model = $this->getModel('quizzes');

		$msg = $model->publish();

			



		$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=quizzes'), $msg );

	}



		function featured()

	{

 

		

		$model = $this->getModel('quizzes');

		$msg = $model->featured();

		

		$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=quizzes'), $msg );

		

	}

			

	function reorder()

	{

	

		$model = $this->getModel('quizzes');

		$msg = $model->reorder();



		$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=quizzes'), $msg );

	

	}

	

	function drawChart()

	{  

		JRequest::checkToken() or jexit( '{"result":"error", "error":"'.JText::_('INVALID_TOKEN').'"}' );

		$model = $this->getModel('quizzes');

		$obj = $model->drawChart();	

		jexit(json_encode($obj));

	} 

	

 

	

 

	function saveOrder()

	{

	

		$model = $this->getModel('quizzes');

		$msg = $model->saveOrder();



	$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=quizzes'), $msg );

	

	}

	 

	function save()

	{

		  $model = $this->getModel('quizzes');



		if($model->store()) {

			$msg = JText::_( 'Quizzes Save Success' );

			$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=quizzes'), $msg );

		} else {

			jerror::raiseWarning('', $model->getError());

			$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=quizzes'));

		}

	

	}

	

	

	

	function apply()

	{

		$model = $this->getModel('quizzes');

		

 

		if($model->store()) {

			$msg = JText::_( 'Quizzes Save Success' );

			$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=quizzes&task=edit&cid[]='.JRequest::getInt('id', 0)), $msg );

		} else {

			jerror::raiseWarning('', $model->getError());

			$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=quizzes&task=edit&cid[]='.JRequest::getInt('id', 0)) );

		}



	}



 

	

	function remove()

	{

		$model = $this->getModel('quizzes');

		

		if(!$model->delete()) 

		{

			$msg = JText::_( 'Error: One or More Greetings Could not be Deleted' );

		} 

		else 

		{

			$msg = JText::_( 'Quizzes(s) Deleted' );

		}

		

		$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=quizzes'), $msg );

	}

	

	

					function cancel()

					{

							$msg = JText::_( 'Operation Cancelled' );

							$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=quizzes'), $msg );

					}

					

	

 		 					function copyquestion()

							{

								$model = $this->getModel('quizzes');

									if(!$model->copyquestion()) 

									$msg = JText::_( 'Error: One or More Greetings Could not be Copy' );

									else 	 

									$msg = JText::_( 'Copy Succesfully' );

									$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=quizzes'), $msg );

							}

							

						  function movequestion()

							{

								$model = $this->getModel('quizzes');

									if(!$model->movequestion()) 

									$msg = JText::_( 'Error: One or More Greetings Could not be Copy' );

									else 	 

									$msg = JText::_( 'Move Succesfully' );

									$this->setRedirect(JRoute::_('index.php?option=com_vquiz&view=quizzes'), $msg );

							}

 

 

 

 

 

}