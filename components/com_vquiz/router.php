<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/ 
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * @param	array	A named array
 * @return	array
 */
function VquizBuildRoute( &$query )
{
	$db = JFactory::getDBO();
	
	$segments = array();
	
	$app = JFactory::getApplication();
	
	$menu = $app->getMenu();
	if (empty($query['Itemid'])) {
		$menuItem = $menu->getActive();
	} else {
		$menuItem = $menu->getItem($query['Itemid']);
	}
	
 
	
	if(isset($query['view']))	{
		
		switch($query['view'])	{
		   
		   case 'vquiz':
			   $segments[] ='quiz-categories';
			   unset($query['view']);
		   break;
		   
			case 'quizmanager' :
			
			$segments[] ='quizzes';
			
 		if(isset($query['layout'])){
  										
				switch($query['layout'])	{ 
				
				case 'quizzes':
				
					if(isset($query['id']))	{ 
						if($query['id']==0)
							$segments[] = 'browse-all';
						else	{
							$q = 'select alias from #__vquiz_category where id = '.(int)$query['id'];
							$db->setQuery( $q );
							$alias = $db->loadResult();
							
							$segments[] = empty($alias)?$query['id']:$alias;
						}
						
						unset($query['id']);
					}
					else
					{	
						$segments[] = 'browse-all';
						
					}
										
					
					if(isset($query['modulorder']))	{ //jexit('exit');
					
						switch($query['modulorder'])	{
						
							case '1':
							$segments[] ='latest';//$query['latest'];
							break;
							case '2':
							$segments[] ='most-popular';//$query['most'];
							break;
							case '3':
							$segments[] ='recently-played';//$query['recent'];
							break;
							case '4':
							$segments[] ='random';//$query['recent'];
							break;
										
						}						
						
					}
					
					if(isset($query['featured']))	{ 
						if($query['featured']==1)
							$segments[] = 'featured';
						
						unset($query['featured']);
					}
				
				break;
				
				case 'description':
									
					if(isset($query['id']))	{ 
					
						$q = 'select i.alias, i.quiz_categoryid, c.alias as catalias from #__vquiz_quizzes as i join #__vquiz_category as c on i.quiz_categoryid=c.id where i.id = '.(int)$query['id'];
						$db->setQuery( $q );
						$item = $db->loadObject();
						
						if(!empty($item)){
							
							$segments[] = empty($item->catalias)?$item->quiz_categoryid:$item->catalias;
														
							$segments[] = empty($item->alias)?$query['id']:$item->alias;
														
						}
						
						unset($query['id']);
					}					
				
  				break;
		 
				}
		    }
 				// default: 
				
					if(isset($query['id']))	{ 
					
						$q = 'select i.alias, i.quiz_categoryid, c.alias as catalias from #__vquiz_quizzes as i join #__vquiz_category as c on i.quiz_categoryid=c.id where i.id = '.(int)$query['id'];
						$db->setQuery( $q );
						$item = $db->loadObject();
						
						if(!empty($item)){
							
							$segments[] = empty($item->catalias)?$item->quiz_categoryid:$item->catalias;
														
							$segments[] = empty($item->alias)?$query['id']:$item->alias;
							
							$segments[] = 'play';
														
						}
						
						unset($query['id']);
						
					}
			   
				
				//break;
				// }
				
			
			
				if(isset($query['layout']))
					unset($query['layout']);
				if(isset($query['modulorder']))
					unset($query['modulorder']);
				if(isset($query['view']))
					unset($query['view']);
				
			break;
			
			case 'quizresult' :
				$segments[] = $query['view'];
				unset($query['view']);
			
			break;
			case 'usersquizzes' :
				$segments[] = $query['view'];
				unset($query['view']);
			
			break;
			case 'quizquestion' :
				$segments[] = $query['view'];
				unset($query['view']);
			
			break;
			case 'users' :
				$segments[] = $query['view'];
				unset($query['view']);
			
			break;
			
			default:
				$segments[] = $query['view'];
				unset($query['view']);
           	break;
			
			 
		}
		
		unset($query['view']);
	}
	
	return $segments;
}



function VquizParseRoute( $segments )
{	
	$db = JFactory::getDBO();
	$vars = array();
	// view is always the first element of the array
	$count = count($segments); 
	
	//print_r($segments);
		
	if ($count)
	{
		
		$segments[0] = str_replace(':', '-', $segments[0]);
		
		switch($segments[0])	{			
 			 
			case 'quizzes':

				$vars['view'] = 'quizmanager';
				$count--;
				array_shift( $segments );
				 
				if($count)	{
				 
					$segments[0] = str_replace(':', '-', $segments[0]);
					
					  if($segments[0]<>'browse-all')	{
						$q = 'select id from #__vquiz_category where alias = '.$db->Quote($segments[0]);			
						$db->setQuery( $q );
						$id = $db->loadResult();
						$vars['id'] = empty($id)?$segments[0]:$id;
					
					}
									
					$count--;

					array_shift( $segments );
					
				}
				
				if($count)	{
					$segments[0] = str_replace(':', '-', $segments[0]);
		
					switch($segments[0])	{
				     
						case 'latest':
						$vars['modulorder']=1;
						$vars['layout']='quizzes';
						break;
						case 'most-popular':
						$vars['modulorder']=2;
						$vars['layout']='quizzes';
						break;
						case 'recently-played':
						$vars['modulorder']=3;
						$vars['layout']='quizzes';
						break;
						case 'random':
						$vars['modulorder']=4;
						$vars['layout']='quizzes';
						break;
						
						default:
							
							$q = 'select id from #__vquiz_quizzes where alias = '.$db->Quote(str_replace(':', '-', $segments[0]));			
							$db->setQuery( $q );
							$id = $db->loadResult();
							
							$vars['id'] = empty($id)?$segments[0]:$id;
							
							$count--;
							array_shift( $segments );
							
							if($count<1)
								$vars['layout']='description';
															
						break;
						
					
					}
					if($count){
						if($segments[0]=='featured')	{
							$vars['featured'] =1;
						}
					}
					
				}
				else
					$vars['layout']='quizzes';					
	
				break;
				
				case 'quizresult' :
				$vars['view'] = 'quizresult';
				$count--;
				array_shift( $segments );
				
				break;
				
				case 'usersquizzes' :
				$vars['view'] = 'usersquizzes';
				$count--;
				array_shift( $segments );
				
				break;
				
				case 'quizquestion' :
				$vars['view'] = 'quizquestion';
				$count--;
				array_shift( $segments );
				
				break;
				case 'users' :
				$vars['view'] = 'users';
				$count--;
				array_shift( $segments );
				
				break;
				
			 default:
				 
				$vars['view'] = 'vquiz';
				
				$count--;
				array_shift( $segments );
				
			break;
			  
		}
		
	}
	//print_r($vars);
	//jexit('exit');
	return $vars;
 
}

function get_text($text)
{
	
	return str_replace(' ', '-', (strtolower(JText::_($text))));
	
}