<?php
/**
 * FW Gallery 3.3.0
 * @copyright (C) 2015 Fastw3b
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.fastw3b.net/ Official website
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );

class fwGalleryViewImage extends JViewLegacy {
    function display($tmpl=null) {
        $model = $this->getModel();
        $row = $model->getObj();
		$buff = new stdclass;

		switch ($this->getLayout()) {
			case 'check_fid' :
				$buff = $model->checkFid();
				break;
			case 'publish' :
				$buff = (object)array(
					'result' => $model->publish(),
					'msg' => $model->getError()
				);
				break;
			case 'unpublish' :
				$buff = (object)array(
					'result' => $model->unpublish(),
					'msg' => $model->getError()
				);
				break;
			case 'contact' :
				$buff = (object)array(
					'result' => $model->contact(),
					'msg' => $model->getError()
				);
				break;
			case 'update' :
				$buff = (object)array(
					'result' => $model->update(),
					'msg' => $model->getError()
				);
				break;
			case 'test' :
				$buff = $model->testGallery();
			break;
			case 'testUser' :
				$buff = $model->testUser();
			break;
			case 'getPlugins' :
				$buff->plugins = $model->getPlugins();
				$buff->msg = $model->getError();
			break;
			case 'save' :
				$buff->id = $model->save();
				$buff->msg = $model->getError();
			break;
			case 'delete' :
				$buff->result = $model->delete();
				$buff->msg = $model->getError();
			break;
			case 'buy' :
				$buff->link = $model->buy();
				$buff->msg = $model->getError();
			break;
			default:
				$buff->id = $row->id;
				$buff->height = $row->height;
				$buff->width = $row->width;
				$buff->size = $row->size;
				$buff->project_id = $row->project_id;
				$buff->name = $row->name;
				$buff->created = $row->created;
				$buff->com_link = JRoute :: _('index.php?option=com_fwgallery&view=image&id='.$row->id.':'.JFilterOutput :: stringURLSafe($row->name));
				$buff->link = JURI :: root(false).JFHelper::getFileFilename($row);
				$buff->th_link = JURI :: root(false).JFHelper::getFileFilename($row, 'th');
				$buff->color = JFHelper :: getGalleryColor($row->project_id);
				$buff->_user_name = $row->_user_name;
				$buff->_tags = $row->_tags;
		}
		die(json_encode($buff));
    }
}
