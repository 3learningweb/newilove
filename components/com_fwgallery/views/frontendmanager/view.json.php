<?php
/**
 * FW Gallery 3.3.0
 * @copyright (C) 2015 Fastw3b
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.fastw3b.net/ Official website
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );

class fwGalleryViewFrontendManager extends JViewLegacy {
    function display($tmpl=null) {
        $model = $this->getModel();
		
		$data = new stdclass;
		switch ($this->getLayout()) {
			case 'batchupload' :
				$data->inserted = $model->install();
				$data->msg = $model->getError();
			break;
		}
        die(json_encode($data));
    }
}
