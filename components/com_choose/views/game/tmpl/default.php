<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		choose
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;
?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
<!--script type="text/javascript" src="https://www.pureexample.com/js/lib/jquery.ui.touch-punch.min.js"></script-->
<script src="components/com_jce/editor/libraries/jquery/js/jquery.ui.touch-punch.min.js"></script>
<style>
	.pretext {
		margin-bottom: 20px;
	}
	.noteTxt {
		color: #6A0000;
		font-size: medium;
		line-height: 25px;
		padding-right: 50px;
		padding-left: 50px;
		background-image: url('components/com_choose/assets/images/note_05.png');
		background-repeat: repeat-y;
	}<!-- .arrow_box {
		position: relative;
		background: #f7ffb3;
		border: 8px solid #f5c36c;
	}
	.arrow_box:after, .arrow_box:before {
		right: 100%;
		top: 50%;
		border: solid transparent;
		content:" ";
		height: 0;
		width: 0;
		position: absolute;
		pointer-events: none;
	}
	.arrow_box:after {
		border-color: rgba(247, 255, 179, 0);
		border-right-color: #f7ffb3;
		border-width: 12px;
		margin-top: -12px;
	}
	.arrow_box:before {
		border-color: rgba(245, 195, 108, 0);
		border-right-color: #f5c36c;
		border-width: 23px;
		margin-top: -23px;
	}
	-->
</style>
<div class="com_choose">
	<div class="game_page-header">
		<div class="title">
			<?php echo $this->escape($menu_title); ?>
		</div>
	</div>
	
	<div class="hint_msg" style="display: none;">此頁面請切換為電腦版，即可正常使用功能</div>

	<div class="pretext">
		<div class="dialog_box">&nbsp;</div>
			<div class="arrow_box">
				<p style="font-size: 17px; line-height: 35px; margin: 10px;">假設您要選擇「結婚」對象，請問對方具備的各項特質， 對您而言其重要性為何？</p>
			</div>
	</div>
	
	<div class="component_intro">
		<div class="intro_text">
			<?php echo JText::_("COM_CHOOSE_INTRO_TEXT"); ?>
		</div>
		
		
		<div class="intro_image">
			<img id="choose_intro_icon" src="filesys/images/com_choose/intro_icon.png" alt="<?php echo $menu_title; ?>" />
			<img id="choose_intro_image" src="filesys/images/com_choose/intro_image.png" alt="<?php echo $menu_title; ?>" />
		</div>
	</div>
	
	<form id="choose_form" name="choose_form" method="post" action="<?php echo JRoute::_("index.php?option=com_choose&view=result&Itemid={$itemid}", false); ?>">
		<!-- 基本資料 -->
		<table class="profile_table">
			<tbody>
			<!-- 性別 -->
			<tr>
				<th width="15%">性別：</th>
				<td>
					<input type="radio" id="gender0" name="gender" />男 &nbsp;
					<input type="radio" id="gender1" name="gender" />女
				</td>
			</tr>
			
			<!-- 年齡 -->
			<tr>
				<th width="10%">年齡：</th>
				<td>
					<input type="radio" id="age0" name="age" />24歲以下 &nbsp;
					<input type="radio" id="age1" name="age" />25-30歲 &nbsp;
					<input type="radio" id="age2" name="age" />31-35歲 &nbsp;
					<input type="radio" id="age3" name="age" />36-40歲 &nbsp;<br/>
					<input type="radio" id="age4" name="age" />41-45歲 &nbsp;&nbsp;
					<input type="radio" id="age5" name="age" />46-50歲 &nbsp;&nbsp;
					<input type="radio" id="age6" name="age" />51-55歲 &nbsp;&nbsp;
					<input type="radio" id="age7" name="age" />56歲以上
				</td>
			</tr>
			</tbody>
		</table>
		<br/><br/>

		<!-- 特質一覽表 -->
		<div class="title_icon">
			<img src="filesys/images/com_choose/title_icon.png" alt="" />
			<span class="title_text">特質一覽表</span>
		</div>
		<div class="title_image">
			<img src="filesys/images/com_choose/person1.png" alt="" />
		</div>
		<table class="char_tbl">
			<tbody>
			<!-- 1~4 -->
			<tr>
				<td>
					<div class="choose_block" id="block_1">
						<div class="item_text">1.身體健康</div>
						<div class="choose_item" id="choose_item1">
							1 健康
							<input type="hidden" class="item_code" value="1">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_2">
						<div class="item_text">2.身材好</div>
						<div class="choose_item" id="choose_item2">
							2 身材
							<input type="hidden" class="item_code" value="2">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_3">
						<div class="item_text">3.沒有家族疾病史</div>
						<div class="choose_item" id="choose_item3">
							3 疾病
							<input type="hidden" class="item_code" value="3">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_4">
						<div class="item_text">4.外貌姣好</div>
						<div class="choose_item" id="choose_item4">
							4 外貌
							<input type="hidden" class="item_code" value="4">
						</div>
					</div>
				</td>
			</tr>
			
			<!-- 5~8 -->
			<tr>
				<td>
					<div class="choose_block" id="block_5">
						<div class="item_text">5.年齡相配</div>
						<div class="choose_item" id="choose_item5">
							5 年齡
							<input type="hidden" class="item_code" value="5">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_6">
						<div class="item_text">6.經濟條件佳</div>
						<div class="choose_item" id="choose_item6">
							6 經濟
							<input type="hidden" class="item_code" value="6">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_7">
						<div class="item_text">7.家世背景良好</div>
						<div class="choose_item" id="choose_item7">
							7 背景
							<input type="hidden" class="item_code" value="7">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_8">
						<div class="item_text">8.種族背景相同(如：省籍、國別)</div>
						<div class="choose_item" id="choose_item8">
							8 種族
							<input type="hidden" class="item_code" value="8">
						</div>
					</div>
				</td>
			</tr>
			
			<!-- 9~12 -->
			<tr>
				<td>
					<div class="choose_block" id="block_9">
						<div class="item_text">9.有穩定工作</div>
						<div class="choose_item" id="choose_item9">
							9 工作
							<input type="hidden" class="item_code" value="9">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_10">
						<div class="item_text">10.學歷相當</div>
						<div class="choose_item" id="choose_item10">
							10 學歷
							<input type="hidden" class="item_code" value="10">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_11">
						<div class="item_text">11.溫柔體貼</div>
						<div class="choose_item" id="choose_item11">
							11 溫柔
							<input type="hidden" class="item_code" value="11">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_12">
						<div class="item_text">12.幽默風趣</div>
						<div class="choose_item" id="choose_item12">
							12 幽默
							<input type="hidden" class="item_code" value="12">
						</div>
					</div>
				</td>
			</tr>
			
			<!-- 13~16 -->
			<tr>
				<td>
					<div class="choose_block" id="block_13">
						<div class="item_text">13.樂觀開朗</div>
						<div class="choose_item" id="choose_item13">
							13 樂觀
							<input type="hidden" class="item_code" value="13">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_14">
						<div class="item_text">14.認真勤勉</div>
						<div class="choose_item" id="choose_item14">
							14 認真
							<input type="hidden" class="item_code" value="14">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_15">
						<div class="item_text">15.個性獨立自主</div>
						<div class="choose_item" id="choose_item15">
							15 獨立
							<input type="hidden" class="item_code" value="15">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_16">
						<div class="item_text">16.與家人相處和睦</div>
						<div class="choose_item" id="choose_item16">
							16 家人
							<input type="hidden" class="item_code" value="16">
						</div>
					</div>
				</td>
			</tr>
			
			<!-- 19~20 -->
			<tr>
				<td>
					<div class="choose_block" id="block_17">
						<div class="item_text">17.彼此興趣相近、有共同話題</div>
						<div class="choose_item" id="choose_item17">
							17 興趣
							<input type="hidden" class="item_code" value="17">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_18">
						<div class="item_text">18.孝順</div>
						<div class="choose_item" id="choose_item18">
							18 孝順
							<input type="hidden" class="item_code" value="18">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_19">
						<div class="item_text">19.人生觀相近</div>
						<div class="choose_item" id="choose_item19">
							19 人生觀
							<input type="hidden" class="item_code" value="19">
						</div>
					</div>
				</td>
				<td>
					<div class="choose_block" id="block_20">
						<div class="item_text">20.對彼此的情感忠誠</div>
						<div class="choose_item" id="choose_item20">
							20 忠誠
							<input type="hidden" class="item_code" value="20">
						</div>
					</div>
				</td>
			</tr>
			</tbody>
		</table>

		<!-- 特質重視程度分類表 -->
		<div class="title_icon">
			<img src="filesys/images/com_choose/title_icon.png" alt="" />
			<span class="title_text">特質重視程度分類表</span>
			<span>（請拖曳上方特質一覽表中的編號，放置下方各分數區塊內)</span>
		</div>		
		
		<table id="mychoose_block">
			<thead>
				<th>8分(1個)</th>
				<th>7分(2個)</th>
				<th>6分(3個)</th>
				<th>5分(4個)</th>
				<th>4分(4個)</th>
				<th>3分(3個)</th>
				<th>2分(2個)</th>
				<th>1分(1個)</th>
			</thead>
			
			<tbody>
				<tr>
					<td>
						<div class="mychoose" id="choose1">
							<input type="hidden" class="code_list" name="code_list_8" value="">
						</div>
					</td>
					<td>
						<div class="mychoose" id="choose2">
							<input type="hidden" class="code_list" name="code_list_7" value="">
						</div>
					</td>
					<td>
						<div class="mychoose" id="choose3">
							<input type="hidden" class="code_list" name="code_list_6" value="">
						</div>
					</td>
					<td>
						<div class="mychoose" id="choose4">
							<input type="hidden" class="code_list" name="code_list_5" value="">
						</div>
					</td>
					<td>
						<div class="mychoose" id="choose5">
							<input type="hidden" class="code_list" name="code_list_4" value="">
						</div>
					</td>
					<td>
						<div class="mychoose" id="choose6">
							<input type="hidden" class="code_list" name="code_list_3" value="">
						</div>
					</td>
					<td>
						<div class="mychoose" id="choose7">
							<input type="hidden" class="code_list" name="code_list_2" value="">
						</div>
					</td>
					<td>
						<div class="mychoose" id="choose8">
							<input type="hidden" class="code_list" name="code_list_1" value="">
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		
		<div class="choose_footer">
			<img src="filesys/images/com_choose/person2.png" alt="" />
			<div class="submit_block"><input type="button" id="submit_btn" value="確定送出" /></div>
			<div class="reset_block"><input type="button" id="reset_btn" value="清空重填" /></div>
		</div>
	</form>
</div>

<script language="JavaScript">
	(function($) {
		$(document).ready(function() {
			var limit = [];
			limit['choose1'] = 1; limit['choose2'] = 2; limit['choose3'] = 3;	limit['choose4'] = 4;
			limit['choose5'] = 4; limit['choose6'] = 3; limit['choose7'] = 2;	limit['choose8'] = 1;
			
			// drag 			
			$(".choose_item").each(function() {
				$(this).draggable({
					activeClass: 'droppable-active',
					containment: '.com_choose',
					stack: '.choose_item',
					cursor: 'move',
					scroll: true,
					revert: true
				});
			});
			
			// accept
			$(".choose_block").each(function() {  // 起始地
				var block = $(this).prop("id").split("_");
				$(this).droppable( {
			      	accept: '#choose_item'+block[1],
			      	activeClass: 'choose_active',
			      	hoverClass: 'hovered',
			      	drop: handleCardDrop
    			});
			});
			
			$(".mychoose").each(function() {  // 目的地
				$(this).droppable( {
			      	accept: '.choose_item',
			      	hoverClass: 'hovered',
			      	drop: handleCardDrop
    			});
			});

			function handleCardDrop( event, ui ) {
				var choose = $(this);
				if(!checkLimit(choose)){
					return false;
				}
				$(this).append($(ui.draggable).css("margin-bottom", "5px"));
			}
			
			// 限制個數
			function checkLimit(choose) {
				var key = choose.prop("id");
				var count = choose.children("div").length;
				if(limit[key] == count){
					alert("此區塊只能放"+limit[key]+"個");
					return false;
				}
				return true;
			}

			
			// submit
			$(document).on("click", "#submit_btn", function() {
				_count = 0;
				for (var i = 0; i < $(".mychoose").length; i++) {
					_val_str = "";
					$(".mychoose:eq(" + i + ") > .choose_item > .item_code").each(function () {
						_val_str += $(this).val() + ",";
						_count++;
					});
					$(".mychoose:eq(" + i + ") > .code_list").val(_val_str);

				}

				if (_count < 20) {
					alert("請將所有特質拖拉至分類表中。");
					return false;
				}

				$("#choose_form").submit();

			});

			// reset
			$(document).on("click", "#reset_btn", function() {
				location.reload();
			});
		});
	})(jQuery);
</script>