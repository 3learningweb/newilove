<?php
/**
 * @version		: default.php 2015-06-30 21:06:39$
 * @author		EFATEK 
 * @package		choose
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$menu      = $app->getMenu();
$menu_title = $menu->getActive()->title;

// 特質分配
$labels[1] = "身體健康"; $labels[2] = "身材好"; $labels[3] = "沒有家族病史"; $labels[4] = "外貌姣好"; $labels[5] = "年齡相配";
$labels[6] = "經濟條件佳"; $labels[7] = "家世背景良好"; $labels[8] = "種族背景相同"; $labels[9] = "有穩定工作"; $labels[10] = "學歷相當";
$labels[11] = "溫柔體貼"; $labels[12] = "幽默風趣"; $labels[13] = "樂觀開朗"; $labels[14] = "認真勤勉"; $labels[15] = "個性獨立自主";
$labels[16] = "與家人相處和睦"; $labels[17] = "彼此興趣相近、有共同話題"; $labels[18] = "孝順"; $labels[19] = "人生觀相近"; $labels[20] = "對彼此的情感忠誠";

$label_code[1] = 1; $label_code[2] = 1; $label_code[3] = 1; $label_code[4] = 1; $label_code[5] = 1;
$label_code[6] = 2; $label_code[7] = 2; $label_code[8] = 2; $label_code[9] = 2; $label_code[10] = 2;
$label_code[11] = 3; $label_code[12] = 3; $label_code[13] = 3; $label_code[14] = 3; $label_code[15] = 3;
$label_code[16] = 4; $label_code[17] = 4; $label_code[18] = 4; $label_code[19] = 4; $label_code[20] = 4;

$impt_code[1] = 3; $impt_code[2] = 3; $impt_code[3] = 3; $impt_code[4] = 2;
$impt_code[5] = 2;$impt_code[6] = 1; $impt_code[7] = 1; $impt_code[8] = 1;


// 初始化
$cats[1] = 0; $cats[2] = 0; $cats[3] = 0; $cats[4] = 0;
$impts[1] = ""; $impts[2] = ""; $impts[3] = "";

for ($i = 1; $i <= 8; $i++) {
	$code_list = $app->input->getString('code_list_'. $i);
	$codes = explode(",", $code_list);
	foreach ($codes as $code) {
		if ($code == "") {
			continue;
		}
		$cats[$label_code[$code]] += $i;
		$impts[$impt_code[$i]] .= $labels[$code]. "<br>";

	}
}


?>
<script src="components/com_choose/assets/js/Chart.js"></script>
<script>
var Datas = {
    labels: ["生理條件", "外在條件", "內在特質", "價值取向"],
    datasets: [
        {
            label: "特質分析",
            fillColor: "rgba(229,153,153,0.5)",
            strokeColor: "rgba(229,153,153,0.8)",
            highlightFill: "rgba(229,153,153,0.75)",
            highlightStroke: "rgba(229,153,153,1)",
            data: [<?php echo $cats[1]; ?>, <?php echo $cats[2]; ?>, <?php echo $cats[3]; ?>, <?php echo $cats[4]; ?>]
        }
    ]
};

window.onload = function(){
    var ctx = document.getElementById("chart-area").getContext("2d");
    var myDoughnut = new Chart(ctx).Bar(Datas, {
        responsive : true,
        animationEasing: "easeOutQuart"
    });
};

</script>

<style>
    .box {
		width:60%;
		margin: 0 auto;
	}
    .zone {
		width:300px; height:300px;
	}

	.cattb {
		width: 100%;
		margin-top: 20px;
		text-align: center;
		border: 1px solid #f89406;
	}

	.cattb th {
		background-color: #f89406;
		font-weight: bold;
		padding: 5px;
	}

	.cattb td {
		vertical-align: top;
	}

	.readmore {
		margin-top: 20px;
	}
</style>
<div class="com_choose">
	<div class="game_page-header">
		<div class="title">
			<?php echo $this->escape($menu_title); ?>
		</div>
	</div>
	<div style="font-size: large;
    color: #333;
    padding-top: 10px;
    padding-bottom: 10px;
    font-family: "微軟正黑體";
    text-align: left;">
		分數最高為你(妳)的面向類型</div>
	<div class="component_intro">
		<div class="box">
			<canvas id="chart-area" class="zone"></canvas>
		</div>
		<div class="tb">
			<table border="1" class="cattb">
				<tr>
					<th width="33%">很重要的特質</th>
					<th width="34%">普通重要的特質</th>
					<th width="33%">不重要的特質</th>
				</tr>
				<tr>
					<td>
						<?php echo $impts[1]; ?>
					</td>
					<td>
						<?php echo $impts[2]; ?>
					</td>
					<td>
						<?php echo $impts[3]; ?>
					</td>
				</tr>
			</table>
		</div>

		<div class="readmore">
			<div class="noteTableExplain" style="font-size: large; color: #333333; padding-top: 10px; padding-bottom: 10px; font-family: 微軟正黑體;">
				<table border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td><img style="width: 42px; height: 48px;" src="images/Speaker_03_02.png" alt="" />
							</td>
							<td style="background: url('images/Speaker_03_bg.png') repeat-x;"><a href="index.php?option=com_content&amp;view=article&amp;id=1&amp;Itemid=130" target="_blank" style="color: #333333; text-decoration: none;">燈火闌珊那一人</a>
							</td>
							<td><img src="images/Speaker_03.png" alt="" width="21" height="48" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="noteTableExplain" style="font-size: large; color: #333333; padding-top: 10px; padding-bottom: 10px; font-family: 微軟正黑體;">
				<table border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td><img style="width: 42px; height: 48px;" src="images/Speaker_03_04.png" alt="" />
							</td>
							<td style="background: url('images/Speaker_03_bg.png') repeat-x;"><a href="index.php?option=com_vquiz&amp;view=quizmanager&amp;id=1&amp;Itemid=148" target="_blank" style="color: #333333; text-decoration: none;">測驗你(妳)的愛情風格</a>
							</td>
							<td><img src="images/Speaker_03.png" alt="" width="21" height="48" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="noteTableExplain" style="font-size: large; color: #333333; padding-top: 10px; padding-bottom: 10px; font-family: 微軟正黑體;">
				<table style="color: #333333; font-size: large;" border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td><img style="width: 42px; height: 48px;" src="images/Speaker_03_2_01.png" alt="" />
							</td>
							<td style="background: url('images/Speaker_03_bg.png') repeat-x;"><a href="index.php?option=com_content&amp;view=article&amp;id=71:2015-07-16-07-40-42&amp;catid=19&amp;Itemid=131" target="_blank" style="color: #333333; text-decoration: none;">情人眼裡出西施</a>
							</td>
							<td><img src="images/Speaker_03.png" alt="" width="21" height="48" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
<div style="text-align: right;">
		<img alt="" src="images/Owl.png" style="text-align: right; width: 188px; height: 164px;"></div>
	</div>
	
	
</div>

<script language="JavaScript">
	(function($) {
		$(document).ready(function() {
			
		});
	})(jQuery);
</script>