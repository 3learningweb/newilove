<?php
/**
 * @version        : default.php 2012-10-16 21:06:39$
 * @author        EFATEK
 * @package        gmap
 * @copyright    Copyright (C) 2011- EFATEK. All rights reserved.
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app    = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

?>
<div class="com_postcards">
<?php
if ($this->image) {
    ?>
        <div class="preview">

            <div class="preview_inner">
                <div class="left_block">

                    <div id="image">
                        <div class="image_inner">
                            <img src="<?php echo JURI::root() . $this->image; ?>" alt="<?php echo JText::_('COM_POSTCARDS_FORM_IMAGE'); ?>">
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <style>
        .submit_block input[type="submit"] {
            font-size: 18px;
            font-weight: bold;
            background-image: url('templates/system/images/submit_icon.png');
            background-repeat: no-repeat;
            background-position: right;

        }

        .submit_block input[type="submit"] {
            width: 185px;
            height: 42px;
            margin: 0px;
            border: 0px;
            line-height: 42px;
            text-align: center;
        }
        </style>
        <div class="message_form">
            <form id="submitForm" name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=viewcardnew&Itemid=' . (int) $itemid); ?>"  enctype="multipart/form-data" >



                <table class="form_table" width="100%" border="0">
                    <?php if ($this->card_type == "sticker") {?>
                    <tr>
                        <td class="local_photo" colspan="2">
                            <span class="highlight">*</span><span style="font-weight: bold;"><?php echo JText::_('COM_POSTCARDS_CARD_PIC'); ?></span>
                            <input type="file" id="pic" name="pic" accept="image/jpeg"> (建議上傳最佳尺寸為400x250，避免上傳圖片被裁切，影響圖片最佳顯示尺寸。)
                        </td>
                    </tr>

                    <tr>
                    <td colspan="2">
                    <span class="highlight" style="display: inline-block; vertical-align: top;">*</span><span style="font-weight: bold; display: inline-block; vertical-align: top;">從Facebook相簿上傳：</span>
                    <button type="button" id="fb-login" style="border:0px;"> <img src="images/fb/login.png" alt="" style="width:150px;"></button>
                    <div class="fb-album-container"></div>

                    <!--img class="download-file">
                        <div class="bucket">
                        </div>
                        <table cellpadding="0" cellspacing="0" class="picked-photos-table">
                            <tr>
                                <th>
                                    Preview
                                </th>
                                <th>
                                    Id
                                </th>
                                <th>
                                    URL
                                </th>
                            </tr>
                        </table>
                    </img -->



                    </td>



                    </tr>



                    <?php }?>
                    <tr>
                        <th width="30%"><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_CONTENT'); ?><br><?php echo JText::_('COM_POSTCARDS_CARD_CONTENT_RESTRICTION'); ?>
                        </th>
                        <td width="70%">
                            <textarea id="content2" name="content" class="required" rows="6" style="width:90%"><?php echo $app->getUserState('form.postcards.content', ''); ?></textarea>
                        </td>
                    </tr>
                </table>



                <div style="text-align: center; margin-top:30px;">
                <div class="submit submit_block">
                    <input id="submit_btn" type="button" value="<?php echo JText::_('COM_POSTCARDS_NEXT'); ?>" onClick="sendForm()" />
                    <!--input type="reset" value="<?php echo JText::_('COM_POSTCARDS_RESET'); ?>"-->
                    <input type="hidden" name="task" value="viewcardnew.check" />
                </div>
                </div>
            </form>
        </div>
    <?php
} else {
    ?>
        <div class="nodata"><?php echo JText::_('COM_POSTCARDS_NOIMAGE'); ?></div>

        <?php
}
?>


</div>
<script>




    (function($) {

        $('#fb-login').click(function(){
            window.fbAsyncInit = function() {
                FB.init({
                  //appId      : '1817645831819064',
                  appId      : '1901317193433996',  //ilove.moe.edu.tw
                  xfbml      : true,
                  version    : 'v2.8'
                });

                //FB.AppEvents.logPageView();

                /* 以下的code只會執行一次而已！ 這樣就不會重複開兩個fb album */
                FB.getLoginStatus(function(response) { //要等 FB init 完畢， 才能執行 getLoginStatus()
                    console.log(response);
                    if (response.status === 'connected') { // 若已經連線 就可以開始存取相簿。
                        console.log('Logged in Greeting.');
                        localStorage.setItem('accessToken', response.authResponse.accessToken);
                        showAlbum();
                    } else if (response.status === 'not_authorized') {
                            FB.login(function(response) {
                                      localStorage.setItem('accessToken', response.authResponse.accessToken);
                                      showAlbum();
                            }, {
                              scope: 'publish_actions,user_photos'  //透過對話方塊貼文 這個權限可有可無！
                            }); // FB.login()  end                        
                    } else {
                          //response.status === 'unknown'
                          // the user isn't logged in to Facebook.
                          FB.login(function(response) {
                                    localStorage.setItem('accessToken', response.authResponse.accessToken);
                                    showAlbum();
                          }, {
                            scope: 'publish_actions,user_photos'  //透過對話方塊貼文 這個權限可有可無！
                          }); // FB.login()  end
                    }
                });


              };

              (function(d, s, id){
                 var js, fjs = d.getElementsByTagName(s)[0];
                 if (d.getElementById(id)) {return;}
                 js = d.createElement(s); js.id = id;
                 js.src = "//connect.facebook.net/en_US/all.js";
                 fjs.parentNode.insertBefore(js, fjs);
               }(document, 'script', 'facebook-jssdk'));



              function showAlbum() {
                $(".fb-album-container").FacebookAlbumBrowser({
                  account: "me",
                  //accessToken: "EAACEdEose0cBAOSzLg0ZBZAkfx1RgW9ifzPAn22wKhop7zW1c7roUwzUMGxY6s7ql32PWKIZCq4I8Ykncii5hNf41AZCzdzsHUeZBUgz4eZAjTJ4HZB9aH3e7pzavwdt9uJjlZAAP8gVavMrBosaYoX7u0gWXA96qn2LBK8iCwIwKt3ow5MqkzDj",
                  accessToken: localStorage.accessToken,

                  //skipAlbums: ["Profile Pictures", "Timeline Photos", "Photos", "Cover Photos"],
                  includeAlbums: ["Profile Pictures", "Timeline Photos"],
                  showComments: true,
                  commentsLimit:3,
                  showAccountInfo: true,
                  showImageCount: true,
                  showImageText: true,
                  shareButton: false,
                  albumsPageSize: 0,
                  photosPageSize: 0,
                  lightbox: true,
                  photosCheckbox: true,
                  pluginImagesPath: "<?php echo JURI::root(); ?>components/com_postcards/assets/fbbrowser/src/",
                  likeButton: true,
                  shareButton: false,
                  addThis:"ra-52638e915dd79612",
                  photoChecked: function(photo){
                      console.log("PHOTO CHECKED: " + photo.id + " ------ " + photo.url + " ------ " + photo.thumb);
                      console.log("CHECKED PHOTOS COUNT: " + this.checkedPhotos.length);
                      $.checkedPhotos = this.checkedPhotos;

                      var tableRow = $("<tr/>");
                      var tableThumbCol = $("<td/>");
                      $(tableThumbCol).append($("<img/>", { src: photo.thumb }));
                      $(tableRow).append(tableThumbCol);
                      $(tableRow).append($("<td/>", { text: photo.id }));
                      $(tableRow).append($("<td/>", { text: photo.url }));
                      $("table.picked-photos-table").append(tableRow);

                      downloadFile(photo.id, photo.url, function(respond) { //從fb相簿下載下來 之後再上傳至自己的server
                                            console.log(respond);  // 印出 Blob {size: 80267, type: "image/jpeg", slice: function}
                                            $(".download-file").attr("src", URL.createObjectURL(respond));
                                            $(".bucket").append(URL.createObjectURL(respond));
                                            $(".bucket").append("<br>");
                                        });
                  },
                  photoUnchecked: function (photo) {
                      console.log("PHOTO UNCHECKED: " + photo.id + " - " + photo.url + " - " + photo.thumb);
                      console.log("CHECKED PHOTOS COUNT: " + this.checkedPhotos.length);
                      $.checkedPhotos = this.checkedPhotos;

                      $("table.picked-photos-table td:contains(" + photo.id + ")").parent().remove();
                  },
                  albumSelected: function (photo) {
                      console.log("ALBUM CLICK: " + photo.id + " - " + photo.url + " - " + photo.thumb);
                  },
                  photoSelected: function (photo) {
                      console.log("PHOTO CLICK: " + photo.id + " - " + photo.url + " - " + photo.thumb);
                  }
                });
              }
        });

        function downloadFile(id, url, success) { // 把檔案從fb 的CDN那邊載下來～
                    var xhr = new XMLHttpRequest(); //送出ajax request!!!

                    xhr.open('GET', url, true); // 這會取得 blob物件！
                    xhr.responseType = "blob";
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState == 4) { //成功後 接受檔案
                            if (success) { //如果有callback
                                success(xhr.response); //呼叫callback 回傳一個Blob物件
                                uploadPhoto(id, xhr.response); //上傳一個blob物件
                            }
                        }
                    };
                    xhr.send(null);
                }

                function uploadPhoto(id, blob) {  //將從fb 下載下來的圖片 上傳到 自己的server 下載下來的圖檔 用 blob的形式儲存！
                    var reader = new FileReader();
                    reader.onload = function(event) { //確定blob檔案完成讀取之後，上傳檔案到自己的server
                        var fd = {};

                        if (blob.type == "image/jpeg")
                            fd["ext"] = "jpg";
                        else if (blob.type == "image/png")
                            fd["ext"] = "png";
                        else if (blob.type == "image/gif")
                            fd["ext"] = "gif";
                        else
                            fd["ext"] = "jpg";

                        fd["fbphoto"] = event.target.result;
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo JURI::root(); ?>components/com_postcards/assets/fbbrowser/upload.php',  //for 測試機環境
                            data: fd,
                            dataType: 'text'
                        }).done(function(data) {
                            console.log(data); // 回傳唯一檔名！
                            //記錄下來放到input tag ，之後用form的方式傳到server端 開始製作影片！
                            //儲存上傳至server的照片檔名

                            $('#submitForm').append('<input type="hidden" name="serverPhotoFilename_' + id + '" value="' + data + '" />');
                        });
                    };
                    reader.readAsDataURL(blob);
                }

    })(jQuery)



    jQuery(document).ready(function($) {

        $.checkedPhotos = []; // fb選取照片清單
        $.checkedLocalPhotos = []; //本機選取照片清單

        /*=================================
        =            加入本機照片瀏覽區            =
        =================================*/

        $("#pic").after('<span class="local_photo_note"></span>'); //解決 IE 在input裡頭無法顯示字的問題

        $('#pic').after("<div id='clear_btn_container'><button type='button' id='clear'>清除選擇照片</button><output id='result' /></div>");

        $('#pic').on("change", function(event) {
            /* 目前本機只接受一張照片 */
            
            $('.thumbnail').parent().remove();
            $('#result').hide();

            var files = event.target.files; //FileList object
            $.checkedLocalPhotos = files;

            if($.checkedLocalPhotos.length + $.checkedPhotos.length >2) {
                alert("照片數量不能超過2張唷！");
                $('.thumbnail').parent().remove();
                $('#result').hide();
                $('#pic').val("");
                return;
            }
            var output = document.getElementById("result");
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if (file.type.match('image.*')) {
                    if (this.files[0].size < 5097152) {
                        // continue;
                        var picReader = new FileReader();
                        picReader.addEventListener("load", function(event) {
                            var picFile = event.target;
                            var div = document.createElement("div");
                            div.innerHTML = "<img class='thumbnail' src='" + picFile.result + "'" +
                                "title='preview image'/>";
                            output.insertBefore(div, null);
                        });
                        //Read the image
                        $('#clear_btn_container, #result').show();
                        picReader.readAsDataURL(file);
                    } else {
                        alert("單張照片請勿超過5MB唷！");
                        $(this).val("");
                    }
                } else {
                    alert("只能上傳圖檔喔～");
                    $(this).val("");
                }
            }
            $(this).hide();
            $(".local_photo_note").hide();

        });

        $(document).on("click", '#clear', function() {
            $('.thumbnail').parent().remove();
            $('#result').hide();
            $('#pic').val("");
            $('#pic').show();
            $(".local_photo_note").show();
            $('#clear_btn_container').hide();
        });
        /*=====  End of 加入本機照片瀏覽區  ======*/


    });

    function sendForm() {


        

        <?php if ($this->card_type == "sticker") {?>
        if(jQuery.checkedLocalPhotos.length + jQuery.checkedPhotos.length <1) { //至少要一張照片
            alert("<?php echo JText::_('COM_POSTCARDS_FORM_PIC'); ?>");
            return false;
        }
        // if (jQuery("#pic").val() == "") {
        //     alert("<?php echo JText::_('COM_POSTCARDS_FORM_PIC'); ?>");
        //     return false;
        // }
        <?php }?>

        if (jQuery("#content2").val() == "") {
            alert("<?php echo JText::_('COM_POSTCARDS_FORM_CONTENT'); ?>");
            return false;
        }


        if(confirm("<?php echo JText::_('COM_POSTCARDS_FORM_CHECK'); ?>")) {
            document.getElementById("submitForm").submit();
        }
    }
</script>