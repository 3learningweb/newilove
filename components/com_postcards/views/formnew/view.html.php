<?php
/**
 * @version		: view.html.php 2012-10-17 05:06:09$
 * @author		efatek 
 * @package		gmap
 * @copyright	Copyright (C) 2011- efatek. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the gmap Component
 */
class PostcardsViewFormnew extends JViewLegacy
{
	protected $item;
	protected $pagination;
	
	// Overwriting JView display method
	function display($tpl = null) 
	{
		$app = JFactory::getApplication();
		$jinput = $app->input;

		$itemid = $app->input->getInt('Itemid');

		// Assign data to the view
		$this->state 	= $this->get('state');
		$this->params 	= $this->state->get('params');

		$menu = $app->getMenu();
		$par = $menu->getParams( $itemid );
		$this->card_type = $par->get('card_type');

		
		$image_id = $jinput->getInt('image_id');
		if ($image_id) {
			$app->setUserState('form.postcards.image_id', $image_id);
		}

		$this->image = $this->get('Image');


		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}


		$document = JFactory::getDocument();
		//$document->addScript('http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');
                        $document->addScript(JURI::root(true) . '/media/jui/js/jquery.min.js');
		$document->addScript(JURI::root(true) . '/components/com_postcards/assets/fbbrowser/src/jquery.fb.albumbrowser.js');
		$document->addStyleSheet(JURI::root(true) . '/components/com_postcards/assets/fbbrowser/src/jquery.fb.albumbrowser.css');


		$style = " .picker-result{
            font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        }
        .picked-photos-table img{
            width:90px;
        }
        .picked-photos-table tr th{
            text-align:left;
            background-color:#e2e1e1;
        }
        .picked-photos-table td,.picked-photos-table tr th {
            padding-right:6px;
            padding-top:6px;
            text-align:left;
        }
        .picked-photos-table{
            width:100%;
            display:block;
        }
        .picker-result hr{
            border:1px solid #808080;
        }




        .local_photo input + span:after {
            content: '*請一次上傳2至10張照片，檔案格式限定為jpg或jpeg。';
            color: #ff3333;
            font-size: 12px;
        }

        #clear_btn_container:after {
            content: '*若要更換照片，請按下此按鈕後，重新進行「選擇檔案」';
            color: #ff3333;
            margin-left: 6em;
            font-size: 12px;
        }

        .local_photo
        {
            width: 80%;
            margin:auto;
            margin-top:10px;
        }


        .thumbnail{

            height: 100px;
            margin: 10px; 
            float: left;
        }
        #clear_btn_container{
           display:none;
        }
        #result {
            border: 4px dotted #cccccc;
            display: none;
            float: right;
            margin:0 auto;
            width: 100%;
        }



        "; 
		$document->addStyleDeclaration( $style );
		
		// Display the view
		parent::display($tpl);


	}
}
