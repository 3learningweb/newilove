<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

?>
<div class="com_postcards">
<?php
if ($this->image) {
	?>
		<div class="preview">
			
			<div class="preview_inner">
				<div class="left_block">

					<div id="image">
						<div class="image_inner">
							<img src="<?php echo JURI::root() . $this->image; ?>" alt="<?php echo JText::_('COM_POSTCARDS_FORM_IMAGE'); ?>">
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<div class="message_form">
			<form id="submitForm" name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=viewcard&Itemid='. (int) $itemid); ?>"  enctype="multipart/form-data" >
				<table class="form_table" width="100%">
					<?php if ($this->card_type == "sticker") { ?>
					<tr>
						<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_PIC'); ?></th>
						<td>
							<input type="file" id="pic" name="pic" accept="image/jpeg"> (建議上傳最佳尺寸為400x250 ，<br>避免上傳圖片被裁切，影響圖片最佳顯示尺寸。)
						</td>
					</tr>
					

					<?php } ?>
					<tr>
						<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_CONTENT'); ?><br><?php echo JText::_('COM_POSTCARDS_CARD_CONTENT_RESTRICTION'); ?></th>
						<td>
							<textarea id="content2" name="content" class="required" rows="6" style="width:90%"><?php echo $app->getUserState('form.postcards.content', ''); ?></textarea>
						</td>
					</tr>
				</table>
				<div class="submit">
					
					<input id="submit_btn" type="button" value="<?php echo JText::_('COM_POSTCARDS_NEXT'); ?>" onClick="sendForm()" />
					<input type="reset" value="<?php echo JText::_('COM_POSTCARDS_RESET'); ?>">
					<input type="hidden" name="task" value="viewcard.check" />
				</div>
			</form>
		</div>
	<?php
} else {
	?>
		<div class="nodata"><?php echo JText::_('COM_POSTCARDS_NOIMAGE'); ?></div>

		<?php
	}
	?>


</div>
<script>
	jQuery(document).ready(function() {
		
	});

	function sendForm() {
		
		<?php if ($this->card_type == "sticker") { ?>
		if (jQuery("#pic").val() == "") {
			alert("<?php echo JText::_('COM_POSTCARDS_FORM_PIC'); ?>");
			return false;
		}
		<?php } ?>

		if (jQuery("#content2").val() == "") {
			alert("<?php echo JText::_('COM_POSTCARDS_FORM_CONTENT'); ?>");
			return false;
		}


		if(confirm("<?php echo JText::_('COM_POSTCARDS_FORM_CHECK'); ?>")) {
			document.getElementById("submitForm").submit();
		}
	}
</script>