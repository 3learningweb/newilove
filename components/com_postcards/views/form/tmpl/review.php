<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');


?>
<div class="com_postcards">
<?php
if ($this->image && $this->figure && $this->frame) {
	?>
		<div class="preview">
			<div id="frame">
				<img src="<?php echo JURI::root() . $this->frame; ?>" alt="<?php echo JText::_('COM_POSTCARDS_FORM_FRAME'); ?>">
			</div>
			<div class="preview_inner">
				<div class="left_block">
					<div id="logo">
						<img src="<?php echo JURI::root() . '/filesys/images/system/logo_pcard.png'; ?>" alt="<?php echo JText::_('COM_POSTCARDS_SITE_LOGO'); ?>">
					</div>
					<div id="image">
						<div class="image_inner">
							<img src="<?php echo JURI::root() . $this->image; ?>" alt="<?php echo JText::_('COM_POSTCARDS_FORM_IMAGE'); ?>">
						</div>
					</div>
				</div>
				<div class="right_block">
					<div id="figure">
						<img src="<?php echo JURI::root() . $this->figure; ?>" alt="<?php echo JText::_('COM_POSTCARDS_FORM_FIGURE'); ?>">
					</div>
					<div id="message" class="message">
						<div class="from">
							<?php echo "Dear ".$this->friend_name.","; ?>
						</div>
						<div class="content">
							<?php echo $this->content; ?>
						</div>
						<div class="to">
							<?php echo $this->name." ".date('Y-m-d'); ?>
						</div>
					</div>
				</div>
			</div>
			<div id="message_m" class="message">
				<div class="from">
					<?php echo "Dear ".$this->friend_name.","; ?>
				</div>
				<div class="content">
					<?php echo $this->content; ?>
				</div>
				<div class="to">
					<?php echo $this->name." ".date('Y-m-d'); ?>
				</div>
			</div>
		</div>
		<div class="submit">
		<form name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=form&Itemid='. (int) $itemid); ?>">

			<input id="submit_btn2" type="submit" value="<?php echo JText::_('COM_POSTCARDS_SEND'); ?>" />
			<input type="hidden" name="task" value="form.send" />
		</form>
		</div>
	<?php
} else {
	?>
		<div class="nodata"><?php echo JText::_('COM_POSTCARDS_NOIMAGE'); ?></div>

		<?php
	}
	?>


</div>
<script>

</script>