<?php
/**
 * @version		: view.html.php 2012-10-17 05:06:09$
 * @author		efatek 
 * @package		gmap
 * @copyright	Copyright (C) 2011- efatek. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the gmap Component
 */
class PostcardsViewViewcard extends JViewLegacy
{
	protected $item;
	protected $pagination;
	
	// Overwriting JView display method
	function display($tpl = null) 
	{
		$app = JFactory::getApplication();
		$jinput = $app->input;

		$itemid = $app->input->getInt('Itemid');
		

		// Assign data to the view
		$this->state 	= $this->get('state');
		$this->params 	= $this->state->get('params');

		$menu = $app->getMenu();
		$par = $menu->getParams( $itemid );
		$this->card_type = $par->get('card_type');
		

		$this->bg_img = $app->getUserState('form.postcards.bg_img', '');
		

		



		
	
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		
		
		// Display the view
		parent::display($tpl);


	}


}
