<?php
/**
 * @version        : default.php 2012-10-16 21:06:39$
 * @author        EFATEK
 * @package        gmap
 * @copyright    Copyright (C) 2011- EFATEK. All rights reserved.
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app    = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

?>
<div class="com_postcards">
<?php
if ($this->bg_img) {
    ?>
	<form id="submitForm" name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=email&Itemid=' . (int) $itemid); ?>"  enctype="multipart/form-data" >

		<div class="preview">
			<table class="form_table" width="100%">

				<tr>
					<td colspan="2">
						<canvas id="card_canvas" width="640" height="480"></canvas>
						<span class="highlight">*</span><span style="font-weight: bold;">您可拖曳或縮放照片和文字，調整至您喜歡的風格。</span><br>

						<span class="highlight">*</span><span style="font-weight: bold;">您可修改：<button class="jscolor {valueElement:'chosen-value', onFineChange:'setTextColor(this)'}" style="background-image: none; background-color: rgb(255, 232, 67); color: rgb(0, 0, 0);"> 文字顏色 </button>，文字字型：<select name="FontStyleNumber" id="FontStyleNumber">
						  <option value="Times New Roman">Times New Roman</option>
						  <option value="Arial">Arial</option>
						  <option value="Georgia">Georgia</option>
						  <option value="細明體">細明體</option>
						  <option value="新細明體">新細明體</option>
						  <option value="標楷體">標楷體</option>
						  <option value="微軟正黑體">微軟正黑體</option>
						  <option value="微軟雅黑體">微軟雅黑體</option>
						</select></span><br><br>

						<div class="submit_block" style="margin-bottom: 20px;">
							<input id="download" type="button" value="下載卡片" />
						</div>
						<button id="fbShare" type="button" style="border: 0;background: initial; top: 15px; position: relative;">


						    <img src="images/fb/share_to_fb.png" style="width:300px;">
						</button>
					</td>

				</tr>
				<?php if ($this->card_type == "postcard") {?>
				<tr>
					<td colspan="2">
						<?php echo $app->getUserState('form.postcards.content', ''); ?>
					</td>
				</tr>
				<?php }?>


				<tr>
					<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_RECEIVER_EMAIL'); ?></th>
					<td class="form_text"><input type="text" id="friend_email" name="friend_email" class="required" value="<?php echo $app->getUserState('form.postcards.friend_email', ''); ?>"></td>
				</tr>
				<tr>
					<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_SENDER_NAME'); ?></th>
					<td class="form_text"><input type="text" name="name" class="required" value="<?php echo $app->getUserState('form.postcards.name', ''); ?>"></td>
				</tr>
				<tr>
					<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_SENDER_EMAIL'); ?></th>
					<td class="form_text"><input type="text" id="email" name="email" class="required" value="<?php echo $app->getUserState('form.postcards.email', ''); ?>"></td>
				</tr>
				<tr>
					<th><span class="highlight">*</span><?php echo JText::_('COM_POSTCARDS_CARD_RECEIVER_SUBJECT'); ?></th>
					<td class="form_text"><input type="text" name="subject" class="required" value="<?php echo $app->getUserState('form.postcards.subject', ''); ?>"></td>
				</tr>

			</table>


		</div>
		<div style="text-align: center; margin-top: 30px;">
		<div class="submit submit_block">
			<input id="submit_btn" type="button" value="<?php echo JText::_('COM_POSTCARDS_SEND'); ?>" onClick="sendForm()" />
			<input type="hidden" name="task" value="viewcardnew.send" />
		</div>
		</div>
	</form>

	<style>


	.form_table input[type="text"] {
		width:350px;
	}
	</style>

	<?php
} else {
    ?>
		<div class="nodata"><?php echo JText::_('COM_POSTCARDS_NODATA'); ?></div>

	<?php
}
?>


</div>
<script>






	var canvas = new fabric.Canvas('card_canvas');
	/* 載入底圖 */
	fabric.Image.fromURL("<?php echo JURI::root() . $this->bg_img; ?>", function(img) {
		img.selectable = false;
		img.scaleToWidth(640);
		canvas.add(img);
		canvas.moveTo(img, 0);
	});


	/* 將本機相簿載入 */
	<?php if (!empty($this->new_img)): ?>
	fabric.Image.fromURL("<?php echo JURI::root() . $this->new_img; ?>", function(img) {
		img.scaleToWidth(300);
		canvas.add(img);
		canvas.moveTo(img, 1);
	});
	<?php endif?>

	/* 將之前頁面輸入的相片載入 */
	<?php if (count($this->servPhotos)) {
    			foreach ($this->servPhotos as $key => $value) {?>
				fabric.Image.fromURL("<?php echo JURI::root() . "tmp/" . $value ?>", function(img) {
					img.scaleToWidth(300);
					img.setLeft(10*<?php echo $key; ?>);
					img.setTop(10*<?php echo $key; ?>);
					canvas.add(img);
					canvas.moveTo(img, 1);
				});
	<?php
			}
		}

	?>

	/* 加入文字 */
	var text = new fabric.Text('<?php echo $this->content; ?>', { left: 100, top: 100, fill: '#f00', fontFamily: '楷體' });
	canvas.add(text);
	canvas.moveTo(text, 2);

	function setTextColor(picker) {  //改變文字顏色
		text.setColor('#'+picker.toString());
		canvas.renderAll();
	}


	var fontControl = $('#FontStyleNumber');
	$(document.body).on('change', '#FontStyleNumber', function () {
	    text.fontFamily = fontControl.val();
	    canvas.renderAll();
	});

	//jQuery(document).ready(function() {

		$("#download").click(function() {
			$("canvas").get(0).toBlob(function(blob){  //可以取得blob格式的圖檔
				saveAs(blob, "myCard.jpg");
			});
		});


	//});

	importFBSDK();


	function importFBSDK() {
	    window.fbAsyncInit = function() {
	        FB.init({
	          //appId      : '1817645831819064',
	          appId      : '1901317193433996', //ilove.moe.edu.tw
	          xfbml      : true,
	          version    : 'v2.8'
	        });

	        //FB.AppEvents.logPageView();

	        FB.getLoginStatus(function(response) { //要等 FB init 完畢， 才能執行 getLoginStatus()
	            console.log(response);
	            if (response.status === 'connected') { // 若已經連線 就可以開始存取相簿。
	                console.log('Logged in Greeting.');
	                localStorage.setItem('accessToken', response.authResponse.accessToken);
	                //checkUser();
	            } else if (response.status === 'not_authorized') {
	                // the user is logged in to Facebook, but has not authenticated the app
	            } else {
	                //response.status === 'unknown'
	                // the user isn't logged in to Facebook.
	                    FB.login(function() {
	        	            var count = 0;
	        	            FB.api("/me/picture", { // 取得大頭照
	        	                "type": "large",
	        	                "height": 1000
	        	            }, function(response) {
	        	                if (response && !response.error) {
	        	                    /* handle the result */
	        	                    userImage = response.data.url;
	        	                    window.sessionStorage["userImage"] = userImage; //暫存在瀏覽器cache
	        	                    console.log(window.sessionStorage["userImage"]);
	        	                    count++;
	        	                    if (count == 2) {
	        	                        //showHUD();
	        	                    };
	        	                }
	        	            });

	        	            FB.api("/me", function(response) { //取得個人姓名資訊
	        	                if (response && !response.error) {
	        	                    /* handle the result */
	        	                    userName = response.name;
	        	                    window.sessionStorage["userName"] = userName;
	        	                    console.log("TEST" + window.sessionStorage["userName"]);
	        	                    count++;
	        	                    if (count == 2) {
	        	                        //showHUD();
	        	                    };
	        	                }
	        	            });
	        	          }, {
	        	          	scope: 'publish_actions,user_photos'  //透過對話方塊貼文 這個權限可有可無！
	        	          });
	            }
	        });


	      };

	      (function(d, s, id){
	         var js, fjs = d.getElementsByTagName(s)[0];
	         if (d.getElementById(id)) {return;}
	         js = d.createElement(s); js.id = id;
	         js.src = "//connect.facebook.net/en_US/all.js";
	         fjs.parentNode.insertBefore(js, fjs);
	       }(document, 'script', 'facebook-jssdk'));
	}

	$("#fbShare").click(function() {
		$("canvas").get(0).toBlob(function(blob) {  //可以取得blob格式的圖檔
			var reader = new FileReader();
			reader.onload = function(event) { //確定blob檔案完成讀取之後，上傳檔案到server
				var fd = {};

				if (blob.type == "image/jpeg")
				    fd["ext"] = "jpg";
				else if (blob.type == "image/png")
				    fd["ext"] = "png";
				else if (blob.type == "image/gif")
				    fd["ext"] = "gif";
				else
				    fd["ext"] = "jpg";

				    fd["fbphoto"] = event.target.result;
				    $.ajax({
				        type: 'POST',
				        url: '<?php echo JURI::root(); ?>components/com_postcards/assets/fbbrowser/upload.php',
				        data: fd,
				        dataType: 'text'
				    }).done(function(data) {
				        console.log(data); // 回傳唯一檔名！

				        postToFacebook(data);
				        //記錄下來放到input tag ，之後用form的方式傳到server端 開始製作影片！
				        //儲存上傳至server的照片檔名
				    });
			};
			reader.readAsDataURL(blob);
		});
	});

	function postToFacebook(imageLink) {   //在facebook貼文！！！
	    // console.log(imageLink);
	    // console.log(window.location.href+imageLink);
	    imageLink = "/tmp/" + imageLink;
	    // var href = window.location.href
	    // var res = href.split("index.php");
	    var pictureUrl = window.location.origin + imageLink; // 找出圖片的url
	    var desc = "想把每一個愛的時刻都記錄下來嗎?快來合成屬於你的愛情大頭貼吧！";   //文字的部分！
	    console.log(pictureUrl);
	    //alert("1234567");
	    FB.ui({
	        method: 'feed',
	        name: '戀愛情境大頭貼｜iLove心動時光',
	        link: 'https://ilove.moe.edu.tw/心動時光/戀愛情境大頭貼',   //該服務的網址！ 宣傳用！
	        picture: pictureUrl,  //縮圖
	        caption: 'ILOVE.MOE.EDU.TW',   //底下的文字！
	        description: desc  //文字內容描述
	    }, function(response) {
	        //alert("765432");
	        if (response && response.post_id) {
	            alert("分享成功！")
	        } else {
	            console.log("分享失敗唷");
	        }
	    });
	}

	function sendForm() { //檢查每個欄位是否填寫正確資料！

		/* 將canvas 用ajax方式上傳至 server，取得檔名後， 也跟著form submit到server */



		if (jQuery(".required").val() == "") {
			alert("<?php echo JText::_('COM_POSTCARDS_FORM_REUIRED'); ?>");

			return false;
		}

		reEcourse=/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
		var friend_email = jQuery("#friend_email").val();
		if (!reEcourse.test(jQuery.trim(friend_email))) {
			alert("<?php echo JText::_('COM_POSTCARDS_FORM_EMAIL'); ?>");
			return false;
		}

		var email = jQuery("#email").val();
		if (!reEcourse.test(jQuery.trim(email))) {
			alert("<?php echo JText::_('COM_POSTCARDS_FORM_EMAIL'); ?>");
			return false;
		}

		$("canvas").get(0).toBlob(function(blob) {  //可以取得blob格式的圖檔
			var reader = new FileReader();
			reader.onload = function(event) { //確定blob檔案完成讀取之後，上傳檔案到server
				var fd = {};

				if (blob.type == "image/jpeg")
				    fd["ext"] = "jpg";
				else if (blob.type == "image/png")
				    fd["ext"] = "png";
				else if (blob.type == "image/gif")
				    fd["ext"] = "gif";
				else
				    fd["ext"] = "jpg";

				    fd["fbphoto"] = event.target.result;
				    $.ajax({
				        type: 'POST',
				        url: '<?php echo JURI::root(); ?>components/com_postcards/assets/fbbrowser/upload.php',
				        data: fd,
				        dataType: 'text'
				    }).done(function(data) {
				        console.log(data); // 回傳唯一檔名！

				        //上傳至server的照片檔名用input 方式送到server去
				        $('#submitForm').append('<input type="hidden" name="final_photo" value="' + data + '" />');

				        /* 檔案上傳完畢 才開始submit form */
				        if(confirm("<?php echo JText::_('COM_POSTCARDS_SEND_CHECK'); ?>")) {
				        	document.getElementById("submitForm").submit();
				        }
				    });
			};
			reader.readAsDataURL(blob);
		});



	}
</script>