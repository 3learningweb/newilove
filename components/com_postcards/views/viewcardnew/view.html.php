<?php
/**
 * @version		: view.html.php 2012-10-17 05:06:09$
 * @author		efatek 
 * @package		gmap
 * @copyright	Copyright (C) 2011- efatek. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the gmap Component
 */
class PostcardsViewViewcardnew extends JViewLegacy
{
	protected $item;
	protected $pagination;
	
	// Overwriting JView display method
	function display($tpl = null) 
	{
		$app = JFactory::getApplication();
		$jinput = $app->input;

		$itemid = $app->input->getInt('Itemid');
		$post = $jinput->getArray($_POST);

		// Assign data to the view
		$this->state 	= $this->get('state');
		//$this->params 	= $this->state->get('params');

		$menu = $app->getMenu();
		$par = $menu->getParams( $itemid );
		$this->card_type = $par->get('card_type');
		

		$this->bg_img = $app->getUserState('form.postcards.bg_img', '');
		$this->new_img = $app->getUserState('form.postcards.new_img', '');
		$this->post_data = json_decode($app->getUserState('form.postcards.post_data', ''));
		$this->content = $app->getUserState('form.postcards.content', '');

		foreach(get_object_vars($this->post_data) as $key => $value) {
		    if (strpos($key, 'serverPhotoFilename_') !== false) {  //專門放fb上傳的照片
		        $this->servPhotos[] = $value;
		    }
		}

	
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		$document = JFactory::getDocument();
		//$document->addScript('http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');
		$document->addScript(JURI::root(true) . '/components/com_judownload/assets/js/jquery.min.js');
		$document->addScript(JURI::root(true) . '/components/com_postcards/assets/fabric.js');
		$document->addScript(JURI::root(true) . '/components/com_postcards/assets/jscolor.min.js');
		$document->addScript(JURI::root(true) . '/components/com_postcards/assets/FileSaver.min.js');
		$document->addScript(JURI::root(true) . '/components/com_postcards/assets/canvas-to-blob.min.js');

		$style = " 
		.canvas-container {
		    margin-bottom: 10px;
		}
        		";	 
        		$document->addStyleDeclaration( $style );

		// Display the view
		parent::display($tpl);


	}


}
