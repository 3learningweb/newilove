<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');

$langId = JFactory::getLanguageId();
?>
<div class="com_postcards">
	<?php
	if ($this->items) {
		?>
		<form name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=form&Itemid='. (int) $itemid); ?>">
			<div class="title">
				<img src="templates/ch/images/pcard_bg_title.png" alt="<?php echo JText::_('COM_POSTCARDS_FRAME'); ?>" title="<?php echo JText::_('COM_POSTCARDS_FRAME'); ?>" />
			</div>
			<div class="frame_blocks">
				<?php
				$check = $app->getUserState('form.postcards.frame_id', '');
				foreach ($this->items as $i => $item) {
					if ($i == 0 && $check == 0) {
						$check = $item->id;
					}
					?>
					<div class="frame_block">
						<div class="image">
							<a class="fancybox" href="<?php echo $item->image; ?>" title="<?php echo $item->title; ?>">
								<img alt="<?php echo $item->title; ?>" src="<?php echo JURI::root() . $item->image; ?>" />
							</a>
						</div>
						<div class="radio">
							<input type="radio" name="frame_id" value="<?php echo $item->id; ?>" <?php echo ($check == $item->id) ? "checked" : ""; ?>>
						</div>
						<!--<div class="img_title"><?php echo $item->title; ?></div>-->
					</div>

					<?php
				}
				?>
			</div>
			<div class="submit">
				<input type="submit" value="<?php echo JText::_('COM_POSTCARDS_NEXT'); ?>">
			</div>
		</form>
		<?php
	} else {
		?>
		<div class="nodata"><?php echo JText::_('COM_POSTCARDS_NODATA'); ?></div>

		<?php
	}
	?>


</div>
<script type="text/javascript">
	jQuery(document).ready(function() {

		jQuery(".fancybox").fancybox({'titlePosition'	: 'inside'});

	});
</script>
