<?php
/**
 * @version		: default.php 2012-10-16 21:06:39$
 * @author		EFATEK 
 * @package		gmap
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');


?>
<div class="com_postcards">
<?php
if ($this->items) {
	?>
		<form name="submitForm" method="post" action="<?php echo JRoute::_('index.php?option=com_postcards&view=form&Itemid='. (int) $itemid); ?>">
			<div class="title">
				<?php echo JText::_('COM_POSTCARDS_IMAGE'); ?>
			</div>
			<div class="img_blocks">
	<?php
	$check = $app->getUserState('form.postcards.image_id', '');
	foreach ($this->items as $i => $item) {
		if ($i == 0 && $check == 0) {
			$check = $item->id;
		}
		?>
					<div class="img_block">
						<div class="image">
							<img alt="<?php echo $item->title; ?>" src="<?php echo JURI::root() . $item->image; ?>" />
						</div>
						<div class="img_title">
							<input type="radio" id="image_id_<?php echo $i; ?>" name="image_id" value="<?php echo $item->id; ?>" <?php echo ($check == $item->id) ? "checked" : ""; ?>>
							<label for="image_id_<?php echo $i; ?>"><?php echo $item->title; ?></label>
							
							
						</div>
						
					</div>
						<?php
					}
					?>
			</div>
			<div class="submit">
				<input type="submit" value="<?php echo JText::_('COM_POSTCARDS_NEXT'); ?>">
			</div>
		</form>
	<?php
} else {
	?>
		<div class="nodata"><?php echo JText::_('COM_POSTCARDS_NODATA'); ?></div>

		<?php
	}
	?>


</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		
		

	});
</script>
