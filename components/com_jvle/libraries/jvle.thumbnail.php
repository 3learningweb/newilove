<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

class JvleThumbnail
{
	var $cfg;
	var $provider = 0;
	var $url = '';

	function __construct($cfg, $url)
	{
		$this->cfg = $cfg;
		$this->provider = $cfg->tp_enable;
		$this->url = $url;
	}

	function getThumbnail($pg)
	{
		if ($this->provider == 1)
		{
			switch ($pg)
			{
				case _JVLE_FEATUREDLINK_PAGE: $size = 'lg'; break;
				case _JVLE_GENCATEGORY_PAGE: $size = 'sm'; break;
				case _JVLE_LATLINKS_PAGE: $size = 'vsm'; break;
				case _JVLE_SEARCHRESULTS_PAGE: $size = 'tny'; break;
				default: $size = 'sm'; break;
			}

			return '{stwpreview url='.$this->url.' size='.$size.' class=thumb}';
		}
		else if ($this->provider == 2)
		{
			if ($this->cfg->thumboo_akey == '')
				return '';

			switch ($pg)
			{
				case _JVLE_FEATUREDLINK_PAGE: $size = 'm'; break;
				case _JVLE_GENCATEGORY_PAGE: $size = 's'; break;
				case _JVLE_LATLINKS_PAGE: $size = 't'; break;
				case _JVLE_SEARCHRESULTS_PAGE: $size = 't'; break;
				default: $size = 's'; break;
			}

			return '<div class="thumb">
						<img src="http://counter.goingup.com/thumbs.html?size='.$size.'&code='.$this->cfg->thumboo_akey.'&wait=&delay=&url='.$this->url.'" />
					</div>';
		}
		else
			return '';
	}
}