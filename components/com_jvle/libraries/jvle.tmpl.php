<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
* @package		JV-LinkExchanger
* @subpackage	com_jvle
* @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
* @license		GNU General Public License version 3 or later
* @author		JV-Extensions
* @link		http://www.jv-extensions.com
*/

defined('_JEXEC') or die('Restricted access');

class JvleTemplate 
{
	public $cfg;
	public $name;
	public $name_for_path;
	public $filename;

	private $properties = array();

	function __construct($filename="") 
	{
		$this->cfg = JvleCfg::getInstance();
		$this->name = $this->cfg->template_name;
		$this->name_for_path = JString::strtolower(JString::str_ireplace(" ", "", $this->name));
		if ($filename != "")
			$this->filename = $this->getTemplateAbsPath().DS."tmpl_".$filename.".php";
		else
			$this->filename = $filename;
	}

	function getName()
	{
		return $this->name;
	}

	function getTemplateAbsPath()
	{
		return JPATH_ROOT.DS."media".DS."com_jvle".DS."templates".DS.$this->name_for_path.DS."tmpl";
	}

	function getTemplateLivePath()
	{
		return _JVLE_LIVESITE."media/com_jvle/templates/".$this->name_for_path."/";
	}

	function getImageLivePath()
	{
		return $this->getTemplateLivePath()."/images/";
	}

	function setMetaData($pagetitle, $metakeys, $metadesc='') 
	{
		JHTML::_('behavior.tooltip');
		
		$pagetitle = htmlspecialchars($pagetitle);
		$metakeys = htmlspecialchars($metakeys);
		$metadesc = htmlspecialchars($metadesc);

		$document = JFactory::getDocument();
		$document->setTitle($pagetitle);
		$document->setMetaData('keywords', (is_array($metakeys)) ? implode(",",$metakeys) : $metakeys);
		$document->setMetaData('description', (($metadesc == '') ? $pagetitle : $metadesc));
		$document->addStyleSheet($this->getTemplateLivePath()."style.css");
	}

	function __get($property) 
	{
		return $this->properties[$property];
	}

	function __set($property, $value) 
	{
		$this->properties[$property] = $value;
	}

	function getContents() 
	{
		ob_start();
		require($this->filename);
		$contents = ob_get_contents();
		ob_end_clean();

		return $contents;
	}
}