<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
* @package		JV-LinkExchanger
* @subpackage	com_jvle
* @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
* @license		GNU General Public License version 3 or later
* @author		JV-Extensions
* @link			http://www.jv-extensions.com
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modelform');

abstract class JvleCustomModel extends JModelForm
{
	private $properties = array();
	private $resmsg = '';
	
	public $cfg;
	
	function __construct($config = array())
	{
		parent::__construct($config);
		
		$this->cfg = JvleCfg::getInstance();
	}
	
	function __get($property)
	{
		return $this->properties[$property];
	}
	
	function __set($property, $value)
	{
		$this->properties[$property] = $value;
	}
	
	function __isset($property)
	{
		if (isset($this->properties[$property]))
			return true;
	
		return false;
	}
	
	public function setResultMessage($msg)
	{
		$this->resmsg = $msg;
	}
	
	public function getResultMessage()
	{
		return $this->resmsg;
	}
	
	protected function preprocessForm(JForm $form, $data, $group = 'content')
	{
		$cfg = JvleCfg::getInstance();
		
		// Captcha preprocessing
		if (!$cfg->enable_captcha)
		{
			$form->removeField('captcha');
			return;
		}
	
		parent::preprocessForm($form, $data, $group);
	}
	
	public function preProcess($session_store=true)
	{
		$data  = JFactory::getApplication()->input->post->get('jform', array(), 'array');
		$form = $this->getForm($data, false);
	
		$validData = $this->validate($form, $data);
		if ($validData === false)
		{
			$msg = '';
			$errors = $this->getErrors();
			for ($i=0;$i<count($errors);$i++)
				$msg .= $errors[$i]->getMessage()."<br />";
	
			throw new Exception($msg);
		}
	
		foreach ($validData as $var=>$val)
			$this->{$var} = $val;
		
		if ($session_store == true)
		{
			JFactory::getSession()->clear('valid_data', 'addlink');						
			JFactory::getSession()->set('valid_data', $validData, 'addlink');
		}

		return;
	}
	
	public function addLinkBanner($exchange_link, $partner_url, $partner_email, $reciprocal_link_url, $link_category, $partner_title, $partner_desc, $banner_loc)
	{
		if (($partner_url == "") || ($partner_email == ""))
			throw new Exception(JText::_("COM_JVLE_INSUFF_INFO"));
		
		if (($exchange_link) && ($partner_title == ''))
			throw new Exception(JText::_("COM_JVLE_LINK_ADD_TITLE_MISSING"));
			
		if ($reciprocal_link_url == '')
			throw new Exception(JText::_("COM_JVLE_LINK_ADD_RECIPURL_MISSING"));
			
		if (!$link_category)
			throw new Exception(JText::_("COM_JVLE_LINK_ADD_CATEGORY_MISSING"));
			
		if (!JvleUtil::isNewLink($partner_url))
			throw new Exception(JText::_('COM_JVLE_LINK_ADD_DUPLICATE'));
			
		$catobj = JvleDb::getRow("select maxlinks, accept_links from #__jvle_categories where id = '".(int)$link_category."'");
		if (($catobj->maxlinks) && ((1 + JvleUtil::getNumlinks($link_category)) > $catobj->maxlinks))
			throw new Exception(JText::_('COM_JVLE_LINK_ADD_MAXREACHED'));
			
		if (!$catobj->accept_links)
			throw new Exception(JText::_("COM_JVLE_LINK_ADD_ACCEPT_NOLINKS"));
		
		if (JvleSiteUtil::isUrlHacked($partner_url, $reciprocal_link_url))
			throw new Exception(JText::_("COM_JVLE_LINK_ADD_URL_HACKED"));
			
		if (JvleSiteUtil::isLoopback($reciprocal_link_url))
			throw new Exception(JText::_("COM_JVLE_LINK_ADD_LOOPBACK"));
		
		if ($this->cfg->enable_rlc_fe)
		{
			$ret = JvleUtil::checkReciprocalLink($reciprocal_link_url, $this->cfg->self_url);
			if ($ret)
				throw new Exception(JText::_("COM_JVLE_LINK_ADD_BACKLINK_CHECK_FAILED"));

			$rlc_last_doneon = JFactory::getDate()->toSql();
		}
		else
			$rlc_last_doneon = '0000-00-00 00:00:00';
			
		$banner_file = '';
		if (!$exchange_link)
			$banner_file = JvleUtil::uploadFile($banner_loc, _JVLE_ABS_BANNERS, $this->cfg->banner_extns, $this->cfg->banner_width_max, $this->cfg->banner_height_max, $this->cfg->banner_kbsize_max, 1);
		
		$link_added_on = JFactory::getDate()->toSql();;
		if ($this->cfg->auto_approve)
		{
			$link_status = 'ESTABLISHED';
			$pubon = JFactory::getDate()->toSql();
		}
		else
		{
			$link_status = 'PENDING';
			$pubon = '0000-00-00 00:00:00';
		}
			
		$emstr = JvleSiteUtil::generateKey(32);
			
		$sql = "insert into #__jvle_links (link_type, partner_title, partner_email, partner_url, partner_desc, reciprocal_link_url, link_category, link_status, link_added_on, link_published_on, rlc_last_checked_on, emstr, enable_scheduler, exchange_link, banner_loc) values('TWO-WAY', '".JvleSecure::defendSQL($partner_title)."', '".JvleSecure::defendSQL($partner_email)."', '".JvleSecure::defendSQL($partner_url)."',  '".JvleSecure::defendSQL($partner_desc)."', '".JvleSecure::defendSQL($reciprocal_link_url)."', '".(int)$link_category."', '".$link_status."', '".JvleSecure::defendSQL($link_added_on)."', '".JvleSecure::defendSQL($pubon)."', '".JvleSecure::defendSQL($rlc_last_doneon)."', '".JvleSecure::defendSQL($emstr)."', '1' ,'".(int)$exchange_link."', '".JvleSecure::defendSQL($banner_file)."')";
		JvleDb::update($sql);
		
		$ploc = _JVLE_LIVESITE."index.php?option=com_jvle&Itemid="._JVLE_ITEMID."&view=all_category_links&cid=".(int)$link_category;
		if ($link_status == 'ESTABLISHED')
		{
			$em_msg = JText::sprintf("COM_JVLE_LINK_ADD_LINK_1_EM_BODY", $partner_url, $this->cfg->self_url, $ploc, $this->cfg->self_email, $this->cfg->self_url);
			JvleUtil::sendEmail($partner_email, JText::_("COM_JVLE_LINK_ADD_LINK_1_EM_SUBJ"), $em_msg);
				
			$result_msg = ($this->cfg->enable_rlc_fe) ? JText::_("COM_JVLE_LINK_ADD_LINK_MSG_1") : JText::_("COM_JVLE_LINK_ADD_LINK_MSG_2");
			$em_msg = JText::sprintf("COM_JVLE_LINK_ADD_LINK_2_EM_BODY", $this->cfg->self_url, $partner_email, $partner_url, JvleUtil::getCategoryName($link_category), $reciprocal_link_url, $ploc, $result_msg, $this->cfg->self_url);
			JvleUtil::sendEmail($this->cfg->self_email, JText::_("COM_JVLE_LINK_ADD_LINK_2_EM_SUBJ"), $em_msg);
			
			$retmsg = ($exchange_link) ? JText::_("COM_JVLE_LINK_ADD_DONE") : JText::_('COM_JVLE_LINK_ADD_BANNER_DONE');
		}
		else
		{
			$approve_msg = $reject_msg = '';
			if ($this->cfg->email_approval)
			{
				$approval_url = _JVLE_LIVESITE."index.php?option=com_jvle&Itemid="._JVLE_ITEMID."&view=emappr&emstr=".$emstr."&act=1";
				$approve_msg = JText::sprintf('COM_JVLE_LINK_ADD_LINK_MSG_3', $approval_url);
					
				$reject_url = _JVLE_LIVESITE."index.php?option=com_jvle&Itemid="._JVLE_ITEMID."&view=emappr&emstr=".$emstr."&act=0";
				$reject_msg = JText::sprintf('COM_JVLE_LINK_ADD_LINK_MSG_4', $reject_url);
			}
				
			$result_msg = ($this->cfg->enable_rlc_fe) ? JText::_("COM_JVLE_LINK_ADD_LINK_MSG_1") : JText::_("COM_JVLE_LINK_ADD_LINK_MSG_2");
			$em_msg = JText::sprintf("COM_JVLE_LINK_ADD_LINK_3_EM_BODY", $this->cfg->self_url, $approve_msg, $reject_msg, $partner_email, $partner_url, JvleUtil::getCategoryName($link_category), $reciprocal_link_url, $ploc, $result_msg, $this->cfg->self_url);
			JvleUtil::sendEmail($this->cfg->self_email, JText::_("COM_JVLE_LINK_ADD_LINK_3_EM_SUBJ"), $em_msg);
			
			$retmsg = ($exchange_link) ? JText::_("COM_JVLE_LINK_ADD_DONE_PENDING") : JText::_('COM_JVLE_LINK_ADD_BANNER_PENDING');
		}

		return $retmsg;
	}
}