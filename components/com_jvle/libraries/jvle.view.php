<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
* @package		JV-LinkExchanger
* @subpackage	com_jvle
* @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
* @license		GNU General Public License version 3 or later
* @author		JV-Extensions
* @link		http://www.jv-extensions.com
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleCustomView extends JViewLegacy
{
	public $dispmsg;
	public $error;
	public $cfg;
	
	public function __construct($config = array())
	{
		parent::__construct($config);
		
		$this->dispmsg = '';
		$this->error = 0;
		$this->cfg = JvleCfg::getInstance();
	}
		
	public function displayTemplate($tpl, $pagetitle, $metakeys, $metadesc, $maintmpl, $subtmpl)
	{
		$tmpl = new JvleTemplate();
		$tmpl->setMetaData($pagetitle, $metakeys, $metadesc);
		
		$this->addTemplatePath($tmpl->getTemplateAbsPath());
		$this->setLayout($maintmpl);
		$this->subtemplate = $subtmpl;
		
		if ($this->cfg->load_bootstrap_css)
		    JHtml::_('bootstrap.loadCss', true);
		
		if ($this->cfg->load_bootstrap_js)
		    JHtml::_('bootstrap.framework');
		
		parent::display($tpl);
	}
}