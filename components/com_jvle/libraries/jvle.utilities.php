<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

abstract class JvleSiteUtil
{
    public static function getPartnerLinkURL($link)
    {
    	$cfg = JvleCfg::getInstance();

    	if (($link->link_type == "ONE-WAY") && ($cfg->track_hits))
    		return JRoute::_("index.php?option=com_jvle&Itemid="._JVLE_ITEMID."&task=redirect&id=".(int)$link->id);

    	return $link->partner_url;
    }

	public static function isUrlHacked($purl, $rurl)
	{
		$purl = JString::rtrim($purl, "/");
		$rurl = JString::rtrim($rurl, "/");
	
		$match = JString::substr($rurl, 0, JString::strlen($purl));
		return ($match == $purl) ? 0 : 1;
	}
	
	public static function isLoopback($rurl)
	{
		$match = JString::substr(JString::rtrim($rurl, "/")."/", 0, JString::strlen(_JVLE_LIVESITE));
		return ($match == _JVLE_LIVESITE) ? 1 : 0;
	}
	
	public static function generateKey($plength)
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		mt_srand(microtime() * 1000000);
		for($pwd='',$i= 0;$i<$plength;$i++)
		{
			$key = mt_rand(0,JString::strlen($chars)-1);
			$pwd = $pwd . $chars{$key};
		}
		for($i = 0; $i < $plength; $i++)
		{
			$key1 = mt_rand(0,JString::strlen($pwd)-1);
			$key2 = mt_rand(0,JString::strlen($pwd)-1);
	
			$tmp = $pwd{$key1};
			$pwd{$key1} = $pwd{$key2};
			$pwd{$key2} = $tmp;
		}
		$pwd = htmlentities($pwd, ENT_QUOTES);
		return $pwd;
	}
	
	public static function drawLinkInfo($link, $pg)
	{
		$cfg = JvleCfg::getInstance();
	
		$t = new JvleTemplate("link");
		$t->link = $link;
		$t->window = JvleUtil::getTargetString();
		$t->cfg = $cfg;
	
		$rating = JvleSiteUtil::drawRatingInfo($link);
		$t->rating = $rating;
	
		$thumb = new JvleThumbnail($cfg, $link->partner_url); 
		$t->thumbnail = $thumb->getThumbnail($pg);
	
		return $t->getContents();
	}

	public static function drawRatingInfo($link)
	{
		$cfg = JvleCfg::getInstance();
		if (!$cfg->enable_link_rating)
			return '';
	
		JHTML::_('behavior.framework');
		
		$document = JFactory::getDocument();		
		$document->addScript(_JVLE_LIVESITE."components/com_jvle/assets/js/moostarrating.js");
				
		$randnum = $link->id.rand(0,9999);
		$voteAjaxQuery = '
                    /* <![CDATA[ */
						var MooStarRatingImages = {
							defaultImageFolder: "'.JUri::root().'components/com_jvle/assets/images",
							defaultImageEmpty:  "star_empty.png",
							defaultImageFull:   "star_full.png",
							defaultImageHover:  "star_hover.png"
						};
										
						window.addEvent("domready",function() {	
							var starRater'.$randnum.' = new MooStarRating({
								form: "rform'.$randnum.'",
								radios: "rating'.$randnum.'",
								half: true,
								width: 17 
							});
		
							var url = "'._JVLE_LIVESITE.'index.php?option=com_jvle";
							starRater'.$randnum.'.addEvent("click",function(value) {
								var req = new Request({
			            			url: url,
			            			method: "post",		
		            				onSuccess: function(response) {
		            					alert(response);
		            				}
		            			}).send("view=ajax&layout=rate&lid='.$link->id.'&rating="+value+"&format=raw");
							});
						});
            		/* ]]> */';
	
		$document->addScriptDeclaration($voteAjaxQuery);
	
		$t = new JvleTemplate("rating");
		$t->randnum = $randnum;
		$t->link = $link;
		return $t->getContents();
	}
	
    public static function getFeaturedLinks()
    {
    	$cfg = JvleCfg::getInstance();

    	$sql = "select * from #__jvle_links where featured = '1' and link_status = 'ESTABLISHED' order by ";
    	$sql .= ($cfg->flinks_dtype == 'recent') ? " link_published_on " : " RAND() ";
    	$sql .= " desc limit 0, ".$cfg->flinks_numshow;

    	return JvleDb::getRows($sql);
    }

    public static function getLatestLinks()
    {
    	return JvleDb::getRows("select * from #__jvle_links where link_status = 'ESTABLISHED' order by link_published_on desc limit 0, 5");
    }
    
    public static function drawCategoryBlock($maincat, $subcats)
    {
    	$out = '';
    	$out .= JvleSiteUtil::drawCategoryInfo($maincat, 0).'<br />';
    	if (count($subcats))
    	{
    		$subcat_info = '';
    		foreach ($subcats as $subcat)
    			$subcat_info .= JvleSiteUtil::drawCategoryInfo($subcat, 1).", ";
    
    		$subcat_info = JString::substr($subcat_info, 0, JString::strlen($subcat_info)-2);
    		$out .= $subcat_info.'<br />';
    	}
    
    	return $out;
    }
    
    public static function drawCategoryInfo($category, $level)
    {
    	$out = '';
    
    	$classname = ($level) ? "jvle_subcatrow" : "jvle_maincatrow";
    	$display_name = JvleUtil::getCategoryName($category->id);
    	$catdesc = ($category->description != '') ? stripslashes($category->description) : $display_name;
    	$numlinks = JvleUtil::getNumlinks($category->id, 0);
    	$pagelnk = JRoute::_("index.php?option=com_jvle&Itemid="._JVLE_ITEMID."&view=links&cid=".(int)$category->id);
    
    	if (!$category->accept_links && !$numlinks)
    		$out .= '<font class="'.$classname.'"><span class="tip hasTooltip" title="'.htmlspecialchars($catdesc).'">'.$display_name.'</span></font>';
    	else
    		$out .= '<font class="'.$classname.'"><span class="tip hasTooltip" title="'.htmlspecialchars($catdesc).'"><a href="'.$pagelnk.'">'.$display_name.'</a></span></font>';
    
    	if ($numlinks)
    		$out .= '&nbsp;('.$numlinks.')';
    
    	return $out;
    }
}