<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'common'.DS.'jvle.resources.php');

define("_JVLE_ITEMID", JvleBase::getItemid());

require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'common'.DS.'jvle.utilities.php');

define("_JVLE_ABSPATH", JPATH_COMPONENT_SITE);
define("_JVLE_LIVESITE", JvleUtil::getLiveSiteURL());
define("_JVLE_IMGDIR", _JVLE_LIVESITE."components/com_jvle/assets/images/");
define("_JVLE_ABS_BANNERS", JPATH_SITE.DS.'images'.DS.'com_jvle'.DS.'banners');
define("_JVLE_BANNERS", _JVLE_LIVESITE.'images/com_jvle/banners/');

require_once(JPATH_COMPONENT_SITE.DS.'libraries'.DS.'jvle.utilities.php');
require_once(JPATH_COMPONENT_SITE.DS.'libraries'.DS.'jvle.model.php');
require_once(JPATH_COMPONENT_SITE.DS.'libraries'.DS.'jvle.view.php');
require_once(JPATH_COMPONENT_SITE.DS.'libraries'.DS.'jvle.tmpl.php');
require_once(JPATH_COMPONENT_SITE.DS.'libraries'.DS.'jvle.thumbnail.php');

jimport('joomla.application.component.controller');

$controller = JControllerLegacy::getInstance('Jvle');

$controller->execute(JFactory::getApplication()->input->getCmd('task'));
$controller->redirect();