<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

define("_JVLE_SEF_ALL_LOWERCASE", 1); 

function jvleGetVal(&$query, $var) 
{
    if (isset($query[$var]))
    {
        $val = $query[$var];
        unset($query[$var]);
        return $val;
    }

    return '';
}

function jvlePutVal($segments, $mypos) 
{
    $varval = '';
    if (isset($segments[$mypos]) && $segments[$mypos] != '')
        $varval = JString::str_ireplace(':', '-', $segments[$mypos]);

    return $varval;
}

function getSubStringTill($string, $tillchar) {
    $myarr = explode($tillchar, $string);
    return $myarr[count($myarr)-1];
}

function jvleFormQuery(&$query, $var, $val) {
    $query[$var] = $val;
}

function JvleBuildRoute(&$query) 
{
	$app = JFactory::getApplication();
	$menu = $app->getMenu();
	if (empty($query['Itemid']))
		$menuItem = $menu->getActive();
	else
		$menuItem = $menu->getItem($query['Itemid']);
	
    $segments = array();
    $database = JFactory::getDBO();

    $controller = jvleGetVal($query, 'controller');
    $task = jvleGetVal($query, 'task');
    $view = jvleGetVal($query, 'view');

    switch ($view) 
    {
        case 'emappr': 
        {
            $segments[] = 'email-action';
            $segments[] = (jvleGetVal($query, 'act')) ? 'approve' : 'reject';
            break;
        }
        case 'recommend': 
        {
            $segments[] = 'recommend-website';
            break;
        }
        case 'checklinkstatus': 
        {
            $segments[] = 'check-link-status';
            break;
        }
        case 'addfeatured': 
        {
            $segments[] = 'add-featured-website';
            break;
        }
        case 'addbanner': 
        {
            $segments[] = 'add-website-banner';
            break;
        }
        case 'addlink': 
        {
            $segments[] = 'add-website-link';
            break;
        }
        case 'search': 
        {
            $segments[] = 'search';
            break;
        }
        case 'all_category_links': 
        {
            $cid = jvleGetVal($query, 'cid');
            $database->setQuery("select id, name from #__jvle_categories where id = '".(int)$cid."'");
            $row = $database->loadObject();
            $cname = JString::trim(htmlspecialchars($row->name));

            $segments[] = JString::str_ireplace(" ", "-", $cname)."-".(int)$cid;
            $segments[] = "index";
            break;
        }
        case 'links': {
            $cid = jvleGetVal($query, 'cid');
            $database->setQuery("select id, name from #__jvle_categories where id = '".(int)$cid."'");
            $row = $database->loadObject();
            $cname = JString::trim(htmlspecialchars($row->name));

            $segments[] = JString::str_ireplace(" ", "-", $cname)."-".(int)$cid;
            break;
        }
        default: 
        {
        	if ($task == 'redirect') 
        	{
	        	$segments[] = 'visit';
	        	$segments[] = jvleGetVal($query, 'id');
        	}
        	else
            	$segments[] = "index";

        	break;
        }
    }

    if (_JVLE_SEF_ALL_LOWERCASE) 
    {
        for ($i=0;$i<count($segments);$i++)
            $segments[$i] = JString::strtolower($segments[$i]);
    }

    return $segments;
}

function JvleParseRoute($segments) 
{
    $database = JFactory::getDBO();
    $query = array();

    $arg_0 = jvlePutVal($segments, 0);
    $arg_1 = jvlePutVal($segments, 1);
    $arg_2 = jvlePutVal($segments, 2);

    if ($arg_0 == "index") 
    {
    }
    else if ($arg_0 == 'email-action') 
    {
        jvleFormQuery($query, 'view', 'emappr');
        jvleFormQuery($query, 'act', ($arg_1 == "approve") ? 1 : 0);
    }
    else if ($arg_0 == 'recommend-website') 
    {
        jvleFormQuery($query, 'view', 'recommend');
    }
    else if ($arg_0 == 'check-link-status') 
    {
        jvleFormQuery($query, 'view', 'checklinkstatus');
    }
    else if ($arg_0 == 'rate-website') 
    {
        jvleFormQuery($query, 'task', 'rate');
        jvleFormQuery($query, 'format', 'raw');
        jvleFormQuery($query, 'lid', (int)$arg_1);
    }
    else if ($arg_0 == 'visit') 
    {
        jvleFormQuery($query, 'task', 'redirect');
        jvleFormQuery($query, 'id', (int)$arg_1);
    }
    else if ($arg_0 == 'add-featured-website') 
    {
        jvleFormQuery($query, 'view', 'addfeatured');
    }
    else if ($arg_0 == 'add-website-link') 
    {
        jvleFormQuery($query, 'view', 'addlink');
    }
    else if ($arg_0 == 'add-website-banner') 
    {
        jvleFormQuery($query, 'view', 'addbanner');
    }
    else if ($arg_0 == 'search') 
    {
        jvleFormQuery($query, 'view', 'search');
    }
    else 
    {
        if ($arg_1 == "index") 
        {
            $cid = getSubStringTill($arg_0, '-');
            jvleFormQuery($query, 'view', 'all_category_links');
            jvleFormQuery($query, 'cid', (int)$cid);
        }
        else 
        {
            $cid = getSubStringTill($arg_0, '-'); 
            jvleFormQuery($query, 'view', 'links');
            jvleFormQuery($query, 'cid', (int)$cid);
        }
    }

    return $query;
}