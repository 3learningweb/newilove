<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewAjax extends JViewLegacy
{
    function display($tpl = null)
    {
    	$layout = JFactory::getApplication()->input->getString('layout', '');
    	switch ($layout)
    	{
    		case 'rate': $this->rateLink($tpl); break;
    		default: echo ''; return;
    	}

    	JFactory::getApplication()->close();
    }
    
    private function rateLink($tpl)
    {
    	$user = JFactory::getUser();    	
    	$cfg = JvleCfg::getInstance();
    	    	
    	if (!$cfg->enable_link_rating)
    	{
    		echo JText::_("COM_JVLE_INVALID_REQ");
    		return;
    	}
    	
    	$allow_rating = 0;
    	if ($cfg->link_rating_by == "all")
    	{
    		$allow_rating = 1;
    	}
    	else
    	{
    		if (($cfg->link_rating_by == "registered") && ($user->get('id')))
    			$allow_rating = 1;
    	}
    	
    	if (!$allow_rating)
    	{
    		echo JText::_("COM_JVLE_RATING_LOGIN");
    		return;    		
    	}
    	 
   		$lid = JFactory::getApplication()->input->getInt('lid', -1);
   		$rating = JFactory::getApplication()->input->getFloat('rating', 0);
    	
   		if (!$rating)
   		{
   			echo JText::_("COM_JVLE_LINKS_RATING_MSG_1");
   			return;
   		}
   		else
   		{
   			$u_ip = $_SERVER['REMOTE_ADDR'];
   			$mrv = JvleDb::getRow("select * from #__jvle_votes where lid = '".(int)$lid."' and vote_ip = '".JvleSecure::defendSQL($u_ip)."'");
    			
   			$allow_vote = 0;
   			if ($mrv)
   			{
   				if (time() < ((24*3600) + $mrv->vote_time))
   				{
   					echo JText::_("COM_JVLE_LINKS_RATING_ALREADY_VOTED");
   					return;
   				}    					
   				else
   					$allow_vote = 1;
   			}
   			else
   				$allow_vote = 2;
    			
   			if ($allow_vote)
   			{
   				$mr = JvleDb::getRow("select num_votes, link_rating from #__jvle_links where id = '".(int)$lid."'");
   				$nrating = (($mr->link_rating * $mr->num_votes) + $rating)/($mr->num_votes + 1);
   				JvleDb::update("update #__jvle_links set num_votes = num_votes + 1, link_rating = ".(float)$nrating." where id = ".(int)$lid);
   			
   				if ($allow_vote == 2)
   					JvleDb::update("insert into #__jvle_votes (lid, vote_ip, vote_time) values('".(int)$lid."', '".JvleSecure::defendSQL($u_ip)."', '".time()."')");
   				else
   					JvleDb::update("update #__jvle_votes set vote_time = '".time()."' where id = '".(int)$mrv->id."'");
   			
   				echo JText::_("COM_JVLE_LINKS_RATING_DONE");
   				return;
   			}
   		}

    	echo '';
    	return;
    }
}