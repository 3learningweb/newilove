<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewAddfeatured extends JvleCustomView
{
	function display($tpl = null)
	{		
		$this->cfg = JvleCfg::getInstance();
		if (!$this->cfg->enable_flinks || !$this->cfg->show_flinks)
		{
			JError::raiseWarning(500, JText::_("COM_JVLE_INVALID_REQ"));
			return;
		}
		
		$item_name = JText::sprintf('COM_JVLE_LINK_ADD_FEATURED_MSG_1', $this->cfg->self_url);
		$this->pay_url = "https://www.paypal.com/cgi-bin/webscr?cmd=_xclick-subscriptions&business=".urlencode($this->cfg->flinks_subemail)."&lc=US&item_name=".urlencode($item_name)."&no_note=1&no_shipping=1&a3=".urlencode($this->cfg->flinks_subamount)."&currency_code=USD&src=1&p3=1&t3=M&sra=1&bn=PP%2dSubscriptionsBF%3abtn_subscribeCC_LG%2egif%3aNonHosted";
		
		$this->displayTemplate($tpl, JText::_("COM_JVLE_LINK_ADD_FEATURED"), JText::_("COM_JVLE_LINK_ADD_FEATURED_KEYWORDS"), JText::_('COM_JVLE_LINK_ADD_FEATURED_METADESC'), 'tmpl', 'addfeatured');
	}
}