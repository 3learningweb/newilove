<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewAddbanner extends JvleCustomView
{	
	protected $form;
	
	function display($tpl = null)
	{
		$this->cfg = JvleCfg::getInstance();		
		if (!$this->cfg->fe_addbanner)
		{
			JError::raiseWarning(500, JText::_("COM_JVLE_INVALID_REQ"));
			return;
		}
		
		$this->info = '<a href="'.$this->cfg->self_url.'" title="'.htmlspecialchars($this->cfg->self_title).'" target="_blank"><img src="'.JUri::root().$this->cfg->banner_loc.'" border="0" alt="'.htmlspecialchars($this->cfg->self_url).'" /></a>';
		$this->form = $this->get('Form');
		
		if (JFactory::getApplication()->input->getInt('mode', 0) == 1)
		{
			$model = $this->getModel();
			$this->error = $model->process(0);
			$this->dispmsg = $model->getResultMessage();
			
			if ($this->error)
			{
				$data_from_session = JFactory::getSession()->get('valid_data', array(), 'addlink');
				$this->form->bind($data_from_session);				
			}
		}
	
		$this->displayTemplate($tpl, JText::_("COM_JVLE_LINK_ADD_BANNER"), JText::_("COM_JVLE_LINK_ADD_BANNER_KEYWORDS"), '', 'tmpl', 'addbanner');
	}
}