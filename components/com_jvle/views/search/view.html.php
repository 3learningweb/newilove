<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewSearch extends JvleCustomView
{
	function display($tpl = null)
	{		
		$this->cfg = JvleCfg::getInstance();
		if (!$this->cfg->enable_search)
		{
			JError::raiseWarning(500, JText::_("COM_JVLE_INVALID_REQ"));
			return;
		}
		
		try
		{
			$this->query = JFactory::getApplication()->input->getString('query', '');
			if ($this->query == '')
				throw new Exception(JText::_('COM_JVLE_SEARCHR_QUERY_NONE'));
			
			if (JString::strlen($this->query) < 4)
				throw new Exception(JText::_('COM_JVLE_SEARCHR_QUERY_SMALL'));
			
			$this->rows = $this->get('Items');
			$this->pagination = $this->get('Pagination');
						
			$this->error = 0;
			$this->dispmsg = '';				
		}			
		catch (Exception $ex)
		{				
			$this->error = 1;
			$this->dispmsg = $ex->getMessage();
		}				
		
		$this->displayTemplate($tpl, JText::_("COM_JVLE_SEARCH"), JText::_("COM_JVLE_SEARCHR_KEYWORDS"), '', 'tmpl', 'searchresults');
	}
}