<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewJvle extends JvleCustomView
{
	protected $form;
	
	function display($tpl = null)
	{		
		$this->cfg = JvleCfg::getInstance();

		$this->pageview = 'home';
		$this->displayTemplate($tpl, JText::_("COM_JVLE_HOME_TITLE"), JText::_("COM_JVLE_HOME_KEYWORDS"), JText::_("COM_JVLE_HOME_METADESC"), 'tmpl', 'home');
	}
}