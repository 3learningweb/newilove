<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewEmappr extends JvleCustomView
{
	function display($tpl = null)
	{		
		$this->cfg = JvleCfg::getInstance();
		if (!$this->cfg->email_approval)
		{
			JError::raiseWarning(500, JText::_("COM_JVLE_INVALID_REQ"));
			return;
		}
		
		try
		{
			$act = JFactory::getApplication()->input->getInt('act', -1);
			$emstr = JFactory::getApplication()->input->getString('emstr', '');
			
			if (($act == -1) || ($emstr == ''))
				throw new Exception(JText::_("COM_JVLE_INVALID_REQ"));
				
			$link = JvleDb::getRow("select * from #__jvle_links where emstr = '".JvleSecure::defendSQL($emstr)."'");
			if (!$link)
				throw new Exception(JText::_("COM_JVLE_INVALID_REQ"));
				
			if ($act)
			{
				if ($link->link_status == 'ESTABLISHED')
					throw new Exception(JText::_("COM_JVLE_LINK_EMAPPR_MSG_1"));
				
				if ($link->link_status != 'PENDING')
					throw new Exception(JText::_("COM_JVLE_LINK_EMAPPR_MSG_2"));
				
				$tnow = JFactory::getDate()->toSql();
				JvleDb::update("update #__jvle_links set link_status = 'ESTABLISHED', link_added_on = '".$tnow."', link_published_on = '".$tnow."' where emstr = '".JvleSecure::defendSQL($emstr)."'");
				
				$ploc = _JVLE_LIVESITE."index.php?option=com_jvle&Itemid="._JVLE_ITEMID."&view=all_category_links&cid=".$link->link_category;				
				$em_msg = JText::sprintf("COM_JVLE_LINK_ADD_LINK_1_EM_BODY", $link->partner_url, $this->cfg->self_url, $ploc, $this->cfg->self_email, $this->cfg->self_url);
				JvleUtil::sendEmail($link->partner_email, JText::_("COM_JVLE_LINK_ADD_LINK_1_EM_SUBJ"), $em_msg);
				
				$this->dispmsg1 = JText::_("COM_JVLE_LINK_EMAPPR_DONE_1");				
			}
			else
			{
				if ($link->link_status == 'REJECTED')
					throw new Exception(JText::_("COM_JVLE_LINK_EMAPPR_MSG_3"));
				
				if ($link->link_status != 'PENDING')
					throw new Exception(JText::_("COM_JVLE_LINK_EMAPPR_MSG_2"));
				
				JvleDb::update("update #__jvle_links set link_status = 'REJECTED' where emstr = '".JvleSecure::defendSQL($emstr)."'");
				
				$em_msg = JText::sprintf("COM_JVLE_LINK_EMAPPR_1_EM_BODY", $link->partner_url, $this->cfg->self_url, $this->cfg->generic_reject_reason, $this->cfg->self_email, $this->cfg->self_url);
				JvleUtil::sendEmail($link->partner_email, JText::_("COM_JVLE_LINK_EMAPPR_1_EM_SUBJ"), $em_msg);
				
				$this->dispmsg1 = JText::_("COM_JVLE_LINK_EMAPPR_DONE_2");				
			}
		}
		catch (Exception $ex)
		{
			$this->dispmsg1 = $ex->getMessage();
		}
		
		$this->displayTemplate($tpl, JText::_("COM_JVLE_LINK_EMAPPR_META"), JText::_("COM_JVLE_LINK_EMAPPR_META"), JText::_('COM_JVLE_LINK_EMAPPR_META'), 'tmpl', 'emappr');
	}
}