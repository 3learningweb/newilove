<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewAll_category_links extends JvleCustomView
{
	protected $rows;
	protected $state;
	
	function display($tpl = null)
	{		
		$this->cfg = JvleCfg::getInstance();
		
		$this->cid = JFactory::getApplication()->input->getInt('cid', 0);
		if (!$this->cid)
		{
			JError::raiseWarning(500, JText::_("COM_JVLE_INVALID_REQ"));
			return;
		}
		
		$cobj = JvleDb::getRow("select name, description from #__jvle_categories where id = '".(int)$this->cid."'");
		$keywords = JText::sprintf('COM_JVLE_LINKS_ALL_KEYWORDS', htmlspecialchars($cobj->name));
				
		$this->rows = $this->get('Items');		
		$this->state = $this->get('State');
		
		$this->sortDirection = $this->state->get('list.direction');
		$this->sortColumn = $this->state->get('list.ordering');
		
		$this->pageview = 'all_category_links';
		$this->displayTemplate($tpl, $cobj->name, $keywords, $cobj->description, 'tmpl', 'all_category_links');
	}
}