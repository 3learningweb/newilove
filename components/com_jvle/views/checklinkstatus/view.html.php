<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewChecklinkstatus extends JvleCustomView
{
	protected $form;
	
	function display($tpl = null)
	{		
		$this->cfg = JvleCfg::getInstance();
		if (!$this->cfg->enable_checklinkstatus)
		{
			JError::raiseWarning(500, JText::_("COM_JVLE_INVALID_REQ"));
			return;
		}
		
		if (JFactory::getApplication()->input->getInt('mode', 0) == 1)
		{
			$model = $this->getModel();
			$this->error = $model->process();
			$this->dispmsg = $model->getResultMessage();
		}
		
		$this->form = $this->get('Form');		
		$this->displayTemplate($tpl, JText::_("COM_JVLE_CLS_TITLE"), JText::_("COM_JVLE_CLS_KEYWORDS"), '', 'tmpl', 'checklinkstatus');
	}
}