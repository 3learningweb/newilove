<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

class JvleModelAddlink extends JvleCustomModel
{
	public function getForm($data = array(), $loadData = false)
	{
		$form = $this->loadForm('com_jvle.addlink', 'addlink', array('control' => 'jform', 'load_data' => $loadData), true);
		if (empty($form))
			return false;

		return $form;
	}
	
	public function process()
	{		
		try
		{
			parent::preProcess();
			
			$retmsg = $this->addLinkBanner(1, $this->partner_url, $this->partner_email, $this->reciprocal_link_url, $this->link_category, $this->partner_title, $this->partner_desc, '');				
			$this->setResultMessage($retmsg);
			
			return 0;						
		}
		catch (Exception $ex)
		{
			$this->setResultMessage($ex->getMessage());
			return 1;
		}
	}
}