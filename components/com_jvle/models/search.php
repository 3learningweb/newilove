<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

class JvleModelSearch extends JModelList
{
	protected function getListQuery()
	{
		$cfg = JvleCfg::getInstance();
		$query = JFactory::getApplication()->input->getString('query', '');
		
		$sql = "select * from #__jvle_links where link_status = 'ESTABLISHED' and (partner_desc like '%".JvleSecure::defendSQL($query, 1)."%' or partner_title like '%".JvleSecure::defendSQL($query, 1)."%' or partner_url like '%".JvleSecure::defendSQL($query, 1)."%') order by link_published_on desc";
		return $sql;
	}
	
	public function getStart()
	{
		return $this->getState('list.start');
	}

	public function getLimit()
	{
		return $this->getState('list.limit');
	}

	protected function getStoreId($id = '')
	{
		return parent::getStoreId($id);
	}

	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		$start_value = $app->input->get('limitstart', 0, 'int');
		$this->setState('list.start', $start_value);

		$limit_value = ($start_value < 0) ? 0 : $app->input->get('limit', $app->getCfg('list_limit', 0), 'uint');
		$this->setState('list.limit', $limit_value);
	}
}