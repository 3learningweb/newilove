<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

class JvleModelRecommend extends JvleCustomModel
{
	public function getForm($data = array(), $loadData = false)
	{
		$form = $this->loadForm('com_jvle.recommend', 'recommend', array('control' => 'jform', 'load_data' => $loadData), true);
		if (empty($form))
			return false;

		return $form;
	}
	
	public function process()
	{		
		try
		{
			parent::preProcess();
			
			if (($this->url == "") || ($this->reason == "") || ($this->name == "") || ($this->email == ""))
				throw new Exception(JText::_("COM_JVLE_INSUFF_INFO"));
				
			if ((JString::substr($this->url, 0, 7) != 'http://') && (JString::substr($this->url, 0, 8) != 'https://'))
				throw new Exception(JText::_("COM_JVLE_URL_START_ERR"));

			$em_msg = JText::sprintf("COM_JVLE_RECSITE_EM_BODY", htmlspecialchars($this->name), htmlspecialchars($this->email), htmlspecialchars($this->url), htmlspecialchars($this->reason), $this->cfg->self_url);
			JvleUtil::sendEmail($this->cfg->self_email, JText::_("COM_JVLE_RECSITE_EM_SUBJECT"), $em_msg);
				
			$this->setResultMessage(JText::_("COM_JVLE_RECSITE_DONE"));
			return 0;						
		}
		catch (Exception $ex)
		{
			$this->setResultMessage($ex->getMessage());
			return 1;
		}
	}
}