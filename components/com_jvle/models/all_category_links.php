<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

class JvleModelAll_category_links extends JModelList
{
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
					'partner_title', 
					'link_published_on',
					'num_votes', 
					'link_rating'
			);
		}
	
		parent::__construct($config);
	}
		
	protected function getListQuery()
	{
		$db = JFactory::getDbo();
		$cfg = JvleCfg::getInstance();
		$input = JFactory::getApplication()->input;
		
		$cid = $input->getInt('cid', 0);
		
		$orderby = $db->escape($this->getState('list.ordering', 'link_published_on'));
		$orderon = $db->escape($this->getState('list.direction', 'DESC'));
		
		$sql = "select * from #__jvle_links where link_status != 'REJECTED' and link_category = ".(int)$cid." order by ".$orderby." ".$orderon;
		return $sql;
	}
	
	public function getStart()
	{
		return $this->getState('list.start');
	}

	public function getLimit()
	{
		return $this->getState('list.limit');
	}

	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		$this->setState('list.start', 0);
		$this->setState('list.limit', 0);
		
		$orderCol = $app->input->get('filter_order', 'link_published_on');		
		if (!in_array($orderCol, $this->filter_fields))
			$orderCol = 'link_published_on';
		
		$this->setState('list.ordering', $orderCol);
		
		$listOrder = $app->input->get('filter_order_Dir', 'desc');		
		if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', '')))
			$listOrder = 'DESC';
		
		$this->setState('list.direction', $listOrder);		
	}
}