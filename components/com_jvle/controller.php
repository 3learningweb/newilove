<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class JvleController extends JControllerLegacy
{
    function display($cachable = false, $urlparams = false)
    {
        parent::display($cachable, $urlparams);
        return $this;
    }
    
    function redirect()
    {    	
    	$id = JFactory::getApplication()->input->getInt('id', 0);
    	if (!$id)
    		return;
    
    	$row = JvleDb::getRow("select link_type, partner_url, hits from #__jvle_links where id = '".(int)$id."'");
    	if ((!$row) || ($row->link_type != "ONE-WAY"))
    		return;
    
    	$hits = $row->hits + 1;
    	JvleDb::update("update #__jvle_links set hits = '".(int)$hits."' where id = '".(int)$id."'");
    
    	header("Location: ".$row->partner_url);
    }
    
    function cron()
    {
    	$cfg = JvleCfg::getInstance();
    	if (!$cfg->enable_scheduler)
    		return;
    	
    	$DELAY_TIME = 60000;
    	$start_time = JFactory::getDate()->toSql();
    	
    	$out_url = $out_email = $out_cstatus = $out_nstatus = $out_msg = array();
    	$links = JvleDb::getRows("select * from #__jvle_links where link_type = 'TWO-WAY' and enable_scheduler = '1' and reciprocal_link_url != ''");
    	if (!count($links))
    		return;
    	
    	foreach ($links as $link)
    	{
    		array_push($out_url, $link->partner_url);
    		array_push($out_email, $link->partner_email);
    		array_push($out_cstatus, $link->link_status);
    	
    		$ret = JvleUtil::checkReciprocalLink($link->reciprocal_link_url, $cfg->self_url);
    		switch ($ret)
    		{
    			case 0: { $msg = JText::_('COM_JVLE_CRON_RESULT_OK'); $new_status = 'ESTABLISHED'; break; }
    			case 1: { $msg = JText::_('COM_JVLE_CRON_RESULT_RERR'); $new_status = $link->link_status; break; }
    			case 2: { $msg = JText::_('COM_JVLE_CRON_RESULT_RELNF'); $new_status = 'PENDING'; break; }
    			case 3: { $msg = JText::_('COM_JVLE_CRON_RESULT_FAIL'); $new_status = 'PENDING'; break; }
    			default: { $msg = JText::_('COM_JVLE_CRON_RESULT_UNKNOWN'); $new_status = $link->link_status; break; }
    		}
    	
    		array_push($out_msg, $msg);
    	
    		if ($cfg->rlc_status_update)
    		{
    			JvleUtil::changeLinkStatus($link->id, $new_status);
    			array_push($out_nstatus, $new_status);
    		}
    		else
    		{
    			array_push($out_nstatus, $link->link_status);
    		}
    	
    		JvleUtil::updateLastRecipDate($link->id);
    		usleep($DELAY_TIME);
    	}
    	
    	$end_time = JFactory::getDate()->toSql();
    	
    	if (!count($out_url))
    		return;
    	
    	$html_msg = "<html><head></head><body>";
    	$html_msg .= "<strong>JV-LinkExchanger</strong>: ".JText::_('COM_JVLE_CRON_MSG_1')."<br /><br />".JText::sprintf('COM_JVLE_CRON_STIME', $start_time)."<br /><br />";
    	$html_msg .= "<table width='100%' cellpadding='3' cellspacing='3' border='1'>";
    	$html_msg .= "<tr>";
    	$html_msg .= "  <th width='5%' align='center'>#</th>";
    	$html_msg .= "  <th width='20%' align='center'>".JText::_('COM_JVLE_CRON_PWEBURL')."</th>";
    	$html_msg .= "  <th width='35%' align='center'>".JText::_('COM_JVLE_CRON_CRESULT')."Check Result</th>";
    	$html_msg .= "  <th width='15%' align='center'>".JText::_('COM_JVLE_CRON_PEMAIL')."Partner Email</th>";
    	$html_msg .= "  <th width='10%' align='center'>".JText::_('COM_JVLE_CRON_LSTSBC')."</th>";
    	$html_msg .= "  <th width='10%' align='center'>".JText::_('COM_JVLE_CRON_LSTSAC')."</th>";
    	$html_msg .= "</tr>";
    	for ($j=1,$i=0;$i<count($out_url);$i++,$j++)
    	{
    		$html_msg .= "<tr>";
    		$html_msg .= "  <td align='center'>".$j."</td>";
            $html_msg .= "  <td align='left'>".$out_url[$i]."</td>";
			$html_msg .= "  <td align='left'>".$out_msg[$i]."</td>";
    	    $html_msg .= "  <td align='center'>".$out_email[$i]."</td>";
    	    $html_msg .= "  <td align='center'>".$out_cstatus[$i]."</td>";
    	    $html_msg .= "  <td align='center'>".$out_nstatus[$i]."</td>";
    	    $html_msg .= "</tr>";
    	}
    	$html_msg .= "</table>";
    	$html_msg .= "<br /><br />";
        $html_msg .= JText::sprintf('COM_JVLE_CRON_ETIME', $end_time)."</body></html>";
    	
        if ($cfg->result_mode == "email")
        {
			JvleUtil::sendEmail($cfg->self_email, "JV-LinkExchanger: ".JText::_('COM_JVLE_CRON_EM_SUBJ'), $html_msg, 1);
        }
        else
		{
			$cdate = JFactory::getDate()->toSql();
			$filename = _JVLE_ABSPATH.DS.'temp'.DS.'SRC_'.JString::str_ireplace(array(" ", ":"), array("-", "_"), $cdate).'.html';
			$fp = fopen($filename, "w");
    	    if (!$fp)
    	    {
    	    	JvleUtil::sendEmail($cfg->self_email, "JV-LinkExchanger: ".JText::_('COM_JVLE_CRON_EM_SUBJ'), $html_msg, 1);
    	    }
            else
            {
    	    	fwrite($fp, $html_msg);
    	        fclose($fp);
    	        JvleUtil::sendEmail($cfg->self_email, "JV-LinkExchanger: ".JText::_('COM_JVLE_CRON_EM_SUBJ'), JText::sprintf('COM_JVLE_CRON_EM_BODY', $filename));
			}
        }
    }
}