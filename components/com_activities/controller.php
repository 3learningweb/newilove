<?php

/**
 * @version		: controller.php 2016-03-29 21:06:39$
 * @author		EFATEK 
 * @package		Activities
 * @copyright	Copyright (C) 2016- EFATEK. All rights reserved.
 * @license		GNU/GPL
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * Activities Component Controller
 */
class ActivitiesController extends JControllerLegacy {

	
}