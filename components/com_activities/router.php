<?php

/**
 * Surveyforce Component for Joomla 3
 * @package Activities
 * @author JoomPlace Team
 * @copyright Copyright (C) JoomPlace, www.joomplace.com
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
defined('_JEXEC') or die;

function ActivitiesBuildRoute(&$query) {
    $segments = array();
	$city_arr = array('changhua_county', 'chiayi_city', 'chiayi_county', 'hsinchu_city', 'hsinchu_county', 'hualien_county',
					  'kaohsiung_city', 'keelung_city', 'kinmen_county', 'lienchiang_county', 'miaoli_county', 'nantou_county',
					  'new_taipei_city', 'penghu_county', 'pingtung_county', 'taichung_city', 'tainan_city', 'taipei_city',
					  'taitung_county', 'taoyuan_city', 'yilan_county', 'yunlin_county');

	// rene 
	if(isset($query['view'])) {
			switch($query['view']) {
				// 首頁
				case 'activity':
					$segments[] = 'activity';
					$layout = $query['layout'];
					if(in_array($layout, $city_arr)) {
						$layout = "city";
					}
					
					if($query['layout']){
						switch($layout) {
							case 'form':
								$segments[] = $query['layout'] . "-" . $query['city'];
								break;
							case 'complete':
								$segments[] = $query['layout'];
								break;
							case 'city':
								$segments[] = $query['layout'];
								break;
						}
						unset($query['city']);
						unset($query['layout']);
					}
					break;
				default:
			}
		
		unset($query['view']);
		
	}
	// end
	
    return $segments;
}

function ActivitiesParseRoute($segments) {

    $segment = explode(':', $segments[0]);
    $vars = array();

   	switch ($segments[0]) {
		// rene
		case 'activity':
			$vars['view'] = 'activity';
			if($segments[1]) {
				$layout = explode(':', $segments[1]);
				$vars['layout'] = $layout[0];
				$vars['city'] = $layout[1];
			}
			break;
    }
	
    return $vars;
}
