<?php
/**
 * @version		: view.html.php 2016-03-29 05:06:09$
 * @author		efatek 
 * @package		activities
 * @copyright	Copyright (C) 2016- efatek. All rights reserved.
 * @license		GNU/GPL
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the gmap Component
 */
class ActivitiesViewActivity extends JViewLegacy
{
	protected $item;
	protected $pagination;
	
	// Overwriting JView display method
	function display($tpl = null) 
	{
		$config = JFactory::getConfig();

		// Assign data to the view
		$this->state 	= $this->get('state');
		$this->params 	= $this->state->get('params');
		$this->city		= $this->get('City');
		$this->catid 	= $this->get('Catid');
		
		$app = JFactory::getApplication();
		$layout = $app->input->getString('layout');
		$option = $app->input->getString('option');
		if($layout != "") {
			// setTitle
			$config = JFactory::getConfig();
			$document = JFactory::getDocument();
			$document->setTitle("我要投稿│999個親子心動時光");
		}elseif($option == "com_content") {
			$document->setTitle("最新消息│999個親子心動時光");
		}
				
		
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		
		// Display the view
		parent::display($tpl);


	}
}
