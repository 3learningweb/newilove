<?php

/**
 * @package   	JCE
 * @copyright 	Copyright (c) 2009-2013 Ryan Demmer. All rights reserved.
 * @license   	GNU/GPL 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * JCE is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
defined('_JEXEC') or die('RESTRICTED');

// Load class dependencies
wfimport('editor.libraries.classes.plugin');

// Link Plugin Controller
class WFMicrodataPlugin extends WFEditorPlugin {

    protected static $_url      = 'http://schema.rdfs.org/all.json';
    protected static $_schema   = null;

    /**
     * Constructor activating the default information of the class
     *
     * @access	protected
     */
    function __construct() {
        parent::__construct();

        $request = WFRequest::getInstance();

        $request->setRequest(array($this, 'getSchema'));
        $request->setRequest(array($this, 'getTypeList'));
        $request->setRequest(array($this, 'getPropertyList'));
    }

    function display() {
        parent::display();

        $document = WFDocument::getInstance();
        $settings = $this->getSettings();

        $document->addScriptDeclaration('MicrodataDialog.settings=' . json_encode($settings) . ';');

        $tabs = WFTabs::getInstance(array(
                    'base_path' => WF_EDITOR_PLUGIN
                ));

        // Add tabs
        $tabs->addTab('microdata', 1);

        // add link stylesheet
        $document->addStyleSheet(array('microdata'), 'plugins');
        // add link scripts last
        $document->addScript(array('microdata'), 'plugins');
    }

    function getSettings($settings = array()) {
        return parent::getSettings($settings);
    }

    public function getPropertyList($type) {
        $schema = $this->getSchema('types');

        if (isset($schema->types->$type)) {
            return $schema->types->$type;
        }

        return false;
    }

    public function getSchema($key = null) {
        if (is_null(self::$_schema)) {
            $data = file_get_contents(self::$_url);

            if ($data) {
                self::$_schema = json_decode($data);
            }
        }
        
        if ($key) {
            $schema = self::$_schema;
            if (isset($key, $schema)) {
                return $schema->$key;
            }           
        }
        
        return self::$_schema;
    }

    public function getTypeList() {
        $schema = $this->getSchema('types');

        $options = array();

        foreach ($schema as $key => $value) {
            $options[] = array('key' => $key, 'value' => $value->url);
        }

        return $options;
    }

}