<?php
/**
 * @version		: default.php 2015-07-07 21:06:39$
 * @author		EFATEK 
 * @package		lovetime
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved. 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$itemid = $app->input->getInt('Itemid');


?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	$ = jQuery;
	$(document).ready(function(){
		// date
		var opt={dayNames:["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],
				 dayNamesMin:["日","一","二","三","四","五","六"],
				 monthNames:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
				 monthNamesShort:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
				 prevText:"上月",
				 nextText:"次月",
				 weekHeader:"週",
				 changeYear: true,
				 showMonthAfterYear:true,
				 dateFormat:"yy-mm-dd",
				 minDate: new Date(1930, 1, 1),
				 maxDate: new Date()
				 };

		$("#datepicker1").datepicker(opt);
		$("#datepicker2").datepicker(opt);
	});

	function send_date(_type) {
		$("#type").val(_type);

		$("#mainForm").submit();
	}
</script>

<div class="com_lovetime">
	<form id="mainForm" method="post" action="<?php echo JRoute::_('index.php?option=com_lovetime&view=game&layout=result&Itemid='. $itemid, false); ?>" >
	<div class="contentTable">
        <div class="contentTableTop">愛的時刻表</div>
        <div class="contentTableMain">
			<div class="loveBox">
				<div class="loveTop">
					<h6>我們的相戀紀念日</h6>
				</div>
				<div class="loveCt">
					<div style="text-align: center">
						<span style="font-family:微軟正黑體,Trebuchet MS,Trebuchet,Arial,Verdana,sans-serif"><span style="line-height:24px"><span style="color:#ff0000"><span style="font-size:1.2em">戀</span></span><span style="color:#4d5051"><span style="font-family:微軟正黑體,Trebuchet MS,Trebuchet,Arial,Verdana,sans-serif"><span style="line-height:24px"><span style="font-size:1em">愛是一件甜蜜的事情，值得紀念、<br>
												也值得好好回憶與分享！<br>
												許多人這麼說：<br>
												「戀愛是一門藝術，<br>
												需要用心學、用心經營。」<br>
												而我們也相信：<br>
												「因為愛，我們學習；<br>
												學習，讓我們更相愛！」<br>
												<br>
												還記得和自己的最愛在一起的那一天<br>
												是幾月幾號呢？<br>
												還記得當時發生甚麼事嗎?<br>
												又是誰和誰表白的呢？<br>
												<br>
												除此之外，會不會想要知道，<br>
												這時候的我們要注意甚麼？<br>
												該怎麼讓感情更加溫呢？</span></span></span></span></span></span></div>

					<div class="loveDate">
						請輸入相戀那一天的日期：

                       
							<input type="text" id="datepicker1" name="love_date" value="<?php echo date("Y-m-d"); ?>" size="15" maxlength="10">
							<input type="button" value="開始計算" onClick="send_date('love')">
						
					</div>
					
				</div>
			</div>
			<hr style="width:80%; margin:20px auto;">
			<div class="loveBox2">
				<div class="lovelornTop">
					<h6>我們分手的那一天</h6>
				</div>
				<div class="loveCt">
					<div style="text-align: center">
						<span style="font-family:微軟正黑體,Trebuchet MS,Trebuchet,Arial,Verdana,sans-serif"><span style="line-height:1.2em"><span style="color:#0000ff"><span style="font-size:18px">分</span></span><span style="color:#ff0000"><span style="color:#4d5051"><span style="font-family:微軟正黑體,Trebuchet MS,Trebuchet,Arial,Verdana,sans-serif"><span style="line-height:24px"><span style="font-size:1em">手讓我們受傷又覺得難受…<br>
													想起當時，是否還酸酸的、甜甜的？<br>
													<br>
													可能是一段不願再想起的過去；<br>
													可能是一段早已經淡忘的回憶…<br>
													<br>
													與其我們只是黯然悲傷，<br>
													不如化悲憤為力量<br>
													重新思考、重新沉澱。<br>
													<br>
													失戀的時候…<br>
													如何才是調適的好方法呢？<br>
													&nbsp;</span></span></span></span></span></span></span></div>

					<div class="loveDate">
						請輸入分手那一天的日期：
                        
							<input type="text" id="datepicker2" name="break_date" value="<?php echo date("Y-m-d"); ?>" size="15" maxlength="10">
							<input type="button" value="開始計算" onClick="send_date('break')">
					
					</div>
					
				</div>
			</div>
        </div>
        <div class="contentTableDown"></div>
    </div>
		<input type="hidden" name="type" id="type" value="">
	</form>
</div>