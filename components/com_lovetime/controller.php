<?php

/**
 * @version		: controller.php 2015-07-07 21:06:39$
 * @author		EFATEK 
 * @package		Lovetime
 * @copyright	Copyright (C) 2011- EFATEK. All rights reserved.
 * @license		GNU/GPL
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * LOVETIME Component Controller
 */
class LovetimeController extends JControllerLegacy {

	
}