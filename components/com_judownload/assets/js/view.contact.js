jQuery(document).ready(function ($) {
    var bsversion = $('#judl-form-contact').data('bsversion') ? $(this).data('bsvsion') : 3;
    var classError = bsversion == 2 ? 'error' : 'has-error';
    $('#judl-form-contact').validate({
        errorClass: "help-block",
        errorElement: "div",
        ignore: ".ignoreValidate",
        highlight: function (element, errorClass, validClass) {
            $(element).closest('.field-group').addClass(classError);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).closest('.field-group').removeClass(classError);
        }
    });
});