jQuery(document).ready(function ($) {
    var bsversion = $('#judl-form-report').data('bsversion') ? $(this).data('bsvsion') : 3;
    var classError = bsversion == 2 ? 'error' : 'has-error';
    $('#judl-form-report').validate({
        errorClass: "help-block",
        errorElement: "div",
        ignore: ".ignoreValidate",
        highlight: function (element, errorClass, validClass) {
            $(element).closest('.field-group').addClass(classError);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).closest('.field-group').removeClass(classError);
        }
    });

    $('select#report-subject').change(function () {
        if ($(this).val() == 'other') {
            $('#other').show();
            $('#report-other-subject').removeClass('ignoreValidate');
        }else{
            $('#other').hide();
            $('#report-other-subject').addClass('ignoreValidate');
        }
    });
});