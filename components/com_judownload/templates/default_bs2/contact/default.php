<?php
/**
 * ------------------------------------------------------------------------
 * JUDownload for Joomla 2.5, 3.x
 * ------------------------------------------------------------------------
 *
 * @copyright      Copyright (C) 2010-2015 JoomUltra Co., Ltd. All Rights Reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @author         JoomUltra Co., Ltd
 * @website        http://www.joomultra.com
 * @----------------------------------------------------------------------@
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.keepalive');
?>

<div id="judl-container" class="jubootstrap component judl-container view-contact">
    <h4><?php echo JText::_('COM_JUDOWNLOAD_CONTACT_LISTING_OWNER') . ': ' . $this->document->title; ?></h4>
    <hr/>
    <form method="POST" action="#" name="contact-form" id="judl-form-contact" class="form-horizontal" data-bsversion="2">
        <div class="form-control field-group">
            <label class="control-label" for="contact-name">
                <?php echo JText::_('COM_JUDOWNLOAD_NAME'); ?>
                <span class="required">*</span>
            </label>

            <div class="controls">
                <input type="text" name="jform[from_name]" value="<?php echo $this->user->name; ?>"
                       id="contact-name"
                       data-rule-required="true"
                       aria-describedby="contact-name-error"/>
                <div class="help-block" style="display: none" id="contact-name-error"></div>
            </div>
        </div>
        <div class="form-control field-group">
            <label class="control-label" for="contact-email">
                <?php echo JText::_('COM_JUDOWNLOAD_EMAIL'); ?>
                <span class="required">*</span>
            </label>

            <div class="controls">
                <input type="text" name="jform[from_email]" value="<?php echo $this->user->email; ?>"
                       id="contact-email"
                       data-rule-required="true"
                       data-rule-email="true"
                       aria-describedby="contact-email-error"/>
                <div class="help-block" style="display: none" id="contact-email-error"></div>
            </div>
        </div>
        <div class="form-control field-group">
            <label class="control-label" for="contact-message">
                <?php echo JText::_('COM_JUDOWNLOAD_MESSAGE'); ?>
                <span class="required">*</span>
            </label>

            <div class="controls">
				<textarea name="jform[message]" rows="5" id="contact-message"
                          data-rule-required="true" aria-describedby="contact-message-error"></textarea>
                <div class="help-block" style="display: none" id="contact-message-error"></div>
            </div>
        </div>

        <?php
        if ($this->requireCaptcha)
        {
            echo JUDownloadFrontHelperCaptcha::getCaptcha();
        }
        ?>

        <div class="form-control field-group">
            <div class="offset-2 controls">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="jform[sendcopy]" id="contact-sendcopy" value="1"> <?php echo JText::_('COM_JUDOWNLOAD_SENDCOPY'); ?>
                    </label>
                </div>
            </div>
        </div>

        <div class="form-control">
            <label class="control-label"></label>

            <div class="controls">
                <button type="submit" class="btn btn-primary">
                    <?php echo JText::_('COM_JUDOWNLOAD_SUBMIT'); ?>
                </button>
                <a href="<?php echo JRoute::_(JUDownloadHelperRoute::getDocumentRoute($this->docId), false); ?>" class="btn">
                    <?php echo JText::_('COM_JUDOWNLOAD_CANCEL'); ?>
                </a>
            </div>
        </div>

        <div>
            <input type="hidden" name="jform[doc_id]" value="<?php echo $this->docId ?>"/>
            <input type="hidden" name="task" value="contact.send"/>
            <?php echo JHtml::_('form.token'); ?>
        </div>
    </form>
</div>