<?php
/**
 * ------------------------------------------------------------------------
 * JUDownload for Joomla 2.5, 3.x
 * ------------------------------------------------------------------------
 *
 * @copyright      Copyright (C) 2010-2015 JoomUltra Co., Ltd. All Rights Reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @author         JoomUltra Co., Ltd
 * @website        http://www.joomultra.com
 * @----------------------------------------------------------------------@
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.keepalive');
?>

<div id="judl-container" class="jubootstrap component judl-container view-report">
	<?php if (isset($this->comment))
	{
		?>
		<h2><?php echo JText::sprintf("COM_JUDOWNLOAD_REPORT_COMMENT_X", $this->comment->title); ?></h2>
	<?php
	}
	else
	{
		?>
		<h2><?php echo JText::sprintf("COM_JUDOWNLOAD_REPORT_DOCUMENT_X", $this->document->title); ?></h2>
	<?php
	} ?>
	<hr/>
	<form method="POST" name="form-report" id="judl-form-report" action="" class="form-horizontal" data-bsversion="2">
		<?php
		if ($this->user->get('guest'))
		{
			?>
			<div class="control-group field-group">
				<label class="control-label" for="report-username">
					<?php echo JText::_('COM_JUDOWNLOAD_NAME'); ?>
					<span class="required">*</span>
				</label>

				<div class="controls">
                    <input type="text" name="jform[username]" value=""
                           id="report-username"
                           data-rule-required="true"
                           aria-describedby="report-username-error"/>
                    <div class="help-block" style="display: none" id="report-username-error"></div>
				</div>
			</div>
			<div class="control-group field-group">
				<label class="control-label" for="report-email">
					<?php echo JText::_('COM_JUDOWNLOAD_EMAIL'); ?>
					<span class="required">*</span>
				</label>

				<div class="controls">
                    <input type="text" name="jform[email]" value=""
                           id="report-email"
                           data-rule-required="true"
                           data-rule-email="true"
                           aria-describedby="report-email-error"/>
                    <div class="help-block" style="display: none" id="report-email-error"></div>
				</div>
			</div>
		<?php
		}
		else
		{
			?>
			<div class="control-group">
				<label class="control-label" for="report-username">
					<?php echo JText::_('COM_JUDOWNLOAD_NAME'); ?>
					<span class="required">*</span>
				</label>

				<div class="controls">
					<input type="text" class="required" name="jform[username]"
					       value="<?php echo $this->user->name; ?>" id="report-username" readonly="readonly"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="report-email">
					<?php echo JText::_('COM_JUDOWNLOAD_EMAIL'); ?>
					<span class="required">*</span>
				</label>

				<div class="controls">
					<input type="text" class="required email" name="jform[email]"
					       value="<?php echo $this->user->email; ?>" id="report-email" readonly="readonly"/>
				</div>
			</div>
		<?php
		}
		?>

		<div class="control-group field-group">
			<label class="control-label" for="report-subject"><?php echo JText::_('COM_JUDOWNLOAD_SUBJECT'); ?>
				<span class="required">*</span>
			</label>

			<div class="controls">
				<?php
				if (count($this->subject) > 0)
				{
					$beginSubject  = array('' => JText::_('COM_JUDOWNLOAD_SELECT'));
					$otherSubject  = array('other' => JText::_('COM_JUDOWNLOAD_OTHER'));
					$reportSubject = array_merge($beginSubject, $this->subject, $otherSubject);
					?>
                    <select name="jform[subject]" id="report-subject" data-rule-required="true" aria-describedby="report-subject-error">
						<?php echo JHtml::_('select.options', $reportSubject, 'value', 'text', ''); ?>
					</select>
				<?php
				}
				else
				{
					?>
                    <input type="text" name="jform[subject]" id="report-subject" data-rule-required="true" aria-describedby="report-subject-error"/>
				<?php
				}
				?>
                <div class="help-block" style="display: none" id="report-subject-error"></div>
			</div>
		</div>

		<div class="control-group field-group" id="other" style="display:none">
			<label class="control-label" for="report-other-subject">
				<?php echo JText::_('COM_JUDOWNLOAD_OTHER_SUBJECT'); ?>
				<span class="required">*</span>
			</label>

			<div class="controls">
                <input type="text" name="jform[other-subject]" class="ignoreValidate" id="report-other-subject"
                       data-rule-required="true" aria-describedby="report-other-subject-error"/>
                <div class="help-block" style="display: none" id="report-other-subject-error"></div>
			</div>
		</div>

		<div class="control-group field-group">
			<label class="control-label" for="report-content">
				<?php echo JText::_('COM_JUDOWNLOAD_CONTENT'); ?>
				<span class="required">*</span>
			</label>

			<div class="controls">
				<textarea name="jform[report]" rows="5" id="report-content"
                          data-rule-required="true" aria-describedby="report-content-error"></textarea>
                <div class="help-block" style="display: none" id="report-content-error"></div>
			</div>
		</div>

		<?php
		if ($this->requireCaptcha)
		{
			?>
			<?php echo JUDownloadFrontHelperCaptcha::getCaptcha(); ?>
		<?php
		}
		?>
		<div class="control-group">
			<label class="control-label"></label>

			<div class="controls">
                <button type="submit" class="btn btn-primary"><?php echo JText::_('COM_JUDOWNLOAD_SUBMIT'); ?></button>
                <a href="<?php echo JRoute::_(JUDownloadHelperRoute::getDocumentRoute($this->docId), false); ?>" class="btn"><?php echo JText::_('COM_JUDOWNLOAD_CANCEL'); ?></a>
			</div>
		</div>

		<div>
			<input type="hidden" name="jform[doc_id]" value="<?php echo $this->docId; ?>"/>
			<?php if ($this->commentId > 0)	{ ?>
				<input type="hidden" name="jform[comment_id]" value="<?php echo $this->commentId; ?>"/>
			<?php } ?>
			<input type="hidden" name="task" value="report.save"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>
</div>