<?php
/**
 * ------------------------------------------------------------------------
 * JUDownload for Joomla 2.5, 3.x
 * ------------------------------------------------------------------------
 *
 * @copyright      Copyright (C) 2010-2015 JoomUltra Co., Ltd. All Rights Reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @author         JoomUltra Co., Ltd
 * @website        http://www.joomultra.com
 * @----------------------------------------------------------------------@
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$dataEditComment = array();
$dataEditComment['title'] = '';
$dataEditComment['guest_name'] = '';
$dataEditComment['guest_email'] = '';
$dataEditComment['title'] = $this->commentObj->title;
$dataEditComment['language'] = '';
$dataEditComment['website'] = '';
if ($this->commentObj->user_id > 0)
{
	$user = JFactory::getUser($this->commentObj->user_id);
	if (is_object($user))
	{
		$dataEditComment['guest_name']  = $user->get('name');
		$dataEditComment['guest_email'] = $user->get('email');
	}
}
else
{
	$dataEditComment['guest_name']  = $this->commentObj->guest_name;
	$dataEditComment['guest_email'] = $this->commentObj->guest_email;
}

if ($this->commentObj->language)
{
	$dataEditComment['language'] = $this->commentObj->language;
}

if ($this->commentObj->website)
{
	$dataEditComment['website'] = $this->commentObj->website;
}

$dataEditComment['comment'] = $this->commentObj->comment_edit;
?>
<div class="comment-edit-wrapper hidden"
     id="comment-edit-wrapper-<?php echo $this->commentObj->id; ?>">
	<form name="comment-edit-form" class="comment-edit-form form-horizontal" method="POST" action="" data-bsversion="2">
		<fieldset>
			<legend><?php echo JText::_('COM_JUDOWNLOAD_EDIT_COMMENT'); ?></legend>
			<div class="judl-comment-wrapper clearfix">
				<div class="comment-message-container"></div>
				<div class="judl-comment">
					<p class="note-required">
						<?php echo JText::sprintf('COM_JUDOWNLOAD_ALL_FIELDS_HAVE_STAR_ARE_REQUIRED', '<span class="required">*</span>'); ?>
					</p>

					<div class="row-fluid">
						<div class="comment-user-info span8">
							<div class="control-group field-group">
								<label class="control-label"
								       for="comment-title-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT_TITLE'); ?>
									<span class="required">*</span></label>

								<div class="controls">
                                    <input name="title" id="comment-title-<?php echo $this->commentObj->id; ?>"
                                           type="text" value="<?php echo $dataEditComment['title']; ?>"
                                           data-rule-required="true"
                                           aria-describedby="comment-title-<?php echo $this->commentObj->id; ?>-error"/>
                                    <div class="help-block" style="display: none" id="comment-title-<?php echo $this->commentObj->id; ?>-error"></div>
								</div>
							</div>

							<?php
							if ($this->user->get('guest'))
							{
								?>
								<div class="control-group field-group">
									<label class="control-label"
									       for="comment-name-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT_NAME'); ?>
										<span class="required">*</span></label>

									<div class="controls">
                                        <input name="guest_name" id="comment-name-<?php echo $this->commentObj->id; ?>" type="text"
                                               value="<?php echo $dataEditComment['guest_name']; ?>"
                                               data-rule-required="true"
                                               aria-describedby="comment-name-<?php echo $this->commentObj->id; ?>-error"/>
                                        <div class="help-block" style="display: none" id="comment-name-<?php echo $this->commentObj->id; ?>-error"></div>
									</div>
								</div>

								<div class="control-group field-group">
									<label class="control-label"
									       for="comment-email-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT_EMAIL'); ?>
										<span class="required">*</span></label>

									<div class="controls">
                                        <input name="guest_email" id="comment-email-<?php echo $this->commentObj->id; ?>" type="text"
                                               value="<?php echo $dataEditComment['guest_email']; ?>"
                                               data-rule-required="true"
                                               data-rule-email="true"
                                               aria-describedby="comment-email-<?php echo $this->commentObj->id; ?>-error"/>
                                        <div class="help-block" style="display: none" id="comment-email-<?php echo $this->commentObj->id; ?>-error"></div>
									</div>
								</div>
							<?php
							}
							else
							{ ?>
								<div class="control-group">
									<label class="control-label"
									       for="comment-name-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT_NAME'); ?>
										<span class="required">*</span></label>

									<div class="controls">
										<input name="guest_name" type="text" id="comment-name-<?php echo $this->commentObj->id; ?>"
										       value="<?php echo $dataEditComment['guest_name']; ?>" readonly="readonly"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label"
									       for="comment-email-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT_EMAIL'); ?>
										<span class="required ">*</span></label>

									<div class="controls">
										<input name="guest_email" type="text" id="comment-email-<?php echo $this->commentObj->id; ?>"
										       value="<?php echo $dataEditComment['guest_email']; ?>" readonly="readonly"/>
									</div>
								</div>
							<?php
							} ?>							

							<?php if ($this->website_field_in_comment_form)
							{
								$require = ($this->website_field_in_comment_form == 1) ? '' : '<span class=required>*</span>';
								?>
								<div class="control-group field-group">
									<label class="control-label"
									       for="comment-website-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT_WEBSITE'); ?><?php echo $require; ?></label>
									<div class="controls">
                                        <input type="text" name="website"
                                            <?php if($this->website_field_in_comment_form == 2) echo 'data-rule-required="true"'; ?>
                                               data-rule-url="true"
                                               aria-describedby="comment-website-<?php echo $this->commentObj->id; ?>-error"
                                               id="comment-website-<?php echo $this->commentObj->id; ?>"
                                               value="<?php echo $dataEditComment['website']; ?>"/>
                                        <div class="help-block" style="display: none" id="comment-website-<?php echo $this->commentObj->id; ?>-error"></div>
									</div>
								</div>
							<?php } ?>

							<?php
							if ($this->params->get('filter_comment_language', 0))
							{
								?>	
							<div class="control-group field-group">
								<label class="control-label"
								       for="comment-language-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_LANGUAGE'); ?>
									<span class="required">*</span></label>

								<div class="controls">
                                    <select name="comment_language" id="comment-language-<?php echo $this->commentObj->id; ?>"
                                            data-rule-required="true"
                                            aria-describedby="comment-language-<?php echo $this->commentObj->id; ?>-error">
                                        <?php echo JHtml::_('select.options', $this->langArray, 'value', 'text', $dataEditComment['language']); ?>
                                    </select>
                                    <div class="help-block" style="display: none" id="comment-language-<?php echo $this->commentObj->id; ?>-error"></div>
								</div>
							</div>
							<?php } ?>
						</div>
						<?php
						if (isset($this->item->fields['rating']) && $this->item->fields['rating']->canView())
						{
							echo '<div class="comment-rating span4">';
							echo $this->item->fields['rating']->getOutput(array("view" => "details", "template" => $this->template, "type" => "comment_form", "comment_object" => $this->commentObj));
							echo '</div>';
						}
						?>
					</div>

                    <div class="form-vertical">
                        <div class="control-group field-group">
                            <label class="control-label" for="comment-editor-<?php echo $this->commentObj->id ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT'); ?><span class="required">*</span></label>
                            <div class="controls">
                                <textarea name="comment" class="comment-editor form-control" id="comment-edit-editor-<?php echo $this->commentObj->id ?>" rows="8"
                                          data-rule-required="true"
                                          data-rule-minlength="<?php echo $this->params->get('min_comment_characters', 20); ?>"
                                          data-rule-maxlength="<?php echo $this->params->get('max_comment_characters', 1000); ?>"
                                          aria-describedby="comment-editor-<?php echo $this->commentObj->id ?>-error"><?php echo $dataEditComment['comment']; ?></textarea>
                                <div class="help-block" style="display: none" id="comment-editor-<?php echo $this->commentObj->id; ?>-error"></div>
                            </div>
                        </div>
                    </div>

                    <?php
                    if(JUDLPROVERSION){
                        if ($this->commentObj->is_subscriber)
                        {
                            ?>
                        <div class="control-group field-group">
                            <div class="controls">
                                <div class="checkbox">
                                    <label class="comment-subscribe-lbl"
                                           for="comment-subscribe-<?php echo $this->commentObj->id ?>">
                                        <input name="subscribe" id="comment-subscribe-<?php echo $this->commentObj->id ?>"
                                               class="comment-subscribe" checked="checked"
                                               type="checkbox" value="1"/>
                                        <?php echo JText::_('COM_JUDOWNLOAD_COMMENT_SUBSCRIBE'); ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                        else
                        {
                            if ($this->params->get('can_subscribe_own_comment', 1))
                            {
                                ?>
                                <div class="checkbox">
                                    <label class="comment-subscribe-lbl"
                                           for="comment-subscribe-<?php echo $this->commentObj->id ?>">
                                        <input name="subscribe" id="comment-subscribe-<?php echo $this->commentObj->id ?>"
                                               class="comment-subscribe" type="checkbox" value="1"/>
                                        <?php echo JText::_('COM_JUDOWNLOAD_COMMENT_SUBSCRIBE'); ?>
                                    </label>
                                </div>
                            <?php
                            }
                        }
                    }
                    ?>

                    <?php
                    if (JUDownloadFrontHelperPermission::showCaptchaWhenComment($this->item->id))
                    {
                        echo JUDownloadFrontHelperCaptcha::getCaptcha(true);
                    }
                    ?>

					<div class="comment-form-submit clearfix">
                        <button type="button" class="btn btn-primary"><?php echo JText::_('COM_JUDOWNLOAD_SUBMIT'); ?></button>
                        <button type="button" class="btn" onclick="judlCancelEditComment('<?php echo $this->commentObj->id; ?>'); return false;"><?php echo JText::_('COM_JUDOWNLOAD_CANCEL'); ?></button>
					</div>

					<div>
						<input name="task" value="document.updateComment" type="hidden"/>
						<input type="hidden" name="comment_id" value="<?php echo $this->commentObj->id?>" />
						<input type="hidden" name="doc_id" value="<?php echo $this->commentObj->doc_id?>" />
						<?php echo JHtml::_('form.token'); ?>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>