<?php
/**
 * ------------------------------------------------------------------------
 * JUDownload for Joomla 2.5, 3.x
 * ------------------------------------------------------------------------
 *
 * @copyright      Copyright (C) 2010-2015 JoomUltra Co., Ltd. All Rights Reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @author         JoomUltra Co., Ltd
 * @website        http://www.joomultra.com
 * @----------------------------------------------------------------------@
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$title = $name = $email = $website = $comment = '';
$hiddenForm = true;
if (isset($this->form['parent_id']) && $this->form['parent_id'] == $this->commentObj->id)
{
	$title      = $this->form['title'];
	$name       = $this->form['guest_name'];
	$email      = $this->form['guest_email'];
	$comment    = $this->form['comment'];
	$website    = (isset($this->form['website'])) ? $this->form['website'] : '';
	$hiddenForm = false;
}
?>
<div class="comment-reply-wrapper <?php echo $hiddenForm ? 'hidden' : ''; ?> "
     id="comment-reply-wrapper-<?php echo $this->commentObj->id; ?>">
	<form name="comment-reply-form" data-bsversion="2" class="comment-reply-form form-horizontal" method="POST"	action="">
		<fieldset>
			<legend><?php echo JText::_('COM_JUDOWNLOAD_REPLY_COMMENT'); ?></legend>
			<div class="judl-comment-wrapper clearfix">
				<div class="comment-message-container"></div>
				<div class="judl-comment">
					<p class="note-required">
						<?php echo JText::sprintf('COM_JUDOWNLOAD_ALL_FIELDS_HAVE_STAR_ARE_REQUIRED', '<span class="required">*</span>'); ?>
					</p>

					<div class="row-fluid">
						<div class="control-group field-group">
							<label class="control-label"
							       for="comment-reply-title-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT_TITLE'); ?>
								<span class="required">*</span></label>

							<div class="controls">
                                <div class="controls">
                                    <input name="title" id="comment-reply-title-<?php echo $this->commentObj->id; ?>"
                                           type="text" value="<?php echo $title; ?>"
                                           data-rule-required="true"
                                           aria-describedby="comment-reply-title-<?php echo $this->commentObj->id; ?>-error"/>
                                    <div class="help-block" style="display: none" id="comment-reply-title-<?php echo $this->commentObj->id; ?>-error"></div>
                                </div>
							</div>
						</div>
						<?php if ($this->user->get('guest'))
						{
							?>
							<div class="control-group field-group">
								<label class="control-label"
								       for="comment-reply-name-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT_NAME'); ?>
									<span class="required">*</span></label>

								<div class="controls">
                                    <input name="guest_name" type="text" id="comment-reply-name-<?php echo $this->commentObj->id; ?>"
                                           value="<?php echo $name; ?>"
                                           data-rule-required="true"
                                           aria-describedby="comment-reply-name-<?php echo $this->commentObj->id; ?>-error"/>
                                    <div class="help-block" style="display: none" id="comment-reply-name-<?php echo $this->commentObj->id; ?>-error"></div>
                                </div>
							</div>
							<div class="control-group field-group">
								<label class="control-label"
								       for="comment-reply-email-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT_EMAIL'); ?>
									<span class="required">*</span></label>

								<div class="controls">
                                    <input name="guest_email" type="text" id="comment-reply-email-<?php echo $this->commentObj->id; ?>"
                                           value="<?php echo $email; ?>"
                                           data-rule-required="true"
                                           data-rule-email="true"
                                           aria-describedby="comment-reply-email-<?php echo $this->commentObj->id; ?>-error"/>
                                    <div class="help-block" style="display: none" id="comment-reply-email-<?php echo $this->commentObj->id; ?>-error"></div>
								</div>
							</div>
						<?php
						}
						else
						{
							?>
							<div class="control-group">
								<label class="control-label"
								       for="comment-reply-name-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT_NAME'); ?>
									<span class="required">*</span></label>

								<div class="controls">
									<input name="guest_name" type="text" id="comment-reply-name-<?php echo $this->commentObj->id; ?>"
									       value="<?php echo $this->user->name; ?>" readonly="readonly"/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label"
								       for="comment-reply-email-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT_EMAIL'); ?>
									<span class="required ">*</span></label>

								<div class="controls">
									<input name="guest_email" type="text" id="comment-reply-email-<?php echo $this->commentObj->id; ?>"
									       value="<?php echo $this->user->email; ?>" readonly="readonly"/>
								</div>
							</div>
						<?php
						} ?>

						<?php if ($this->website_field_in_comment_form)
						{
							$require = ($this->website_field_in_comment_form == 1) ? '' : '<span class=required>*</span>';
							?>
							<div class="control-group field-group">
								<label class="control-label"
								       for="comment-reply-website-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT_WEBSITE'); ?><?php echo $require; ?></label>

								<div class="controls">
                                    <input type="text" name="website" id="comment-reply-website-<?php echo $this->commentObj->id; ?>"
                                           value="<?php echo $website; ?>"
                                        <?php if($this->website_field_in_comment_form == 2) echo 'data-rule-required="true"'; ?>
                                           data-rule-url="true"
                                           aria-describedby="comment-reply-website-<?php echo $this->commentObj->id; ?>-error" />
                                    <div class="help-block" style="display: none" id="comment-reply-website-<?php echo $this->commentObj->id; ?>-error"></div>
								</div>
							</div>
						<?php
						} ?>
					</div>

                    <div class="form-vertical">
                        <div class="control-group field-group">
                            <label class="control-label" for="comment-reply-editor-<?php echo $this->commentObj->id; ?>"><?php echo JText::_('COM_JUDOWNLOAD_COMMENT'); ?><span class="required">*</span></label>
                            <div class="controls">
                                <textarea name="comment" class="comment-editor form-control" id="comment-reply-editor-<?php echo $this->commentObj->id; ?>" rows="8"
                                          data-rule-required="true"
                                          data-rule-minlength="<?php echo $this->params->get('min_comment_characters', 20); ?>"
                                          data-rule-maxlength="<?php echo $this->params->get('max_comment_characters', 1000); ?>"
                                          aria-describedby="comment-reply-editor-<?php echo $this->commentObj->id; ?>-error"><?php echo $this->comment; ?></textarea>
                                <div class="help-block" style="display: none" id="comment-reply-editor-<?php echo $this->commentObj->id; ?>-error"></div>
                            </div>
                        </div>
                    </div>

                    <?php if (JUDLPROVERSION && $this->params->get('can_subscribe_own_comment', 1))
                    {
                        ?>
                    <div class="control-group field-group">
                        <div class="controls">
                            <div class="checkbox">
                                <label class="comment-subscribe-lbl"
                                       for="comment-reply-subscribe-<?php echo $this->commentObj->id ?>">
                                    <input name="subscribe" id="comment-reply-subscribe-<?php echo $this->commentObj->id ?>"
                                           class="comment-subscribe" type="checkbox" value="1"/>
                                    <?php echo JText::_('COM_JUDOWNLOAD_COMMENT_SUBSCRIBE'); ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                    ?>

                    <?php
                    if (JUDownloadFrontHelperPermission::showCaptchaWhenComment($this->item->id))
                    {
                        echo JUDownloadFrontHelperCaptcha::getCaptcha(true);
                    }
                    ?>

					<div class="comment-form-submit clearfix">
                        <button type="button" class="btn btn-primary"><?php echo JText::_('COM_JUDOWNLOAD_SUBMIT'); ?></button>
                        <button type="reset" class="btn" onclick="judlResetReplyForm(this);"><?php echo JText::_('COM_JUDOWNLOAD_RESET'); ?></button>
                    </div>
					<div>
						<input type="hidden" name="task" value="document.addComment"/>
						<input type="hidden" name="parent_id" value="<?php echo $this->commentObj->id; ?>"/>
						<input type="hidden" name="doc_id" value="<?php echo $this->item->id; ?>"/>
						<?php echo JHtml::_('form.token'); ?>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>