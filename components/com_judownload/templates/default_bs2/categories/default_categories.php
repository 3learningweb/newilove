<?php
/**
 * ------------------------------------------------------------------------
 * JUDownload for Joomla 2.5, 3.x
 * ------------------------------------------------------------------------
 *
 * @copyright      Copyright (C) 2010-2015 JoomUltra Co., Ltd. All Rights Reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @author         JoomUltra Co., Ltd
 * @website        http://www.joomultra.com
 * @----------------------------------------------------------------------@
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>

<ul class="category-regular category-regular-level-<?php echo $this->cat_regular_level;?>">
	<?php
	$newParentId = $this->new_parent_id;
	foreach ($this->all_categories AS $category)
	{
		if ($category->parent_id == $newParentId)
		{
			?>
			<li class="category-item">
				<a href="<?php echo JRoute::_(JUDownloadHelperRoute::getCategoryRoute($category->id)); ?>"><?php echo $category->title; ?></a>
				<?php
				$couter = array();
				$couter[] = '<span class="total-categories"><span>' . $category->total_nested_categories . '</span> ' . JText::_('COM_JUDOWNLOAD_CATEGORIES') . '</span>';
				$couter[] = '<span class="total-documents"><span>' . $category->total_documents . '</span> ' . JText::_('COM_JUDOWNLOAD_DOCUMENTS') . '</span>';
				echo '<span class="category-counter">(' . implode(" / ", $couter) . ')</span>';

				if ($category->total_childs > 0)
				{
					$this->new_parent_id = $category->id;
					 $this->cat_regular_level++;
					echo $this->loadTemplate('categories');
				}
				?>
			</li>
		<?php
		}
	} ?>
</ul>