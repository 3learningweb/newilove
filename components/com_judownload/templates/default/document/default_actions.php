<?php
/**
 * ------------------------------------------------------------------------
 * JUDownload for Joomla 2.5, 3.x
 * ------------------------------------------------------------------------
 *
 * @copyright      Copyright (C) 2010-2015 JoomUltra Co., Ltd. All Rights Reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @author         JoomUltra Co., Ltd
 * @website        http://www.joomultra.com
 * @----------------------------------------------------------------------@
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<div class="actions clearfix">
	<div class="general-actions">
	<?php if ($this->item->params->get('access-report'))
	{
		?>
		<span class="action-report">
			<?php echo '<a href="' . $this->item->report_link . '" title="' . JText::_('COM_JUDOWNLOAD_REPORT') . '" class="hasTooltip report-task btn btn-default"><i class="fa fa-warning"></i></a>'; ?>
		</span>
	<?php
	}
	if ($this->item->params->get('access-contact'))
	{
		?>
		<span class="action-contact">
			<?php echo '<a href="' . $this->item->contact_link . '" title="' . JText::_('COM_JUDOWNLOAD_CONTACT') . '" class="hasTooltip btn btn-default"><i class="fa fa-user"></i></a>'; ?>
		</span>
	<?php
	}

	if (!$this->item->is_subscriber)
	{
		?>
		<span class="action-subscribe">
			<?php echo '<a href="' . $this->item->subscribe_link . '" title="' . JText::_('COM_JUDOWNLOAD_SUBSCRIBE') . '" class="hasTooltip btn btn-default"><i class="fa fa-bookmark"></i></a>'; ?>
		</span>
	<?php
	}
	else
	{
		?>
		<span class="action-unsubscribe">
			<?php echo '<a href="' . $this->item->unsubscribe_link . '" title="' . JText::_('COM_JUDOWNLOAD_UNSUBSCRIBE') . '" class="hasTooltip btn btn-default"><i class="fa fa-bookmark-o"></i></a>'; ?>
		</span>
	<?php
	} ?>

	<span class="action-print">
		<?php
			$windowOpenSpecs = 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no';
			$onclick = "window.open(this.href, 'document_print', '" . $windowOpenSpecs . "'); return false;";
			echo '<a class="hasTooltip btn btn-default" title="' . JText::_('COM_JUDOWNLOAD_PRINT') . '" href="' . $this->item->print_link . '" rel="nofollow" onclick="' . $onclick . '"><i class="fa fa-print" ></i></a>';
		?>
	</span>

	<span class="action-mailtofriend">
		<a href="#judl-mailtofriend"
		   title="<?php echo JText::_('COM_JUDOWNLOAD_SEND_EMAIL_TO_FRIEND'); ?>" class="hasTooltip btn btn-default"
		   data-toggle="modal">
            <i class="fa fa-envelope"></i>
        </a>
	</span>

	<div class="modal fade" id="judl-mailtofriend" tabindex="-1" role="dialog"
	     aria-labelledby="judl-mailtofriend-label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
                <form id="judl-form-mailtofriend" action="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"
                            id="judl-mailtofriend-label"><?php echo JText::_('COM_JUDOWNLOAD_SEND_EMAIL'); ?>
                        </h3>
                    </div>
                    <div class="modal-body form-horizontal">
                        <div class="message-wrap form-group hide">
                            <label class="control-label col-sm-3"></label>
                            <div class="col-sm-9 message"></div>
                        </div>
                        <div class="form-group field-group">
                            <label class="control-label col-sm-3" for="mtf-email">
                                <?php echo JText::_('COM_JUDOWNLOAD_SEND_TO'); ?>
                                <span class="required">*</span>
                            </label>

                            <div class="col-sm-9">
                                <input id="mtf-email" type="text" name="to_email" size="32"
                                       data-rule-required="true"
                                       data-rule-email="true"
                                       aria-describedby="mtf-email-error"/>
                                <div class="help-block" style="display: none" id="mtf-email-error"></div>
                            </div>
                        </div>

                        <?php
                        if ($this->user->get('guest'))
                        {
                            ?>
                            <div class="form-group field-group">
                                <label class="control-label col-sm-3"
                                       for="mtf-name"><?php echo JText::_('COM_JUDOWNLOAD_YOUR_NAME'); ?>
                                    <span class="required">*</span></label>

                                <div class="col-sm-9">
                                    <input name="name" id="mtf-name" type="text"
                                           value=""
                                           data-rule-required="true"
                                           aria-describedby="mtf-name-error"/>
                                    <div class="help-block" style="display: none" id="mtf-name-error"></div>
                                </div>
                            </div>
                            <div class="form-group field-group">
                                <label class="control-label col-sm-3"
                                       for="mtf-email"><?php echo JText::_('COM_JUDOWNLOAD_YOUR_EMAIL'); ?>
                                    <span class="required">*</span></label>

                                <div class="col-sm-9">
                                    <input name="email" id="mtf-email" type="text"
                                           value=""
                                           data-rule-required="true"
                                           data-rule-email="true"
                                           aria-describedby="mtf-email-error"/>
                                    <div class="help-block" style="display: none" id="mtf-email-error"></div>
                                </div>
                            </div>
                        <?php
                        }
                        else
                        {
                            ?>
                            <div class="form-group">
                                <label class="control-label col-sm-3"
                                       for="input-name"><?php echo JText::_('COM_JUDOWNLOAD_YOUR_NAME'); ?>
                                    <span class="required">*</span></label>

                                <div class="col-sm-9">
                                    <input name="name" type="text" id="input-name"
                                           value="<?php echo $this->user->name; ?>" readonly="readonly"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3"
                                       for="input-email"><?php echo JText::_('COM_JUDOWNLOAD_YOUR_EMAIL'); ?>
                                    <span class="required ">*</span></label>

                                <div class="col-sm-9">
                                    <input name="email" type="text" id="input-email"
                                           value="<?php echo $this->user->email; ?>" readonly="readonly"/>
                                </div>
                            </div>
                        <?php
                        }
                        ?>

                        <div>
                            <input type="hidden" name="task" value="document.sendemail"/>
                            <input type="hidden" name="doc_id" value="<?php echo $this->item->id; ?>"/>
                            <input type="hidden" name="tmpl" value="component"/>
                            <?php echo JHtml::_('form.token'); ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default btn-primary"><?php echo JText::_("COM_JUDOWNLOAD_SEND"); ?></button>
                        <button class="btn btn-default" aria-hidden="true" data-dismiss="modal"><?php echo JText::_("COM_JUDOWNLOAD_CANCEL"); ?></button>
                    </div>
                </form>
			</div>
		</div>
	</div>

	<?php if ($this->collection_popup)
	{
		?>
		<span class="action-addcollection">
	            <a class="hasTooltip btn btn-default judl-add-collection"
	               title="<?php echo JText::_('COM_JUDOWNLOAD_ADD_TO_COLLECTIONS'); ?>">
		            <i class="fa fa-inbox"></i>
	            </a>
			</span>

		<div class="judl-collection-list" style="display: none;">
			<div class="collection-popup jubootstrap component judl-container">
				<ul class="collection-list">
					<?php
					foreach ($this->collections AS $collection)
					{
						$added = "";
						if ($collection->hasThisDoc)
						{
							$added = " added";
						}
						echo "<li class='collection-item'>
	                                <i class='add-to-collection fa fa-check" . $added . "' id='collection-" . $collection->id . "'></i>
	                                <a class='collection-item-popup' href=\"" . $collection->collection_link . "\">" . $collection->title . "</a>
	                            </li>";
					}
					?>
				</ul>
				<div class="create-new-collection">
					<a href="#create-collection-modal" data-toggle="modal" id="create-new-collection">
						<i class='fa fa-plus'></i>
						<?php echo JText::_("COM_JUDOWNLOAD_CREATE_A_NEW_COLLECTION"); ?>
					</a>
				</div>
			</div>
			<input type="hidden" name="token" value="<?php echo JSession::getFormToken(); ?>">
		</div>

		<div class="modal fade" id="create-collection-modal" tabindex="-1" role="dialog"
		     aria-labelledby="create-collection-modal-label" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
                    <form id="judl-form-addcollection" action="" method="post" class="form-horizontal">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title" id="create-collection-modal-label">
                                <?php echo JText::_("COM_JUDOWNLOAD_CREATE_A_NEW_COLLECTION"); ?>
                            </h3>
                        </div>
                        <div class="modal-body">
                            <div class="form-group field-group">
                                <label class="control-label col-sm-2" for="cl-title">
                                    <?php echo JText::_("COM_JUDOWNLOAD_FIELD_TITLE"); ?>
                                    <span class="required">*</span>
                                </label>

                                <div class="col-sm-10">
                                    <input id="cl-title" type="text" name="title" size="53" data-rule-required="true" aria-describedby="cl-title-error"/>
                                    <div class="help-block" style="display: none" id="cl-input-error"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="cl-description"><?php echo JText::_("COM_JUDOWNLOAD_FIELD_DESCRIPTION"); ?></label>

                                <div class="col-sm-10">
                                    <textarea name="description" id="cl-description" class="form-control" rows="3"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2"><?php echo JText::_("COM_JUDOWNLOAD_FIELD_PRIVATE"); ?></label>
                                <div class="col-sm-10">
                                    <label class="radio">
                                        <input type="radio" name="private" value="1"/> <?php echo JText::_("COM_JUDOWNLOAD_ONLY_ME_CAN_VIEW_THIS_COLLECTION"); ?>
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="private" value="0" checked/> <?php echo JText::_("COM_JUDOWNLOAD_ANYONE_CAN_VIEW_THIS_COLLECTION"); ?>
                                    </label>
                                </div>
                            </div>

                            <?php echo JHtml::_('form.token'); ?>
                            <input type="hidden" name="doc_id" value="<?php echo $this->item->id; ?>"/>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default btn-primary"><?php echo JText::_("COM_JUDOWNLOAD_SUBMIT"); ?></button>
                            <button type="reset" class="btn btn-default"><?php echo JText::_("COM_JUDOWNLOAD_RESET"); ?></button>
                        </div>
                    </form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
	<?php
	} ?>
	</div>
</div>
<!-- /.actions -->