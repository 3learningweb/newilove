<?php
/**
 * ------------------------------------------------------------------------
 * JUDownload for Joomla 2.5, 3.x
 * ------------------------------------------------------------------------
 *
 * @copyright      Copyright (C) 2010-2015 JoomUltra Co., Ltd. All Rights Reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @author         JoomUltra Co., Ltd
 * @website        http://www.joomultra.com
 * @----------------------------------------------------------------------@
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$fb_app_id = trim($this->params->get('fb_app_id'));
if ($fb_app_id != "")
{
	$document = JFactory::getDocument();
	$document->addCustomTag('<meta property="fb:app_id" content="' . $fb_app_id . '"/>');
}
?>
<div id="judl-comments" class="clearfix">
	<div id="fb-root"></div>
	<script>
		//<![CDATA[
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/all.js#xfbml=1<?php echo $fb_app_id ? "&appId=" . $fb_app_id : ""; ?>&version=v2.3";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		//]]>
	</script>
	<div class="fb-comments"
	     data-href="<?php echo JUri::root() . JUDownloadHelperRoute::getDocumentRoute($this->item->id); ?>"
	     data-numposts="10"
	     data-colorscheme="light"
		></div>
</div>