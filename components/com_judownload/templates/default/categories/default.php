<?php
/**
 * ------------------------------------------------------------------------
 * JUDownload for Joomla 2.5, 3.x
 * ------------------------------------------------------------------------
 *
 * @copyright      Copyright (C) 2010-2015 JoomUltra Co., Ltd. All Rights Reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @author         JoomUltra Co., Ltd
 * @website        http://www.joomultra.com
 * @----------------------------------------------------------------------@
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>

<div id="judl-container" class="jubootstrap component judl-container view-categories <?php echo $this->pageclass_sfx; ?>">
	<?php if ($this->params->get('show_page_heading'))
	{
		?>
		<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
	<?php
	} ?>

	<?php if ($this->params->get('all_categories_show_title', 1))
	{
		?>
		<h2 class="cat-title"><?php echo $this->category->title; ?></h2>
	<?php
	} ?>

	<?php
	// Total category level 1 (virtual level)
	$totalCategoryLevel1 = 0;
	foreach ($this->all_categories AS $category)
	{
		if ($category->level == ($this->category->level + 1))
		{
			$totalCategoryLevel1++;
		}
	}

	// Get total columns
	$columns = (int) $this->params->get('all_categories_columns', 2);
	if (!is_numeric($columns) || ($columns <= 0))
	{
		$columns = 1;
	}
	$this->subcategory_bootstrap_columns = JUDownloadFrontHelper::getBootstrapColumns($columns);

	// Row and column class
	$rows_class = htmlspecialchars($this->params->get('all_categories_row_class', ''));
	$columns_class = htmlspecialchars($this->params->get('all_categories_column_class', ''));

	// Index row
	$indexRow = 0;

	// Index column
	$indexColumn = 0;

	// Index element in array
	$indexElementArray = 0;
	?>
	<div
		class="categories-row <?php echo $rows_class; ?> categories-row-<?php echo $indexRow + 1; ?> clearfix row">
		<?php
		foreach ($this->all_categories AS $category)
		{
		if ($category->level == ($this->category->level + 1))
		{
		$indexElementArray++;
		?>
		<div
			class="categories-col <?php echo $columns_class; ?> categories-col-<?php echo $indexColumn + 1; ?> col-md-<?php echo $this->subcategory_bootstrap_columns[$indexColumn]; ?>">

			<div class="category-top clearfix">
				<?php 
					$images = json_decode($category->images);
					if($images->intro_image){
				?>
					<img class="category-image" src="<?php echo JURi::root(true). "/media/com_judownload/images/category/intro/".$images->intro_image; ?>" />
				<?php } ?>

				<h3 class="category-title">
					<a href="<?php echo JRoute::_(JUDownloadHelperRoute::getCategoryRoute($category->id)); ?>"><?php echo $category->title; ?></a>
				</h3>

				<?php 
					$couter = array();
					$couter[] = '<span class="total-categories"><span>' . $category->total_nested_categories . '</span> ' . JText::_('COM_JUDOWNLOAD_CATEGORIES') . '</span>';
					$couter[] = '<span class="total-documents"><span>' . $category->total_documents . '</span> ' . JText::_('COM_JUDOWNLOAD_DOCUMENTS') . '</span>';
					echo '<div class="category-counter">(' . implode(" / ", $couter) . ')</div>';
				?>

				<div class="category-description"><?php echo JUDownloadFrontHelperString::truncateHtml($category->introtext, 150, ''); ?></div>
			</div>

			<?php
				if ($category->total_childs > 0)
				{
					$this->new_parent_id = $category->id;
					$this->cat_regular_level = 1;
					echo $this->loadTemplate('categories');
				}
			?>
			<!-- /.panel -->
		</div>
		<!-- /.categories-col -->
		<?php
		$indexColumn++;
		if ((($indexColumn % $columns) == 0) && ($indexElementArray < $totalCategoryLevel1))
		{
		$indexRow++;
		// Reset index column
		$indexColumn = 0;
		?>
	</div>
	<div class="categories-row <?php echo $rows_class; ?> categories-row-<?php echo $indexRow; ?> clearfix row">
		<?php
		} // end if column
		} // end if level + 1
		} // end foreach
		?>
	</div>
	<!-- /.categories-row -->
</div> <!-- /#judl-container -->
