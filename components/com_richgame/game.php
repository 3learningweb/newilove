<?php 
	define( '_JEXEC', 1 );
	define('JPATH_BASE', '../../');
	define( 'DS', DIRECTORY_SEPARATOR );
	require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
	//require_once ( JPATH_BASE .DS.'libraries'.DS.'phputf8'.DS.'utf8.php' );
	
	$mainframe =& JFactory::getApplication('site');
	$mainframe->initialise();
?>
<script>var site_url = "<?php echo JURI::base(); ?>"</script>

<link type="text/css" rel="stylesheet" href="<?php echo JURI::base(); ?>assets/css/style.css" />
<link type="text/css" rel="stylesheet" href="<?php echo JURI::base(); ?>assets/css/role.css" />
<link type="text/css" rel="stylesheet" href="<?php echo JURI::base(); ?>assets/css/extra_view.css" />
	
<div id="richgame_block" style="width: 800px; height:650px;">
	<div class="game_page-header">
		<div class="title">
			愛情博士大作戰
		</div>
	</div>
	
	<div class="role role-1" id="player1">
		<!-- <div class="money">2039</div> -->
		<div class="notice normal-notice">
			<div class="tip">點擊骰子即可開始遊戲</div>
		</div>
		<div class="notice chance-notice">
			<div class="tip"></div>
			<div class="buttons">
				<span class="btn" id="chance_btn">確定</span>
			</div>
		</div>
		<div class="notice confirm-notice">
			<img src="<?php echo JURI::base(); ?>assets/images/question_icon.png" alt="" />
			<span class="qtype"></span>
			<div class="tip">生日或紀念日時送上禮物</div>
			<div class="buttons">
				<div class="option_btn" id="btn1" style="display: none;">
					<input type="radio" name="r_value" class="radio_btn" id="radio1" />
					<span class="btn option_text"></span>
				</div>
				
				<div class="option_btn" id="btn2" style="display: none;">
					<input type="radio" name="r_value" class="radio_btn" id="radio2" />
					<span class="btn option_text"></span>
				</div>
				
				<div class="option_btn" id="btn3" style="display: none;">
					<input type="radio" name="r_value" class="radio_btn" id="radio3" />
					<span class="btn option_text"></span>
				</div>
				
				<div class="option_btn" id="btn4" style="display: none;">
					<input type="radio" name="r_value" class="radio_btn" id="radio4" />
					<span class="btn option_text"></span>
				</div>
			</div>
		</div>
		<div class="notice certificate-notice">
			<div class="tip">遊戲結束，恭喜你過關，以下是您的畢業證書</div>
			<img src="" href="" id="certificate" alt="證書" /><br/>
			<a href="" title="證書下載" target="_blank">
				<span class="btn" id="download" style="color: #FFFFFF">下載</span>
			</a>
			<span class="btn" id="again">重新挑戰</span>
		</div>
		
		<div class="notice gameover-notice">
			<div class="tip">GAME OVER</div>
			<span class="btn" id="again">重新挑戰</span>
		</div>
	</div>
	
	<div class="actions actions-1" id="action-panel-1">
		<div class="block-item blocker" id="blocker1"><span></span></div>
		<div class="block-item blocker" id="blocker2"><span></span></div>
		<div class="block-item blocker" id="blocker3"><span></span></div>
		<div class="saizi"></div>
	</div>

	<div class="arrow">
		<img src="<?php echo JURI::base(); ?>assets/images/arrow.png" alt="arrow" />
		<span>點擊骰子就可以開始遊戲了~</span>
	</div>

	<div class="chance_1"></div>
	<div class="chance_2"></div>
	
	<!-- <div class="game_img"></div> -->
</div>
	
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/js/config.js"></script>
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/js/mapConfig.js"></script>
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/js/ClassBase.js"></script>
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/js/ClassSound.js"></script>
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/js/ClassMapElement.js"></script>
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/js/ClassMap.js"></script>
	
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/js/person.js"></script>
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/js/block.js"></script>
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/js/extra.js"></script>
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/js/model.js"></script>
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/js/game.js"></script>
	
	<!-- fancybox -->
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="<?php echo JURI::base(); ?>assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo JURI::base(); ?>assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
	
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery("#certificate").fancybox();
			
			jQuery(".radio_btn").mouseover(function() {
				jQuery(this).attr("checked", true);
			});
			
			jQuery(".radio_btn").mouseout(function() {
				jQuery(this).attr("checked", false);
			});
		});
		
		var imageCache=loadImage(IMAGE_LIST,init);
		var engine;
		function init(){
			engine = new Game();
		
			$(window).bind("click,dblclick", function(evt) {
				evt.preventDefault();
			});
		}
	</script>	
