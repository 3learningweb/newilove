<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_menu
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

?>
<script>
;(function(window, $){
	$(function(){
		var <?php echo $menuid; ?> = $('#<?php echo $menuid; ?>');
		var open<?php echo $menuid; ?> = $('#open-<?php echo $menuid; ?>');
		
		open<?php echo $menuid; ?>.bind('click', function(){
			if(<?php echo $menuid; ?>.is(':hidden')){
				$('.menu').hide();}

			<?php echo $menuid; ?>.toggle({speed: 500});
		});
		
		$(document).bind('responsive', initMenuState);
	});
	
	var initMenuState = function(){
		if(window.responsive.platformId == 3){			
			<?php echo $menuid; ?>.hide();
		} else {
			<?php echo $menuid; ?>.show();
		}
	}
})(window, jQuery);
