<?php
/**
 * @version		:  2011-07-07 05:07:21$
 * @author		 
 * @package		Tabs
 * @copyright	Copyright (C) 2011- . All rights reserved.
 * @license		
 */

// no direct access
defined('_JEXEC') or die;
?>
<script>
jQuery(document).ready(function () {
	$ = jQuery;
	var tabs_num = jQuery('#<?php echo $idTag; ?> .tab-title').length;
	
	jQuery('#<?php echo $idTag; ?> .tab-separator').each(function(index, obj){
		var offset = <?php echo $start; ?> + ( (index + 1) * <?php echo $separatorLeft; ?> );
		var top = <?php echo $separatorTop; ?>;
		jQuery(obj).css('left', offset + 'px');
		jQuery(obj).css('top', top + 'px');
	});
	
//	jQuery('#<?php echo $idTag; ?> .tab-title').each(function(index, obj){
//		var offset = <?php echo $start; ?> + ( (tabs_num - index -1) * <?php echo $titleLeft; ?> );
//		jQuery(obj).css('left', offset + 'px');
//	});
	
	jQuery('#<?php echo $idTag; ?> .tab-link').each(function(index, obj){		
		jQuery(obj).bind('click focus', function(){
			jQuery('#<?php echo $idTag; ?>  .tab-link').removeClass("active");
			jQuery(this).addClass("active");
			jQuery('#<?php echo $idTag; ?> .tab-content').hide().eq(index).show();
	    });
	});
	
	jQuery('#<?php echo $idTag; ?> .tab-link').eq(0).click();
	
	$('#<?php echo $idTag; ?> .tab-link').mouseover(function(){
		$(this).addClass('hover');
	}).mouseout(function(){
		$(this).removeClass('hover');
	});
});
