<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_leftmenu
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_leftmenu
 *
 * @package     Joomla.Site
 * @subpackage  mod_leftmenu
 * @since       1.5
 */
class ModLeftmenuHelper
{
	/**
	 * Retrieve list of leftmenu
	 *
	 * @param   JRegistry  &$params  module parameters
	 *
	 * @return  mixed
	 */
	public static function &getMenu(&$params)
	{
		$app = JFactory::getApplication();
		$itemid = $app->input->getInt("Itemid");
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(true);
		$query->select("*");
		$query->from($db->quoteName('#__menu'));
		$query->where("id = '{$itemid}'");
		$db->setQuery($query);
		$menu = $db->loadObject();

		if($menu->parent_id == 1) {
			$menu_id = $menu->id; 
		}else{
			$menu_id = $menu->parent_id;
		}
		
		$query_items = $db->getQuery(true);
		$query_items->select("*");
		$query_items->from($db->quoteName("#__menu"));
		$query_items->where("id = '{$menu_id}' OR parent_id = '{$menu_id}'");
		$query_items->where("published = '1'");
		$query_items->order("lft ASC");
		$db->setQuery($query_items);
		$items = $db->loadObjectList();

		return $items;
	}

	/**
	 * Get base menu item.
	 *
	 * @param   JRegistry  &$params  The module options.
	 *
	 * @return   object
	 *
	 * @since	3.0.2
	 */
	public static function getBase(&$params)
	{
		// Get base menu item from parameters
		if ($params->get('base'))
		{
			$base = JFactory::getApplication()->getMenu()->getItem($params->get('base'));
		}
		else
		{
			$base = false;
		}

		// Use active menu item if no base found
		if (!$base)
		{
			$base = self::getActive($params);
		}

		return $base;
	}

	/**
	 * Get active menu item.
	 *
	 * @param   JRegistry  &$params  The module options.
	 *
	 * @return  object
	 *
	 * @since	3.0.2
	 */
	public static function getActive(&$params)
	{
		$menu = JFactory::getApplication()->getMenu();

		return $menu->getActive() ? $menu->getActive() : $menu->getDefault();
	}
}
