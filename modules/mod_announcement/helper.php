<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_announcement
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_announcement
 *
 * @package     Joomla.Site
 * @subpackage  mod_announcement
 * @since       1.5
 */
class ModAnnouncementHelper
{
	/**
	 * Retrieve list of banners
	 *
	 * @param   JRegistry  &$params  module parameters
	 *
	 * @return  mixed
	 */
	public static function &getList(&$params)
	{
		$limit = $params->get('limit');
		$order = $params->get('order');
		$type = "";
		$cat = $params->get('cat');
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		
		$query->select("a.*");
		$query->from($db->quoteName('#__content') . ' AS a');
		$query->where("a.state = '1'");
		$query->where("a.catid = '{$cat}'");
		
		// Filter by publish
		$nullDate = $db->Quote($db->getNullDate());
		$date = JFactory::getDate();
		$nowDate = $db->Quote($date->toSql());
		$query->where('(a.publish_up = ' . $nullDate . ' OR a.publish_up <= ' . $nowDate . ')');
		$query->where('(a.publish_down = ' . $nullDate . ' OR a.publish_down >= ' . $nowDate . ')');
		
		$query->order('a.'. $order. ' limit 0, '. $limit);

		$db->setQuery($query);

		return $db->loadObjectList();
	}
}
