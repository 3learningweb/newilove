<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_banners
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_banners
 *
 * @package     Joomla.Site
 * @subpackage  mod_banners
 * @since       1.5
 */
class ModBannersChHelper
{
	/**
	 * Retrieve list of banners
	 *
	 * @param   JRegistry  &$params  module parameters
	 *
	 * @return  mixed
	 */
	public static function &getBanner(&$params)
	{
		$app = JFactory::getApplication();
		$itemid = $app->input->getInt("Itemid");
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(true);
		$query->select("*");
		$query->from($db->quoteName('#__menu'));
		$query->where("id = '{$itemid}'");
		$db->setQuery($query);
		$menu = $db->loadObject();

		if($menu->parent_id == 1) {
			$menu_id = $menu->id; 
		}else{
			$menu_id = $menu->parent_id;
		}
		
		$heart 	= $params->get('heart_time_id');
		$date	= $params->get('date_time_id');
		$interaction = $params->get('interaction_time_id');
		$maintain = $params->get('maintain_time_id');

		switch($menu_id) {
			case $heart :
				$banner = $params->get('heart_time_img');
				break;
			case $date :
				$banner = $params->get('date_time_img');
				break;
			case $interaction :
				$banner = $params->get('interaction_time_img');
				break;
			case $maintain :
				$banner = $params->get('maintain_time_img');
				break;
			default:
				$banner = $params->get('love_time_img');
		}

		return $banner ;
	}
}
