<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_hit
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$menu = $app->getMenu();
$menulink = $menu->getItem($menuid)->link;

?>
<div class="mod_hit">
	<span class="hit_name"><?php echo JText::_("MOD_HIT"); ?></span>
	<div class="hit_block">
		<div class="datablock">
			<div class="h_title"><?php echo $h_title; ?></div>
			<div class="h_hr"></div>
			<div class="h_content"><?php echo preg_replace("/\\n/", "<br />", $h_content);?></div>
			<div class="h_more">
				<a href="<?php echo $menulink . "&Itemid=" . $menuid; ?>" title="<?php echo JText::_('MOD_HIT_MORE'); ?>">
					<?php echo JText::_('MOD_HIT_MORE'); ?>
				</a>
			</div>
			<div class="h_youtube">
				<embed src="http://www.youtube.com/v/<?php echo $h_youtube; ?>&version=3" type="application/x-shockwave-flash" wmode="transparent" allowfullscreen="true" title="<?php echo $item->title; ?>" />
			</div>
		</div>
	</div>
</div>
