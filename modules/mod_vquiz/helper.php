<?php

/*------------------------------------------------------------------------

# mod_vquiz - vQuiz

# ------------------------------------------------------------------------

# author Team WDMtech

# copyright Copyright (C) 2015 www.wdmtech.com. All Rights Reserved.

# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Websites: http://www.wdmtech.com

# Technical Support: Forum - http://www.wdmtech.com/support-forum

-----------------------------------------------------------------------*/

// no direct access

defined('_JEXEC') or die('Restricted access');

class modvQuizHelper

{

	static function getItems($params)

	{

			$app = JFactory::getApplication();

			$lang = JFactory::getLanguage();	

			//print_r($params);

			$id = $params->def('cid', 0);

			//echo $id;

			//jexit('exit');

			$ordering = $params->get('ordering');

			$featured = $params->get('featured');

			$limit = (int)$params->get('numberofquizzes', 5);

			$db = JFactory::getDBO();

			$user = JFactory::getUser();

			$date = JFactory::getDate();

			

				//recent Played quizzes

				if($ordering==3){

				$query = 'select i.* ,(c.quiztitle) as categoryname,c.id as categoryid from #__vquiz_quizzes as i left join ( select max(id) as id, quizzesid from #__vquiz_quizresult group by quizzesid) as r ON r.quizzesid=i.id LEFT JOIN #__vquiz_category as c ON i.quiz_categoryid = c.id where i.published = 1';

				}

				else

				{

				$query = 'select i.*, count(r.id) as items ,count(r.quizzesid) as playedquiz,(c.quiztitle) as categoryname ,c.id as categoryid from #__vquiz_quizzes as i left join #__vquiz_quizresult as r ON r.quizzesid=i.id LEFT JOIN #__vquiz_category as c ON i.quiz_categoryid = c.id where i.published = 1';

				}

				

				

				$where = array();

				

				$where[] = 'i.access in ('.implode(',', $user->getAuthorisedViewLevels()).')';

				

				//$where[] = ' i.startpublish_date <= '.$db->quote($date->format('Y-m-d')).' and i.endpublish_date >= '.$db->quote($date->format('Y-m-d'));

				

				$where[] = ' i.startpublish_date <= '.$db->quote($date->format('Y-m-d')).' and (i.endpublish_date >= '.$db->quote($date->format('Y-m-d')).' or i.endpublish_date='.('0000-00-00').')';

				

				if($app->getLanguageFilter())	{

					$where[] = 'i.language in ('.$db->quote($lang->getTag()).', '.$db->Quote('*').')';

				}



				

				if($id)	{

					$query .= ' and i.quiz_categoryid = '.$db->quote($id);

				}

				if($featured==1)	{

					$query .= ' and i.featured =1';

				}



				//	latest_quizzes	

				if($ordering==1)	{

					$query .= ' group by i.id order by i.created_date desc';

				}



				//most Played quizzes

				elseif($ordering==2)	{

					$query .= ' group by i.id order by items desc';

				}



				//recent Played quizzes

				elseif($ordering==3)	{

					$query .= ' order by r.id desc';

				}



				//random played quizzes

				elseif($ordering==4)	{

					$query .= ' group by i.id ORDER BY RAND()';

				}

				else

				{

					$query .= ' order by created_date desc';

				}



				

				$query .= ' limit '.$limit;



				$db->setQuery( $query );

				$items = $db->loadObjectList();

				//echo $db->getErrorMsg();



				return $items;



				



	}



 

}