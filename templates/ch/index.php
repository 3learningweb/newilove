<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Get Reponsive params
$platformId = $app->getUserState('responsive.platformId');
$userSwitch = $app->getUserState('responsive.userSwitch');


// Get template params
$sitetitle = $app->getTemplate(true)->params->get('sitetitle');
$home_ids = explode(",", $app->getTemplate(true)->params->get('home_id'));


// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');
$MetaDesc = $app->getCfg('MetaDesc');


//rename title (if home page)
if (in_array($itemid, $home_ids)) {
	$doc->setTitle($sitetitle);
} else {
	$doc->setTitle($doc->getTitle() . " | " . $sitetitle);
}


// Remove meta generator tag
$this->setGenerator($sitetitle);

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript('templates/' . $this->template . '/js/template.js');


// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Add current user information
$user = JFactory::getUser();

$filter = JFilterInput::getInstance();
$uri = JURI::getInstance();
$uri = $filter->clean($uri, 'string');
$uri = htmlspecialchars($uri);

if ($doc->getMetaData( 'og:description') == "") {
	$doc->setMetaData( 'og:description', $MetaDesc);	// 描述
}

if ($doc->getMetaData( 'og:image') == "") {
	$doc->setMetaData('og:image', "https://ilove.moe.edu.tw/filesys/images/system/logo.png");	// 圖片
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
	<head>
		<jdoc:include type="head" />
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="og:type" content="article">
	<meta property="og:site_name" content="<?php echo $sitename;?>">

		<?php
		// Add CSS & JS
		$doc->addStyleSheet('templates/system/css/reset.css');
		$doc->addStyleSheet('templates/system/css/site.css');
		$doc->addStyleSheet('templates/system/css/layout.css');
		$doc->addStyleSheet('templates/system/css/style.css');
		$doc->addStyleSheet('templates/' . $this->template . '/css/site.css');
		$doc->addStyleSheet('templates/' . $this->template . '/css/layout.css');
		$doc->addStyleSheet('templates/' . $this->template . '/css/style.css');
		

		if (($platformId == 3 && $userSwitch == 3) || $platformId != 3):
			if ($platformId != 3): ?>
				<?php
				if ( !strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 8' )  && !strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 7' )) {
					$doc->addStyleSheet('templates/' . $this->template . '/css/w1000.css', 'text/css', 'only screen and (min-width: 1000px)');
					$doc->addStyleSheet('templates/' . $this->template . '/css/w6501000.css', 'text/css', 'only screen and (min-width: 650px) and (max-width: 999px)');
					$doc->addStyleSheet('templates/' . $this->template . '/css/w650.css', 'text/css', 'only screen and (max-width: 649px)');
				}
				?>

			<!--[if lt IE 9]>
				<?php
				if ( strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 8' )  || strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 7' )) {
					$doc->addStyleSheet('templates/' . $this->template . '/css/w1000.css', 'text/css', 'only screen and (min-width: 982px)');
					$doc->addStyleSheet('templates/' . $this->template . '/css/w6501023.css', 'text/css', 'only screen and (min-width: 650px) and (max-width: 981px)');
					$doc->addStyleSheet('templates/' . $this->template . '/css/w650.css', 'text/css', 'only screen and (max-width: 649px)');
				}
				?>
			<![endif]-->
			<?php
			endif; ?>
			<?php
			$doc->addStyleSheet('templates/' . $this->template . '/css/w1000.css', 'text/css', 'only screen and (min-width: 1000px)');
			$doc->addStyleSheet('templates/' . $this->template . '/css/w6501000.css', 'text/css', 'only screen and (min-width: 650px) and (max-width: 999px)');
			$doc->addStyleSheet('templates/' . $this->template . '/css/w650.css', 'text/css', 'only screen and (max-width: 649px)');
			$doc->addScript('templates/system/js/respond.min.js');
		endif;
		?>		
		<!--[if lt IE 9]>
			<script src="templates/system/js/html5shiv.js"></script>
			<script src="/media/jui/js/html5.js"></script>
		<![endif]-->

	<script>
	jQuery(document).ready(function() {
		jQuery("#retopmenu").append(jQuery(".itp-gs_research"));
		jQuery("#retopmenu").append(jQuery(".mod_retopshare"));
	});
	</script>

	</head>
	<body class="<?php
					echo $option
					 . ' view-' . $view
					 . ($layout ? ' layout-' . $layout : ' no-layout')
					 . ($task ? ' task-' . $task : ' no-task')
					 . ($itemid ? ' itemid-' . $itemid : '');
					?>">
		<!-- Body -->
		<div class="all">
			<div class="container">
				<?php 
					if (in_array($itemid, $home_ids)) :	// 首頁 item id
						require_once(JPATH_BASE . DIRECTORY_SEPARATOR . "templates/" . $this->template . "/home.php");
					else :								// 內頁
						require_once(JPATH_BASE . DIRECTORY_SEPARATOR . "templates/" . $this->template . "/tmpl.php");
					endif;
				?>
			</div>
		</div>
		<?php if ($platformId == 3 && $userSwitch == 1): ?>
			<div class="pda_change">
				<form action="<?php echo $uri; ?>" method="post" >
					<input type="hidden" value="3" name="userswitch" id="userswitch"/>
					<input type="submit" value="切換手機版"/>
				</form>
			</div>

		<?php elseif ($platformId == 3 && $userSwitch == 3): ?>
			<div class="pda_change">
				<form action="<?php echo $uri; ?>" method="post" >
					<input type="hidden" value="1" name="userswitch" id="userswitch"/>
					<input type="submit" value="切換電腦版"/>
				</form>
			</div>
		<?php endif; ?>
		<jdoc:include type="modules" name="debug" style="none" />
	</body>
</html>
