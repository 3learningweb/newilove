<script>
	
	jQuery(document).ready(function() {
		jQuery(".listtable tbody tr:nth-child(even)").addClass("even");
		jQuery(".listtable tbody tr:nth-child(odd)").addClass("odd");

		jQuery(".datatable thead th:nth-child(even)").addClass("even");
		jQuery(".datatable thead th:nth-child(odd)").addClass("odd");
		jQuery(".datatable tbody tr:nth-child(even)").addClass("even");
		jQuery(".datatable tbody tr:nth-child(odd)").addClass("odd");
		jQuery(".datatable tbody tr td:nth-child(even)").addClass("even");
		jQuery(".datatable tbody tr td:nth-child(odd)").addClass("odd");		
	});
</script>

<div class="theme_top">
	<!-- Header -->
	<div class="header">
		<div class="header-logo">
			<jdoc:include type="modules" name="ct_logo" />
		</div>
		<div class="header-menu">
			<jdoc:include type="modules" name="ct_topmenu" />
			<jdoc:include type="modules" name="ct_search" />
			<jdoc:include type="modules" name="ct_topshare" />
		</div>
	</div>
	
	<div class="mainmenu">
		<jdoc:include type="modules" name="ct_mainmenu" />
	</div>
	<div class="head_banner" id="tmpl_banner">
		<jdoc:include type="modules" name="ct_banner" />
	</div>
</div>

<div class="contentarea" id="contentarea_block">
	<div class="contentarea_middle">
		<jdoc:include type="modules" name="ct_middle" />
	</div>
	<div class="contentarea inner">
		<div id="left_menu">
			<jdoc:include type="modules" name="ct_leftmenu" />
		</div>
		<div id="content">
			<jdoc:include type="modules" name="ct_breadcrumbs" />
			<div id="content_title">
				<jdoc:include type="modules" name="ct_section_title" />
				<div id="content_block">
					<jdoc:include type="message" />
					<jdoc:include type="modules" name="ct_content_top" />
					<jdoc:include type="component" />
					<jdoc:include type="modules" name="ct_content_bottom" />
				</div>
			</div>
		</div>
		<div class="return">
			<div class="return_back">
				<div class="return_inner">
					<span class="icon-undo"></span>
					<a href="javascript:history.go(-1)"><span><?php echo JText::_('JGLOBAL_RETURN'); ?></span></a>
					<noscript>您的瀏覽器不支援script程式碼。請使用 "Backspace" 按鍵回到上一頁。</noscript>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Footer -->
<div class="footer clear" id="footer_tmpl">
	<div class="foot_main">
		<jdoc:include type="modules" name="ct_footmenu" />
		<jdoc:include type="modules" name="ct_footmain" />
	</div>
</div>