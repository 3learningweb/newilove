<div class="theme_top">
	<!-- Header -->
	<div class="header">
		<div class="header-logo">
			<jdoc:include type="modules" name="ct_logo" />
		</div>
		<div class="header-menu">
			<jdoc:include type="modules" name="ct_topmenu" />
			<jdoc:include type="modules" name="ct_search" />
			<jdoc:include type="modules" name="ct_topshare" />
		</div>
	</div>

	<div class="mainmenu">
		<jdoc:include type="modules" name="ct_mainmenu" />
	</div>
	<div class="head_banner" id="home_banner">
		<jdoc:include type="modules" name="ct_banner" />
	</div>
</div>

<div class="home">
	<div class="home_middle">
		<jdoc:include type="modules" name="ct_middle" />
	</div>
	<div class="home_area">
		<div class="left">
			<jdoc:include type="modules" name="ct_area_left" />
		</div>
		<div class="middle">
			<jdoc:include type="modules" name="ct_area_middle" />
		</div>
		<div class="right">
			<jdoc:include type="modules" name="ct_area_right" />
		</div>
	</div>
</div>

<!-- Footer -->
<div class="footer clear" id="footer_home">
	<div class="foot_main">
		<jdoc:include type="modules" name="ct_footmenu" />
		<jdoc:include type="modules" name="ct_footmain" />
	</div>
</div>