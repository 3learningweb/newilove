<?php defined('_JEXEC') or die('Restricted access'); ?>

<div class="row-fluid">

	<h1 class="jvle_h1"><?php echo JText::_('COM_JVLE_LINK_ADD_FEATURED'); ?></h1>
	<p><?php echo JText::sprintf('COM_JVLE_LINK_ADD_FEATURED_MSG_2', $this->cfg->flinks_subamount); ?></p>
	<ul>
    	<li><?php echo JText::_("COM_JVLE_LINK_ADD_FEATURED_MSG_3"); ?></li>
        <li><?php echo JText::_("COM_JVLE_LINK_ADD_FEATURED_MSG_4"); ?></li>
        <li><?php echo JText::sprintf('COM_JVLE_LINK_ADD_FEATURED_MSG_5', $this->cfg->self_email); ?></li>
    </ul>
	<p><?php echo JText::sprintf('COM_JVLE_LINK_ADD_FEATURED_MSG_6'); ?></p>
	
	<p style="text-align:center;">
		<a href="<?php echo $this->pay_url; ?>" title="<?php echo JText::_("COM_JVLE_LINK_ADD_FEATURED_MSG_7"); ?>">
			<img alt="<?php echo JText::_("COM_JVLE_LINK_ADD_FEATURED_MSG_7"); ?>" src="<?php echo _JVLE_IMGDIR; ?>subscribe.gif" border="0" />
		</a>
	</p>
	
</div>
