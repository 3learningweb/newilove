<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php 
JHtml::_('behavior.framework');

$sortFields = array(
					"partner_title" => JText::_("COM_JVLE_LINK_ADD_WEBTITLE"),
					"link_published_on" => JText::_("COM_JVLE_LINKS_CAT_PUBON"),
					"num_votes" => JText::_("COM_JVLE_LINKS_VOTES"),
					"link_rating" => JText::_("COM_JVLE_LINKS_RATING"),
				);

$listDirn = $this->escape($this->sortDirection);
$listOrder = $this->escape($this->sortColumn);
?>

<script type="text/javascript">
function tableOrdering()
{
	var form = document.adminForm;
 
    form.filter_order.value = document.id("sortTable").value;
    form.filter_order_Dir.value = document.id("directionTable").value;
    document.adminForm.submit();
}
</script>
	
<?php echo $this->loadTemplate("featuredlinks"); ?>

	<div id="jvle_maincontent">

		<h1 class="jvle_h1"><?php echo JText::sprintf('COM_JVLE_LINKS_IN_ALL', JvleUtil::getCategoryName($this->cid)); ?></h1>

		<form action="<?php echo htmlspecialchars(JFactory::getURI()->toString()); ?>" method="post" name="adminForm" id="adminForm">
			<div id="jvle_sorter">
				<div id="filter-bar" class="btn-toolbar">
					<div class="btn-group pull-right hidden-phone">
						<label for="directionTable" class="element-invisible"><?php echo JText::_('COM_JVLE_SELECT_ORDER');?></label>
						<select name="directionTable" id="directionTable" class="input-medium" onchange="javascript:tableOrdering();">
							<option value=""><?php echo JText::_('COM_JVLE_SELECT_ORDER');?></option>
							<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('COM_JVLE_ASC');?></option>
							<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('COM_JVLE_DESC');?></option>
						</select>
					</div>
					<div class="btn-group pull-right">
						<label for="sortTable" class="element-invisible"><?php echo JText::_('COM_JVLE_SORTBY');?></label>
						<select name="sortTable" id="sortTable" class="input-medium" onchange="javascript:tableOrdering();">
							<option value=""><?php echo JText::_('COM_JVLE_SORTBY');?></option>
							<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder);?>
						</select>
					</div>
				</div>
			</div>
	
			<input type="hidden" name="view" value="all_category_links" />
			<input type="hidden" id="filter_order" name="filter_order" value="<?php echo $listOrder; ?>" />
			<input type="hidden" id="filter_order_Dir" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		</form>
		
<?php if (count($this->rows)) : ?>

<?php 	foreach ($this->rows as $row) : ?>
<?php 		echo JvleSiteUtil::drawLinkInfo($row, _JVLE_GENCATEGORY_PAGE); ?>
<?php 	endforeach; ?>

<?php else: ?>

		<div class="pagination pagination-centered">
			<?php echo JText::_('COM_JVLE_LINKS_CAT_NOLINKS'); ?>
		</div>
	
<?php endif; ?>

	</div>

<?php echo $this->loadTemplate("latestlinks"); ?>