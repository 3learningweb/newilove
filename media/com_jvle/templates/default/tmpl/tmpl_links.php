<?php defined('_JEXEC') or die('Restricted access'); ?>

<form action="<?php echo htmlspecialchars(JFactory::getURI()->toString()); ?>" method="post" name="adminForm" id="adminForm">

<?php echo $this->loadTemplate("featuredlinks"); ?>

	<div id="jvle_maincontent">
		
		<h1 class="jvle_h1"><?php echo JText::sprintf('COM_JVLE_LINKS_IN', JvleUtil::getCategoryName($this->cid)); ?></h1>

<?php if (count($this->rows)) : ?>

<?php 	$allurl = JRoute::_("index.php?option=com_jvle&Itemid="._JVLE_ITEMID."&view=all_category_links&cid=".$this->cid); ?>

		<p class="menu">
			<a href="<?php echo $allurl; ?>" title=""><?php echo JText::_('COM_JVLE_LINKS_SHOW_ALL'); ?></a>
		</p>
	
<?php 	foreach ($this->rows as $row) : ?>
<?php 		echo JvleSiteUtil::drawLinkInfo($row, _JVLE_GENCATEGORY_PAGE); ?>
<?php 	endforeach; ?>

		<div class="pagination pagination-centered">
			<?php echo $this->pagination->getListFooter(); ?>
		</div>

<?php else: ?>

		<div class="pagination pagination-centered">
			<?php echo JText::_('COM_JVLE_LINKS_CAT_NOLINKS'); ?>
		</div>

<?php endif; ?>

	</div>

<?php echo $this->loadTemplate("latestlinks"); ?>

<input type="hidden" name="view" value="links" />
</form>