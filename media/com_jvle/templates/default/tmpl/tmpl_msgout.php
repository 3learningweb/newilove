<?php defined('_JEXEC') or die('Restricted access'); ?> 

<?php $alert_type = ($this->error) ? 'alert-danger' : 'alert-info'; ?>

<?php if ((isset($this->dispmsg)) && ($this->dispmsg != '')) : ?>
<div class="alert <?php echo $alert_type; ?>">
	<strong><?php echo htmlspecialchars($this->dispmsg); ?></strong>
</div>
<?php endif; ?>