<?php defined('_JEXEC') or die('Restricted access'); ?>

<div id="jvle_dir">

    <div class="row-fluid">
    	<div class="span12">
    		<h2 class="jvle_page_title"><?php echo htmlspecialchars($this->cfg->fe_title); ?></h2>
    <?php if ($this->cfg->header_slogan != '') : ?>
        	<p class="jvle_page_slogan"><?php echo htmlspecialchars($this->cfg->header_slogan); ?></p>
    <?php endif; ?>
    	</div>
    </div>    
    
    <div class="row-fluid">
    	<div class="span9">
    		<?php echo $this->loadTemplate("menu"); ?>
    	</div>
    	<div class="span3">
    		<?php echo $this->loadTemplate("search"); ?>
    	</div>	
    </div>
        
    <?php if (($this->cfg->enable_ads) && ($this->cfg->ad_top != '')) : ?>
    <div class="row-fluid">
    	<div class="span12">
    		<?php echo $this->cfg->ad_top; ?>
    	</div>	
    </div>
    <?php endif; ?>
    
    <?php echo $this->loadTemplate("msgout"); ?>
    
    <?php echo $this->loadTemplate($this->subtemplate); ?>
    
    <?php if (($this->cfg->enable_ads) && ($this->cfg->ad_bottom != '')) : ?>
    <div class="row-fluid">
    	<div class="span12">
    		<?php echo $this->cfg->ad_bottom; ?>
    	</div>	
    </div>
    <?php endif; ?>
    
    <?php echo $this->loadTemplate("footer"); ?>
        
</div>