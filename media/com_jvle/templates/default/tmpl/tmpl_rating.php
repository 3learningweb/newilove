<?php defined('_JEXEC') or die('Restricted access'); ?>

<form name="rform<?php echo $this->randnum; ?>">
    <input type="radio" name="rating<?php echo $this->randnum; ?>" value="0.5" <?php echo (($this->link->link_rating > 0) && ($this->link->link_rating <= 0.5)) ? 'checked="checked"' : ''; ?>>
    <input type="radio" name="rating<?php echo $this->randnum; ?>" value="1.0" <?php echo (($this->link->link_rating > 0.5) && ($this->link->link_rating <= 1.0)) ? 'checked="checked"' : ''; ?>>
    <input type="radio" name="rating<?php echo $this->randnum; ?>" value="1.5" <?php echo (($this->link->link_rating > 1.0) && ($this->link->link_rating <= 1.5)) ? 'checked="checked"' : ''; ?>>
    <input type="radio" name="rating<?php echo $this->randnum; ?>" value="2.0" <?php echo (($this->link->link_rating > 1.5) && ($this->link->link_rating <= 2.0)) ? 'checked="checked"' : ''; ?>>
    <input type="radio" name="rating<?php echo $this->randnum; ?>" value="2.5" <?php echo (($this->link->link_rating > 2.0) && ($this->link->link_rating <= 2.5)) ? 'checked="checked"' : ''; ?>>
    <input type="radio" name="rating<?php echo $this->randnum; ?>" value="3.0" <?php echo (($this->link->link_rating > 2.5) && ($this->link->link_rating <= 3.0)) ? 'checked="checked"' : ''; ?>>
    <input type="radio" name="rating<?php echo $this->randnum; ?>" value="3.5" <?php echo (($this->link->link_rating > 3.0) && ($this->link->link_rating <= 3.5)) ? 'checked="checked"' : ''; ?>>
    <input type="radio" name="rating<?php echo $this->randnum; ?>" value="4.0" <?php echo (($this->link->link_rating > 3.5) && ($this->link->link_rating <= 4.0)) ? 'checked="checked"' : ''; ?>>
    <input type="radio" name="rating<?php echo $this->randnum; ?>" value="4.5" <?php echo (($this->link->link_rating > 4.0) && ($this->link->link_rating <= 4.5)) ? 'checked="checked"' : ''; ?>>
    <input type="radio" name="rating<?php echo $this->randnum; ?>" value="5.0" <?php echo (($this->link->link_rating > 4.5) && ($this->link->link_rating <= 5.0)) ? 'checked="checked"' : ''; ?>>
</form>