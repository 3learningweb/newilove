<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php
	$user = JFactory::getUser();

	$main_lnk = JRoute::_("index.php?option=com_jvle&Itemid="._JVLE_ITEMID);
	$addlink_lnk = JRoute::_("index.php?option=com_jvle&Itemid="._JVLE_ITEMID."&view=addlink");
	$addbanner_lnk = JRoute::_("index.php?option=com_jvle&Itemid="._JVLE_ITEMID."&view=addbanner");
	$cls_lnk = JRoute::_("index.php?option=com_jvle&Itemid="._JVLE_ITEMID."&view=checklinkstatus");
	$recsite_lnk = JRoute::_("index.php?option=com_jvle&Itemid="._JVLE_ITEMID."&view=recommend");

	$recweb_auth = (($this->cfg->enable_recweb) && (($this->cfg->recweb_by == "all") || (($this->cfg->recweb_by == "registered") && ($user->get('id'))))) ? 1 : 0;
?>

<div class="row-fluid">
	<div class="span12" id="jvle_menu">
		<a href="<?php echo $main_lnk; ?>"><?php echo JText::_("COM_JVLE_MENU_HOME"); ?></a>

<?php if ($this->cfg->fe_addlink) : ?>
			&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo $addlink_lnk; ?>" title="<?php echo JText::_("COM_JVLE_LINK_ADD_TITLE"); ?>"><?php echo JText::_("COM_JVLE_LINK_ADD_TITLE"); ?></a>
<?php endif; ?>

<?php if ($this->cfg->fe_addbanner) : ?>
			&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo $addbanner_lnk; ?>" title="<?php echo JText::_("COM_JVLE_LINK_ADD_BANNER"); ?>"><?php echo JText::_("COM_JVLE_LINK_ADD_BANNER"); ?></a>
<?php endif; ?>

<?php if ($this->cfg->enable_checklinkstatus) : ?>
			&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo $cls_lnk; ?>" title="<?php echo JText::_("COM_JVLE_CLS_TITLE"); ?>"><?php echo JText::_("COM_JVLE_CLS_TITLE"); ?></a>
<?php endif; ?>

<?php if ($recweb_auth) : ?>
			&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo $recsite_lnk; ?>" title="<?php echo JText::_("COM_JVLE_RECSITE_TITLE"); ?>"><?php echo JText::_("COM_JVLE_RECSITE_TITLE"); ?></a>
<?php endif; ?>
	
	</div>
</div>
