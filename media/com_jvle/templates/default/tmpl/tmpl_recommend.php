<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
JHtml::_('behavior.keepalive');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
?>

<div class="row-fluid">

	<form action="<?php echo JRoute::_('index.php?option=com_jvle&view=recommend'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-vertical">

		<h1 class="jvle_h1"><?php echo JText::_('COM_JVLE_RECSITE_TITLE'); ?></h1>
		
		<?php if ($this->cfg->recommend_site_intro != '') : ?>
		<div class="well well-small">
			<?php echo stripslashes($this->cfg->recommend_site_intro); ?>
		</div>
		<?php endif; ?>		
		
		<div class="control-group">
	       	<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
	        <div class="controls"><?php echo $this->form->getInput('name'); ?></div>
		</div>

		<div class="control-group">
	       	<div class="control-label"><?php echo $this->form->getLabel('email'); ?></div>
	        <div class="controls"><?php echo $this->form->getInput('email'); ?></div>
		</div>
		
		<div class="control-group">
	       	<div class="control-label"><?php echo $this->form->getLabel('url'); ?></div>
	        <div class="controls"><?php echo $this->form->getInput('url'); ?></div>
		</div>
		
		<div class="control-group">
	       	<div class="control-label"><?php echo $this->form->getLabel('reason'); ?></div>
	        <div class="controls"><?php echo $this->form->getInput('reason'); ?></div>
		</div>
								
		<div class="control-group">
	       	<div class="control-label"><?php echo $this->form->getLabel('captcha'); ?></div>
	        <div class="controls"><?php echo $this->form->getInput('captcha'); ?></div>
		</div>

		<div class="control-group">
		    <div class="controls">
		    	<button type="submit" name="submit" id="submit" class="btn btn-primary"><?php echo JText::_('COM_JVLE_SUBMIT'); ?></button>
		    	<button type="reset" name="reset" id="reset" class="btn btn-warning"><?php echo JText::_('COM_JVLE_RESET'); ?></button>
		    </div>
		</div>

	<input type="hidden" name="mode" value="1" />
	<?php echo JHtml::_('form.token'); ?>
	</form>

</div>
