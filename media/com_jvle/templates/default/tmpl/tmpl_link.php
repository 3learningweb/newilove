<?php defined('_JEXEC') or die('Restricted access'); ?>

<div class="row-fluid">
	<div class="span12">
		<div class="jvle_link">
		
<?php if ($this->thumbnail != '') : ?>
	<?php echo $this->thumbnail; ?>
<?php endif; ?>
	
			<div class="info">
<?php if ($this->link->exchange_link) :?>
				<span class="title">
					<a <?php echo $this->window; ?> href="<?php echo JvleSiteUtil::getPartnerLinkURL($this->link); ?>">
						<?php echo stripslashes($this->link->partner_title); ?>
					</a>
				</span>
<?php 	if ($this->link->partner_desc != '') : ?>
				<span class="desc">
					<?php echo stripslashes($this->link->partner_desc); ?>
				</span>
<?php 	endif; ?>
<?php else: ?>
				<span class="desc">
					<a <?php echo $this->window; ?> href="<?php echo JvleSiteUtil::getPartnerLinkURL($this->link); ?>">
						<img src="<?php echo _JVLE_BANNERS.$this->link->banner_loc; ?>" border="0" alt="<?php echo stripslashes($this->link->partner_title); ?>" />
					</a>
				</span>
<?php endif; ?>
			</div>
		
			<div class="misc">
<?php if ($this->rating != '') : ?>
				<div style="margin-bottom:2px;"><?php echo $this->rating; ?></div>
<?php endif; ?>
<?php if ($this->link->featured) : ?>
				<span class="label label-warning">* <?php echo JText::_("COM_JVLE_LINKS_FEATURED"); ?></span>
<?php endif; ?>
<?php if (($this->link->link_type == "ONE-WAY") && ($this->cfg->track_hits)) : ?>
				<span class="label label-info"><?php echo JText::_("COM_JVLE_LINKS_VISITS"); ?>: <?php echo $this->link->hits; ?></span>
<?php endif; ?>
<?php if ($this->rating != '') : ?>
				<span class="label label-success"><?php echo $this->link->num_votes." ".JText::_("COM_JVLE_LINKS_VOTES"); ?> </span>
				<span class="label label-info"><?php echo JText::_("COM_JVLE_LINKS_RATING").": ".round($this->link->link_rating, 1); ?> </span>
<?php endif; ?>
			</div>
			
		</div>		
	</div>
</div>