<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php if ($this->cfg->enable_search) : ?>

<div class="row-fluid">
	<div class="span12">
		<div class="jvle_search">
			<form action="<?php echo JRoute::_('index.php?option=com_jvle&Itemid='._JVLE_ITEMID.'&view=search'); ?>" method="post">
		    <p>
		        <input class="input input-medium" type="text" name="query" size="20" value="<?php echo JText::_("COM_JVLE_SEARCH"); ?>" maxlength="20" onblur="if (this.value == '') this.value='<?php echo JText::_("COM_JVLE_SEARCH"); ?>';" onfocus="if (this.value=='<?php echo JText::_("COM_JVLE_SEARCH"); ?>') this.value='';" />
		    </p>
		    <input type="hidden" name="view" value="search" />
		    <input type="hidden" name="option" value="com_jvle" />
		    </form>
		</div>
	</div>
</div>

<?php endif; ?>