<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
JHtml::_('behavior.keepalive');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
?>

<div class="row" id="jvle_addlink">

    <div class="col-md-12">

    	<form action="<?php echo JRoute::_('index.php?option=com_jvle&view=addlink'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-horizontal">
    
    		<h1 class="jvle_h1"><?php echo JText::_('COM_JVLE_LINK_ADD_TITLE'); ?></h1>
    		
    		<?php if ($this->cfg->fe_newlink_summary != '') : ?>
    		<div class="well well-sm">
    			<?php echo stripslashes($this->cfg->fe_newlink_summary); ?>
    		</div>
    		<?php endif; ?>
    		
    		<p><?php echo JText::_('COM_JVLE_LINK_ADD_MSG_1'); ?></p>
    		
    		<div class="well well-sm jvle_exchange">
    			<h3 class="jvle_h3"><?php echo JText::_('COM_JVLE_LINK_ADD_MSG_2'); ?></h3>		
    			<dl class="jvle_dl">
    				<dt><?php echo JText::_("COM_JVLE_WEBURL"); ?></dt>
    				<dd><?php echo stripslashes($this->cfg->self_url); ?></dd>
    				
    				<dt><?php echo JText::_("COM_JVLE_LINK_ADD_WEBTITLE"); ?></dt>
    				<dd><?php echo stripslashes($this->cfg->self_title); ?></dd>
    				
    				<dt><?php echo JText::_("COM_JVLE_LINK_ADD_WEBDESC"); ?></dt>
    				<dd><?php echo stripslashes($this->cfg->self_desc); ?></dd>
    			</dl>
    		</div>
    
    		<p class="jvle_or"><?php echo JText::_('COM_JVLE_LINK_ADD_OR'); ?></p>
    		
    		<div class="well well-sm jvle_exchange">
    			<h3 class="jvle_h3"><?php echo JText::_('COM_JVLE_LINK_ADD_CODE'); ?></h3>
    			<div class="jvle_code">
    				<?php echo htmlentities($this->info, ENT_COMPAT, "UTF-8"); ?>
    			</div>
    		</div>
    		
    		<h3 class="jvle_h3"><?php echo JText::_('COM_JVLE_LINK_ADD_FORM'); ?></h3>		
    		<div class="control-group">
    	       	<div class="control-label"><?php echo $this->form->getLabel('partner_url'); ?></div>
    	        <div class="controls"><?php echo $this->form->getInput('partner_url'); ?></div>
    		</div>
    
    		<div class="control-group">
    	       	<div class="control-label"><?php echo $this->form->getLabel('partner_email'); ?></div>
    	        <div class="controls"><?php echo $this->form->getInput('partner_email'); ?></div>
    		</div>
    		
    		<div class="control-group">
    	       	<div class="control-label"><?php echo $this->form->getLabel('partner_title'); ?></div>
    	        <div class="controls"><?php echo $this->form->getInput('partner_title'); ?></div>
    		</div>
    		
    		<div class="control-group">
    	       	<div class="control-label"><?php echo $this->form->getLabel('partner_desc'); ?></div>
    	        <div class="controls"><?php echo $this->form->getInput('partner_desc'); ?></div>
    		</div>
    								
    		<div class="control-group">
    	       	<div class="control-label"><?php echo $this->form->getLabel('reciprocal_link_url'); ?></div>
    	        <div class="controls"><?php echo $this->form->getInput('reciprocal_link_url'); ?></div>
    		</div>
    		
    		<div class="control-group">
    	       	<div class="control-label"><?php echo $this->form->getLabel('link_category'); ?></div>
    	        <div class="controls"><?php echo $this->form->getInput('link_category'); ?></div>
    		</div>		
    
    		<div class="control-group">
    	       	<div class="control-label"><?php echo $this->form->getLabel('captcha'); ?></div>
    	        <div class="controls"><?php echo $this->form->getInput('captcha'); ?></div>
    		</div>	
    		
    		<div class="control-group">
    		    <div class="controls">
    		    	<button type="submit" name="submit" id="submit" class="btn btn-primary"><?php echo JText::_('COM_JVLE_SUBMIT'); ?></button>
    		    	<button type="reset" name="reset" id="reset" class="btn btn-warning"><?php echo JText::_('COM_JVLE_RESET'); ?></button>
    		    </div>
    		</div>
    
    	<input type="hidden" name="mode" value="1" />
    	<?php echo JHtml::_('form.token'); ?>
    	</form>

    </div>
    	   
</div>
