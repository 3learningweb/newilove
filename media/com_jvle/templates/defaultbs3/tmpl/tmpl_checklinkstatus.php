<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
JHtml::_('behavior.keepalive');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
?>

<div class="row" id="jvle_clstatus">

    <div class="col-md-12">

    	<form action="<?php echo JRoute::_('index.php?option=com_jvle&view=checklinkstatus'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-vertical">
    
    		<h1 class="jvle_h1"><?php echo JText::_('COM_JVLE_CLS_TITLE'); ?></h1>
    		
    		<div class="control-group">
    	       	<div class="control-label"><?php echo $this->form->getLabel('url'); ?></div>
    	        <div class="controls"><?php echo $this->form->getInput('url'); ?></div>
    		</div>
    
    		<div class="control-group">
    	       	<div class="control-label"><?php echo $this->form->getLabel('captcha'); ?></div>
    	        <div class="controls"><?php echo $this->form->getInput('captcha'); ?></div>
    		</div>
    
    		<div class="control-group">
    		    <div class="controls">
    		    	<button type="submit" name="submit" id="submit" class="btn btn-primary"><?php echo JText::_('COM_JVLE_SUBMIT'); ?></button>
    		    	<button type="reset" name="reset" id="reset" class="btn btn-warning"><?php echo JText::_('COM_JVLE_RESET'); ?></button>
    		    </div>
    		</div>
    
    	<input type="hidden" name="mode" value="1" />
    	<?php echo JHtml::_('form.token'); ?>
    	</form>

	</div>
	
</div>
