<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php 
		$show_lalinks = 0;
		switch ($this->pageview)
		{
			case 'home':
			{ 
				$show_lalinks = ($this->cfg->show_latest_links) ? 1 : 0; 
				break;
			}
			case 'links': 
			case 'all_category_links':
			{
				$show_lalinks = ($this->cfg->show_latest_links == 2) ? 1 : 0; 
				break;
			}
			default: 
				break;
		}
		
		if ($show_lalinks) : 
			$lalinks = JvleSiteUtil::getLatestLinks();
?>

<div class="row">
	<div class="col-md-12">
			<h3 class="jvle_h3"><?php echo JText::_('COM_JVLE_LINKS_LATLINKS'); ?></h3>

<?php 		if (count($lalinks)) : ?>

<?php 			foreach ($lalinks as $link) : ?>
<?php 				echo JvleSiteUtil::drawLinkInfo($link, _JVLE_LATLINKS_PAGE); ?>
<?php 			endforeach; ?>

<?php 		endif; ?>

	</div>
</div>


<?php 	endif; ?>