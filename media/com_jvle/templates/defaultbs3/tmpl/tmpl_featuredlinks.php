<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php if ($this->cfg->enable_flinks) :
		$flink_url = JRoute::_("index.php?option=com_jvle&Itemid="._JVLE_ITEMID."&view=addfeatured");

		$show_flinks = 0;
		switch ($this->pageview)
		{
			case 'home':
			{ 
				$show_flinks = ($this->cfg->show_flinks) ? 1 : 0; 
				break;
			}
			case 'links': 
			case 'all_category_links':
			{
				$show_flinks = ($this->cfg->show_flinks == 2) ? 1 : 0; 
				break;
			}
			default: 
				break;
		}
		
		if ($show_flinks) : 
			$felinks = JvleSiteUtil::getFeaturedLinks();
?>

<div class="row">
	<div class="col-md-12">
		<div class="jvle_feblock">
			<h3 class="jvle_h3">
				<span class="label label-success"><?php echo JText::_('COM_JVLE_LINKS_FLINKS'); ?></span>
			</h3>

<?php 		if (count($felinks)) : ?>

<?php 			foreach ($felinks as $link) : ?>
<?php 				echo JvleSiteUtil::drawLinkInfo($link, _JVLE_FEATUREDLINK_PAGE); ?>
<?php 			endforeach; ?>

<?php 		endif; ?>

		</div>
	</div>
</div>

<?php 	endif; ?>

<p class="menu">
	<a href="<?php echo $flink_url; ?>" title="<?php echo JText::_("COM_JVLE_LINK_ADD_FEATURED_METADESC"); ?>">
		<?php echo JText::_("COM_JVLE_LINK_ADD_FEATURED_METADESC"); ?>
	</a>
</p>

<?php endif; ?>