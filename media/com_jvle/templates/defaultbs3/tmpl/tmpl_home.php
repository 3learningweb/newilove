<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php JHtml::_('bootstrap.tooltip'); ?>

<?php echo $this->loadTemplate("featuredlinks"); ?>

<?php if ($this->cfg->fe_dir_summary != '') : ?>
	<div class="well well-sm">
    	<?php echo stripslashes($this->cfg->fe_dir_summary); ?>
	</div>
<?php endif; ?>

	<div class="row" id="jvle_maincontent">
	   <div class="col-md-12">		
    		<h1 class="jvle_h1"><?php echo JText::_('COM_JVLE_CATEGORIES'); ?></h1>
<?php
        $x=1;
        $maincats = JvleDb::getRows("select * from `#__jvle_categories` where `cpid` = '0' and `visibility` = '1' order by `name` asc");
        foreach ($maincats as $maincat) : 
            $subcats = JvleDb::getRows("select * from `#__jvle_categories` where `cpid` = '".(int)$maincat->id."' and `visibility` = '1' order by `name` asc");
            if ($x%2) :
?>
                <div class="jvle_dirmodel_lb"><?php echo JvleSiteUtil::drawCategoryBlock($maincat, $subcats); ?></div>
<?php		else:  ?>
                <div class="jvle_dirmodel_rb"><?php echo JvleSiteUtil::drawCategoryBlock($maincat, $subcats); ?></div>
                <div class="jvle_clear"></div>
<?php		endif; 
            $x++;
        endforeach;
                
        if (!($x%2)) : ?>
    	        <div class="jvle_dirmodel_rb">&nbsp;</div>
    	        <div class="jvle_clear"></div>
<?php	endif; ?>

        </div>
    </div>
    
<?php if ($this->cfg->fe_instructions != '') : ?>
	<div class="well well-sm">
    	<?php echo stripslashes($this->cfg->fe_instructions); ?>
	</div>
<?php endif; ?>

<?php echo $this->loadTemplate("latestlinks"); ?>
