<?php defined('_JEXEC') or die('Restricted access'); ?>

<div class="row" id="jvle_maincontent">

    <div class="col-md-12">
		
        <form action="<?php echo htmlspecialchars(JFactory::getUri()->toString()); ?>" method="post" name="adminForm" id="adminForm">
    
    		<h1 class="jvle_h1"><?php echo JText::sprintf('COM_JVLE_SEARCHR_TITLE', $this->escape($this->query)); ?></h1>
    
<?php if (count($this->rows)) : ?>

<?php 	foreach ($this->rows as $row) : ?>
<?php 		echo JvleSiteUtil::drawLinkInfo($row, _JVLE_SEARCHRESULTS_PAGE); ?>
<?php 	endforeach; ?>

    		<div class="pagination pagination-centered">
    			<?php echo $this->pagination->getListFooter(); ?>
    		</div>

<?php else: ?>

    		<div class="pagination pagination-centered">
    			<?php echo JText::_('COM_JVLE_SEARCHR_NORESULTS'); ?>
    		</div>

<?php endif; ?>		
		
        <input type="hidden" name="view" value="search" />
        <input type="hidden" name="option" value="com_jvle" />
        <input type="hidden" name="Itemid" value="<?php echo _JVLE_ITEMID; ?>" />
        </form>
    
    </div>
    
</div>    