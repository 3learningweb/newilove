<?php defined('_JEXEC') or die('Restricted access'); ?>

<div id="jvle_dir" class="container-fluid">

    <div class="row" id="jvle_title">
    	<div class="col-md-12">
    		<h2><?php echo htmlspecialchars($this->cfg->fe_title); ?></h2>
    <?php if ($this->cfg->header_slogan != '') : ?>
        	<p><?php echo htmlspecialchars($this->cfg->header_slogan); ?></p>
    <?php endif; ?>
    	</div>
    </div>
    
    <div class="row" id="jvle_menusearch">
    	<div class="col-md-9">
    		<?php echo $this->loadTemplate("menu"); ?>
    	</div>
    	<div class="col-md-3">
    		<?php echo $this->loadTemplate("search"); ?>
    	</div>	
    </div>
        
    <?php if (($this->cfg->enable_ads) && ($this->cfg->ad_top != '')) : ?>
    <div class="row" id="ad_top">
    	<div class="col-md-12">
    		<?php echo $this->cfg->ad_top; ?>
    	</div>	
    </div>
    <?php endif; ?>
    
    <?php echo $this->loadTemplate("msgout"); ?>
    
    <?php echo $this->loadTemplate($this->subtemplate); ?>
    
    <?php if (($this->cfg->enable_ads) && ($this->cfg->ad_bottom != '')) : ?>
    <div class="row" id="ad_bottom">
    	<div class="col-md-12">
    		<?php echo $this->cfg->ad_bottom; ?>
    	</div>	
    </div>
    <?php endif; ?>
    
    <?php echo $this->loadTemplate("footer"); ?>
        
</div>