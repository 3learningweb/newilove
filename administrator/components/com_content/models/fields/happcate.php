<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
 
JFormHelper::loadFieldClass('list');
 
class JFormFieldHappCate extends JFormFieldList {
 
    protected $type = 'HappinessCate'; 


    public function getLabel() {
         return '<span style="text-decoration: underline;">' . parent::getLabel() . '</span>';
    }

    // public function getInput() {
    //     return '<select id="'.$this->id.'" name="'.$this->name.'">'.
    //            '<option value="1" >New York</option>'.
    //            '<option value="2" >Chicago</option>'.
    //            '<option value="3" >San Francisco</option>'.
    //            '</select>';
    // }
 
    public function getOptions() {

        // $model_categories = JCategories::getInstance('Content');
        // $root = $model_categories->get(38);
        // $categories = $root->getChildren();

        $options = array();
        // $extension = $this->element['extension'] ? (string) $this->element['extension'] : (string) $this->element['scope'];
        $extension = 'com_content';
        //$published = (string) $this->element['published'];  // 這邊可以取得 filter_featured.xml那邊的自訂欄位資料
        $published = "0,1,2";  

        // Load the category options for a given extension.
        if (!empty($extension))
        {
            // Filter over published state or not depending upon if it is present.
            if ($published)
            {
                $options = JHtml::_('category.options', $extension, array('filter.published' => explode(',', $published)));
            }
            else
            {
                $options = JHtml::_('category.options', $extension);
            }

            // Verify permissions.  If the action attribute is set, then we scan the options.
            if ((string) $this->element['action'])
            {
                // Get the current user object.
                $user = JFactory::getUser();

                foreach ($options as $i => $option)
                {
                    /*
                     * To take save or create in a category you need to have create rights for that category
                     * unless the item is already in that category.
                     * Unset the option if the user isn't authorised for it. In this field assets are always categories.
                     */
                    if ($user->authorise('core.create', $extension . '.category.' . $option->value) != true)
                    {
                        unset($options[$i]);
                    }
                }
            }

            if (isset($this->element['show_root']))
            {
                array_unshift($options, JHtml::_('select.option', '0', JText::_('JGLOBAL_ROOT')));
            }
        }
        else
        {
            JLog::add(JText::_('JLIB_FORM_ERROR_FIELDS_CATEGORY_ERROR_EXTENSION_EMPTY'), JLog::WARNING, 'jerror');
        }

        // array_unshift($options, JHtml::_('select.option', '0', JText::_('JGLOBAL_ROOT')));

        // Merge any additional options in the XML definition.
        // $options = array_merge(parent::getOptions(), $options);


        //不同選單會有不同的分類內容， 這邊判斷之後將多餘的內容刪除！

        $app = JFactory::getApplication();

        // if($app->input->get('view') === 'category' && $app->input->get('layout') === 'st_metanicvideo-default') {
        if($app->input->get('view') === 'category') {
            $found = false;
            foreach ($options as $i => $option)
                        {
                            if ($option->value == '20' && !$found) //category 20: 幸福。放送
                            {
                                //found!! enter safe mode!
                                $found = true;
                                unset($options[$i]);
                            } elseif($found && strpos($option->text, "- ") !== FALSE) {
                                continue;
                            } else {
                                $found = false;
                                unset($options[$i]);
                            }
                        }
        }

        return $options;
    }
}