<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Methods supporting a list of item records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesModelSearch_2015 extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'Name', 'a.Name',
				'UserID', 'a.UserID',
				'Tel', 'a.Tel',
				'Email', 'a.Email',
				'city', 'a.city',
				'CreateTime', 'a.CreateTime'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $published);

		$categoryId = $this->getUserStateFromRequest($this->context . '.filter.category_id', 'filter_category_id', '');
		$this->setState('filter.category_id', $categoryId);

		$title = $this->getUserStateFromRequest($this->context . '.filter.title', 'filter_title', '');
		$this->setState('filter.title', $title);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_activities');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'desc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id    A prefix for the store id.
	 * @return  string  A store id.
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');
		$id .= ':' . $this->getState('filter.category_id');
		$id .= ':' . $this->getState('filter.language');
		$id .= ':' . $this->getState('filter.peference_id');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$user = JFactory::getUser();
		$db		= $this->getDbo();
		$app = JFactory::getApplication();

		$query	= $db->getQuery(true);
		$query->select('a.*');
		$query->from($db->quoteName('#__contribute_list_2015') . ' AS a');

       	return $query;
	
	}
	
	public function getSchool() {
		$school = array('289'=>'新竹女中', '290'=>'臺中一中', '291'=>'彰化高中', '292'=>'臺南女中', '293'=>'高雄中學', '294'=>'中山高中', '295'=>'金門高中');
		
		return $school;
	}

}
