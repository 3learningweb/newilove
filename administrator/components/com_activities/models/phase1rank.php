<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Methods supporting a list of item records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesModelPhase1rank extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
//				'serial_num', 'a.serial_num',
				'state', 'a.state'
			);
		}

		parent::__construct($config);
	}


	protected function populateState($ordering = null, $direction = null)
	{
		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $published);

		$type = $this->getUserStateFromRequest($this->context . '.filter.type', 'filter_type', '');
		$this->setState('filter.type', $type);


		// Load the parameters.
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'desc');
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	public function getListQuery()
	{
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		
		// catid
		$peference_id = $app->getUserState("form.activities.peference");	
		$peferences = self::getPeference();		

		// city
		$self_gps = JUserHelper::getUserGroups($user->get('id'));
		$city =  "'" . implode("','", $self_gps) . "'";
		
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.id AS appid, a.*'
			)
		);

		$query->from($db->quoteName('#__contribute_list') . ' AS a ');
		
		// rene
		$query->where("a.catid = '{$peferences[$peference_id]->catid}'");
		$query->where("a.state = '1'");
		$query->where("a.city IN ({$city})");

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		return $query;
	}

	
	public function getCnt()
	{
		$user = JFactory::getUser();
		
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		
		// Select the required fields from the table.
		$query->select('count(*) as cnt');
		$query->from($db->quoteName('#__contribute_list') . ' AS a ');
		$query->where('a.state = 1');
		
		$db->setQuery($query);
		$cnt = $db->loadRow();
		
		return $cnt;
	}

	public function getPeference() {
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		
		$query->select("a.*");
		$query->from($db->quoteName('#__activities_set', 'a'));
		$query->where("a.state = '1'");

		$query->select('c.title AS category_title')
			->join('LEFT', $db->quoteName('#__categories'). ' AS c ON c.id = a.catid');
		
		$db->setQuery($query);
		$items = $db->loadObjectList();		

		array_unshift($items, "--請選擇活動--");

		return $items;
	}
	
	public function getScore() {
		$app = JFactory::getApplication();
		$catid = $app->getUserState("form.activities.catid");
		
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		
		// 平均的話 /count(ContributeListCde)
		$query->select("ContributeListCde, sum(Score) as score");
		$query->from($db->quoteName('#__preliminary_score'));
		$query->where("catid = '{$catid}'");
		$query->group("ContributeListCde");
		$query->order("score DESC");
		
		$db->setQuery($query);
		$score = $db->loadObjectList("ContributeListCde");

		return $score;
	}
	
	public function getFields($item_id = '', $limit = '') {
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		$query->select("t.preliminary_type, f.text, f.id AS fid, f.num");
		$query->from($db->quoteName('#__activities_ranktype', 't'));
		$query->leftJoin($db->quoteName('#__activities_ranktype_field', 'f') . " ON f.type_id = t.id");
		$query->where("t.item_id = '{$item_id}'");
		$query->where("f.type = '0'");

		$db->setQuery($query);
		$items = $db->loadObjectList("fid");
		
		$fields[0] = new stdClass();
		$fields[0]->text = "-----請選擇-----";
		$fields[0]->fid = 0;
		
		if($items) {
			$fields[0]->preliminary_type = $item->preliminary_type;
			foreach($items as $key => $item) {
				$fields[$key] = new stdClass();
				$fields[$key]->preliminary_type = $item->preliminary_type;
				$fields[$key]->text = $item->text;
				$fields[$key]->fid = $item->fid;
				$fields[$key]->num = $item->num;
			}
		}else{
			$fields[0]->preliminary_type = 1;
			for($i=1 ; $i<=$limit ; $i++) {
				$fields[$i] = new stdClass();
				$fields[$i]->preliminary_type = 1;
				$fields[$i]->text = $i;
				$fields[$i]->fid = $i;
			}
		}
		
		return $fields;
	}

	// 判斷是否已完成初賽評選排名
	public function getPreliminaryRankCheck() {
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		$catid = $app->getUserState("form.activities.catid");
		
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		$query->select("COUNT(*)");
		$query->from($db->quoteName('#__preliminary_rank_check'));
		$query->where("UsrID = '{$user->get('id')}'");
		$query->where("catid = '{$catid}'");
		$query->where("IsCheck = '1'");

		$db->setQuery($query);
		$check = $db->loadResult();
		
		return $check;
	}

	// 未完成的初賽評審
	public function getUndone() {
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		$catid = $app->getUserState("form.activities.catid");
		$preliminary = JComponentHelper::getParams('com_activities')->get('preliminary_access');  // 初賽
		
		// city
		$self_gps = JUserHelper::getUserGroups($user->get('id'));
		$city =  "'" . implode("','", $self_gps) . "'";
				
		$db = $this->getDBO();
		
		// 取得group id
		$query_level = $db->getQuery(true);
		$query_level->select("*");
		$query_level->from($db->quoteName('#__viewlevels'));
		$query_level->where("id = '{$preliminary}'");
		$db->setQuery($query_level);
		$level = $db->loadObject();
		$group = json_decode($level->rules);
		
		foreach($group as $g) {
			if(in_array($g, $self_gps)) {
				$target_city = $g;
				break;
			}
		}
		
		// 取得已評選完的專家評審
		$query_check = $db->getQuery(true);
		$query_check->select("UsrID");
		$query_check->from($db->quoteName('#__preliminary_check'));
		$query_check->where("catid = '{$catid}'");
		$query_check->where("City IN ({$city})");
		$db->setQuery($query_check);
		$check = $db->loadObjectList();
		
		// 尚未評選的評審資料
		$query = $db->getQuery(true);
		$query->select("m.user_id");
		$query->from($db->quoteName('#__user_usergroup_map', 'm'));
		$query->where("m.group_id = '{$target_city}'");
		$query->where("m.user_id != '{$user->get('id')}'");
		
		if($check) {
			foreach($check as $key => $c) {
				$check_arr[$key] = $c->UsrID;
			}
			$check_str = "'" . implode("','", $check_arr) . "'";
			$query->where("m.user_id NOT IN ($check_str)");
		}

		$query->select('u.name, u.email')
			  ->join('LEFT', $db->quoteName('#__users'). ' AS u ON u.id = m.user_id');
		$db->setQuery($query);
		$items = $db->loadObjectList();

		return $items;
	}

}
