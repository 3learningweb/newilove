<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Methods supporting a list of item records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesModelPhase2check extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
//				'serial_num', 'a.serial_num',
				'state', 'a.state'
			);
		}

		parent::__construct($config);
	}


	protected function populateState($ordering = null, $direction = null)
	{
		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $published);

		$type = $this->getUserStateFromRequest($this->context . '.filter.type', 'filter_type', '');
		$this->setState('filter.type', $type);

		// rene
		$city = $this->getUserStateFromRequest($this->context . '.filter_city_id', 'filter_city_id', '');
		$this->setState('filter.city_id', $city);

		// Load the parameters.
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'desc');
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	public function getListQuery()
	{
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		
		// catid
		$peference_id = $app->getUserState("form.activities.peference");	
		$peferences = self::getPeference();		
		
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.*'
			)
		);

		$query->from($db->quoteName('#__preliminary_decision') . ' AS a ');
		
		// rene
		$query->where("a.catid = '{$peferences[$peference_id]->catid}'");
		
		// join profile
		$query->select('c.id, c.Name, c.Title, c.PicPath, c.Introduction')
              ->join('LEFT', $db->quoteName('#__contribute_list') . ' AS c ON c.id = a.ContributeListCde');
			  
		$query->select('f.text')
			  ->join('LEFT', $db->quoteName('#__activities_ranktype_field') . ' AS f ON f.id = a.Rno');
		
		// Filter
		$city_id = $this->getState('filter.city_id');
		if(is_numeric($city_id)) {
			$query->where("c.city = '{$city_id}'");
		}

		$query->order("a.total DESC");

		return $query;
	}

	
	public function getCnt()
	{
		$user = JFactory::getUser();
		
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		
		// Select the required fields from the table.
		$query->select('count(*) as cnt');
		$query->from($db->quoteName('#__contribute_list') . ' AS a ');
		$query->where('a.state = 1');
		
		$db->setQuery($query);
		$cnt = $db->loadRow();
		
		return $cnt;
	}

	public function getPeference() {
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		
		$query->select("a.*");
		$query->from($db->quoteName('#__activities_set', 'a'));
		$query->where("a.state = '1'");

		$query->select('c.title AS category_title')
			->join('LEFT', $db->quoteName('#__categories'). ' AS c ON c.id = a.catid');
		
		$db->setQuery($query);
		$items = $db->loadObjectList();		

		array_unshift($items, "--請選擇活動--");

		return $items;
	}

	// 判斷台師大帳號是否已完成檢核
	public function getCheck() {
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		$catid = $app->getUserState("form.activities.catid");
		
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		$query->select("COUNT(*)");
		$query->from($db->quoteName('#__preliminary_decision'));
		$query->where("Flg = '1'");
		$query->where("catid = '{$catid}'");

		$db->setQuery($query);
		$check = $db->loadResult();
		
		return $check;
	}
	
	// 未完成的專家評審
	public function getUndone() {
		$app = JFactory::getApplication();
		$catid = $app->getUserState("form.activities.catid");
		$rematch = JComponentHelper::getParams('com_activities')->get('rematch_access');	// 決賽
		
		$db = $this->getDBO();
		
		// 取得group id
		$query_level = $db->getQuery(true);
		$query_level->select("*");
		$query_level->from($db->quoteName('#__viewlevels'));
		$query_level->where("id = '{$rematch}'");
		$db->setQuery($query_level);
		$level = $db->loadObject();
		$group = json_decode($level->rules);
		
		
		// 取得已評選完的專家評審
		$query_check = $db->getQuery(true);
		$query_check->select("UsrID");
		$query_check->from($db->quoteName('#__rematch_check'));
		$query_check->where("catid = '{$catid}'");
		$db->setQuery($query_check);
		$check = $db->loadObjectList();
		
		// 尚未評選的評審資料
		$query = $db->getQuery(true);
		$query->select("m.user_id");
		$query->from($db->quoteName('#__user_usergroup_map', 'm'));
		$query->where("m.group_id = '{$group[0]}'");
		
		if($check) {
			foreach($check as $key => $c) {
				$check_arr[$key] = $c->UsrID;
			}
			$check_str = "'" . implode("','", $check_arr) . "'";
			$query->where("m.user_id NOT IN ($check_str)");
		}
		
		$query->select('u.name, u.email')
			  ->join('LEFT', $db->quoteName('#__users'). ' AS u ON u.id = m.user_id');
		$db->setQuery($query);
		$items = $db->loadObjectList();

		return $items;
	}

	public function getCity() {
		$db = $this->getDBO();
		
		$query = $db->getQuery(true);
		$query->select("id, title");
		$query->from($db->quoteName('#__usergroups'));
		$query->where("parent_id = '10'");
		$query->order("id asc");
		
		$db->setQuery($query);
		$city = $db->loadObjectList();
		
		return $city;
	}

}
