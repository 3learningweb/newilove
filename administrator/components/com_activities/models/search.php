<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Methods supporting a list of item records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesModelSearch extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'Name', 'a.Name',
				'UserID', 'a.UserID',
				'Tel', 'a.Tel',
				'Email', 'a.Email',
				'city', 'a.city',
				'CreateTime', 'a.CreateTime'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $published);

		$categoryId = $this->getUserStateFromRequest($this->context . '.filter.category_id', 'filter_category_id', '');
		$this->setState('filter.category_id', $categoryId);

		$title = $this->getUserStateFromRequest($this->context . '.filter.title', 'filter_title', '');
		$this->setState('filter.title', $title);

		//rene
		$peferences = $this->getUserStateFromRequest($this->context . '.filter_peference_id', 'filter_peference_id', '');
		$this->setState('filter.peference_id', $peferences);
		
		$city = $this->getUserStateFromRequest($this->context . '.filter_city_id', 'filter_city_id', '');
		$this->setState('filter.city_id', $city);
		
		// Load the parameters.
		$params = JComponentHelper::getParams('com_activities');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'desc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id    A prefix for the store id.
	 * @return  string  A store id.
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');
		$id .= ':' . $this->getState('filter.category_id');
		$id .= ':' . $this->getState('filter.language');
		$id .= ':' . $this->getState('filter.peference_id');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$user = JFactory::getUser();
		$db		= $this->getDbo();
		$app = JFactory::getApplication();
		$post = $app->input->getArray($_POST);

		$level = JAccess::getAuthorisedViewLevels($user->get('id'));
		$rank = JComponentHelper::getParams('com_activities')->get('rank_access');	// 家教

		$query	= $db->getQuery(true);
		$query->select('a.*');
		$query->from($db->quoteName('#__contribute_list') . ' AS a');

		$query->select('u.title AS city_name')
			  ->join('LEFT', $db->quoteName('#__usergroups'). ' AS u ON u.id = a.city');

		if(in_array($rank, $level)) {  // 家教
			// city
			$self_gps = JUserHelper::getUserGroups($user->get('id'));
			$city =  "'" . implode("','", $self_gps) . "'";
			$query->where("a.city IN ({$city})");
		}

		// Filter by category(speciality).
		$peference_id = $this->getState('filter.peference_id');
		if(is_numeric($peference_id)) {
			$query->where("a.catid = '{$peference_id}'");
		}
		
		$city_id = $this->getState('filter.city_id');
		if(is_numeric($city_id)) {
			$query->where("a.city = '{$city_id}'");
		}
		
		// Filter by search in name
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(a.Name LIKE ' . $search . ')');
			}
		}		
		
		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
		if ($orderCol == 'a.ordering' || $orderCol == 'category_title')
		{
			$orderCol = 'c.title ' . $orderDirn . ', a.ordering';
		}
		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		$start = $post['calendar_start'];
		$end = $post['calendar_end']; 
		if($start != "" && $end != "") {
			$query->where("substring_index(a.CreateTime, ' ', 1) >= '{$start}'");
			$query->where("substring_index(a.CreateTime, ' ', 1) <= '{$end}'");
		}	

       	return $query;
	
	}

	public function getPeference() {
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		
		$query->select("a.*");
		$query->from($db->quoteName('#__activities_set', 'a'));
		$query->where("a.state = '1'");

		$query->select('c.title AS category_title')
			->join('LEFT', $db->quoteName('#__categories'). ' AS c ON c.id = a.catid');
		
		$db->setQuery($query);
		$items = $db->loadObjectList();		

		return $items;
	}
	
	public function getTotal() {
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		$query = self::getListQuery();
		
		$db->setQuery($query);
		$items = $db->loadObjectList();
		
		return count($items);
	}
	
	public function getCity() {
		$db = $this->getDBO();
		
		$query = $db->getQuery(true);
		$query->select("id, title");
		$query->from($db->quoteName('#__usergroups'));
		$query->where("parent_id = '10'");
		$query->order("id asc");
		
		$db->setQuery($query);
		$city = $db->loadObjectList();
		
		return $city;
	}

}
