<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Activities Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.5
 */
class ActivitiesController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean			$cachable	If true, the view output will be cached
	 * @param   array  $urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController		This object to support chaining.
	 * @since   1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		require_once JPATH_COMPONENT.'/helpers/activities.php';

		$user = JFactory::getUser();
		$level = JAccess::getAuthorisedViewLevels($user->get('id'));
		
		$preliminary = JComponentHelper::getParams('com_activities')->get('preliminary_access');  // 初賽
		$rematch = JComponentHelper::getParams('com_activities')->get('rematch_access');  // 決賽
		$rank = JComponentHelper::getParams('com_activities')->get('rank_access');  // 家教
		$check = JComponentHelper::getParams('com_activities')->get('check_access'); // 台師大檢核
		
		
		// 不同層級顯示不同頁面
		if(in_array($rank, $level)) {  // 家教
			$view = $this->input->get('view', 'phase1rank');
		}elseif(in_array($preliminary, $level)) {  //初賽
			$view = $this->input->get('view', 'phase1as');
		}elseif(in_array($rematch, $level)) {  // 決賽
			$view = $this->input->get('view', 'rematch');
		}elseif(in_array($check, $level)) {  // 台師大檢核
			$view = $this->input->get('view', 'phase2check');
		}else{
			$view = $this->input->get('view', 'phase1as');
		}
		
		$layout = $this->input->get('layout', 'default');
		$id     = $this->input->getInt('id');

		JRequest::setVar('view', $view);

		parent::display();

		return $this;
	}
}
