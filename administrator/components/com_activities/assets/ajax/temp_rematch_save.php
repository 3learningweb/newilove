<?php 
	define( '_JEXEC', 1 );
	define('JPATH_BASE', '../../../../../');
	define( 'DS', DIRECTORY_SEPARATOR );
	require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
	
	$mainframe =& JFactory::getApplication('administrator');
	$mainframe->initialise();
	
	$app = JFactory::getApplication();
	
	$post = $app->input->getArray($_POST);
	$id = $post['id'];
	$catid = $post['catid'];
	$score1 = $post['score1'];
	$score2 = $post['score2'];
	$score3 = $post['score3'];
	
	$db	= JFactory::getDBO();
	$query = $db->getQuery(true);
	
	$query->update($db->quoteName('#__rematch_score'));
	$query->set("Score1 = '{$score1}'");
	$query->set("Score2 = '{$score2}'");
	$query->set("Score3 = '{$score3}'");
	$query->where("ContributeListCde = '{$id}'");
	$query->where("catid = '{$catid}'");
	
	$db->setQuery($query);
	if(!$db->execute()) {
		echo "0";
	}else{
		echo "1";
	}
	
	die();
	
?>