<?php 
	define( '_JEXEC', 1 );
	define('JPATH_BASE', '../../../../../');
	define( 'DS', DIRECTORY_SEPARATOR );
	require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
	
	$mainframe =& JFactory::getApplication('administrator');
	$mainframe->initialise();
	
	$app = JFactory::getApplication();
	$delete = array();
	$delete = (array) $app->getUserState('form.activities.delete');
	
	$post = $app->input->getArray($_POST);
	$item_id = explode("_", $post['item_id']);
	$id = $item_id[1];
	
	if(!array_key_exists($id, $delete)) {
		$delete[$id] = $id;
	}
	

	$app->setUserState('form.activities.delete', $delete);
	
	echo count($delete); die();
?>