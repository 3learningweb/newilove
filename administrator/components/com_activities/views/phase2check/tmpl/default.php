<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
$delete = (array) $app->getUserState('form.activities.delete');
$catid = $app->getUserState('form.activities.catid');

$user		= JFactory::getUser();
$userId		= $user->get('id');

?>

<script type="text/javascript" src="components/com_activities/assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="components/com_activities/assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="components/com_activities/assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			$(".item_img img").fancybox();

			$("#undone").fancybox();
			
			var num = "<?php echo count($this->items); ?>";
			
			$(".delete_btn").on("click", function() {
				if(confirm("是否確定刪除")) {
					var item_id = $(this).attr("id");
					var url = "components/com_activities/assets/ajax/delete_check_item.php";
					$(this).attr('disabled',true);
					
					$.post(url, {item_id:item_id}, function(data) {
						var id = item_id.split("_");
						
						alert("成功刪除入選作品");
						$(".num").text(num-data);
						$("#item_block_"+id[1]).css("background", "#E7E6E6");
						$("#item_block_"+id[1]).css("border-color", "#CCC");
					});
				}else{
					return false;
				}
			});

			$("#peference").change(function() {
				var peference = $(this).val();
				var url = "<?php echo JRoute::_('index.php?option=com_activities&task=phase2check.savepeference&peference=', false); ?>" + peference;
				document.location.href = url;
			});			
		});
	})(jQuery);
</script>

<form action="<?php echo JRoute::_('index.php?option=com_activities&view=phase2check'.$append); ?>" method="post" name="adminForm" id="adminForm">
	<!-- 左方選單 -->
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif;?>
	
	<?php if($this->peference_id && $this->pre_check) { ?>
		<input type="button" class="btn" id="undone" href="index.php?option=com_activities&view=phase2check&layout=undone&tmpl=component" value="未完成評選的專家評審" /><br/><br/>
	<?php } ?>
	
	<?php	
	if(!$this->pre_check) { 
		echo JHtml::_('select.genericlist ', $this->peferences , 'peference', 'class="peference"', 'id', 'title', $this->peference_id);
		if($this->peference_id && $this->items) {
	?>
	
	<br/><br/>
	<div class="notice_text">請選擇排名最多選出<?php echo $preliminary_rank_limit; ?>個名次送至中央參加複賽，填寫完成確定後請點選確定送出。</div>
	
	<fieldset>
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>

			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', '初賽入選作品(<span class="num">'. (int)(count($this->items)-count($delete)) .'</span>件)'); ?>
				<?php foreach($this->items as $i => $item): ?>
					<div class="item_block" id="item_block_<?php echo $item->id; ?>" style="<?php if(in_array($item->id, $delete)){echo "background: #E7E6E6; border-color: #CCC;";} ?>">
						<div class="item_id">作品編號：<?php echo $item->id; ?></div>
						<div class="item_name">投稿人姓名：<?php echo $item->Name; ?></div>
						<div class="item_img">
							<img src="../<?php echo $item->PicPath; ?>" href="../<?php echo $item->PicPath; ?>" title="<?php echo $item->Introduction; ?>" />
						</div>
						<!-- <div class="item_title">主題：<?php echo $item->Title; ?></div> -->
						<div class="item_score">
							排名：<span class="rank"><?php echo $item->text; ?></span>&nbsp;&nbsp;&nbsp;
							總分：<span class="score"><?php echo round($item->total, 1); ?></span>
						</div>
						<br/>
						
						<div class="item_delete">
							<input type="button" class="btn delete_btn" id="ditem_<?php echo $item->id; ?>" <?php if(in_array($item->id, $delete)) {echo "disabled";} ?> value="刪除" />
						</div>						
					</div>
				<?php endforeach; ?>
				<?php echo $this->pagination->getListFooter(); ?>
			<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
		
		<input type="hidden" name="catid" value="<?php echo $catid; ?>" />
		<input type="hidden" name="task" value="" />
		
		<?php echo JHtml::_('form.token'); ?>
	</fieldset>
	<?php }else{ ?>
		<br/><br/>
		<div>
			<?php 
				if(!$this->items && $this->peference_id) {
					echo "尚無資料";
				}
			?>
		</div>
	<?php 
		}
	}else{
		echo "您已完成作品檢核";
	}
 	?>
</form>

<style>
	.item_block {
		float: left;
		width: 250px;
		margin: 10px;
		padding: 10px;
		border: 2px solid;
		min-height: 345px;
		text-align: center
	}
	
	.item_img img:hover {
		cursor: pointer;
	}

	.item_img img {
		max-height: 220px;
	}
	
	.item_select,
	.item_delete {
		text-align: center;
	}
	
	.pagination-toolbar {
		clear: both;
	}
	
	.score, .rank, .notice_text {
		color: red;
	}
	
	.item_rank .chzn-container {
		width: 190px !important;
	}
	
	.fields {
		width: 195px !important;
	}
</style>