<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
// JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
$preliminary_rank_limit = $app->getUserState('form.activities.preliminary_rank_limit');
$ranks = (array) $app->getUserState('form.activities.ranks');
$catid = $app->getUserState('form.activities.catid');

$user		= JFactory::getUser();
$userId		= $user->get('id');

?>
<script type="text/javascript" src="components/com_activities/assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="components/com_activities/assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="components/com_activities/assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			$(".item_img img").fancybox();
			
			$("#undone").fancybox();
			
			var num_limit = [];
			<?php
				if($this->peference_id) { 
					foreach($this->fields as $key => $field) { if($key == 0){continue;} 
			?>
					num_limit[<?php echo $key; ?>] = <?php echo $field->num; ?>;
			<?php 
					} 
				}
			?>

			var num = "<?php echo count($ranks); ?>";
			$(".num").text(num);
			
			var ranks = "<?php echo implode(",", $ranks); ?>";
			var rank_arr = ranks.split(",");
			var rank_groups = rank_arr.reduce(function(acc,e){acc[e] = (e in acc ? acc[e]+1 : 1); return acc}, {});
			$(".fields").each(function() {
				if("<?php echo $this->fields[0]->preliminary_type; ?>" == 1) {
					for(var i=0 ; i<rank_arr.length ; i++){
						$(this).children("option").eq(rank_arr[i]).attr("disabled", "disabled");
						$(this).children("option").eq(rank_arr[i]).css("background", "#CCC");
					}
				}else{
					for(var i=0 ; i<rank_arr.length ; i++){
				        if(rank_groups[rank_arr[i]] >= num_limit[rank_arr[i]]) {
							$(this).children("option[value="+ rank_arr[i] +"]").attr("disabled", "disabled");
						    $(this).children("option[value="+ rank_arr[i] +"]").css("background", "#CCC");
						}
					}					
				}
			});
			
			
			$(".fields").change(function() {
				var value = $(this).val();
				var item_id = $(this).attr("id");
		        var url = "components/com_activities/assets/ajax/select_rank.php";
				if( parseInt($(".num").text()) < "<?php echo $preliminary_rank_limit; ?>" || (parseInt($(".num").text()) == "<?php echo $preliminary_rank_limit; ?>" && value == 0)) {
					$.post(url, {item_id:item_id, value: value}, function(data) {
				        var arr = data.split(",");
				        var groups = arr.reduce(function(acc,e){acc[e] = (e in acc ? acc[e]+1 : 1); return acc}, {});
				        $(".num").text(arr.length);
				        if(value == 0) {
				        	alert("成功取消排名");
				        }else{
				        	alert("成功排名作品");
				        }
				        
						// 把選過的option disabled & background #CCC 				        	
			        	$(".fields").each(function() {
			        		$(this).children("option").removeAttr("disabled");
			        		$(this).children("option").css("background", "");
			        		if("<?php echo $this->fields[0]->preliminary_type; ?>" == 1) {
				        		for(var i=0 ; i<arr.length ; i++){
					        		$(this).children("option").eq(arr[i]).attr("disabled", "disabled");
					        		$(this).children("option").eq(arr[i]).css("background", "#CCC");
				        		}
				        	}else{
				        		for(var i=0 ; i<arr.length ; i++){
				        			if(groups[arr[i]] >= num_limit[arr[i]]) {
						        		$(this).children("option[value="+ arr[i] +"]").attr("disabled", "disabled");
						        		$(this).children("option[value="+ arr[i] +"]").css("background", "#CCC");
				        			}
				        		}					        		
				        	}
			        	});       			        
					});
				}else{
			       	alert("入選件數不能超過<?php echo $preliminary_rank_limit; ?>件");
			       	$(this).val(0);
			       	return false;
				}
			});

			$("#peference").change(function() {
				var peference = $(this).val();
				var url = "<?php echo JRoute::_('index.php?option=com_activities&task=phase1rank.savepeference&peference=', false); ?>" + peference;
				document.location.href = url;
			});
			
		});
	})(jQuery);
</script>

<form action="<?php echo JRoute::_('index.php?option=com_activities&view=phase1rank'.$append); ?>" method="post" name="adminForm" id="adminForm">
	<!-- 左方選單 -->
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif;?>

	<?php if($this->peference_id) { ?>
		<input type="button" class="btn" id="undone" href="index.php?option=com_activities&view=phase1rank&layout=undone&tmpl=component" value="未完成評選的縣市評審" /><br/><br/>
	<?php } ?>
	
	<?php
	if(!$this->pre_check) { 
		echo JHtml::_('select.genericlist ', $this->peferences , 'peference', 'class="peference"', 'id', 'title', $this->peference_id);
		if($this->peference_id && $this->items) {
			
	?>
	
	<br/><br/>
	<div class="notice_text">
		請選擇排名最多選出<?php echo $preliminary_rank_limit; ?>個名次送至中央參加複賽，填寫完成確定後請點選確定送出。<br/>
		請注意確定送出後即不能再更改任何資料。
	</div>
	
	<fieldset>
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>

			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', '初賽評選作品(<span class="num"></span>)'); ?>
				<?php foreach($this->items as $i => $item): ?>
					<div class="item_block">
						<div class="item_id">作品編號：<?php echo $item->id; ?></div>
						<div class="item_name">投稿人姓名：<?php echo $item->Name; ?></div>
						<div class="item_img">
							<img src="../<?php echo $item->PicPath; ?>" href="../<?php echo $item->PicPath; ?>" title="<?php echo $item->Introduction; ?>" />
						</div>
						<!-- <div class="item_title">主題：<?php echo $item->Title; ?></div> -->
						<div class="item_score">總分：<span class="score"><?php echo round($this->score[$item->id]->score, 1); ?></span></div><br/>
						
						<div class="item_rank">
							排名：
							<?php echo JHtml::_('select.genericlist ', $this->fields , "fields_{$item->id}", 'class="fields"', 'fid', 'text', $ranks[$item->id]); ?>
						</div>
					</div>
				<?php endforeach; ?>
				<?php echo $this->pagination->getListFooter(); ?>
			<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
		
		<input type="hidden" name="catid" value="<?php echo $catid; ?>" />
		<input type="hidden" name="preliminary_rank_limit" value="<?php echo $preliminary_rank_limit; ?>" />
		<input type="hidden" name="task" value="" />
		
		<?php echo JHtml::_('form.token'); ?>
	</fieldset>
	<?php }else{ ?>
		<br/><br/>
		<div>
			<?php 
				if(!$this->items && $this->peference_id) {
					echo "尚無投稿資料";
				}
			?>
		</div>
	<?php 
		}
	}else{
		echo "您已完成初賽排名";
	}
 	?>
</form>

<style>
	.item_block {
		float: left;
		width: 250px;
		margin: 10px;
		padding: 10px;
		border: 2px solid;
		min-height: 350px;
		text-align: center
	}
	
	.item_img img:hover {
		cursor: pointer;
	}

	.item_img img {
		max-height: 220px;
	}
	
	.item_select,
	.item_delete {
		text-align: center;
	}
	
	.pagination-toolbar {
		clear: both;
	}
	
	.score, .notice_text {
		color: red;
	}
	
	.item_rank .chzn-container {
		width: 190px !important;
	}
	
	.fields {
		width: 195px !important;
	}
</style>