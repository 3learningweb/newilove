<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
$post = $app->input->getArray($_POST);
$start = $post['calendar_start'];
$end = $post['calendar_end']; 

$user		= JFactory::getUser();
$userId		= $user->get('id');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$canOrder	= $user->authorise('core.edit.state', 'com_activities.category');
$saveOrder	= $listOrder == 'a.ordering';
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_activities&task=search.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'weblinkList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();

$peference_id = $this->state->get('filter.peference_id');

?>
<script type="text/javascript" src="components/com_activities/assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="components/com_activities/assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="components/com_activities/assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

<script type="text/javascript">
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			$(".item_img img").fancybox();
			
			$("#filter_submit").click(function() {
				$("#adminForm").submit();
			});
		});
	})(jQuery);
</script>

<form action="<?php echo JRoute::_('index.php?option=com_activities&view=search'.$append); ?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>

		<div id="filter-bar" class="btn-toolbar">
			<div class="filter-search btn-group pull-left">
				
				<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" class="hasTooltip" title="<?php echo JHtml::tooltipText('COM_ACTIVITIES_SEARCH_IN_TITLE'); ?>" />
			</div>
			<div class="btn-group pull-left">
				<button type="submit" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
				<button type="button" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_search').value='';this.form.submit();"><i class="icon-remove"></i></button>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC');?></label>
				<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC');?></option>
					<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING');?></option>
					<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING');?></option>
				</select>
			</div>
			<div class="btn-group pull-right">
				<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY');?></label>
				<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JGLOBAL_SORT_BY');?></option>
					<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder);?>
				</select>
			</div>
		</div>

		<div class="clearfix"><br/>
			<?php 
				echo "開始日期：" . JHtml::_('calendar', "{$start}", "calendar_start", "calendar_start") . "&nbsp;&nbsp;&nbsp;";
				echo "結束日期：" . JHtml::_('calendar', "{$end}", "calendar_end", "calendar_end");
			?>
			<input type="button" class="btn" id="filter_submit" value="查詢" />
		</div>
	<?php if($peference_id) { ?>
		<div class="total_num">投稿人數:<?php echo $this->total; ?></div>	
		<table class="table table-striped" id="weblinkList">
			<thead>
				<tr>
					<th width="1%" class="nowrap center hidden-phone">
						作品編號
					</th>
					<th class="nowrap center">
						投稿人姓名
					</th>
				
					<th class="nowrap center">
						身分證字號
					</th>
					
					<th class="nowrap center">
						聯絡電話
					</th>
					
					<th class="nowrap center">
						行動電話
					</th>
					
					<th class="nowrap center">
						Email
					</th>

					<th class="nowrap center">
						縣市
					</th>

					<th class="nowrap center">
						作品照片
					</th>

					<th class="nowrap center">
						投稿時間
					</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="10">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php foreach ($this->items as $i => $item) :
				$ordering   = ($listOrder == 'a.ordering');
				$canCreate  = $user->authorise('core.create',     'com_activities.category.' . $item->catid);
				$canEdit    = $user->authorise('core.edit',       'com_activities.category.' . $item->catid);
				$canCheckin = $user->authorise('core.manage',     'com_checkin') || $item->checked_out == $user->get('id') || $item->checked_out == 0;
				$canChange  = $user->authorise('core.edit.state', 'com_activities.category.' . $item->catid) && $canCheckin;
				?>
				<tr class="row<?php echo $i % 2; ?>" sortable-group-id="<?php echo $item->catid?>">
					<td class="center hidden-phone">
						<?php echo (int) $item->id; ?>
					</td>

					<td class="center">
						<?php echo $this->escape($item->Name); ?>
					</td>
					
					<td class="center">
						<?php echo $this->escape($item->UserID); ?>
					</td>

					<td class="center">
						<?php echo $this->escape($item->Tel); ?>
					</td>
					
					<td class="center">
						<?php echo $this->escape($item->Mobile); ?>
					</td>
					
					<td class="center">
						<?php echo $this->escape($item->Email); ?>
					</td>
					
					<td class="center">
						<?php echo $this->escape($item->city_name); ?>
					</td>
					
					<td class="center item_img">
						<img src="../<?php echo $item->PicPath; ?>" href="../<?php echo $item->PicPath; ?>" title="<?php echo $item->Introduction; ?>" />
					</td>
					
					<td class="center">
						<?php echo $this->escape($item->CreateTime); ?>
					</td>
					
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	<?php }else{ ?>
		<div>請選擇活動。</div>
	<?php } ?>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

<style>
	.item_img img {
		max-width: 160px;
		max-height: 160px;
	}
	
	.item_img img:hover {
		cursor: pointer;
	}
	
	#filter_submit {
		margin-bottom: 10px;
	}
	
</style>