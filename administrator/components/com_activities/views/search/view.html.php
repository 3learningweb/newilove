<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of activities.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.5
 */
class ActivitiesViewSearch extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		error_reporting(0);
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->peferences	= $this->get('Peference');
		$this->total		= $this->get('Total');
		$this->city			= $this->get('City');
		
		ActivitiesHelper::addSubmenu('search');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/activities.php';

		$state	= $this->get('State');
		$canDo	= ActivitiesHelper::getActions($state->get('filter.category_id'));
		$user	= JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_ACTIVITIES') . "-作品資料查詢", 'activities.png');

		JHtmlSidebar::setAction('index.php?option=com_activities&view=search');

		JHtmlSidebar::addFilter(
			JText::_('- 選擇活動 -'),
			'filter_peference_id',
			JHtml::_('select.options', $this->peferences, 'catid', 'title', $this->state->get('filter.peference_id'))
		);

		$level = JAccess::getAuthorisedViewLevels($user->get('id'));
		$rank = JComponentHelper::getParams('com_activities')->get('rank_access');	// 家教
		if(!in_array($rank, $level)) {
			JHtmlSidebar::addFilter(
				JText::_('- 選擇縣市 -'),
				'filter_city_id',
				JHtml::_('select.options', $this->city, 'id', 'title', $this->state->get('filter.city_id'))
			);
		}

	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			'a.id' => "作品編號",
			'a.Name' => "投稿人姓名",
			'a.UserID' => "身分證字號",
			'a.Tel' => "聯絡電話",
			'a.Email' => "Email",
			'a.city' => "縣市",
			'a.CreateTime' => "投稿時間"
		);
	}
}
