<table class="datatable" width="800">
	<thead>
	<tr>
		<th width="5%" align="center">#</th>
		<th width="20%" align="center">編號</th>
		<th width="20%" align="center">分數</th>
		<th width="">照片</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach( $this->rows as $key => $row ):?>
	<tr>
		<td align="center"><?php echo $key+1; ?></td>
		<td align="center"><?php echo $row->id; ?></td>
		<td align="center"><?php echo ($row->Score) ? $row->Score : "0"; ?></td>
		<td align="center"><img src="../<?php echo $row->PicPath; ?>" /></td>
	</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<style>
	.contentpane {
		padding-top: 0px;
	}
	
	.datatable th {
		padding: 5px;		
	}
	
	.datatable td {
		line-height: 25px;
		border-top: 1px solid #ddd;
	}
	
	img {
		max-height: 80px;
		max-width: 150px;
	}
</style>