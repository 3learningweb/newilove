<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of rematch.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.5
 */
class ActivitiesViewRematch extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		error_reporting(0);
		$app = JFactory::getApplication();
		$layout = $app->input->getString('layout');
		$this->peference_id = $app->getUserState("form.activities.peference");
		$this->peferences	= $this->get('Peference');
		$catid = $this->peferences[$this->peference_id]->catid;
		
		if($catid) {
			$this->state		= $this->get('State');
			$this->items 		= $this->get('Items');
			$this->allitems 	= $this->get('AllItems');
			$this->pagination	= $this->get('Pagination');
			
			$app->setUserState("form.activities.rematch_limit", $this->peferences[$this->peference_id]->rematch_limit);
			$app->setUserState("form.activities.catid", $catid);

			if($layout == 'score') {
				$model = $this->getModel();
				$this->selected = $model->getSelected($catid);
			}

			$this->check		= $this->get('Check');
			$this->re_check 	= $this->get('RematchCheck');

			if($layout == 'sort') {
				$this->rows = $this->get('Sort');
			}

			if($this->check && !$this->selected && !$this->re_check && $layout != 'sort') {
				$app->redirect("index.php?option=com_activities&view=rematch&layout=score");
			}
	
		}
		

		ActivitiesHelper::addSubmenu('rematch');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/activities.php';
		
		$app = JFactory::getApplication();
		
		$state	= $this->get('State');
		$canDo	= ActivitiesHelper::getActions($state->get('filter.category_id'));
		$user	= JFactory::getUser();
		$layout = $app->input->getString("layout");

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		if ($canDo->get('core.admin'))
		{
			JToolbarHelper::preferences('com_activities');
		}

		JToolbarHelper::title(JText::_('COM_ACTIVITIES') . "-決賽評選", 'appform.png');
		
		if($this->peference_id && !$this->re_check) {
			switch($layout) {
				case 'score':
					JToolbarHelper::custom('rematch.score_save', 'save', '', '確定送出', false, true );
					break;
					
				default:
					JToolbarHelper::custom('rematch.score', 'pencil-2', '', '下一步：評分', false, true );
			}
		}
		
		JHtmlSidebar::setAction('index.php?option=com_activities&view=rematch');

	}


	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			// 'a.state' => '處理狀態',
			// 'a.doc_num' => '案件編號',
			// 'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}

}
