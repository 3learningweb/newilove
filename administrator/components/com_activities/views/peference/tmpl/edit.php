<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
$id = $app->input->getInt('id');

?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'Peference.cancel' || document.formvalidator.isValid(document.id('peference-form')))
		{
			<?php // echo $this->form->getField('id')->save(); ?>
			Joomla.submitform(task, document.getElementById('peference-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_activities&layout=edit&id='.(int) $this->item->id); ?>" method="post" 
	  name="adminForm" id="peference-form" class="form-validate" enctype="multipart/form-data">

	<?php echo JLayoutHelper::render('joomla.edit.item_title', $this); ?>

	<div class="row-fluid">
		<!-- Begin Activities -->
		<div class="span10 form-horizontal">

	<fieldset>
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details')); ?>

			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'details', empty($this->item->id) ? "新增項目" : "修改項目"); ?>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('catid'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('catid'); ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('title'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('title'); ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('preliminary_limit'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('preliminary_limit'); ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('preliminary_rank_limit'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('preliminary_rank_limit'); ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('rematch_limit'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('rematch_limit'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('state'); ?></div>
				</div>

			<?php echo JHtml::_('bootstrap.endTab'); ?>
			
		<?php echo JHtml::_('bootstrap.endTab'); ?>

			<input type="hidden" name="task" value="" />
			<?php echo JHtml::_('form.token'); ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
		
		</fieldset>
		</div>
	</div>
		<!-- End Activities -->
</form>