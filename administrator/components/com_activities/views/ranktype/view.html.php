<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of ranktype.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.5
 */
class ActivitiesViewRanktype extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		error_reporting(0);
		$this->form		= $this->get('Form');
		$this->type 	= $this->get('RankType');
		
		$model = $this->getModel() ;
  		$this->pfields	= $model->getPFields($this->type->id);
		$this->rfields	= $model->getRFields($this->type->id);

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user		= JFactory::getUser();
		$isNew		= ($this->item->id == 0);
		$checkedOut	= !($this->item->checked_out == 0 || $this->item->checked_out == $user->get('id'));
		// Since we don't track these assets at the item level, use the category id.
		$canDo		= ActivitiesHelper::getActions($this->item->catid, 0);

		JToolbarHelper::title(JText::_('活動排名設定'), 'generic.png');

		// If not checked out, can save the item.
		if (!$checkedOut && ($canDo->get('core.edit')||(count($user->getAuthorisedCategories('com_activities', 'core.create')))))
		{
			JToolbarHelper::custom('ranktype.save', 'save', '', '儲存', false, true );
		}

		if (empty($this->item->id))
		{
			JToolbarHelper::cancel('ranktype.cancel');
		}
		else
		{
			JToolbarHelper::cancel('ranktype.cancel', 'JTOOLBAR_CLOSE');
		}
	}
}
