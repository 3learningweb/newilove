<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
$item_id = $app->input->getInt("item_id");

$rank_type = array("0"=>"自訂選項", "1"=>"數字排名");

?>
<script>
	(function($) {
		$(document).ready(function() {
			// 初賽
			var rank_ptype = $("#preliminary_type");
			if(rank_ptype.val() == "0") {
				$("#preliminary_type_field").show();
			}
			
			$("#preliminary_type").change(function() {
				if(rank_ptype.val() == "0") {
					$("#preliminary_type_field").show();
				}else{
					$("#preliminary_type_field").hide();
				}
			});
			
			$("#ptext_btn").click(function() {
				var num = $(".ptext").length;
				$("#ptext_td").append("<input type='text' name='ptext[]' class='ptext' id='ptext"+(num+1)+"' value='' /> <input type='text' name='pnum[]' class='pnum' id='pnum"+(num+1)+"' /><br/>");
			});
			
			// 決賽
			var rank_rtype = $("#rematch_type");
			if(rank_rtype.val() == "0") {
				$("#rematch_type_field").show();
			} 
			
			$("#rematch_type").change(function() {
				if(rank_rtype.val() == "0") {
					$("#rematch_type_field").show();
				}else{
					$("#rematch_type_field").hide();
				}
			});
			
			$("#rtext_btn").click(function() {
				var num = $(".rtext").length;
				$("#rtext_td").append("<input type='text' name='rtext[]' class='rtext' id='rtext"+(num+1)+"' value='' /><br/>");
			});
		});
	})(jQuery);
</script>

<div class="accordion-group">
	<form action="<?php echo JRoute::_('index.php?option=com_activities&view=ranktype'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate">
		<div class="row-fluid">
			<!-- Begin ranktype -->
			<div class="span10 form-horizontal">
	
			<fieldset>
			<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'preliminary')); ?>		
				
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'preliminary', JText::_('初賽', true)); ?>
					<table width="100%" class="admintable" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="key">初賽排名類型：</td>
							<td><?php echo JHtml::_('select.genericlist ', $rank_type , 'preliminary_type', 'class="select_type"', '', '', $this->type->preliminary_type); ?></td>
						</tr>
						<tr id="preliminary_type_field" style="display: none;">
							<td class="key">
								選項文字&人數：
								<input type="button" id="ptext_btn" class="btn add_btn" value="增加選項" />	
							</td>
							<td id="ptext_td">
								<?php if($this->pfields) { ?>
									<?php foreach($this->pfields as $key => $pfield) { ?> 
										<input type="text" name="ptext[]" class="ptext" id="ptext<?php echo $key+1; ?>" value="<?php echo $pfield->text; ?>" />
										<input type="text" name="pnum[]" class="pnum" id="pnum<?php echo $key+1; ?>" value="<?php echo $pfield->num; ?>" /><br/>
									<?php } ?>
								<?php }else{ ?>
									<input type="text" name="ptext[]" class="ptext" id="ptext1" value="" />
									<input type="text" name="pnum[]" class="pnum" id="pnum1" value="" /><br/>
									
									<input type="text" name="ptext[]" class="ptext" id="ptext2" value="" />
									<input type="text" name="pnum[]" class="pnum" id="pnum2" value="" /><br/>
								<?php } ?>
							</td>
						</tr>
					</table>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				
				<?php /* echo JHtml::_('bootstrap.addTab', 'myTab', 'rematch', JText::_('決賽', true)); ?>
					<table width="100%" class="admintable" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="key">決賽排名類型</td>
							<td><?php echo JHtml::_('select.genericlist ', $rank_type , 'rematch_type', 'class="select_type"', '', '', ($this->type) ? $this->type->rematch_type : "1"); ?></td>
						</tr>
						<tr id="rematch_type_field" style="display: none;">
							<td class="key">
								選項文字：
								<input type="button" id="rtext_btn" class="btn add_btn" value="增加選項" />
							</td>
							<td id="rtext_td">
								<?php if($this->rfields) { ?>
									<?php foreach($this->rfields as $key => $rfield) { ?>
										<input type="text" name="rtext[]" class="rtext" id="rtext<?php echo $key+1; ?>" value="<?php echo $rfield->text; ?>" /><br/>
									<?php } ?>
								<?php }else{ ?>
									<input type="text" name="rtext[]" class="rtext" id="rtext1" value="" /><br/>
									<input type="text" name="rtext[]" class="rtext" id="rtext2" value="" /><br/>
								<?php } ?>
							</td>
						</tr>
					</table>
				<?php echo JHtml::_('bootstrap.endTab'); */ ?>
				
				<input type="hidden" name="id" value="<?php echo $this->type->id; ?>" />
				<input type="hidden" name="item_id" value="<?php echo $item_id; ?>" />
				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>
				
			<?php echo JHtml::_('bootstrap.endTabSet'); ?>		
			</fieldset>
			</div>
		</div>
		<!-- End activities -->
	</form>
</div>

<style>
	.accordion-group {
		padding: 10px 10px 10px 10px;
	}
	#admin-form {
		margin-bottom: 0px;
	}
	.admintable .key {
		background-color: #f6f6f6;
		text-align: right;
		width: 140px;
		color: #666;
		font-weight: bold;
		border-bottom: 1px solid #e9e9e9;
		border-right: 1px solid #e9e9e9;		
	}
	.admintable td {
		padding: 3px;
	}
	.admintable #message {
		width: 500px;
	}
	
	.ptext, .rtext {
		margin-bottom: 5px !important;
	}
	
	.pnum {
		width: 30px !important;
		margin-bottom: 5px !important;
	}
</style>