<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
$post = $app->input->getArray($_POST);

$user		= JFactory::getUser();
$userId		= $user->get('id');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$canOrder	= $user->authorise('core.edit.state', 'com_activities.category');
$saveOrder	= $listOrder == 'a.ordering';
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_activities&task=search_2015.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'weblinkList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();

?>
<script type="text/javascript" src="components/com_activities/assets/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="components/com_activities/assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="components/com_activities/assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

<script type="text/javascript">
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			$(".item_img img").fancybox();
			
		});
	})(jQuery);
</script>

<form action="<?php echo JRoute::_('index.php?option=com_activities&view=search_2015'.$append); ?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
	<?php if(1) { ?>
		<table class="table table-striped" id="weblinkList">
			<thead>
				<tr>
					<th width="1%" class="nowrap center hidden-phone">
						作品編號
					</th>
					<th class="nowrap center">
						投稿人姓名
					</th>
					
					<th class="nowrap center">
						聯絡電話
					</th>
					
					<th class="nowrap center">
						Email
					</th>

					<th class="nowrap center">
						學校
					</th>

					<th class="nowrap center">
						標題
					</th>

					<th class="nowrap center">
						作品照片
					</th>

					<th class="nowrap center">
						投稿時間
					</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="10">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php foreach ($this->items as $i => $item) :
				$ordering   = ($listOrder == 'a.ordering');
				$canCreate  = $user->authorise('core.create',     'com_activities.category.' . $item->catid);
				$canEdit    = $user->authorise('core.edit',       'com_activities.category.' . $item->catid);
				$canCheckin = $user->authorise('core.manage',     'com_checkin') || $item->checked_out == $user->get('id') || $item->checked_out == 0;
				$canChange  = $user->authorise('core.edit.state', 'com_activities.category.' . $item->catid) && $canCheckin;
				?>
				<tr class="row<?php echo $i % 2; ?>" sortable-group-id="<?php echo $item->catid?>">
					<td class="center hidden-phone">
						<?php echo (int) $item->id; ?>
					</td>

					<td class="center">
						<?php echo $this->escape($item->Name); ?>
					</td>

					<td class="center">
						<?php echo $this->escape($item->Tel); ?>
					</td>
					
					<td class="center">
						<?php echo $this->escape($item->Email); ?>
					</td>
					
					<td class="center">
						<?php echo $this->school[$this->escape($item->School)]; ?>
					</td>

					<td class="center">
						<?php echo $this->escape($item->Title); ?>
					</td>
					
					<td class="center item_img">
						<img src="../<?php echo $item->PicPath; ?>" href="../<?php echo $item->PicPath; ?>" title="<?php echo $item->Introduction; ?>" />
					</td>
					
					<td class="center">
						<?php echo $this->escape($item->CreateTime); ?>
					</td>
					
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	<?php }else{ ?>
		<div>請選擇活動。</div>
	<?php } ?>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

<style>
	.item_img img {
		max-width: 160px;
		max-height: 160px;
	}
	
	.item_img img:hover {
		cursor: pointer;
	}
	
	#filter_submit {
		margin-bottom: 10px;
	}
	
</style>