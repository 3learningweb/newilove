<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * phase2check list controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesControllerPhase2check extends JControllerAdmin {

	/**
	 * Proxy for getModel.
	 * @since   1.6
	 */
	public function getModel($name = 'Guide', $prefix = 'ActivitiesModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	/**
	 * Method to provide child classes the opportunity to process after the delete task.
	 *
	 * @param   JModelLegacy   $model   The model for the component
	 * @param   mixed          $ids     array of ids deleted.
	 *
	 * @return  void
	 *
	 * @since   3.1
	 */
	protected function postDeleteHook(JModelLegacy $model, $ids = null) {

	}
	
	function savepeference() {
		$app = JFactory::getApplication();
		$peference_id = $app->input->getInt('peference');
		
		if(!$peference_id) {
			$msg = "請選擇活動";
		}
		
		$app->setUserState("form.activities.peference", $peference_id);
		
		$link = JRoute::_("index.php?option=com_activities&view=phase2check", false);
		$this->setRedirect($link, $msg, 'error');
		
		return;
	}
	
	function save() {
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		$post = $app->input->getArray($_POST);
		$delete = (array) $app->getUserState('form.activities.delete');
		$in = "'" . implode("','", $delete) . "'";

		// 存入DB  更新decision flg欄位 確認台師大帳號已檢核過
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		
		$query->update($db->quoteName('#__preliminary_decision'));
		$query->set("Flg = '1'");
		$query->where("ContributeListCde NOT IN ({$in})");
		$query->where("catid = '{$post['catid']}'");

		$db->setQuery($query);
		if(!$db->execute()) {
			$msg = "資料存入時發生不明錯誤！請連繫網站管理人員。";
			$link = JRoute::_("index.php?option=com_activities&view=phase2check", false);
			$this->setRedirect($link, $msg);
			return;
		}
		
		// 成功後清除陣列
		$app->setUserState('form.activities.delete', null);
		
		$msg = "您已完成作品檢核，資料已送出";
		$link = JRoute::_("index.php?option=com_activities&view=phase2check", false);
		$this->setRedirect($link, $msg);
		
	}
	
}
