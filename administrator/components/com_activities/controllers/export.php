<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Activities list controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesControllerExport extends JControllerAdmin
{
	/**
	 * Proxy for getModel.
	 * @since   1.6
	 */
	public function getModel($name = 'Export', $prefix = 'ActivitiesModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	/**
	 * Method to provide child classes the opportunity to process after the delete task.
	 *
	 * @param   JModelLegacy   $model   The model for the component
	 * @param   mixed          $ids     array of ids deleted.
	 *
	 * @return  void
	 *
	 * @since   3.1
	 */
	protected function postDeleteHook(JModelLegacy $model, $ids = null)
	{
	}
	
	
	function savepeference() {
		$app = JFactory::getApplication();
		$peference_id = $app->input->getInt('peference');
		
		if(!$peference_id) {
			$msg = "請選擇活動";
		}
		
		$app->setUserState("form.activities.peference", $peference_id);
		
		$link = JRoute::_("index.php?option=com_activities&view=export", false);
		$this->setRedirect($link, $msg, 'error');
		
		return;
	}

	function savetype() {
		$app = JFactory::getApplication();
		$type = $app->input->getString('type');
		
		if(!type) {
			$msg = "請選擇活動";
		}
		
		$app->setUserState("form.activities.type", $type);
		
		$link = JRoute::_("index.php?option=com_activities&view=export", false);
		$this->setRedirect($link, $msg, 'error');
		
		return;
	}

}
