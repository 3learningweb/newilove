<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * phase1rank list controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_activities
 * @since       1.6
 */
class ActivitiesControllerPhase1rank extends JControllerAdmin {

	/**
	 * Proxy for getModel.
	 * @since   1.6
	 */
	public function getModel($name = 'Guide', $prefix = 'ActivitiesModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	/**
	 * Method to provide child classes the opportunity to process after the delete task.
	 *
	 * @param   JModelLegacy   $model   The model for the component
	 * @param   mixed          $ids     array of ids deleted.
	 *
	 * @return  void
	 *
	 * @since   3.1
	 */
	protected function postDeleteHook(JModelLegacy $model, $ids = null) {

	}
	
	function savepeference() {
		$app = JFactory::getApplication();
		$peference_id = $app->input->getInt('peference');
		
		if(!$peference_id) {
			$msg = "請選擇活動";
		}
		
		$app->setUserState("form.activities.peference", $peference_id);
		
		$link = JRoute::_("index.php?option=com_activities&view=phase1rank", false);
		$this->setRedirect($link, $msg, 'error');
		
		return;
	}
	
	function save() {
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		$post = $app->input->getArray($_POST);
		$ranks = (array) $app->getUserState('form.activities.ranks');
		$rank_limit = $app->getUserState("form.activities.preliminary_rank_limit");
		$model = JModelLegacy::getInstance( 'phase1rank', 'ActivitiesModel');
		$score = $model->getScore();
		$db = JFactory::getDBO();

		if(count($ranks) < 1 or count($ranks) > $rank_limit) {
			$msg = "入選件數不得少於或超過限制";
			$link = JRoute::_("index.php?option=com_activities&view=phase1rank", false);
			$this->setRedirect($link, $msg, 'error');
			return;
		}

		// 存入DB
		foreach($ranks as $key => $rank) {
			$query = $db->getQuery(true);
			$query->select("ContributeListCde");
			$query->from($db->quoteName('#__preliminary_decision'));
			$query->where("ContributeListCde = '{$key}'");
			$query->where("catid = '{$post['catid']}'");
			
			$db->setQuery($query);
			$item = $db->loadObject();

			$date = JFactory::getDate();
			$datetime = $date->toSql();

			if(!$item) {
				$total = $score[$key]->score;
				$columns = array('ContributeListCde', 'catid', 'total', 'Rno', 'IsCheck', 'CreateTime', 'ModifyTime', 'Flg');
				$values = array(
						"'{$key}'",
						"'{$post['catid']}'",
						"'{$total}'",
						"'{$rank}'",
						"'1'",
						"'{$datetime}'",
						"'{$datetime}'",
						"'0'"
				);
				
				$query_rank = $db->getQuery(true);
				$query_rank->insert($db->quoteName('#__preliminary_decision'));
				$query_rank->columns($columns);
				$query_rank->values(implode(',', $values));
				
				$db->setQuery($query_rank);
				if(!$db->execute()) {
					$msg = "資料存入時發生不明錯誤！請連繫網站管理人員。";
					$link = JRoute::_("index.php?option=com_activities&view=phase1rank", false);
					$this->setRedirect($link, $msg);
					return;
				}
			}else{
				$query_rank = $db->getQuery(true);
				$query_rank->update($db->quoteName('#__preliminary_decision'));
				$query_rank->set("Rno = '{$rank}'");
				$query_rank->set("ModifyTime = '{$datetime}'");
				$query_rank->where("ContributeListCde = '{$key}'");
				$query_rank->where("catid = '{$post['catid']}'");
				
				$db->setQuery($query_rank);
				if(!$db->execute()) {
					$msg = "資料存入時發生不明錯誤！請連繫網站管理人員。";
					$link = JRoute::_("index.php?option=com_activities&view=phase1rank", false);
					$this->setRedirect($link, $msg);
					return;
				}
			}
		}

		$columns = array('UsrID', 'catid', 'IsCheck');
		$values = array(
				"'{$user->get('id')}'",
				"'{$post['catid']}'",
				"'1'"
		);

		$query_check = $db->getQuery(true);
		$query_check->insert($db->quoteName('#__preliminary_rank_check'));
		$query_check->columns($columns);
		$query_check->values(implode(',', $values));
		
		$db->setQuery($query_check);
		
		if(!$db->execute()) {
			$msg = "資料存入時發生不明錯誤！請連繫網站管理人員。";
			$link = JRoute::_("index.php?option=com_activities&view=phase1asrank", false);
			$this->setRedirect($link, $msg);
			return;
		}
		
		// 成功後清除陣列
		$app->setUserState('form.activities.ranks', null);
		
		$msg = "您已完成評選排名，資料已送出";
		$link = JRoute::_("index.php?option=com_activities&view=phase1rank", false);
		$this->setRedirect($link, $msg);
		
	}
	
}
