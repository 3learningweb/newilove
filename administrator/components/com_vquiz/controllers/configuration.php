<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class VquizControllerConfiguration extends VquizController
{
		function __construct()
		{
			parent::__construct();
			$this->registerTask( 'add'  , 	'edit' );
		}
 
		function edit()
		{
			JRequest::setVar( 'view', 'configuration' );
			JRequest::setVar( 'layout', 'form'  );
			JRequest::setVar('hidemainmenu', 1);
			parent::display();
		}
 
	function apply()
		{
				  $model = $this->getModel('configuration');
				if($model->store()) {
					$msg = JText::_( 'Greeting Save' );
					$this->setRedirect( 'index.php?option=com_vquiz&view=configuration', $msg );
				} else {
					jerror::raiseWarning('', $this->model->getError());
					$this->setRedirect( 'index.php?option=com_vquiz&view=configuration');
				}

		}
		function cancel()
		{
			$msg = JText::_( 'Operation Cancelled' );
			$this->setRedirect( 'index.php?option=com_vquiz&view=vquiz', $msg );
		}
 
}