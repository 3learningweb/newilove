<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class VquizControllerUsers extends VquizController
{
	
	function __construct()
	{
		parent::__construct();
		
		$this->model = $this->getModel('users');
 		JRequest::setVar( 'view', 'users' );
 		$this->registerTask( 'add'  , 	'edit' );
		$this->registerTask( 'unpublish',	'publish' );

	}
 
	function edit()
	{
		JRequest::setVar( 'view', 'users' );
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);
		
		parent::display();
	}

 
	function save()
	{
		
		if($this->model->store()) {
			$msg = JText::_('RECORD_SAVED');
			$this->setRedirect( 'index.php?option=com_vquiz&view=users', $msg );
		} else {
			jerror::raiseWarning('', $this->model->getError());
			$this->setRedirect( 'index.php?option=com_vquiz&view=users');
		}

	}
	
	function apply()
	{
		
		if($this->model->store()) {
			$msg = JText::_('RECORD_SAVED');
			$this->setRedirect( 'index.php?option=com_vquiz&view=users&task=edit&cid[]='.JRequest::getInt('id', 0), $msg );
		} else {
			jerror::raiseWarning('', $this->model->getError());
			$this->setRedirect( 'index.php?option=com_vquiz&view=users&task=edit&cid[]='.JRequest::getInt('id', 0) );
		}

	}
 
	function publish()
	{
		$task		= JRequest::getCmd( 'task' );
		$msg  	= $task == 'publish' ? JText::_( 'Record(s) Published successfully' ) : JText::_( 'Record(s) Unublished successfully' );
		if($this->model->publish()) {
			 
			$this->setRedirect( 'index.php?option=com_vquiz&view=users', $msg );
		} else {
			jerror::raiseWarning('', $this->model->getError());
			$this->setRedirect( 'index.php?option=com_vquiz&view=users' );
		}

	}
 
	function remove()
	{
		
		if($this->model->delete()) {
			$msg = JText::_('RECORD_DELETED');
			$this->setRedirect( 'index.php?option=com_vquiz&view=users', $msg );
		} else {
			jerror::raiseWarning('', $this->model->getError());
			$this->setRedirect( 'index.php?option=com_vquiz&view=users');
		}
		
	}
 
	function cancel()
	{
		$msg = JText::_('CANCELLED');
		$this->setRedirect( 'index.php?option=com_vquiz&view=users', $msg );
	}
	
}