<?php

/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access

defined('_JEXEC') or die('Restricted access');

class VquizControllerResultcats extends VquizController {

	function __construct() {

		parent::__construct();
		$this->registerTask('add', 'edit');
		$this->registerTask('unpublish', 'publish');
		$this->registerTask('orderup', 'reorder');
		$this->registerTask('orderdown', 'reorder');
	}

	function edit() {
		JRequest::setVar('view', 'resultcats');
		JRequest::setVar('layout', 'form');
		JRequest::setVar('hidemainmenu', 1);
		parent::display();
	}

	function publish() {

		$model = $this->getModel('resultcats');
		$msg = $model->publish();
		$this->setRedirect('index.php?option=com_vquiz&view=resultcats', $msg);
	}

	function reorder() {

		$ids = JFactory::getApplication()->input->post->get('cid', array(), 'array');
		$inc = ($this->getTask() == 'orderup') ? -1 : 1;
		$model = $this->getModel('resultcats');
		$return = $model->reorder($ids, $inc);
		if ($return) {
			$msg = 're-Order Saved';
			$this->setRedirect('index.php?option=com_vquiz&view=resultcats', $msg);
		}
	}

	function saveOrder() {
		$input = JFactory::getApplication()->input;
		$pks = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');
		JArrayHelper::toInteger($pks);
		JArrayHelper::toInteger($order);
		$model = $this->getModel('resultcats');
		$return = $model->saveorder($pks, $order);
		if ($return) {
			$msg = 'New Order Saved';
			$this->setRedirect('index.php?option=com_vquiz&view=resultcats', $msg);
		}
	}

	function save() {
		$model = $this->getModel('resultcats');
		if ($model->store()) {

			$msg = JText::_('Category saved successfully');
			$this->setRedirect('index.php?option=com_vquiz&view=resultcats', $msg);
		} else {
			$msg = $model->getError();
			$this->setRedirect('index.php?option=com_vquiz&view=resultcats', $msg);
		}
	}

	function apply() {

		$model = $this->getModel('resultcats');
		if ($model->store()) {

			$msg = JText::_('Category saved successfully');
			$this->setRedirect('index.php?option=com_vquiz&view=resultcats&task=edit&cid[]=' . JRequest::getInt('id', 0), $msg);
		} else {
			jerror::raiseWarning('', $model->getError());
			$this->setRedirect('index.php?option=com_vquiz&view=resultcats&task=edit&cid[]=' . JRequest::getInt('id', 0));
		}
	}

	function remove() {
		$model = $this->getModel('resultcats');
		if (!$model->delete()) {
			$msg = JText::_('Error: One or More Greetings Could not be Deleted');
		} else {
			$msg = JText::_('Resultcats(s) Deleted');
		}
		$this->setRedirect('index.php?option=com_vquiz&view=resultcats', $msg);
	}

	function cancel() {
		$msg = JText::_('Operation Cancelled');
		$this->setRedirect('index.php?option=com_vquiz&view=resultcats', $msg);
	}

	function importcategorycsv() {

		jimport('joomla.filesystem.file');
		$db = JFactory::getDBO();
		$questioncsv = JRequest::getVar("categorycsv", null, 'files', 'array');

		$questioncsv['categorycsv'] = str_replace(' ', '', JFile::makeSafe($csv['name']));
		$temp = $questioncsv["tmp_name"];


		if (is_file($temp)) {

			$fp = fopen($temp, "r");

			while (($data = fgetcsv($fp, 100000, ",", '"')) !== FALSE) {

				$insert = new stdClass();
				$insert->id = null;
				$insert->quiztitle = $data[1];
				$insert->alias = $data[2];
				$insert->published = $data[3];
				$insert->created_date = $data[4];
				$insert->photopath = $data[5];
				$insert->ordering = $data[6];
				$insert->language = $data[7];
				$insert->access = $data[8];
				$insert->meta_desc = $data[9];
				$insert->meta_keyword = $data[10];

				if (!$db->insertObject('#__vquiz_resultcats', $insert, 'id')) {

					$msg = $this->setError($db->stderr());
					$this->setRedirect('index.php?option=com_vquiz&view=resultcats', $msg);
					return false;
				}
			}
			fclose($fp);
			$msg = JText::_('CSV_IMPORT_SUCCESS');
			$this->setRedirect('index.php?option=com_vquiz&view=resultcats', $msg);
		}
	}

	function export() {
		$model = $this->getModel('resultcats');
		$model->getCsv();
		$dispatcher = JDispatcher::getInstance();

		try {
			$dispatcher->trigger('startExport');
			jexit(/* JText::_('INTERNAL_SERVER_ERROR') */);
		} catch (Exception $e) {
			jerror::raiseWarning('', $e->getMessage());
			$this->setRedirect('index.php?option=com_vquiz&view=resultcats', $msg);
		}
	}

}