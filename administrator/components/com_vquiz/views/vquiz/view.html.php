<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' );
class VquizViewVquiz extends JViewLegacy
{ 

    function display($tpl = null)
    {

		$document =  JFactory::getDocument();
		JToolBarHelper::title( JText::_( 'vQuiz' ), 'vquiz.png' );
//		JToolBarHelper::help('help', true);
		$this->category = $this->get('Category');
		parent::display($tpl);

 
    }

  

}
 