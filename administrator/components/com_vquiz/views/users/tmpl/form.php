<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_vquiz/assets/css/style.css');
JHtml::_('behavior.tooltip');
 ?>

<script type="text/javascript">
	
	Joomla.submitbutton = function(task) {
		if (task == 'cancel') {
			
			Joomla.submitform(task, document.getElementById('adminForm'));
		} else {
			
			 
			if(!jQuery('input[name="name"]').val()){
				alert('<?php echo JText::_('PLZ_ENTER_NAME', true); ?>');
				document.adminForm.name.focus();
				return false;
			}
			if(!jQuery('input[name="username"]').val()){
				alert('<?php echo JText::_('PLZ_ENTER_USERNAME', true); ?>');
				document.adminForm.username.focus();
				return false;
			}
			if(!jQuery('input[name="email"]').val()){
				alert('<?php echo JText::_('PLZ_ENTER_EMAIL', true); ?>');
				 document.adminForm.email.focus();
				return false;
			}
			if(!jQuery('input[name="country"]').val()){
				alert('<?php echo JText::_('PLEASE_ENTER_COUNTRY_NAME', true); ?>');
				 document.adminForm.country.focus();
				return false;
			}
						
			Joomla.submitform(task, document.getElementById('adminForm'));
			
		}
	}
	
</script>

<div id="dabpanel">

<form action="index.php?option=com_vquiz&view=users" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<div class="col100">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'DETAILS' ); ?></legend>
<table class="adminform table table-striped">
  <tr>
    <td width="200"><label class="required"><?php echo JText::_('NAME'); ?></label></td>
    <td><input type="text" name="name" id="name" class="inputbox required" value="<?php echo $this->item->name; ?>" size="50" /></td>
  </tr>
  <tr>
    <td><label class="required"><?php echo JText::_('USERNAME'); ?></label></td>
    <td><input type="text" name="username" id="username" class="inputbox required" value="<?php echo $this->item->username; ?>" size="50" /></td>
  </tr>
  <tr>
    <td><label class="required"><?php echo JText::_('PASSWORD'); ?></label></td>
    <td><input type="password" class="inputbox" name="password" autocomplete="off" value="" cols="39" rows="5" /></td>
  </tr>
   <tr>
    <td><label class="required"><?php echo JText::_('EMAIL'); ?></label></td>
    <td><input type="text" class="inputbox required" name="email" maxlength="100" size="50" value="<?php echo $this->item->email;?>" /></td>
  </tr>
  <tr>
    <td><label class="required"><?php echo JText::_('Block'); ?></label></td>
    <td>
    <select name="block" id="block" class="inputbox">
 
        
        <option value="0" <?php if($this->item->block=="0") echo 'selected="selected"'; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php if($this->item->block == "1") echo 'selected="selected"'; ?>><?php echo JText::_('Yes'); ?></option>
    </select>
   </td>
  </tr>
  <tr>
    <td><label class="required"><?php echo JText::_('Country'); ?></label></td>
    <td><input type="text" class="inputbox required" name="country" maxlength="100" size="50" value="<?php echo $this->item->country;?>" /></td>
  </tr>
  <tr>
  <td><label class="hasTip" title="<?php echo JText::_('PROFILE_PICS'); ?>"><?php echo JText::_('PROFILE_PICS'); ?></label></td>
  <td class="profile_picture"><input type="file" name="profile_pic" id="profile_pic" class="inputbox required" size="50" value=""/>
<?php /*?>  		<?php echo '<img src="'.JURI::root().'administrator/components/com_vquiz/assets/uploads/users/'.$this->item->profile_pic.'"/>'?><?php */?>
        <?php 
                if(!empty($this->item->profile_pic)){ 
                echo '<img src="'.JURI::root().'administrator/components/com_vquiz/assets/uploads/users/'.$this->item->profile_pic.'"/>'; 
                }else { echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/no_image.png" alt="Image Not available" border="1" />';} 
                ?>
      </td>
  </tr>
</table>
	</fieldset>
</div>
<div class="clr"></div>
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="com_vquiz" />
<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="view" value="users" />
</form>

</div>
<div class="copyright" align="center">
	<?php echo JText::_( 'WDM' );?>
</div>
