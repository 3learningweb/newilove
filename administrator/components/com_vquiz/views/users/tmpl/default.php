<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_vquiz/assets/css/style.css');
?>

<form action="index.php?option=com_vquiz&view=users" method="post" name="adminForm" id="adminForm">
<div class="work_sheet">
<table class="adminlist filter">
<tr>
<td class="filter_sheet" >
<div class="search_buttons">
        <div class="btn-wrapper input-append">
<input placeholder="Search" type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
<button class="btn" onclick="this.form.submit();"><i class="icon-search"></i><span class="search_text"><?php echo JText::_('SEARCH'); ?></button>
<button class="btn" onclick="document.getElementById('search').value='';this.form.submit(); "><?php echo JText::_( 'Reset' ); ?></button></div></div>
</td>

</tr>
</table>
<div id="editcell">
    <table class="adminlist table table-striped table-hover">
    <thead>
			<tr>
				<th width="10" align="left">
					<?php echo JText::_( 'Num' ); ?>
				</th>
				<th width="3%">
					<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
				</th>
				<th width="10" nowrap="nowrap">
					<?php echo JHTML::_('grid.sort', JText::_('NAME'), 'name', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
                <th width="10" nowrap="nowrap">
					<?php echo JHTML::_('grid.sort', JText::_('USERNAME'), 'username', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
  				
                <th width="10" nowrap="nowrap">
					<?php echo JHTML::_('grid.sort', JText::_('EMAIL'), 'email', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
                <th width="10" nowrap="nowrap">
					<?php echo JHTML::_('grid.sort', JText::_('ENABLED'), 'block', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
                 <th width="10" nowrap="nowrap">
					<?php echo JHTML::_('grid.sort', JText::_('LAST_VISITED'), 'lastvisitDate', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
                 <th width="10" nowrap="nowrap">
					<?php echo JHTML::_('grid.sort', JText::_('REG_DATE'), 'registerDate', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
                <th width="10" nowrap="nowrap">
					<?php echo JHTML::_('grid.sort', JText::_('ID'), 'id', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
				</th>
			</tr>
		</thead>

	<tfoot>
    <tr>
      <td colspan="9"><?php echo $this->pagination->getListFooter(); ?></td>
    </tr>
  	</tfoot>

    <?php
    $k = 0;
		
    for ($i=0, $n=count( $this->items ); $i < $n; $i++)
    {
        $row = $this->items[$i];
		$checked    = JHTML::_( 'grid.id', $i, $row->id );
				
        ?>
        <tr class="<?php echo "row$k"; ?>">
            <td><?php echo $this->pagination->getRowOffset($i); ?></td>
			
			<td align="center"><?php echo $checked; ?></td>

            <td><a href="index.php?option=com_vquiz&view=users&task=edit&cid[]=<?php echo $row->id; ?>"><?php	echo $row->name;	?></a></td>
            <td><?php echo $row->username; ?></td>
            <td><?php echo $row->email; ?></td>
            
            <td><?php $icon = $row->block?'icon-16-deny.png':'icon-16-allow.png'; ?>
            		<img src="components/com_vquiz/assets/images/<?php echo $icon; ?>" alt="<?php echo JText::_($row->block ? 'JNO' : 'JYES'); ?>" width="16" />
            </td>
            <td><?php if ($row->lastvisitDate != '0000-00-00 00:00:00'):?>
						<?php echo JHtml::_('date', $row->lastvisitDate, 'Y-m-d H:i:s'); ?>
					<?php else:?>
						<?php echo JText::_('JNEVER'); ?>
					<?php endif;?></td>
            <td><?php echo JHtml::_('date', $row->registerDate, 'Y-m-d H:i:s'); ?></td>
            <td><?php echo $row->id; ?></td>
            
        </tr>
        <?php
        $k = 1 - $k;
    }
    ?>
    </table>
</div>
</div>
 <?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="com_vquiz" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="view" value="users" />
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />

</form>
<div class="copyright">
<?php echo JText::_( 'WDM' );?>
</div>
