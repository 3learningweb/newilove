<?php
/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access
defined('_JEXEC') or die('Restricted access');

JHTML::_('behavior.tooltip');
$document = JFactory::getDocument();
$document->addScript('components/com_vquiz/assets/js/library.js');
$document->addStyleSheet('components/com_vquiz/assets/css/style.css');
//$document->addScript('components/com_vquiz/assets/js/jquery-ui.js');
$document->addStyleSheet('components/com_vquiz/assets/css/dialog.css');
if (version_compare(JVERSION, '3.0', '>='))
	JHtml::_('formbehavior.chosen', 'select');
?>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script> 
	var jq=jQuery.noConflict();
	jq(document).ready(function(){
		jq(function() {
			jq( "#tabs" ).tabs();
		});
			
			
		var html ='<select  name="show_correctans" id="show_correctans" ><option><?php echo JText::_('SELECT') ?></option>';
		html +='<option value="0" <?php if ($this->item->show_correctans == 0) echo 'selected="selected"'; ?>><?php echo JText::_('NO_WHERE'); ?> </option>';
		html +='<option value="2" <?php if ($this->item->show_correctans == 2) echo 'selected="selected"'; ?> ><?php echo JText::_('AFTER_QUIZ_END'); ?> </option>';
			
		if(jq('input[name="prev_button"]:checked').val()==0)
			html +='<option value="1" <?php if ($this->item->show_correctans == 1) echo 'selected="selected"'; ?> class="aeq"><?php echo JText::_('AFTER_EVERY_QUESTION'); ?> </option>';
 			
		html +='</select'>
			jq('td#append_td').html(html);
 			
		jq('input[name="prev_button"]').on( "click", function() {
			
			jq('td#append_td select').remove();
				
			var backbtn=jq(this).val();
				
			var html ='<select  name="show_correctans" id="show_correctans" ><option><?php echo JText::_('SELECT') ?></option>';
			html +='<option value="0" <?php if ($this->item->show_correctans == 0) echo 'selected="selected"'; ?>><?php echo JText::_('NO_WHERE'); ?> </option>';
			html +='<option value="2" <?php if ($this->item->show_correctans == 2) echo 'selected="selected"'; ?> ><?php echo JText::_('AFTER_QUIZ_END'); ?> </option>';
			
			if(backbtn==0)
				html +='<option value="1" <?php if ($this->item->show_correctans == 1) echo 'selected="selected"'; ?> class="aeq"><?php echo JText::_('AFTER_EVERY_QUESTION'); ?> </option>';
 			
			html +='</select'>
				jq('td#append_td').html(html);
				
		});
			
		if(jq("#paging").val()==1 || jq("#paging_limit").val()==1){
			jq('.question_limit_tr ').css('visibility','hidden');
			jq('#paging_limit').val(1);
		}
		else{
			jq('.question_limit_tr ').css('visibility','visible');
		}
			  
		jq('#paging').on('change',function(){
			var paging=jq(this).val();
			if(paging==1){
				jq('.question_limit_tr ').css('visibility','hidden');
				jq('#paging_limit').val(1);
			}
			else
				jq('.question_limit_tr ').css('visibility','visible');
		});
			
			  
			  
		if(jq('#optinscoretype').val()==2){
			jq('#SetMessages').css('display','inline-block');
		}
		jq('#optinscoretype').on('change',function(){
			var values=jq(this).val();
			if(values==2)
				jq('#SetMessages').css('display','inline-block');
			else
				jq('#SetMessages').css('display','none');
					 
		});
	});

	Joomla.submitbutton = function(task) {
		if (task == 'cancel') {
			
			Joomla.submitform(task, document.getElementById('adminForm'));
		} else {
			
			 
			if(!jQuery('input[name="quizzes_title"]').val()){
				alert('<?php echo JText::_('PLZ_ENTER_QUIZ_TITLE', true); ?>');
				document.adminForm.name.focus();
				return false;
			}
			if(!jQuery('select[name="quiz_categoryid"]').val()){
				alert('<?php echo JText::_('PLZ_ENTER_QUIZ_CATEGORY', true); ?>');
				document.adminForm.quiz_categoryid.focus();
				return false;
			}
 					
			Joomla.submitform(task, document.getElementById('adminForm'));
			
		}
	}


</script>



<form action="index.php?option=com_vquiz" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
	<div class="col101">
		<div id="tabs">
			<ul>
				<li><a href="#main"><?php echo JText::_('MAINSETTING'); ?></a></li>
				<li><a href="#additioanal"><?php echo JText::_('ADDITIONALSETTING'); ?></a></li>
				<!--<li><a href="#access"><?php echo JText::_('ACCESSETTING'); ?></a></li>-->
				<!--<li><a href="#meta_key"><?php echo JText::_('META_KEY'); ?></a></li>-->
			</ul>

<!--			<div id="meta_key">
				<table class="adminform table table-striped">
					<tr>
						<td class="key" width="200"><label class="hasTip" title="<?php echo JText::sprintf('META_DES_TOLTIP'); ?>"><?php echo JText::_('META_DESCRIPTION'); ?></label></td>
						<td><textarea name="meta_desc" id="meta_desc" cols="150" rows="5"><?php echo $this->item->meta_desc; ?></textarea></td>
					</tr>
					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('META_KEY_TOLTIP'); ?>"><?php echo JText::_('META_KEYWORD'); ?></label></td>
						<td><textarea name="meta_keyword" id="meta_keyword" cols="150" rows="5"><?php echo $this->item->meta_keyword; ?></textarea></td>
					</tr>
				</table>
			</div>-->


			<div id="main">
                <table class="adminform table table-striped">
					<tr>
						<td class="key" width="200"><label class="hasTip" title="<?php echo JText::sprintf('QUIZZES_TOLTIP'); ?>"><?php echo JText::_('TITLE'); ?></label></td>
						<td><input type="text" name="quizzes_title" id="quizzes_title" class="quizzes_title" value="<?php echo $this->item->quizzes_title; ?>">
						</td>
					</tr>

<!--					<tr>
						<td><label><?php echo JText::_('ALIAS'); ?></label></td>
						<td><input type="text" name="alias" id="alias"  value="<?php echo!empty($this->item->alias) ? $this->item->alias : ''; ?>"  placeholder="Automatic Genarated"/></td>
					</tr>-->

					<tr>
						<td class="key"><label  class="hasTip" title="<?php echo JText::sprintf('IMAGE_TOLTIP'); ?>"><?php echo JText::_('IMAGE'); ?></label></td>
						<td class="upimg"><input type="file" name="image" id="image" />
							<?php
							if (!empty($this->item->image) and file_exists(JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/quizzes/thumbs/' . 'thumb_' . $this->item->image)) {
								echo '<img src="' . JURI::root() . '/media/com_vquiz/vquiz/images/photoupload/quizzes/thumbs/' . 'thumb_' . $this->item->image . '" alt="" />';
							} else {
								echo '<img src="' . JURI::root() . '/components/com_vquiz/assets/images/no_image.png" alt="Image Not available" border="1" />';
							}
							?>
						</td>
					</tr>

					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('SPDATE_TOLTIP'); ?>"><?php echo JText::_('STARTPUBLISHDATE'); ?></label>
						</td>
						<td><input type="text" name="startpublish_date" id="startpublish_date" class="startpublish_date"
								   value="<?php if (empty($this->item->startpublish_date)) echo date("Y-m-d");else echo $this->item->startpublish_date; ?>">


							<script type="text/javascript">
								jQuery(function(){
									jQuery('*[name=startpublish_date]').datepicker({
										minDate: 0,
										maxDate: "" ,
										dateFormat: "yy-mm-dd",
										changeMonth: true,
										changeYear: true
										//showButtonPanel: true,
									});
	
								});
							</script>

						</td>


					</tr>

					<tr>

						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('EPDATE_TOLTIP'); ?>"><?php echo JText::_('ENDPUBLISHDATE'); ?></label></td>

						<td><input type="text" name="endpublish_date" id="endpublish_date" class="endpublish_date" value="<?php echo $this->item->endpublish_date; ?>">

							<script type="text/javascript">
								jQuery(function(){
									jQuery('*[name=endpublish_date]').datepicker({
										minDate:  0,
										maxDate: "+10Y" ,
										dateFormat: "yy-mm-dd",
										changeMonth: true,
										changeYear: true
										//showButtonPanel: true,
									});
								});
							</script>
						</td>
					</tr>
					<tr>

						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('SCATEGORY_TOLTIP'); ?>"><?php echo JText::_('CATEGORY'); ?></label></td>
						<td>
							<select name="quiz_categoryid" id="quiz_categoryid">
								<?php
								for ($i = 0; $i < count($this->category); $i++) {
									?>
									<option value="<?php echo $this->category[$i]->id; ?>"  <?php if ($this->category[$i]->id == $this->item->quiz_categoryid) echo 'selected="selected"'; ?> >
										<?php
										if ($this->category[$i]->level > 0) {
											echo str_repeat('-', $this->category[$i]->level - 1);
											echo $this->category[$i]->quiztitle;
										}
										?>
									</option>
									<?php
								}
								?>
							</select>

						</td>
					</tr>

					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('PUBLISHED_TOLTIP'); ?>"><?php echo JText::_('PUBLISHEDCATEGORY'); ?></label></td>
						<td>
							<select  name="published" id="published" >
								<option value="1" <?php if ($this->item->published) echo 'selected="selected"'; ?>><?php echo JText::_('published'); ?> </option>
								<option value="0" <?php if ($this->item->published == 0) echo 'selected="selected"'; ?>><?php echo JText::_('Unpublished'); ?> </option>
							</select>
						</td>
					</tr>

					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('ORDETING_TOLTIP'); ?>"><?php echo JText::_('ORDERING'); ?></label></td>
						<td>
							<input type="text" name="ordering" id="ordering" value="<?php echo $this->item->ordering; ?>" />
						</td>
					</tr>


<!--					<tr>
						<td class="key" width="200"><label class="hasTip" title="<?php echo JText::_('FEATURED_TOLTIP'); ?>"><?php echo JText::_('FEATURED'); ?></label></td>
						<td>
							<fieldset class="radio btn-group">
								<label for="featured1" id="featured1-lbl" class="radio"><?php echo JText::_('YS'); ?></label>
								<input type="radio" name="featured" id="featured1" value="1" <?php if ($this->item->featured == 1) echo 'checked="checked"'; ?>/>
								<label for="featured0" id="featured0-lbl" class="radio"><?php echo JText::_('NOS'); ?></label>
								<input type="radio" name="featured" id="featured0" value="0" <?php if ($this->item->featured == 0) echo 'checked="checked"'; ?>/>
							</fieldset>
						</td>
					</tr>-->

<!--					<tr>
						<td class="key"><label  class="hasTip" title="<?php echo JText::sprintf('PASSED_SCORE_TOLTIP'); ?>"><?php echo JText::_('QUIZPASSEDSCORE'); ?></label></td>
						<td>
							<input type="text" name="passed_score"  id="passed_score" value="<?php echo $this->item->passed_score; ?>" />
<?php echo JText::_('%'); ?>
						</td>
					</tr>-->


<!--					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('TTIME_TOLTIP'); ?>"><?php echo JText::_('TOTALTIMELIMIT'); ?></label>

						</td>
						<td>

							<input type="text" name="total_timelimit"  id="total_timelimit" value="<?php echo $this->item->total_timelimit; ?>" />
							<select  name="totaltime_parameter" id="totaltime_parameter" >
								<option value="seconds" <?php if ($this->item->totaltime_parameter = 'seconds') echo 'selected="selected"'; ?>><?php echo JText::_('Seconds'); ?> </option>

								<option value="minutes" <?php if ($this->item->totaltime_parameter = 'minutes') echo 'selected="selected"'; ?>><?php echo JText::_('Minutes'); ?> </option>

							</select>
						</td>
					</tr>-->

					<tr>
						<td class="key" width="200"><label class="hasTip" title="<?php echo JText::_('RQUESTION_TOLTIP'); ?>"><?php echo JText::_('RANDOMQUESTION'); ?></label></td>
						<td>
							<fieldset class="radio btn-group">
								<label for="random_question1" id="random_question1-lbl" class="radio"><?php echo JText::_('YS'); ?></label>
								<input type="radio" name="random_question" id="random_question1" value="1" <?php if ($this->item->random_question == 1) echo 'checked="checked"'; ?>/>
								<label for="random_question0" id="random_question0-lbl" class="radio"><?php echo JText::_('NOS'); ?></label>
								<input type="radio" name="random_question" id="random_question0" value="0" <?php if ($this->item->random_question == 0) echo 'checked="checked"'; ?>/>
							</fieldset>
						</td>
					</tr>

					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('QUESTION_LIMIT_TOLTIP'); ?>"><?php echo JText::_('GIVE_QUESTION_LIMIT'); ?></label></td>
						<td>
							<input type="text" name="question_limit" id="question_limit" value=" <?php echo $this->item->question_limit; ?>" />
						</td>
					</tr>

<!--					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('OPTION_TYPE_SCORE_TOLTIP'); ?>"><?php echo JText::_('OPTION_TYPE_SCORE'); ?></label></td>
						<td>
							<select  name="optinscoretype" id="optinscoretype" >
								<option value="1" <?php if ($this->item->optinscoretype == 1) echo 'selected="selected"'; ?>><?php echo JText::_('SINGLE_SCORE_FOR_ALL_OPTIONS'); ?> </option>
								<option value="2" <?php if ($this->item->optinscoretype == 2) echo 'selected="selected"'; ?> ><?php echo JText::_('DIFFERENT_SCORE_FOR_DIFFERENT_OPTIONS'); ?> </option>
							</select>
							<a style="display:none; margin:0 10px;" class="modal" id="SetMessages" title="Show Message" href="<?php echo 'index.php?option=com_vquiz&view=quizmanager&layout=messages&id=' . $this->item->id . '&tmpl=component'; ?>'" rel="{handler: 'iframe', size: {x: 800, y: 500}}">
								<input class="btn" type="button" value="<?php echo JText::_('MESSAGE'); ?>" /></a>
						</td>
					</tr>-->

					<tr>
						<td class="key hasTip" title="<?php echo JText::sprintf('DESCRIPTION_TOLTIP'); ?>"><label><?php echo JText::_('DESCRIPTION'); ?></label></td>
						<td>
							<?php
							$editor = JFactory::getEditor();
							echo $editor->display("description", $this->item->description, "400", "600", "20", "5", true, null, null, null, array('mode' => 'simple'));
							?>
						</td>
					</tr>

					<tr>
						<td class="key hasTip" title="<?php echo JText::sprintf('EXPLANATION_TOLTIP'); ?>"><label><?php echo JText::_('EXPLANATION'); ?></label></td>
						<td>
							<?php
							echo $editor->display("explanation", $this->item->explanation, "400", "600", "20", "5", true, null, null, null, array('mode' => 'simple'));
							?>
						</td>
					</tr>


				</table>

			</div>

			<div id="additioanal">
				<table class="adminform table table-striped">

					<tr>
						<td class="key" width="200">
							<label class="hasTip" title="<?php echo JText::_('SHOWDESC_TOLTIP'); ?>"><?php echo JText::_('SHOWDESC'); ?></label>
						</td>
						<td>
							<fieldset class="radio btn-group">
								<label for="desc_button1" id="desc_button1-lbl" class="radio"><?php echo JText::_('YS'); ?></label>
								<input type="radio" name="desc_button" id="desc_button1" value="1" <?php if ($this->item->desc_button == 1) echo 'checked="checked"'; ?>/>
								<label for="desc_button0" id="desc_button0-lbl" class="radio"><?php echo JText::_('NOS'); ?></label>
								<input type="radio" name="desc_button" id="desc_button0" value="0" <?php if ($this->item->desc_button == 0) echo 'checked="checked"'; ?>/>
							</fieldset>
						</td>
					</tr>

<!--					<tr>
						<td class="key" width="200"><label class="hasTip" title="<?php echo JText::_('SKIPBUTTON_TOLTIP'); ?>"><?php echo JText::_('SKIPBUTTON'); ?></label></td>
						<td>
							<fieldset class="radio btn-group">
								<label for="skip_button1" id="skip_button1-lbl" class="radio"><?php echo JText::_('YS'); ?></label>
								<input type="radio" name="skip_button" id="skip_button1" value="1" <?php if ($this->item->skip_button == 1) echo 'checked="checked"'; ?>/>
								<label for="skip_button0" id="skip_button0-lbl" class="radio"><?php echo JText::_('NOS'); ?></label>
								<input type="radio" name="skip_button" id="skip_button0" value="0" <?php if ($this->item->skip_button == 0) echo 'checked="checked"'; ?>/>
							</fieldset>
						</td>
					</tr>-->


					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::_('PREVIOUSBUTTON_TOLTIP'); ?>"><?php echo JText::_('PREVIOUSBUTTON'); ?></label></td>
						<td>
							<fieldset class="radio btn-group">
								<label for="prev_button1" id="prev_button1-lbl" class="radio"><?php echo JText::_('YS'); ?></label>
								<input type="radio" name="prev_button" id="prev_button1" value="1" <?php if ($this->item->prev_button == 1) echo 'checked="checked"'; ?>/>
								<label for="prev_button0" id="prev_button0-lbl" class="radio"><?php echo JText::_('NOS'); ?></label>
								<input type="radio" name="prev_button" id="prev_button0" value="0" <?php if ($this->item->prev_button == 0) echo 'checked="checked"'; ?>/>
							</fieldset>
						</td>
					</tr>
<!--					<tr>
                        <td class="key"><label class="hasTip" title="<?php echo JText::sprintf('SHOWCORRECTANS_TOLTIP'); ?>"><?php echo JText::_('SHOWCORRECTANS'); ?></label></td>
						<td id="append_td">
							<?php /* ?>                        <select  name="show_correctans" id="show_correctans" >
							  <option><?php echo JText::_('SELECT')?></option>
							  <option value="1" <?php if($this->item->show_correctans==1) echo 'selected="selected"'; ?> class="aeq"><?php echo JText::_('AFTER_EVERY_QUESTION'); ?> </option>
							  <option value="2" <?php if($this->item->show_correctans ==2) echo 'selected="selected"'; ?> ><?php echo JText::_('AFTER_QUIZ_END'); ?> </option>
							  <option value="0" <?php if($this->item->show_correctans ==0) echo 'selected="selected"'; ?>><?php echo JText::_('NO_WHERE'); ?> </option>
							  </select><?php */ ?>
                        </td>
					</tr>-->



					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::_('SHOWEXPLANATION_TOLTIP'); ?>"><?php echo JText::_('SHOWEXPLANATION'); ?></label></td>
						<td>
							<fieldset class="radio btn-group">
								<label for="show_explanation1" id="show_explanation1-lbl" class="radio"><?php echo JText::_('YS'); ?></label>
								<input type="radio" name="show_explanation" id="show_explanation1" value="1" <?php if ($this->item->show_explanation == 1) echo 'checked="checked"'; ?>/>
								<label for="show_explanation0" id="show_explanation0-lbl" class="radio"><?php echo JText::_('NOS'); ?></label>
								<input type="radio" name="show_explanation" id="show_explanation0" value="0" <?php if ($this->item->show_explanation == 0) echo 'checked="checked"'; ?>/>
							</fieldset>
						</td>
					</tr>


<!--					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::_('FLAGE_TOLTIP'); ?>"><?php echo JText::_('SHOW_FLAG'); ?></label></td>
						<td>
							<fieldset class="radio btn-group">
								<label for="show_flage1" id="show_flage1-lbl" class="radio"><?php echo JText::_('YS'); ?></label>
								<input type="radio" name="show_flage" id="show_flage1" value="1" <?php if ($this->item->show_flage == 1) echo 'checked="checked"'; ?>/>
								<label for="show_flage0" id="show_flage0-lbl" class="radio"><?php echo JText::_('NOS'); ?></label>
								<input type="radio" name="show_flage" id="show_flage0" value="0" <?php if ($this->item->show_flage == 0) echo 'checked="checked"'; ?>/>
							</fieldset>
						</td>
					</tr>-->


<!--					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::_('LIVESCORE_TOLTIP'); ?>"><?php echo JText::_('LIVE_SCORE'); ?></label></td>
						<td>
							<fieldset class="radio btn-group">
								<label for="show_livescore1" id="show_livescore1-lbl" class="radio"><?php echo JText::_('YS'); ?></label>
								<input type="radio" name="show_livescore" id="show_livescore1" value="1" <?php if ($this->item->show_livescore == 1) echo 'checked="checked"'; ?>/>
								<label for="show_livescore0" id="show_livescore0-lbl" class="radio"><?php echo JText::_('NOS'); ?></label>
								<input type="radio" name="show_livescore" id="show_livescore0" value="0" <?php if ($this->item->show_livescore == 0) echo 'checked="checked"'; ?>/>
							</fieldset>
						</td>
					</tr>-->


<!--					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::_('NOANSNOPENALITY_TOLTIP'); ?>"><?php echo JText::_('NOANSNOPENALITY'); ?></label></td>
						<td>
							<fieldset class="radio btn-group">
								<label for="penality1" id="penality1-lbl" class="radio"><?php echo JText::_('YS'); ?></label>
								<input type="radio" name="penality" id="penality1" value="1" <?php if ($this->item->penality == 1) echo 'checked="checked"'; ?>/>
								<label for="penality0" id="penality0-lbl" class="radio"><?php echo JText::_('NOS'); ?></label>
								<input type="radio" name="penality" id="penality0" value="0" <?php if ($this->item->penality == 0) echo 'checked="checked"'; ?>/>
							</fieldset>
						</td>
					</tr>-->


					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('SCORE_DISPLAY_TOLTIP'); ?>"><?php echo JText::_('SCORE_DISPLAY_GRAPH'); ?></label></td>
						<td>
							<fieldset class="radio btn-group">
								<label for="display_userscore1" id="display_userscore-lbl" class="radio"><?php echo JText::_('YS'); ?></label>
								<input type="radio" name="display_userscore" id="display_userscore1" value="1" <?php if ($this->item->display_userscore == 1) echo 'checked="checked"'; ?>/>
								<label for="display_userscore0" id="display_userscore-lbl" class="radio"><?php echo JText::_('NOS'); ?></label>
								<input type="radio" name="display_userscore" id="display_userscore0" value="0" <?php if ($this->item->display_userscore == 0) echo 'checked="checked"'; ?>/>
							</fieldset>

						</td>
					</tr>

					
					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('CHOSEGRAPH_TOLTIP'); ?>"><?php echo JText::_('CHOSEGRAPH'); ?></label></td>
						<td>
							<select  name="graph_type" id="graph_type" >
								<?php foreach ($this->graph_types as $key => $val) { ?>
								<option value="<?php echo $key; ?>" <?php if ($this->item->graph_type == $key) echo 'selected="selected"'; ?>><?php echo $val; ?> </option>
								<?php } ?>
							</select>
						</td>
					</tr>

					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('RESULTCATS'); ?>"><?php echo JText::_('RESULTCATS'); ?></label></td>
						<td>
							<select name="resultcats_id" id="resultcats_id">
								<option value="0"  <?php if (0 == $this->item->resultcats_id) echo 'selected="selected"'; ?> >
									<?php echo JText::_('NONES'); ?>
								</option>
								<?php
								for ($i = 0; $i < count($this->resultcats); $i++) {
									?>
									<option value="<?php echo $this->resultcats[$i]->id; ?>"  <?php if ($this->resultcats[$i]->id == $this->item->resultcats_id) echo 'selected="selected"'; ?> >
										<?php
										if ($this->resultcats[$i]->level > 0) {
											echo str_repeat('-', $this->resultcats[$i]->level - 1);
											echo $this->resultcats[$i]->quiztitle;
										}
										?>
									</option>
									<?php
								}
								?>
							</select>

						</td>
					</tr>


					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('RESULTCATS_TYPE'); ?>"><?php echo JText::_('RESULTCATS_TYPE'); ?></label></td>
						<td>
							<select  name="resultcats_type" id="resultcats_type" >
								<option value="0" <?php if ($this->item->resultcats_type == 0) echo 'selected="selected"'; ?>><?php echo JText::_('SINGLECHOICE'); ?> </option>
								<option value="1" <?php if ($this->item->resultcats_type == 1) echo 'selected="selected"'; ?>><?php echo JText::_('MULTIPLECHOICE'); ?> </option>
							</select>

						</td>
					</tr>


					<tr>
						<td class="key"><label class="hasTip" title="是否顯示選項分數">填寫選項的分數</label></td>
						<td>
							<fieldset class="radio btn-group">
								<label for="display_opscore1" id="display_opscore-lbl" class="radio"><?php echo JText::_('YS'); ?></label>
								<input type="radio" name="display_opscore" id="display_opscore1" value="1" <?php if ($this->item->display_opscore == 1) echo 'checked="checked"'; ?>/>
								<label for="display_opscore0" id="display_opscore-lb0" class="radio"><?php echo JText::_('NOS'); ?></label>
								<input type="radio" name="display_opscore" id="display_opscore0" value="0" <?php if ($this->item->display_opscore == 0) echo 'checked="checked"'; ?>/>
							</fieldset>

						</td>
					</tr>


<!--					<tr>
						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('PAGING_TOLTIP'); ?>" ><?php echo JText::_('PAGING'); ?></label></td>
						<td>
							<select  name="paging" id="paging" >
								<option value="1" <?php if ($this->item->paging == 1) echo 'selected="selected"'; ?>><?php echo JText::_('ONEQUESTION_PERPGE'); ?> </option>
								<option value="2" <?php if ($this->item->paging == 2 and $this->item->paging_limit > 1) echo 'selected="selected"'; ?>><?php echo JText::_('ALLQUESTION_PERPAGE'); ?> </option>
							</select>
						</td>
					</tr>
					<tr class="question_limit_tr" style="visibility:hidden">
						<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('QUESTIONS_LIMIT_TOLTIP'); ?>" ><?php echo JText::_('QLIMIT_PAGING'); ?></label></td>
						<td>
							<input type="text" name="paging_limit" id="paging_limit" value="<?php echo $this->item->paging_limit > 1 ? $this->item->paging_limit : 1; ?>" /></td>
					</tr>-->

				</table>
            </div>
			<!--                     <div id="access">
						  <table class="adminform table table-striped">
									<tr>
									<td valign="top" class="key" width="200">
									<label><?php echo JText::_('ACCESS_LEVEL'); ?></label>
									</td>
									<td><label><?php echo $this->lists['access']; ?></label></td>


									</tr>
									<tr>
									<td class="key"><label><?php echo JText::_('LANGUAGE'); ?></label></td>
									<td colspan="2">
									<select name="language" id="language">
<?php echo JHtml::_('select.options', JHtml::_('contentlanguage.existing', true, true), 'value', 'text', $this->item->language); ?>
									</select>
									</td>
									</tr>

									<tr>
									<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('ATTEMPTCOUN_TOLTIP'); ?>"><?php echo JText::_('ATTEMPTCOUND'); ?></label></td>
									<td>
										<input type="text" name="attemped_count" id="attemped_count" value="<?php echo $this->item->attemped_count ?>" />
									</td>
									</tr>
									<tr>
									<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('DELAYBETWEATTEPED_TOLTIP'); ?>"><?php echo JText::_('DELAYBETWEATTEPED'); ?></label></td>
									<td>

					<input type="text" name="attemped_delay" id="attemped_delay"   value="<?php echo $this->item->attemped_delay ?>"  style="width:50px" />
					<select  name="delay_periods" id="delay_periods" >
					<option value="hour" <?php if ($this->item->delay_periods == 'hour') echo 'selected="selected"'; ?>><?php echo JText::_('Hours'); ?> </option>
					<option value="days" <?php if ($this->item->delay_periods == 'days') echo 'selected="selected"'; ?> ><?php echo JText::_('Days'); ?> </option>
					<option value="week" <?php if ($this->item->delay_periods == 'week') echo 'selected="selected"'; ?> ><?php echo JText::_('Weeks'); ?> </option>
					<option value="month" <?php if ($this->item->delay_periods == 'month') echo 'selected="selected"'; ?> ><?php echo JText::_('Months'); ?> </option>
					</select>
									</td>
									</tr>
								</table>
								 </div>-->

		</div>

	</div>
	<div class="clr"></div>
<?php echo JHTML::_('form.token'); ?>
	<input type="hidden" name="option" value="com_vquiz" />
	<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="view" value="quizmanager" />
</form>







