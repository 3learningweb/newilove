<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/ 
defined( '_JEXEC' ) or die( 'Restricted access' );

$document = JFactory::getDocument();
$document->addScript('components/com_vquiz/assets/js/library.js');
$document->addStyleSheet('components/com_vquiz/assets/css/style.css');
$document->addScript('components/com_vquiz/assets/js/jquery-ui.js');
/*jimport( 'joomla.html.pagination');	
JHtml::_('behavior.framework');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.tooltip');
JHTML::_('behavior.modal');
*/
?>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
 
 <script type="text/javascript">
  $( document ).ready(function() {

		$('#sendmail').click(function() {
			
			var userid=$('input[name="cid[]"]:checked').parents('tr').find('.users_id').attr('usersid');
			var resultid=$('input[name="cid[]"]:checked').parents('tr').find('.rowresultid').attr('rowresultid');
			
			if($('input[name="cid[]"]:checked').length==0)
				alert('<?php echo JText::_("Please Choose One Result")?>');
			else if($('input[name="cid[]"]:checked').length>1)
				alert('<?php echo JText::_("Certificate Send at a time only One Result")?>');
			else if(userid==0){
				alert('<?php echo JText::_("Sorry Certificate not Send to guest User")?>');
				}
			else{
 				sendmail(userid,resultid);
			}
		 
		});
		
function sendmail(userid,resultid) 
	{ 
			jQuery.ajax(
			{
				url: "",
				type: "POST",
				dataType:"json",
				data: {'option':'com_vquiz', 'view':'quizresult', 'task':'sendmail', 'tmpl':'component','userid':userid,'resultid':resultid,"<?php echo JSession::getFormToken(); ?>":1},
				beforeSend: function()	{
				$(".poploadingbox").show();
				},
				complete: function()	{
				$(".poploadingbox").hide();
				},

				success: function(data)	
				{
				if(data.result==1)
					alert('<?php echo JText::_("THNKS_FOR_MAIL_SEND")?>');
				else if(data.result==0)
					alert('<?php echo JText::_("MESSAGE_NOT_SEND")?>');
 				else
				alert(data.error);

			}
		});
	}
	
 
 });
 
</script>
 
  <script type="text/javascript">
Joomla.submitbutton = function(task) {
 
 				if (task == 'cancel') {
				Joomla.submitform(task, document.getElementById('adminForm'));
				} 
 				  else if(task=='export') {
					var form = document.adminForm;
					if (document.adminForm.boxchecked.value==0)  {
					alert("<?php echo JText::_('PLZ_CHOSE_RESULT'); ?>");
					return false;
					}
				  }

 				Joomla.submitform(task, document.getElementById('adminForm'));

}

</script>
<div class="poploadingbox">
<?php echo '<img src="'.JURI::root().'/components/com_vquiz/assets/images/loading.gif"   />' ?>
</div>
<form action="index.php?option=com_vquiz&view=quizresult" method="post" name="adminForm" id="adminForm">
<div class="" style="float:right;">
            <div class="search_buttons">
                <div class="btn-wrapper input-append">
                <input placeholder="Search" type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
                <button class="btn" onclick="this.form.submit();"><i class="icon-search"></i><span class="search_text"><?php echo JText::_('SEARCH'); ?></button>
                <button class="btn" onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
                </div>
            </div>
			<!--document.getElementById('filter_user').value='';this.form.submit();--> 
			<div class="filter_calendar filter-select fltrt" style="float:right;">
            <input placeholder="Start Date" type="text" name="startdatesearch" id="startdatesearch" value="<?php echo $this->lists['startdatesearch'];?>"  class="text_area" />
			<script type="text/javascript">
            $(function(){
            $('*[name=startdatesearch]').datepicker({
			 dateFormat: "yy-mm-dd"
			});
            });
            </script>
			<input placeholder="End Date" type="text" name="enddatesearch" id="enddatesearch" value="<?php echo $this->lists['enddatesearch'];?>" class="text_area" />
			<script type="text/javascript">
            $(function(){
            $('*[name=enddatesearch]').datepicker({
			 dateFormat: "yy-mm-dd"
			});
            });
            </script>
    <button class="btn" onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
	<button class="btn" onclick="document.getElementById('startdatesearch').value='';document.getElementById('enddatesearch').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
    </div>
  </div>  
    <div class="search_b" style="float:left;">
    <input class="btn btn-small" type="button" id="sendmail" value="<?php echo JText::_('Send Certificate'); ?>" />
    </div>
<div id="editcell">
	<table class="adminlist table table-striped table-hover">
	<thead>
		<tr>
        	<th width="5">
                 <?php echo JText::_( 'Num' ); ?>
			</th>
			<th width="20">
			<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
			</th>			
			<th>
                 <?php echo JHTML::_('grid.sort', 'QUIZ', 'i.quiztitle', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
			</th>
		    	<th>

                <?php echo JHTML::_('grid.sort', 'USER', 'i.userid', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

			</th>

             <th>

              <?php echo JHTML::_('grid.sort', 'PLAY_DATE', 'i.start_datetime', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>


			</th>

              <th>

              <?php echo JHTML::_('grid.sort', 'PLAY_TIME', 'i.startdatetime ', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

			</th>

             <th> 

                 <?php echo JHTML::_('grid.sort', 'SCORE', 'i.score', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

			</th>

             <th>

               <?php echo JHTML::_('grid.sort', 'STATUS', 'i.score', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>


			</th>

             <th>

             <?php echo JHTML::_('grid.sort', 'FLAG', 'i.flag', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

			</th>

 

            

            <th width="5">

				 <?php echo JHTML::_('grid.sort', 'ID', 'i.id', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>

			</th>

		</tr>

	</thead>

    <tfoot>

    <tr>

    <td colspan="10"> <?php echo $this->pagination->getListFooter(); ?></td>

    </tr>

    </tfoot>

	<?php

	

	$k = 0;

	for ($i=0, $n=count( $this->items ); $i < $n; $i++)	{

		$row = &$this->items[$i];

		$maxscore=$row->maxscore;

		$score=$row->score;

		$passed_score=$row->passed_score;

		$persentagescore=$score/$maxscore*100;



		

		$checked 	= JHTML::_('grid.id',   $i, $row->id );

	
		$link 		= JRoute::_( 'index.php?option=com_vquiz&view=quizresult&task=edit&cid[]='. $row->id );

		?>

		<tr class="<?php echo "row$k"; ?>">

 		<td><?php echo $this->pagination->getRowOffset($i); ?></td>

			<td>

				<?php echo $checked; ?>

			</td>

			<td>

		    <a href="<?php echo $link; ?>"> <?php echo $row->quiztitle;  ?></a>

			</td> 

			<td align="center" class="users_id" usersid="<?php echo $row->userid;?>">

				<?php 					

                if($row->userid==0)

                echo 'Guest';

                else

                echo $row->username; 

                ?>  

            </td>

            <td align="center"><?php echo $row->start_datetime;  ?></td>

            <td align="center"><?php echo $row->startdatetime;  ?></td>

            <td align="center"><?php echo $row->score;  ?></td>

            <td align="center">

            <?php if($persentagescore>=$passed_score)

            echo '<span style="color:green">Passed</span>';	

            else

            echo '<span style="color:red">Failed</span>'; ?>

            </td>

            <td align="center"><?php echo $row->flag;  ?></td>



             <td class="rowresultid" rowresultid="<?php echo $row->id;?>">

				<?php echo $row->id; ?>

			</td>

		</tr>

		<?php

		$k = 1 - $k;

	}

	?>



	</table>



</div>

<input type="hidden" name="option" value="com_vquiz" />

<input type="hidden" name="task" value="" />

<input type="hidden" name="boxchecked" value="0" />

<input type="hidden" name="view" value="quizresult" />

<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />

<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />



</form>



