<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view' );

class VquizViewConfiguration extends JViewLegacy

{ 

	function display($tpl = null)

		{
	 
		$layout = JRequest::getCmd('layout', '');
		
		$item = $this->get('Item');
		
		$this->assignRef( 'item', $item );
		
		JToolBarHelper::title( JText::_( 'Configuration ' ), 'configuration.png' );
		
		JToolBarHelper::apply();
		
		JToolBarHelper::cancel();
		
		parent::display($tpl);
//		JToolBarHelper::help('help', true);
	
 	
		}
 

}



			  

			  

