<?php
/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access

defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.framework');
JHtml::_('behavior.tooltip');
//jimport( 'joomla.html.pagination' );

$user = JFactory::getUser();
$document = JFactory::getDocument();
$document->addScript('components/com_vquiz/assets/js/library.js');
$document->addScript('components/com_vquiz/assets/js/popup.js');
$document->addStyleSheet('components/com_vquiz/assets/css/style.css');
$document->addStyleSheet('components/com_vquiz/assets/css/popup.css');

if (version_compare(JVERSION, '3.0', '>='))
	JHtml::_('formbehavior.chosen', 'select');

$listOrder = $this->lists['order'];
$listDirn = $this->lists['order_Dir'];
$canOrder = $user->authorise('core.edit.state', 'com_vquiz.quizcategory');
$ordering = ($listOrder == 'i.lft');
$saveOrder = ($listOrder == 'i.lft' && strtolower($listDirn) == 'asc');

if ($saveOrder and version_compare(JVERSION, '3.0', '>=')) {
	$saveOrderingUrl = 'index.php?option=com_vquiz&task=saveOrder&tmpl=component';
	JHtml::_('sortablelist.sortable', 'itemList', 'adminForm', strtolower($listDirn), $saveOrderingUrl, false, true);
}
$sortFields = $this->getSortFields();

$function = JFactory::getApplication()->input->getCmd('function', 'jSelectCategory');
?> 
<script type="text/javascript">
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>
<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		var x=jQuery('#categorycsv').val();

		if (task == 'cancel') {
			Joomla.submitform(task, document.getElementById('adminForm'));
		}
		else if(task=='import') {
			lightbox_open();
			var form = document.adminForm;
			if(x == "")  {
				alert("<?php echo JText::_('PLZCOOSECSV'); ?>");
				return false;
			}

		}
		else if(task=='export') {
			var form = document.adminForm;
			if (document.adminForm.boxchecked.value==0)  {
				alert("<?php echo JText::_('PLZCATEGORY'); ?>");
				return false;
			}
		}

		Joomla.submitform(task, document.getElementById('adminForm'));
	}

</script>

<form action="index.php?option=com_vquiz&view=quizcategory" method="post" name="adminForm" id="adminForm"  enctype="multipart/form-data" onSubmit="return true">
	<div class="filter-select fltrt" style="float:right;">
		<select name="publish_item" id="publish_item" class="inputbox" onchange="this.form.submit()">
			<option value=""><?php echo JText::_('All State'); ?></option>
			<option value="p" <?php if ('p' == $this->lists['publish_item']) echo 'selected="selected"'; ?>><?php echo JText::_('Published'); ?></option>
			<option value="u" <?php if ('u' == $this->lists['publish_item']) echo 'selected="selected"'; ?>><?php echo JText::_('Unpublished'); ?></option>
		</select>
	</div>
	<div class="search_buttons">
        <div class="btn-wrapper input-append">
			<input placeholder="Search" type="text" name="search" id="search" value="<?php echo $this->lists['search']; ?>" class="text_area" onchange="document.adminForm.submit();" />
			<button class="btn" onclick="this.form.submit();"><i class="icon-search"></i><span class="search_text"><?php echo JText::_('SEARCH'); ?></span></button>
			<button  class="btn" onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_('RESET'); ?></button></div></div>
	<!--document.getElementById('filter_user').value='';this.form.submit();-->

	<div id="editcell">
		<table class="adminlist table table-striped table-hover" id="itemList">
			<thead>
				<tr>

					<th width="5">
						<?php echo JText::_('Num'); ?>
					</th>
					<th width="20">
						<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
					</th>

					<th  width="30">
						<?php echo JText::_('JSTATUS'); ?>
					</th>

					<th>
						<?php echo JHTML::_('grid.sort', 'TITLE', 'i.quiztitle', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>

					<th>
						<?php echo JText::_('IMAGE'); ?>
					</th>
					<th>
						<?php echo JText::_('QUIZZES'); ?>
					</th>
					<th width="5">
						<?php echo JHTML::_('grid.sort', 'ID', 'i.id', @$this->lists['order_Dir'], @$this->lists['order']); ?>
					</th>
				</tr>
			</thead>


			<?php
			$k = 0;
			for ($i = 0, $n = count($this->items); $i < $n; $i++) {
				$row = &$this->items[$i];
				$checked = JHTML::_('grid.id', $i, $row->id);
				//$published    = JHTML::_( 'grid.published', $row, $i );
				$published = JHTML::_('jgrid.published', $row->published, $i);
				$link = JRoute::_('index.php?option=com_vquiz&view=quizcategory&task=edit&cid[]=' . $row->id);

				$orderkey = array_search($row->id, $this->ordering[$row->parent_id]);
				$canCreate = $user->authorise('core.create', 'com_vquiz');
				$canEdit = $user->authorise('core.edit', 'com_vquiz');
				$canCheckin = $user->authorise('core.manage', 'com_checkin') || $row->checked_out == $user->get('id') || $row->checked_out == 0;
				$canChange = $user->authorise('core.edit.state', 'com_vquiz') && $canCheckin;

				// Get the parents of item for sorting
				if ($row->level > 1) {
					$parentsStr = "";
					$_currentParentId = $row->parent_id;
					$parentsStr = " " . $_currentParentId;
					for ($j = 0; $j < $row->level; $j++) {
						foreach ($this->ordering as $k => $v) {
							$v = implode("-", $v);
							$v = "-" . $v . "-";
							if (strpos($v, "-" . $_currentParentId . "-") !== false) {
								$parentsStr .= " " . $k;
								$_currentParentId = $k;
								break;
							}
						}
					}
				} else {
					$parentsStr = "";
				}
				?>

				<tr class="<?php echo "row$k"; ?>" sortable-group-id="1">
				<tr class="row<?php echo $i % 2; ?>" sortable-group-id="<?php echo $row->parent_id; ?>" item-id="<?php echo $row->id ?>" parents="<?php echo $parentsStr ?>" level="<?php echo $row->level ?>">

					<td><?php echo $this->pagination->getRowOffset($i); ?></td>
					<td>
						<?php echo $checked; ?>
					</td>

					<td><?php echo $published; ?></td>

					<td>
						<a href="<?php echo $link; ?>" onclick="if (window.parent) window.parent.<?php echo $this->escape($function); ?>('<?php echo $row->id; ?>','<?php echo $this->escape(addslashes($row->quiztitle)); ?>',null);">
							<?php
							echo str_repeat('-', $row->level - 1);
							echo substr($row->quiztitle, 0, 50);
							?>
						</a>
					</td>


					<td>

						<?php
						if (!empty($row->photopath) and file_exists(JPATH_ROOT . '/media/com_vquiz/vquiz/images/photoupload/thumbs/' . 'thumb_' . $row->photopath)) {

							echo '<img src="' . JURI::root() . '/media/com_vquiz/vquiz/images/photoupload/thumbs/' . 'thumb_' . $row->photopath . '" alt=""  style="height:37px;"/>';
						} else {
							echo '<img src="' . JURI::root() . '/components/com_vquiz/assets/images/no_image.png" alt="Image Not available" border="1"  style="height:37px;"    />';
						}
						?>

					</td>
					<td>
						<a href="index.php?option=com_vquiz&view=quizmanager&qcategoryid=<?php echo $row->id; ?>">
							<input type="button" value="<?php echo JText::_('VIEW'); ?> (<?php echo $row->totalquizzes; ?>)" class="btn btn-small">
						</a>
					</td>
					<td>
						<?php echo $row->id; ?>
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>

			<tfoot>
				<tr>
					<td colspan="8"><?php echo $this->pagination->getListFooter(); ?></td>
				</tr>
			</tfoot>
		</table>
	</div>
	<input type="hidden" name="option" value="com_vquiz" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="view" value="quizcategory" />
	<input type="hidden" name="tmpl" value="<?php echo JRequest::getVar('tmpl', 'index'); ?>" />
	<input type="hidden" name="function" value="<?php echo JRequest::getVar('function', ''); ?>" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />

	<div id="light">
		<a href="javascript:void(0);"  onclick="lightbox_close3();" class="closepop">X</a>
		<input type="file" name="categorycsv" id="categorycsv">
		<input type="button" value="Import" name="submitcsv" id="submitcsv" onclick="Joomla.submitform('importcategorycsv');">
	</div>
	<div id="fade" onClick="lightbox_close3();"></div>

</form>
