<?php
/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access

defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_vquiz/assets/css/style.css');
$document->addScript('components/com_vquiz/assets/js/library.js');
if (version_compare(JVERSION, '3.0', '>='))
	JHtml::_('formbehavior.chosen', 'select');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task == 'cancel') {
			
			Joomla.submitform(task, document.getElementById('adminForm'));
		} else {
 
			if(!jQuery('input[name="quiztitle"]').val()){
				alert('<?php echo JText::_('PLZ_ENTER_CATEGORY_TITLE', true); ?>');
				document.adminForm.qtitle.focus();
				return false;
			}
			Joomla.submitform(task, document.getElementById('adminForm'));
			
		}
	}
</script>
<form action="index.php?option=com_vquiz" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
	<div class="col101">
		<fieldset class="adminform">
			<legend><?php echo JText::_('DETAILS'); ?></legend>
			<table class="adminform table table-striped">
				<tr>
					<td class="key" width="200"><label  class="hasTip" title="<?php echo JText::sprintf('RESULTCATS'); ?>"><?php echo JText::_('TITLE'); ?></label></td>
					<td><input type="text"  name="quiztitle" id="quiztitle" class="quiztitle" value="<?php echo $this->item->quiztitle; ?>"/></td>
				</tr>
				<tr>
					<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('PARENT_TOLTIP'); ?>"><?php echo JText::_('PARENT'); ?></label></td>
					<td>

						<select  name="parent_id" id="parent_id" >
							<option level="0"  value="1"><?php echo JText::_('Root'); ?></option>
							<?php for ($k = 0; $k < count($this->parentcategory); $k++) { ?>
								<option value="<?php echo $this->parentcategory[$k]->id; ?>" <?php if ($this->parentcategory[$k]->id == $this->item->parent_id) echo 'selected="selected"'; ?> level="<?php echo $this->parentcategory[$k]->level; ?>">
									<?php
									if ($this->parentcategory[$k]->level > 0) {
										echo str_repeat('-', $this->parentcategory[$k]->level - 1);
										echo $this->parentcategory[$k]->quiztitle;
									}
									?>
								</option>
							<?php } ?>
						</select>
						<input type="hidden" name="level" id="level" value="0" />

					</td>
				</tr>

				<tr>

					<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('JSTATUS'); ?>"><?php echo JText::_('JSTATUS'); ?></label></td>
					<td>
						<select  name="published" id="published" >
							<option value="1" <?php if ($this->item->published == 1) echo 'selected="selected"'; ?>><?php echo JText::_('published'); ?> </option>
							<option value="0" <?php if ($this->item->published == 0) echo 'selected="selected"'; ?>><?php echo JText::_('Unpublished'); ?> </option>
						</select>

					</td>
				</tr>
<!--				<tr>
					<td class="key"><label class="hasTip" title="<?php echo JText::sprintf('ORDETING_TOLTIP'); ?>"><?php echo JText::_('ORDERING'); ?></label></td>
					<td>
						<input type="text" name="ordering" id="ordering" value="<?php echo $this->item->ordering; ?>" />
					</td>
				</tr>-->

				

			</table>
		</fieldset>

	</div>

	<div class="clr"></div>

	<?php echo JHTML::_('form.token'); ?>

	<input type="hidden" name="option" value="com_vquiz" />
	<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="view" value="resultcats" />
</form>







