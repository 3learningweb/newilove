<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined('_JEXEC') or die();
jimport('joomla.application.component.modellist');
class VquizModelVquiz extends JModelList
{

 
function __construct()
	{
		parent::__construct();
	}
	
	function _buildQuery()
	{
		$db=JFactory::getDbo();
		$date = JFactory::getDate();
		
		$query='select * from `#__vquiz_category` where  published=1';
		
		return $query;
	}
	
	function getData()
	{
		// Lets load the data if it doesn't already exist
		if (empty( $this->_data ))
		{
			$query = $this->_buildQuery();

			$this->_data = $this->_getList( $query, $this->getState('limitstart'), $this->getState('limit'));
			echo $this->_db->getErrorMsg();
		}
		return $this->_data;
	}
	
	
	
				function getCategory()
				{
					$db = JFactory::getDbo();		
					$query = 'select id,quiztitle from #__vquiz_category WHERE published=1';
					$db->setQuery( $query );
					$result = $db->loadObjectList();
					
					return $result;
				}
				
				
				function getLinechart()
				{
					
				    $db = JFactory::getDbo();
					$date = JFactory::getDate();	
					$type = JRequest::getVar('type', 'day');
					$category = JRequest::getInt('category', 0);
					
					$obj = new stdClass();
					$obj->result = "error";
 
					
					switch($type)
					{
					case 'day':
					$query = 'SELECT date_format(r.start_datetime, "%e, %b"),date_format(r.start_datetime, "%e, %b") as func';
 
					break;
					case 'week':
					$query = 'SELECT date_format(r.start_datetime, "%U, %Y"),date_format(r.start_datetime, "%U, %Y") as func';
					break;
					
					case 'month':
					$query = 'SELECT date_format(r.start_datetime, "%b"),date_format(r.start_datetime, "%b") as func';
					break;
					}
					
					
					$query .= ' ,count(r.userid) from #__vquiz_quizresult as r left join #__vquiz_quizzes as q on r.quizzesid=q.id ';
					
					if($category)
					{
					$query .=' where r.categoryid='.$category.'';
					}
					
					$query .= ' group by func order by r.id asc';
					
					$this->_db->setQuery( $query );	
					$result=$this->_db->loadRowList(); 	
 
					$obj->playedquiz=$result;
 
					
					$obj->result = "success";
 
					return $obj;
				}
							
							
			function getpiechart()
				{
					
				    $db = JFactory::getDbo();
					$date = JFactory::getDate();	
					$category = JRequest::getInt('category', 0);
					
					$obj = new stdClass();
					$obj->result = "error";
 					
					$arr2=array();
 
					
					$query = ' SELECT q.quizzes_title as qtitle,q.id as quizid ,count(r.userid) as totaluser from #__vquiz_quizresult as r left join #__vquiz_quizzes as q on r.quizzesid=q.id ';
					 
					if($category)
					{
					$query .=' where r.categoryid='.$category.'';
					}
					
					$query .= ' group by r.quizzesid order by r.id desc LIMIT 10';
					
					$this->_db->setQuery( $query );	
										
					$resulted=$this->_db->loadObjectList(); 
					
 
					for($i=0;$i<count($resulted);$i++){
						$arr=array();
						$x=$resulted[$i]->qtitle;
						$y=$resulted[$i]->totaluser;
 
						$link=$resulted[$i]->quizid;
						array_push($arr,$x);
						array_push($arr,$link);
						array_push($arr,$y);
						array_push($arr2,$arr);
					}
					
  								
					$obj->playedquiz=$arr2;				
					$obj->result = "success";
 
					return $obj;
				} 
				
				
				function getgeochart()
				{
					
				    $db = JFactory::getDbo();
					$date = JFactory::getDate();	
					$agegroup = JRequest::getInt('agegroup');
					$obj = new stdClass();
					$obj->result = "error";
					
					$query = ' SELECT i.country,count(u.id) from #__vquiz_users as i left join #__users as u on i.userid=u.id ';
 					
					if($agegroup)
					{	
					$age=$agegroup+10;
					
					if($agegroup==60)
					$query .=' where TIMESTAMPDIFF(YEAR ,i.dob, NOW()) >='.$agegroup.'';
					else
					$query .=' where TIMESTAMPDIFF(YEAR ,i.dob, NOW()) >='.$agegroup.' and TIMESTAMPDIFF(YEAR ,i.dob, NOW()) <='.$age.'';
					
					}
					
					$query .= ' group by i.country order by  i.id desc';
					
					$this->_db->setQuery( $query );	
					$result=$this->_db->loadRowList(); 					
					$obj->userinfo=$result;
 
					
					$obj->result = "success";
 
					return $obj;
				} 
				
				
				
			function getflagpiechart()
			{
				$db = JFactory::getDbo();
				$date = JFactory::getDate();	
				$category = JRequest::getInt('category', 0);
				
				$obj = new stdClass();
				$obj->result = "error";
 
				

 				$arr2=array();
				$query = ' SELECT qtitle,id,flagcount from #__vquiz_question where flagcount!=0  order by id asc LIMIT 10';				
				$this->_db->setQuery( $query );	
				$result=$this->_db->loadObjectList(); 
					
				
					for($i=0;$i<count($result);$i++){
					$arr=array();
					$x=strip_tags($result[$i]->qtitle);
					$id=$result[$i]->id;
					$y=$result[$i]->flagcount;
					array_push($arr,substr($x,0,100));
					array_push($arr,$id);
					array_push($arr,$y);
					array_push($arr2,$arr);
					}

				$obj->flagquestion=$arr2;

				
				$obj->result = "success";

				return $obj;
			}
			 
}

?>