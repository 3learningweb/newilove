<?php

/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');

class VquizModelQuizquestion extends JModelList {

	function __construct() {
		parent::__construct();
		$mainframe = JFactory::getApplication();


		$context = 'com_vquiz.question.list.';
		// Get pagination request variables
		$limit = $mainframe->getUserStateFromRequest($context . 'limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest($context . 'limitstart', 'limitstart', 0, 'int');

		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);


		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);

		$array = JRequest::getVar('cid', 0, '', 'array');
		$this->setId((int) $array[0]);
	}

	function _buildQuery() {

		$db = JFactory::getDBO();

		$query = "SELECT i.*, u.quizzes_title as categoryname FROM #__vquiz_question as i JOIN #__vquiz_quizzes as u ON i.quizzesid = u.id";
		return $query;
	}

	function setId($id) {
		$this->_id = $id;
		$this->_data = null;
	}

	function &getItem() {
		if (empty($this->_data)) {

			$query = ' SELECT * FROM #__vquiz_question ' .
					'  WHERE id = ' . $this->_id;

			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();
		}

		if (!$this->_data) {

			$this->_data = new stdClass();

			$this->_data->id = 0;
			$this->_data->qtitle = null;
			$this->_data->quiz_categoryid = null;
			$this->_data->optiontype = null;
			$this->_data->quiztitle = null;
			$this->_data->titleid = null;
			$this->_data->score = null;
			$this->_data->penality = null;
			$this->_data->attemped_count = null;
			$this->_data->explanation = null;
			$this->_data->published = null;
			$this->_data->flagcount = null;
			$this->_data->ordering = null;
			$this->_data->question_timelimit = null;
			$this->_data->questiontime_parameter = null;
			$this->_data->expire_timescore = null;
		}
		return $this->_data;
	}

	function &getItems() {
		// Lets load the data if it doesn't already exist
		if (empty($this->_data)) {
			$query = $this->_buildQuery();

			$filter = $this->_buildContentFilter();
			$orderby = $this->_buildItemOrderBy();

			$query .= $filter;
			$query .= $orderby;
			//$this->_data = $this->_getList( $query );
			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		}

		return $this->_data;
	}

	function getTotal() {

		if (empty($this->_total)) {

			$query = $this->_buildQuery();
			$query .= $this->_buildContentFilter();
			$this->_total = $this->_getListCount($query);
		}

		return $this->_total;
	}

	function _buildItemOrderBy() {
		$mainframe = JFactory::getApplication();

		$context = 'com_vquiz.question.list.';

		$filter_order = $mainframe->getUserStateFromRequest($context . 'filter_order', 'filter_order', 'id', 'cmd');
		$filter_order_Dir = $mainframe->getUserStateFromRequest($context . 'filter_order_Dir', 'filter_order_Dir', 'desc', 'word');

		$orderby = ' group by i.id order by ' . $filter_order . ' ' . $filter_order_Dir . ' ';

		return $orderby;
	}

	function getPagination() {
		// Load the content if it doesn't already exist

		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
		}
		return $this->_pagination;
	}

	function _buildContentFilter() {


		$mainframe = JFactory::getApplication();

		$context = 'com_vquiz.question.list.';
		$search = $mainframe->getUserStateFromRequest($context . 'search', 'search', '', 'string');
		$quizzesid = $mainframe->getUserStateFromRequest($context . 'quizzesid', 'quizzesid', '', 'string');

		$search = JString::strtolower($search);
		$publish_item = $mainframe->getUserStateFromRequest($context . 'publish_item', 'publish_item', '', 'string');


		$where = array();

		if ($publish_item) {

			if ($publish_item == 'p')
				$where[] = 'i.published= 1';

			else if ($publish_item == 'u')
				$where[] = 'i.published = 0';
		}

		if ($search) {
			if (is_numeric($search)) {
				$where[] = 'LOWER(  i.id ) =' . $this->_db->Quote($this->_db->escape($search, true), false);
			} else {

				$where[] = 'i.qtitle LIKE ' . $this->_db->Quote('%' . $this->_db->escape($search, true) . '%', false);
			}
		}

		if ($quizzesid) {

			$where[] = ' i.quizzesid =' . $this->_db->Quote($this->_db->escape($quizzesid, true), false);
		}


		$filter = count($where) ? ' WHERE ' . implode(' AND ', $where) : '';

		return $filter;
	}

	function getOptinscoretype() {
		$context = 'com_vquiz.question.list.';
		$mainframe = JFactory::getApplication();
		$quizzesid = $mainframe->getUserStateFromRequest($context . 'quizzesid', 'quizzesid', '', 'string');
		$query = ' SELECT * FROM #__vquiz_quizzes  WHERE id = ' . $quizzesid;
		$this->_db->setQuery($query);
		$optinscoretype = $this->_db->loadObject();


		return $optinscoretype;
	}

	function reorder() {
		$mainframe = & JFactory::getApplication();
		// Check for request forgeries
		//JRequest::checkToken() or jexit( 'Invalid Token' );
		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		JArrayHelper::toInteger($cid);
		$task = JRequest::getCmd('task', '');
		$inc = ($task == 'orderup' ? -1 : 1);
		if (empty($cid)) {
			return JError::raiseWarning(500, 'No items selected');
		}
		$row = & $this->getTable();
		$row->load((int) $cid[0]);
		$row->move($inc);
		return 'reordered successfully!';
	}

	function saveOrder() {
		// Check for request forgeries
		//JRequest::checkToken() or jexit( 'Invalid Token' );
		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		JArrayHelper::toInteger($cid);
		if (empty($cid)) {
			return JError::raiseWarning(500, 'No items selected');
		}
		$total = count($cid);
		$row = & $this->getTable();
		$groupings = array();
		$order = JRequest::getVar('order', array(0), 'post', 'array');
		JArrayHelper::toInteger($order);
		// update ordering values
		for ($i = 0; $i < $total; $i++) {
			$row->load((int) $cid[$i]);
			// track postions
			$groupings[] = $row->position;
			if ($row->ordering != $order[$i]) {
				$row->ordering = $order[$i];
				if (!$row->store()) {
					return JError::raiseWarning(500, $this->_db->getErrorMsg());
				}
			}
		}

		// execute updateOrder for each parent group
		$groupings = array_unique($groupings);
		foreach ($groupings as $group) {
			$row->reorder();
		}
		return 'New ordering saved';
	}

	function store() {
		$time = time();
		$row = & $this->getTable();
		$data = JRequest::get('post');




		$data['qtitle'] = JRequest::getVar('qtitle', '', 'post', 'string', JREQUEST_ALLOWRAW);
		$data['explanation'] = JRequest::getVar('explanation', '', 'post', 'string', JREQUEST_ALLOWRAW);

		if (!$data["id"]) {
			$datee = JFactory::getDate();
			$data['created_date'] = $datee->toSQL();
		}


		if (!$row->bind($data)) {

			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		if (!$row->store()) {
			$this->setError($row->getErrorMsg());
			return false;
		}

		if (!$data['id'])
			JRequest::setVar('id', $row->id);

		return true;
	}

	function publish() {

		$cid = JRequest::getVar('cid', array(), 'post', 'array');
		$task = JRequest::getCmd('task');
		$publish = ($task == 'publish') ? 1 : 0;
		$n = count($cid);
		if (empty($cid)) {
			return 'No item selected';
		}

		$cids = implode(',', $cid);
		//implode() convert array into string

		$query = 'UPDATE #__vquiz_question SET published = ' . (int) $publish . ' WHERE id IN ( ' . $cids . ' )';


		$this->_db->setQuery($query);

		if (!$this->_db->query())
			return $this->_db->getErrorMsg();
		else
			return ucwords($task) . 'ed successfully.';
	}

	function delete() {
		$cids = JRequest::getVar('cid', array(0), 'post', 'array');
		$row = & $this->getTable();
		if (count($cids)) {
			foreach ($cids as $cid) {
				if (!$row->delete($cid)) {

					$this->setError($row->getErrorMsg());
					return false;
				}
			}
		}

		return true;
	}

	function getOptions() {

		$query = "select * from #__vquiz_option where qid = " . $this->_data->id . ' order by id asc';
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;
	}

	function getQuizes() {
		$query = "select * from #__vquiz_quizzes where published = " . '1' . ' order by id asc';
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;
	}

	function getCsv() {
		$db = JFactory::getDbo();
		//get the column titles for heading row
		$query = 'show columns from #__vquiz_question';
		$db->setQuery($query);
		$columnhead = $db->loadColumn();

		$cids = JRequest::getVar('cid', array(0), 'post', 'array');
		$query = 'select * from #__vquiz_question WHERE id IN (' . implode(',', $cids) . ')';
		$db->setQuery($query);
		$data = $db->loadRowList();


		$count = count($data);

		$max = 0;
		for ($i = 0; $i < $count; $i++) {
			$q = 'select qoption,correct_ans,options_score from #__vquiz_option where qid=' . $this->_db->quote($data[$i][0]) . ' order by id asc';
			$db->setQuery($q);
			$qoption = $db->loadRowList();

			unset($correct_array);
			$correct_array = array();

			$correct = 0;
			$max = count($qoption) > $max ? count($qoption) : $max;

			for ($j = 0; $j < count($qoption); $j++) {
				if ($qoption[$j][1] == 1) {
					$correct = $j;
					array_push($correct_array, $correct);
				}
				$options_score = $qoption[$j][2];
			}
			array_push($data[$i], implode(',', $correct_array));
			array_push($data[$i], $options_score);


			for ($j = 0; $j < count($qoption); $j++) {

				array_push($data[$i], $qoption[$j][0]);
				if ($qoption[$j][1] == 1) {
					$correct = $j;
					array_push($correct_array, $correct);
				}
			}
		}

		array_push($columnhead, "correct_ans");
		array_push($columnhead, "options_score");
		for ($i = 0; $i < $max; $i++)
			array_push($columnhead, "qoption");


		//push the heading row at the top
		array_unshift($data, $columnhead);


		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');



		header('Content-Disposition: attachment; filename=Quizzes.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		foreach ($data as $fields) {

			$f = array();
			foreach ($fields as $v)
				array_push($f, mb_convert_encoding($v, 'Windows-1252', "UTF-8"));
			fputcsv($output, $f, ',', '"');
			//fputcsv($output, $fields, ',', '"');
		}

		fclose($output);
		return true;
	}

	function checktotaltime() {
		$quizzesid = JRequest::getInt('quizzesid', 0);
		$obj = new stdClass();
		$obj->result = "error";

		$query = 'select total_timelimit from #__vquiz_quizzes where id=' . $this->_db->quote($quizzesid) . ' order by id asc';
		$this->_db->setQuery($query);
		$result = $this->_db->loadResult();
		if ($result > 0)
			$obj->totaltime = 1;
		else
			$obj->totaltime = 0;

		$obj->result = "success";
		return $obj;
	}

	function movequestion() {

		$cids = JRequest::getVar('cid', array(0), 'post', 'array');
		$quizzesid = JRequest::getInt('quizzesmoveid');

		if (count($cids)) {
			foreach ($cids as $cid) {

				$query = 'UPDATE #__vquiz_question SET quizzesid = ' . $quizzesid . ' WHERE id = ' . $cid;
				$this->_db->setQuery($query);

				if (!$this->_db->query()) {
					$msg = $this->setError($this->_db->stderr());
					$this->setRedirect('index.php?option=com_vquiz&view=quizquestion', $msg);
					return false;
				}
			}
		}
		return true;
	}

	function copyquestion() {

		$cids = JRequest::getVar('cid', array(0), 'post', 'array');
		$quizzesid = JRequest::getInt('quizzescopyid');

		if (count($cids)) {
			foreach ($cids as $cid) {

				$query = ' SELECT * FROM #__vquiz_question WHERE id = ' . $cid;
				$this->_db->setQuery($query);
				$question = $this->_db->loadObject();

				$insert = new stdClass();
				$insert->id = null;
				$insert->quizzesid = $quizzesid;
				$insert->questiontime_parameter = $question->questiontime_parameter;
				$insert->question_timelimit = $question->question_timelimit;
				$insert->optiontype = $question->optiontype;
				$insert->flagcount = $question->flagcount;
				$insert->qtitle = $question->qtitle;
				$insert->explanation = $question->explanation;
				$insert->scoretype = $question->scoretype;
				$insert->score = $question->score;
				$insert->expire_timescore = $question->expire_timescore;
				$insert->penality = $question->penality;
				$insert->ordering = $question->ordering;
				$insert->created_date = $question->created_date;
				$insert->modified_date = $question->modified_date;
				$insert->published = $question->published;

				if (!$this->_db->insertObject('#__vquiz_question', $insert, 'id')) {

					$msg = $this->setError($this->_db->stderr());
					$this->setRedirect('index.php?option=com_vquiz&view=quizquestion', $msg);
					return false;
				}

				$query = ' SELECT * FROM #__vquiz_option WHERE qid = ' . $question->id;
				$this->_db->setQuery($query);
				$option = $this->_db->loadObjectList();

				$qid = $this->_db->insertid();

				for ($i = 0; $i < count($option); $i++) {

					$insertoption = new stdClass();
					$insertoption->id = null;
					$insertoption->qid = $qid;
					$insertoption->qoption = $option[$i]->qoption;
					$insertoption->correct_ans = $option[$i]->correct_ans;
					$insertoption->options_score = $option[$i]->options_score;


					if (!$this->_db->insertObject('#__vquiz_option', $insertoption, 'id')) {

						$msg = $this->setError($this->_db->stderr());
						$this->setRedirect('index.php?option=com_vquiz&view=quizquestion', $msg);

						return false;
					}
				}
			}
		}
		return true;
	}
	
	
	function getQuiz($quizzesid) {
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);

		$query->select('a.*');
		$query->from('#__vquiz_quizzes AS a');
		$query->where('a.id = '. (int) $quizzesid);


		$this->_db->setQuery($query);
		$result = $this->_db->loadObject();

		return $result;
	}


	function getResultcats($quizzesid) {
		$db =& JFactory::getDBO();
		$query = $db->getQuery(true);

		$query->select('a.id , a.quiztitle , a.level');
		$query->from('#__vquiz_resultcats AS a');
		$query->join('LEFT', '#__vquiz_quizzes AS c ON a.parent_id = c.resultcats_id');
		$query->where('c.id = '. (int) $quizzesid);


		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();

		return $result;
	}

}