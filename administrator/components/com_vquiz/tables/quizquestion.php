<?php

/* ------------------------------------------------------------------------
  # com_vquiz - vQuiz
  # ------------------------------------------------------------------------
  # author    Team WDMtech
  # copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
  # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Websites: http://www.wdmtech..com
  # Technical Support:  Forum - http://www.wdmtech.com/support-forum
  ----------------------------------------------------------------------- */
// No direct access

defined('_JEXEC') or die('Restricted access');

class TableQuizquestion extends JTable {

	var $id = null;
	var $titleid = null;
	var $qtitle = null;
	var $optiontype = null;
	var $created_date = null;
	var $modified_date = null;
	var $published = null;
	var $correct_point = null;
	var $negative_point = null;
	var $options_score = null;

	function __construct(& $db) {

		parent::__construct('#__vquiz_question', 'id', $db);
	}

	function store($updateNulls = false) {

		if (!parent::store($updateNulls)) {

			return false;
		}

		$data = JRequest::get('post');

		$data['qoption'] = JRequest::getVar('qoption', array(), 'post', 'array');
		$data['correct_ans'] = JRequest::getVar('correct_ans', array(), 'post', 'array');
		$data['options_score'] = JRequest::getVar('options_score', array(), 'post', 'array');

		$query = 'select id from #__vquiz_option where qid=' . $this->id . ' order by id asc';
		$this->_db->setQuery($query);
		$exid = $this->_db->loadColumn();


		for ($i = 0; $i < count($data['qoption']); $i++) {

			if (in_array($i, $data['correct_ans']))
				$ans = 1;
			else
				$ans = 0;

			$ids = implode(",", $data['resultcats_ids'][$i]);

			if (count($exid) > 0) {
				$id = array_shift($exid);
				$query = 'update #__vquiz_option set qoption = ' . $this->_db->quote($data['qoption'][$i]) . ', options_score = ' . $this->_db->quote($data['options_score'][$i]) . ', correct_ans = ' . $this->_db->quote($ans) . ', resultcats_ids = ' . $this->_db->quote($ids) . ' where id = ' . $this->_db->quote($id);
			} else {
				$query = 'insert into #__vquiz_option(qid,qoption,correct_ans,options_score, resultcats_ids) values(' . $this->_db->quote($this->id) . ', ' . $this->_db->quote($data['qoption'][$i]) . ', ' . $this->_db->quote($ans) . ',' . $this->_db->quote($data['options_score'][$i]). ', '. $this->_db->quote($ids). ')';
			}
			$this->_db->setQuery($query);

			if (!$this->_db->execute()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}

		if (count($exid) > 0 and !empty($data['qoption'])) {

			$query = 'delete from #__vquiz_option where id in (' . implode(', ', $exid) . ')';
			$this->_db->setQuery($query);

			if (!$this->_db->execute()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}


		return true;
	}

	function delete($id = null) {

		if (!parent::delete($id))
			return false;
		else {
			$query = 'delete from #__vquiz_option where qid = ' . (int) $this->id;

			$this->_db->setQuery($query);

			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}

		return true;
	}

}