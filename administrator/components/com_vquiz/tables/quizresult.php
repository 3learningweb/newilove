<?php
/*------------------------------------------------------------------------
# com_vquiz - vQuiz
# ------------------------------------------------------------------------
# author    Team WDMtech
# copyright Copyright (C) 2015 wwww.wdmtech.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.wdmtech..com
# Technical Support:  Forum - http://www.wdmtech.com/support-forum
-----------------------------------------------------------------------*/
// No direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

 

class TableQuizresult extends JTable

{

 

	var $id = null;

	var $quiztitle  = null;

	var $userid = null;

	var $startdatetime  = null;

	var $enddatetime = null;

	var $score  = null;

	var $passed_score = null;

	var $maxscore  = null;

	var $quiz_spentdtime = null;

 

	function __construct(& $db) {

		parent::__construct('#__vquiz_quizresult', 'id', $db);

	}

 

}