<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

class com_jvleInstallerScript
{
	public function install(JAdapterInstance $adapter)
	{
		return $this->continueInstallation();
	}

	public function update(JAdapterInstance $adapter)
	{
		return $this->continueInstallation();
	}

	public function uninstall(JAdapterInstance $adapter)
	{
		try
		{
			$db = JFactory::getDbo();
			$db->setQuery("drop table if exists #__jvle_settings, #__jvle_categories, #__jvle_links, #__jvle_votes");
			$db->execute();

			echo '<div class="well">'.JText::_('COM_JVLE_UNINSTALL_TEXT').'</div>';
		}
		catch (Exception $ex)
    	{
	    	echo '<div class="well">'.$ex->getMessage().'</div>';
		}
	}

	private function continueInstallation()
	{
		echo '	<br />
   				<div class="alert alert-error">
					<h4>'.JText::_("COM_JVLE_SCRIPT_CONTINUE").'....</h4><br />
   					<p class="text-left">'.JText::_("COM_JVLE_MSG_1").'</p>
   					<p class="text-center">
   						<button class="btn btn-primary btn-large" type="button" onclick="document.location.href=\'index.php?option=com_jvle&view=install\';">'.JText::_("COM_JVLE_MSG_2").'</button>
   					</p>
   				</div>';

		return true;
	}
}