<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.database.table');

class JvleTableLink extends JTable
{
	public function __construct($db)
	{
		parent::__construct('#__jvle_links', 'id', $db);
	}

	public function check()
	{
		$cfg = JvleCfg::getInstance();
		
		try
		{
			if ($this->partner_url == '')
				throw new Exception(JText::sprintf('COM_JVLE_LINK_ADDEDIT_ERR_1', JText::_('COM_JVLE_LINK_URL')));
							
			if ($this->partner_email == '')
				throw new Exception(JText::sprintf('COM_JVLE_LINK_ADDEDIT_ERR_1', JText::_('COM_JVLE_LINK_EMAIL')));
								
			if (!$this->link_category)
				throw new Exception(JText::sprintf('COM_JVLE_LINK_ADDEDIT_ERR_1', JText::_('COM_JVLE_LINK_CATEGORY')));

			if ($this->id) // edit
			{
				$elink = JvleDb::getRow("select * from #__jvle_links where id = ".(int)$this->id);
				
				if (($elink->partner_url != $this->partner_url) && (!JvleUtil::isNewLink($this->partner_url)))
					throw new Exception(JText::_('COM_JVLE_LINK_ADDEDIT_ERR_2'));				
			}
			else // add
			{
				if (!JvleUtil::isNewLink($this->partner_url))
					throw new Exception(JText::_('COM_JVLE_LINK_ADDEDIT_ERR_2'));

				$catobj = JvleDb::getRow("select maxlinks, accept_links from #__jvle_categories where id = '".(int)$this->link_category."'");
				if (($catobj->maxlinks) && ((1 + JvleUtil::getNumlinks($this->link_category)) > $catobj->maxlinks))
					throw new Exception(JText::_('COM_JVLE_LINK_ADDEDIT_ERR_3'));
				
				if (!$catobj->accept_links)
					throw new Exception(JText::_("COM_JVLE_LINKS_MOVE_NOT_ACCEPTING_LINKS"));
			}
			
			switch ($this->exchange_link) // link vs banner
			{
				case 1: 
				{
					if ($this->partner_title == '')
						throw new Exception(JText::sprintf('COM_JVLE_LINK_ADDEDIT_ERR_1', JText::_('COM_JVLE_LINK_TITLE')));
					
					break;						
				}
				case 0: 
				{
					$err_on_noupload = ($this->id) ? 0 : 1;
					$this->banner_loc = JvleUtil::uploadFile('banner_loc', _JVLE_ABS_BANNERS, $cfg->banner_extns, $cfg->banner_width_max, $cfg->banner_height_max, $cfg->banner_kbsize_max, $err_on_noupload);
					if (($this->id) && ($this->banner_loc == ''))
						$this->banner_loc = JvleDb::getValue("select banner_loc from #__jvle_links where id = ".(int)$this->id);
						
					break;
				}
				default:
					throw new Exception(JText::sprintf('COM_JVLE_LINK_ADDEDIT_ERR_1', JText::_('COM_JVLE_ADD_WHAT')));
			}
			
			return true;			
		}
		catch (Exception $ex)
		{
			$this->setError($ex->getMessage());
			return false;
		}
	}	
	
	public function store($updateNulls = false)
	{
		if ($this->id) // edit
		{
			$elink = JvleDb::getRow("select * from #__jvle_links where id = ".(int)$this->id);
			
			if (($this->link_type == 'TWO-WAY') && (($this->reciprocal_link_url != $elink->reciprocal_link_url) || ($this->partner_url != $elink->partner_url)))
			{
				$this->link_status = 'PENDING';
				$this->link_published_on = '0000-00-00 00:00:00';
			}				
		}
		else // add
		{
			$this->link_added_on = JFactory::getDate()->toSql();			

			if ($this->link_type == 'ONE-WAY')
			{
				$this->link_status = 'ESTABLISHED';
				$this->link_published_on = JFactory::getDate()->toSql();
			}
			else
			{
				$this->link_status = 'PENDING';
				$this->link_published_on = '0000-00-00 00:00:00';
			}
		}		
				
		return parent::store($updateNulls);
	}
}