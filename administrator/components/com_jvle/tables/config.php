<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.database.table');

class JvleTableConfig extends JTable
{
	public function __construct($db)
	{
		parent::__construct('#__jvle_settings', 'id', $db);
	}

	public function check()
	{
		try
		{
			if (($this->enable_flinks) && (($this->flinks_subemail == '') || ($this->flinks_subamount == 0)))
				throw new Exception(JText::_('COM_JVLE_CONFIG_ERR_FLINKS_2'));

			if (($this->fe_addbanner) && ($this->banner_loc == ''))
				throw new Exception(JText::_('COM_JVLE_CONFIG_ERR_BX_1'));

			return true;
		}
		catch (Exception $ex)
		{
			$this->setError($ex->getMessage());
			return false;
		}
	}
}