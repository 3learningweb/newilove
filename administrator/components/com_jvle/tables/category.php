<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.database.table');

class JvleTableCategory extends JTable
{
	public function __construct($db)
	{
		parent::__construct('#__jvle_categories', 'id', $db);
	}

	public function check()
	{
		try
		{
			if ($this->name == '')
				throw new Exception(JText::_('COM_JVLE_CAT_ADDEDIT_ERR_1'));
			
			if (JString::strtolower($this->name) == 'links')
				throw new Exception(JText::_('COM_JVLE_CAT_ADDEDIT_ERR_2'));
			
			if (JString::stristr($this->name, "_") != NULL)
				throw new Exception(JText::_('COM_JVLE_CAT_ADDEDIT_ERR_3'));
			
			if ($this->id)
			{				
				if ($this->id == $this->cpid)
					throw new Exception(JText::_('COM_JVLE_CAT_ADDEDIT_ERR_5'));
			}
			else	
			{
				$cnt = JvleDb::getCount("select count(id) as CNT from #__jvle_categories where `name` = '".JvleSecure::defendSQL($this->name)."' and cpid = ".(int)$this->cpid);
				if ($cnt)
					throw new Exception(JText::_('COM_JVLE_CAT_ADDEDIT_ERR_4'));
			}
				
			return true;			
		}
		catch (Exception $ex)
		{
			$this->setError($ex->getMessage());
			return false;
		}
	}
}