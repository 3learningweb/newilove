<?php
/**
 * @version		$Id$
 * @package		JV-ShrinkTheWeb
 * @subpackage	com_jvshrinktheweb
 * @copyright	Copyright 2012-2013 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

class JvleVersionInstaller_3_1 extends JvleVersionInstaller
{
	function __construct($version)
	{
		$sqls = array(
						"alter table `#__jvle_settings` add `load_bootstrap_css` tinyint not null default '0'",
						"alter table `#__jvle_settings` add `load_bootstrap_js` tinyint not null default '0'",
				);

		parent::__construct($version, $sqls);
	}
}