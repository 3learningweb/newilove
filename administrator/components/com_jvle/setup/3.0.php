<?php	
/**
 * @version		$Id$
 * @package		JV-ShrinkTheWeb
 * @subpackage	com_jvshrinktheweb
 * @copyright	Copyright 2012-2013 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

class JvleVersionInstaller_3_0 extends JvleVersionInstaller
{
	var $links;
	
	function __construct($version)
	{
		$sqls = array(	"alter table `#__jvle_settings` change `self_url` `self_url` varchar(250) not null default ''",
						"alter table `#__jvle_settings` change `self_desc` `self_desc` varchar(400) not null default ''",
						"alter table `#__jvle_settings` drop `display_ratings`",
						"alter table `#__jvle_settings` drop `fe_num_links`",
						"alter table `#__jvle_settings` drop `enable_snap`",
						"alter table `#__jvle_settings` drop `stw_access_key`",
						"alter table `#__jvle_categories` change `name` `name` varchar(80) not null default ''",
						"alter table `#__jvle_categories` change `description` `description` varchar(400) not null default ''",
						"alter table `#__jvle_categories` drop `snap_status`",
						"alter table `#__jvle_links` change `partner_url` `partner_url` varchar(250) not null default ''",
						"alter table `#__jvle_links` change `partner_desc` `partner_desc` varchar(400) not null default ''",
						"alter table `#__jvle_links` change `reciprocal_link_url` `reciprocal_link_url` varchar(250) not null default ''",
						"alter table `#__jvle_links` change `reject_reason` `reject_reason` varchar(400) not null default ''",
						"alter table `#__jvle_links` change `link_added_on` `link_added_on` datetime not null default '0000-00-00 00:00:00'",
						"alter table `#__jvle_links` change `link_published_on` `link_published_on` datetime not null default '0000-00-00 00:00:00'",
						"alter table `#__jvle_links` change `rlc_last_checked_on` `rlc_last_checked_on` datetime not null default '0000-00-00 00:00:00'",
						"alter table `#__jvle_links` change `date_of_last_reminder` `date_of_last_reminder` datetime not null default '0000-00-00 00:00:00'",
						"alter table `#__jvle_links` drop `snap_status`",
				);
		
		parent::__construct($version, $sqls);
	}
	
	function execPreDb()
	{
		$this->links = JvleDb::getRows("select id, link_added_on, link_published_on, rlc_last_checked_on, date_of_last_reminder from #__jvle_links");	
	}
	
	function execPostDb()
	{
		if (count($this->links))
		{
			foreach ($this->links as $link)
			{
				$t1 = ($link->link_added_on != 0) ? gmdate("Y-m-d H:i:s", $link->link_added_on) : '0000-00-00 00:00:00';
				$t2 = ($link->link_published_on != 0) ? gmdate("Y-m-d H:i:s", $link->link_published_on) : '0000-00-00 00:00:00';
				$t3 = ($link->rlc_last_checked_on != 0) ? gmdate("Y-m-d H:i:s", $link->rlc_last_checked_on) : '0000-00-00 00:00:00';
				$t4 = ($link->date_of_last_reminder != 0) ? gmdate("Y-m-d H:i:s", $link->date_of_last_reminder) : '0000-00-00 00:00:00';				
				JvleDb::update("update #__jvle_links set link_added_on = '".JvleSecure::defendSQL($t1)."', link_published_on = '".JvleSecure::defendSQL($t2)."', rlc_last_checked_on = '".JvleSecure::defendSQL($t3)."', date_of_last_reminder = '".JvleSecure::defendSQL($t4)."' where id = '".(int)$link->id."'");
			}
		}
	}	
}