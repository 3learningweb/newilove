<?php
/**
 * @version		$Id$
 * @package		JV-ShrinkTheWeb
 * @subpackage	com_jvshrinktheweb
 * @copyright	Copyright 2012-2013 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

class JvleVersionInstaller_3_2 extends JvleVersionInstaller
{
	function __construct($version)
	{
		$sqls = array(
						"alter table `#__jvle_settings` add `template_name` varchar(50) not null default 'default' after `websnapr_api_key`",
						"alter table `#__jvle_settings` drop `websnapr_api_key`",
						"update #__jvle_settings set `template_name` = 'default'",
						"alter table `#__jvle_settings` add `thumboo_akey` varchar(50) not null default ''",
				);

		parent::__construct($version, $sqls);
	}

	function execPreDb()
	{
		try {
			JvleDb::update("alter table `#__jvle_settings` drop `template_name`");
		}
		catch (Exception $ex) {}
	}
}