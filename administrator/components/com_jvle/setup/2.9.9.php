<?php
/**
 * @version		$Id$
 * @package		JV-ShrinkTheWeb
 * @subpackage	com_jvshrinktheweb
 * @copyright	Copyright 2012-2013 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

class JvleVersionInstaller_2_9_9 extends JvleVersionInstaller
{
	function __construct($version)
	{
		parent::__construct($version);
	}
}