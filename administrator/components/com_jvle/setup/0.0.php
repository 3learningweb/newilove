<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

class JvleVersionInstaller_0_0 extends JvleVersionInstaller
{
	function __construct($version)
	{
		$sqls = array(
					"create table if not exists `#__jvle_settings` (
						`id` int(11) unsigned not null auto_increment,
						`version` varchar(40) not null default '',
						`self_title` varchar(80) not null default '',
						`self_url` varchar(250) not null default '',
						`self_desc` varchar(400) not null default '',
						`self_email` varchar(100) not null default '',
						`rlc_status_update` tinyint not null default '1',
						`enable_rlc_fe` tinyint not null default '1',
						`auto_approve` tinyint not null default '0',
						`show_latest_links` tinyint not null default '1',
						`enable_link_rating` tinyint not null default '1',
						`link_rating_by` varchar(12) not null default 'registered',
						`enable_search` tinyint not null default '1',
						`fe_addlink` tinyint not null default '1',
						`fe_title` varchar(80) not null default 'link partners',
						`fe_dir_summary` longtext not null,
						`fe_newlink_summary` longtext not null,
						`fe_instructions` longtext not null,
						`email_approval` tinyint not null default '0',
						`enable_checklinkstatus` tinyint not null default '1',
						`enable_recweb` tinyint not null default '1',
						`recommend_site_intro` longtext not null,
						`recweb_by` varchar(12) not null default 'registered',
						`enable_ads` tinyint not null default '0',
						`ad_top` longtext not null,
						`ad_bottom` longtext not null,
						`enable_captcha` tinyint not null default '0',
						`enable_scheduler` tinyint not null default '0',
						`result_mode` varchar(10) not null default 'email',
						`window_new` tinyint not null default '0',
						`enable_flinks` tinyint not null default '0',
						`flinks_numshow` smallint not null default '5',
						`flinks_dtype` varchar(20) not null default 'recent',
						`flinks_subamount` float not null default '0',
						`flinks_subemail` varchar(80) not null default '',
						`fe_addbanner` tinyint not null default '0',
						`banner_loc` varchar(250) not null default '',
						`fe_newbanner_summary` longtext not null,
						`banner_kbsize_max` bigint not null default '30',
						`banner_width_max` bigint not null default '468',
						`banner_height_max` bigint not null default '60',
						`banner_extns` varchar(250) not null default 'jpg,gif,png',
						`generic_reject_reason` longtext not null,
						`header_slogan` varchar(80) not null default '',
						`show_flinks` tinyint not null default '0',
						`tp_enable` tinyint not null default '0',
						`track_hits` bigint not null default '0',
						`template_name` varchar(50) not null default 'default',
                        `load_bootstrap_css` tinyint not null default '0',
                        `load_bootstrap_js` tinyint not null default '0',
						`thumboo_akey` varchar(50) not null default '',
						primary key  (`id`)
					) engine=myisam default charset=utf8",

					"create table if not exists `#__jvle_categories` (
						`id` int(11) unsigned not null auto_increment,
						`cpid` bigint not null default '0',
						`name` varchar(80) not null default '',
						`description` varchar(400) not null default '',
						`maxlinks` bigint not null default '0',
						`accept_links` tinyint not null default '1',
						`visibility` tinyint not null default '1',
						primary key  (`id`)
					) engine=myisam default charset=utf8",

					"create table if not exists `#__jvle_links` (
						`id` int(11) unsigned not null auto_increment,
						`link_type` varchar(20) not null default '',
						`partner_title` varchar(80) not null default '',
						`partner_url` varchar(250) not null default '',
						`partner_desc` varchar(400) not null default '',
						`partner_email` varchar(100) not null default '',
						`reciprocal_link_url` varchar(250) not null default '',
						`link_category` int(11) not null default '0',
						`link_status` varchar(50) not null default '',
						`link_added_on` datetime not null default '0000-00-00 00:00:00',
						`link_published_on` datetime not null default '0000-00-00 00:00:00',
						`link_rating` float not null default '0',
						`num_votes` int(11) not null default '0',
						`rlc_last_checked_on` datetime not null default '0000-00-00 00:00:00',
						`reject_reason` varchar(400) not null default '',
						`emstr` varchar(32) not null default '',
						`date_of_last_reminder` datetime not null default '0000-00-00 00:00:00',
						`enable_scheduler` tinyint not null default '0',
						`featured` tinyint not null default '0',
						`exchange_link` tinyint not null default '1',
						`banner_loc` varchar(250) not null default '',
						`hits` int(11) not null default '0',
						primary key  (`id`)
					) engine=myisam default charset=utf8",

					"create table if not exists `#__jvle_votes` (
						`id` int(11) unsigned not null auto_increment,
						`lid` int(11) not null default '0',
						`vote_ip` varchar(30) not null default '',
						`vote_time` int(11) not null default '0',
						primary key  (`id`)
					) engine=myisam default charset=utf8",

					"insert into #__jvle_settings (id) values('1')",
				);

		parent::__construct($version, $sqls);
	}

	function execPostDb()
	{
		$config = JFactory::getConfig();
		JvleDb::update("update #__jvle_settings set self_title = '".JvleSecure::defendSQL($config->get('sitename'))."', self_url = '".JUri::root()."', self_email = '".JvleSecure::defendSQL($config->get('mailfrom'))."', self_desc = '".JvleSecure::defendSQL($config->get('MetaDesc'))."' where id = 1");

		if (!JFolder::exists(_JVLE_ABS_BANNERS))
			JFolder::create(_JVLE_ABS_BANNERS);
	}
}
