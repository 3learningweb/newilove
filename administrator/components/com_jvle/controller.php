<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class JvleController extends JControllerLegacy
{
    function display($cachable = false, $urlparams = false)
    {
        $view = JFactory::getApplication()->input->getCmd('view', 'jvle');
        if ($view == 'configs') 
        {
			$this->setRedirect(JRoute::_('index.php?option=com_jvle&view=jvle', false));
			return;
        }

        JvleAdminUtil::addSubmenu($view);

        parent::display($cachable, $urlparams);
        return $this;
    }
}