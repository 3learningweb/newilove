<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controllerform');

class JvleControllerLink extends JControllerForm
{
	public function chgcat()
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
	
		try
		{
			$input = JFactory::getApplication()->input;	
			$newcid = $input->getInt('chgcat_newcat', 0);
	
			$lid = JFactory::getApplication()->input->get('cid', array(), 'array');
			if (empty($lid))
				throw new Exception(JText::_('COM_JVLE_LINKS_NO_ITEM_SELECTED'));
	
			if (!$newcid)
				throw new Exception(JText::_('COM_JVLE_LINKS_NO_CID'));
	
			JArrayHelper::toInteger($lid);
			
			$catobj = JvleDb::getRow("select maxlinks, accept_links from #__jvle_categories where id = ".(int)$newcid);
			if (($catobj->maxlinks) && ((count($lid) + JvleUtil::getNumlinks($newcid)) > $catobj->maxlinks))
				throw new Exception(JText::_('COM_JVLE_LINK_ADDEDIT_ERR_3'));
					
			if (!$catobj->accept_links)
				throw new Exception(JText::_("COM_JVLE_LINKS_MOVE_NOT_ACCEPTING_LINKS"));
			
			for ($i=0;$i<count($lid);$i++)
				JvleDb::update("update #__jvle_links set link_category = ".(int)$newcid." where id = ".(int)$lid[$i]);
	
			$this->setMessage(JText::_('COM_JVLE_LINKS_MOVE_OK'));
		}
		catch (Exception $ex)
		{
			$this->setMessage($ex->getMessage(), 'error');
		}
	
		$this->setRedirect('index.php?option=com_jvle&view=links');
	}

	public function chgsts()
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
	
		try
		{
			$input = JFactory::getApplication()->input;
			$newsts = $input->getString('chgsts_newsts', 'UNKNOWN');
			$reject_reason = $input->getString('chgsts_rejreason', '');
			
			if (($newsts == 'REJECTED') && ($reject_reason == ''))
				throw new Exception(JText::_('COM_JVLE_LINKS_REASON_MANDATORY'));
	
			$lid = JFactory::getApplication()->input->get('cid', array(), 'array');
			if (empty($lid))
				throw new Exception(JText::_('COM_JVLE_LINKS_NO_ITEM_SELECTED'));
	
			JArrayHelper::toInteger($lid);
			
			for ($i=0;$i<count($lid);$i++)
				JvleUtil::changeLinkStatus($lid[$i], $newsts, $reject_reason);
				
			$this->setMessage(JText::_('COM_JVLE_LINKS_CHGSTS_OK'));
		}
		catch (Exception $ex)
		{
			$this->setMessage($ex->getMessage(), 'error');
		}
	
		$this->setRedirect('index.php?option=com_jvle&view=links');
	}	
}