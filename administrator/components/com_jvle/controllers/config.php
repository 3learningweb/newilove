<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controllerform');

class JvleControllerConfig extends JControllerForm
{
	public function edit($key = null, $urlVar = null)
	{
		$canDo = JvleAdminUtil::getActions();
		if (!$canDo->get('core.admin'))
		{
        	$this->setMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'error');
        	$this->setRedirect(JRoute::_('index.php?option=com_jvle&view=jvle', false));
			return false;
		}

		return parent::edit($key, $urlVar);
	}	
}