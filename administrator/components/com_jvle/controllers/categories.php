<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controlleradmin');

class JvleControllerCategories extends JControllerAdmin
{
	protected $text_prefix = 'COM_JVLE_CATEGORIES';

	public function getModel($name = 'Category', $prefix = 'JvleModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
	
	public function delete()
	{
		JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));
	
		try
		{
			$cid = JFactory::getApplication()->input->get('cid', array(), 'array');
			if (empty($cid))
				throw new Exception(JText::_($this->text_prefix . '_NO_ITEM_SELECTED'));
	
			JArrayHelper::toInteger($cid);
	
			for ($i=0,$j=0;$i<count($cid);$i++)
			{
				if (JvleUtil::getNumlinks($cid[$i]))
					continue;
				
				$cnt = JvleDb::getCount("select count(id) as CNT from #__jvle_categories where cpid = '".(int)$cid[$i]."'");
				if ($cnt)
					continue;

				JvleDb::update("delete from #__jvle_categories where id = ".(int)$cid[$i]);
				$j++;
			}
	
			$this->setMessage(JText::sprintf('COM_JVLE_CATEGORIES_N_ITEMS_DELETED', $j));
		}
		catch (Exception $ex)
		{
			$this->setMessage($ex->getMessage(), 'error');
		}
	
		$this->setRedirect('index.php?option=com_jvle&view=categories');
	}	
}
