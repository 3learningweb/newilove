<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

abstract class JvleInstallerHelper
{
	public static function getDbVersion()
	{
		try
		{
			$db = JFactory::getDbo();
			$db->setQuery("select version from #__jvle_settings where id = 1");
			$obj = $db->loadObject();
			if ($obj)
				return (string)$obj->version;

			return 0;
		}
		catch (Exception $ex)
		{
			JvleInstallerDebug::log(__CLASS__."::".__FUNCTION__.":: ".$ex->getMessage());
			return 0;
		}
	}

	public static function getVersionFromManifest()
	{
		$db = JFactory::getDbo();
		$db->setQuery("select manifest_cache from #__extensions where element = 'com_jvle' and type = 'component'");
		$tmp_version = json_decode($db->loadResult());
		return JString::trim($tmp_version->version);
	}

	public static function getVersionsUpgradeList($db_version, &$sindex, &$tindex)
	{
		// if db version is 2.x hack a little
		$db_major_version_t = explode('.', $db_version);
		JvleInstallerDebug::log("DB Major version: ".$db_major_version_t[0]);
		
		if ($db_major_version_t[0] == 2)
		{
			$db_version = '2.9.9';
			JvleInstallerDebug::log("Current DB version reset to ".$db_version);
		}		
		
		$versions = array();
		$files = JFolder::files(JPATH_ADMINISTRATOR.DS."components".DS."com_jvle".DS."setup");
		for ($i=0;$i<count($files);$i++)
			$versions[$i] = JString::trim(basename($files[$i], ".php"));

		usort($versions, "version_compare");
		
		$key = array_search($db_version, $versions, true);
		if ($key === false)
			throw new Exception("Current DB Version [".$db_version."] is not available in the list of available versions [".print_r($versions, true)."]");

		$key++;
		$sindex = $key;
		$tindex = count($versions);
		
		$upgrade_versions = array();
		for ($k=0,$i=$key;$i<count($versions);$i++)
			$upgrade_versions[$k++] = $versions[$i];
		
		return $upgrade_versions;
	}

	public static function getInstallerObject($version)
	{
		$filename = JPATH_ADMINISTRATOR.DS."components".DS."com_jvle".DS."setup".DS.$version.".php";
		if (!JFile::exists($filename))
			throw new Exception("Installer PHP file [".$filename."] does not exist");

		require_once($filename);
		$clsname = "JvleVersionInstaller_".JString::str_ireplace(".", "_", $version);
		return new $clsname($version);
	}

	public static function updateVersionInDb($version)
	{
		$db = JFactory::getDbo();
		$db->setQuery("update #__jvle_settings set version = '".JvleSecure::defendSQL($version)."' where id = 1");
		$db->execute();

		JvleInstallerDebug::log(__CLASS__."::".__FUNCTION__.":: Version updated in Db as : ".$version);
	}
}

abstract class JvleVersionInstaller
{
	var $version;
	var $dversion;
	var $sqls = array();
	var $msgs = array();
	var $error;

	function __construct($version, $sqls=array())
	{
		$this->error = 0;
		$this->version = $version;
		$this->sqls = $sqls;
		$this->dversion = ($this->version == "0.0") ? JvleInstallerHelper::getVersionFromManifest() : $this->version;

		$this->setMessage("Installing Component Version: ".$this->dversion);
		JvleInstallerDebug::log(__CLASS__."::".__FUNCTION__.":: Installing Component Version: ".$this->dversion);
	}
	function execPreDb()
	{
		return;
	}
	function execPostDb()
	{
		return;
	}
	function execSql()
	{
		$db = JFactory::getDbo();
		try
		{
			$db->transactionStart();

			for ($i=0;$i<count($this->sqls);$i++)
			{
				JvleInstallerDebug::log(__CLASS__."::".__FUNCTION__.":: Query to run: ".$this->sqls[$i]);

				$db->setQuery($this->sqls[$i]);
				$db->execute();
			}

			$db->transactionCommit();

			JvleInstallerDebug::log(__CLASS__."::".__FUNCTION__.":: ".count($this->sqls)." Sql queries have been run successfully");

		}
		catch (Exception $ex)
		{
			$db->transactionRollback();
			JvleInstallerDebug::log(__CLASS__."::".__FUNCTION__.":: Error: ".$ex->getMessage());

			throw $ex;
		}

		return;
	}

	function execute()
	{
		try
		{
			$this->execPreDb();
			$this->execSql();
			$this->execPostDb();

			$this->setMessage("Installation to version : ".$this->dversion." completed");
			JvleInstallerDebug::log(__CLASS__."::".__FUNCTION__.":: Installation to version : ".$this->dversion." completed");
		}
		catch (Exception $ex)
		{
			$this->error = 1;
			$this->setMessage("Error while running installation to version : ".$this->dversion." >> ".$ex->getMessage());
			JvleInstallerDebug::log(__CLASS__."::".__FUNCTION__.":: Error while running installation to version : ".$this->dversion." >> ".$ex->getMessage());
		}

		return -99;
	}

	function setMessage($msg)
	{
		$this->msgs[] = $msg;
	}

	function getMessage()
	{
		return implode("<br />", $this->msgs);
	}

	function getErrorFlag()
	{
		return $this->error;
	}
}

abstract class JvleInstallerDebug
{
	public static function log($msg)
	{
		$install_dbg_file = JPATH_ROOT.DS."tmp".DS."com_jvle.install.html";
		$fp = (JFile::exists($install_dbg_file)) ? fopen($install_dbg_file, "a+") : fopen($install_dbg_file, "w");
		$tnow = date("Y-m-d H:i:s", time());
		fwrite($fp, "[".$tnow."] ".$msg."<br />");
		fclose($fp);
	}
}
