<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

abstract class JvleAdminUtil
{
	public static function addSubmenu($in_view = 'jvrelatives')
	{
		$canDo = JvleAdminUtil::getActions();

		JHtmlSidebar::addEntry(JText::_('COM_JVLE_DASHBOARD'), 'index.php?option=com_jvle&view=jvle', $in_view == 'jvle');

		if ($canDo->get('core.admin'))
			JHtmlSidebar::addEntry(JText::_('COM_JVLE_CONFIGURATION'), 'index.php?option=com_jvle&task=config.edit&id=1', $in_view == 'config');

		JHtmlSidebar::addEntry(JText::_('COM_JVLE_CATEGORIES'), 'index.php?option=com_jvle&view=categories', $in_view == 'categories');
		JHtmlSidebar::addEntry(JText::_('COM_JVLE_LINKS'), 'index.php?option=com_jvle&view=links', $in_view == 'links');

		switch ($in_view)
		{
			case 'config': $title = JText::_('COM_JVLE_CONFIGURATION'); break;
			case 'categories': $title = JText::_('COM_JVLE_CATEGORIES'); break;
			case 'links': $title = JText::_('COM_JVLE_LINKS'); break;
			default: $title = JText::_('COM_JVLE_DASHBOARD'); break;
		}

		$document = JFactory::getDocument();
		$document->setTitle($title);
	}

	public static function getActions()
	{
		$user = JFactory::getUser();
		$result = new JObject;

		$actions = array('core.admin', 'core.manage');
		foreach ($actions as $action)
			$result->set($action, $user->authorise($action, 'com_jvle'));

		return $result;
	}

	public static function showFooter()
	{
		$val = '<div class="well well-small">
					<strong>JV-LinkExchanger '.JvleInstallerHelper::getDbVersion().'</strong><br />
					Copyright 2007 - 2015. Niranjan SrinivasaRao / JV-Extensions.com<br />
					<strong>If you like this free software, please post a rating and a review at the <a href="http://extensions.joomla.org/extensions/extension/ads-a-affiliates/text-a-link-ads/jv-linkexchanger-jvlinx" target="_blank">Joomla! Extensions Directory</a>.</strong><br />
				</div>';
		echo $val;
	}
}