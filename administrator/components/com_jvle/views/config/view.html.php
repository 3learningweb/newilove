<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewConfig extends JViewLegacy
{
	protected $form;
	protected $item;
	protected $fieldsets_ids = array();
	protected $fieldsets_names = array();

    function display($tpl = null)
    {
    	$this->form = $this->get('Form');
        $this->item = $this->get('Item');

		if (count($errors = $this->get('Errors')))
        {
			JError::raiseError(500, implode('\n', $errors));
            return false;
		}
		
		$this->fieldsets_ids = array('selfconfig', 'layout', 'xchange', 'bannerxchange', 'flinks', 'cron');
		$this->fieldsets_names = array('COM_JVLE_CONFIG_SELFCFG', 'COM_JVLE_CONFIG_LAYOUT', 'COM_JVLE_XCHANGE', 'COM_JVLE_BANNERX', 'COM_JVLE_FLINKS', 'COM_JVLE_CRON');		

        $this->addToolBar();
        $this->setDocument();

        parent::display($tpl);
    }

    protected function addToolBar()
    {
    	JFactory::getApplication()->input->set('hidemainmenu', true);

        JToolBarHelper::title(JText::_('COM_JVLE_INFO').' '.JText::_('COM_JVLE_CONFIGURATION'), 'jvle');
        JToolBarHelper::apply('config.apply', 'JTOOLBAR_APPLY');
        JToolBarHelper::save('config.save', 'JTOOLBAR_SAVE');
		JToolBarHelper::cancel('config.cancel', 'JTOOLBAR_CLOSE');
    }

    protected function setDocument()
    {
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('COM_JVLE_INFO').' '.JText::_('COM_JVLE_CONFIGURATION'));
    }
}