<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');
?>

<p><?php echo JText::_('COM_JVLE_THUMB_INTRO'); ?></p>

<div class="control-group">
	<div class="control-label"><?php echo $this->form->getLabel('tp_enable'); ?></div>
	<div class="controls"><?php echo $this->form->getInput('tp_enable'); ?></div>
</div>

<h4><?php echo JText::_('COM_JVLE_THUMB_STW'); ?></h4>
<p><?php echo JText::_('COM_JVLE_THUMB_STW_DESC'); ?></p>

<h4><?php echo JText::_('COM_JVLE_THUMB_THO'); ?></h4>
<p><?php echo JText::_('COM_JVLE_THUMB_THO_DESC'); ?></p>

<div class="control-group">
	<div class="control-label"><?php echo $this->form->getLabel('thumboo_akey'); ?></div>
	<div class="controls"><?php echo $this->form->getInput('thumboo_akey'); ?></div>
</div>