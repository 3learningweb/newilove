<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.formvalidation');
?>

<form action="<?php echo JRoute::_('index.php?option=com_jvle&layout=edit&id='.(int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-horizontal">
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset>
				<ul class="nav nav-tabs">
<?php for ($f = 0; $f < count($this->fieldsets_ids); $f++) : 
			$active = (!$f) ? ' class="active"' : '';
?>
					<li<?php echo $active; ?>>
						<a href="#<?php echo $this->fieldsets_ids[$f]; ?>" data-toggle="tab">
							<?php echo JText::_($this->fieldsets_names[$f]);?>
						</a>
					</li>					
<?php endfor; ?>
					<li>
						<a href="#thumbs" data-toggle="tab">
							<?php echo JText::_('COM_JVLE_THUMB');?>
						</a>
					</li>
					<li>
						<a href="#misc" data-toggle="tab">
							<?php echo JText::_('COM_JVLE_MISC');?>
						</a>
					</li>					
				</ul>
				<div class="tab-content">
<?php for ($f = 0; $f < count($this->fieldsets_ids); $f++) : 
 			$active = (!$f) ? ' active' : ''; 		
?>
					<div class="tab-pane<?php echo $active; ?>" id="<?php echo $this->fieldsets_ids[$f]; ?>">
<?php 
			foreach ($this->form->getFieldset($this->fieldsets_ids[$f]) as $field) : 
?>
						<div class="control-group">
							<div class="control-label"><?php echo $field->label; ?></div>
							<div class="controls"><?php echo $field->input; ?></div>
						</div>
<?php 		endforeach; ?>
					</div>
<?php endfor; ?>		
					<div class="tab-pane" id="thumbs">
						<?php echo $this->loadTemplate("thumbs"); ?>
					</div>
					<div class="tab-pane" id="misc">
						<?php echo $this->loadTemplate("misc"); ?>
					</div>					
				</div>
			</fieldset>

		</div>
	</div>

<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>

<?php echo JvleAdminUtil::showFooter(); ?>