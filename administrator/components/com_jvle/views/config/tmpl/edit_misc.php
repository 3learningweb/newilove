<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');
?>

<h4><?php echo JText::_('COM_JVLE_MISC_RATING_TITLE'); ?></h4>
<div class="control-group">
	<div class="control-label"><?php echo $this->form->getLabel('enable_link_rating'); ?></div>
	<div class="controls"><?php echo $this->form->getInput('enable_link_rating'); ?></div>
</div>
<div class="control-group">
	<div class="control-label"><?php echo $this->form->getLabel('link_rating_by'); ?></div>
	<div class="controls"><?php echo $this->form->getInput('link_rating_by'); ?></div>
</div>

<h4><?php echo JText::_('COM_JVLE_MISC_HITS_TITLE'); ?></h4>
<div class="control-group">
	<div class="control-label"><?php echo $this->form->getLabel('track_hits'); ?></div>
	<div class="controls"><?php echo $this->form->getInput('track_hits'); ?></div>
</div>

<h4><?php echo JText::_('COM_JVLE_MISC_CLSTATUS_TITLE'); ?></h4>
<div class="control-group">
	<div class="control-label"><?php echo $this->form->getLabel('enable_checklinkstatus'); ?></div>
	<div class="controls"><?php echo $this->form->getInput('enable_checklinkstatus'); ?></div>
</div>

<h4><?php echo JText::_('COM_JVLE_MISC_RECWEB_TITLE'); ?></h4>
<div class="control-group">
	<div class="control-label"><?php echo $this->form->getLabel('enable_recweb'); ?></div>
	<div class="controls"><?php echo $this->form->getInput('enable_recweb'); ?></div>
</div>
<div class="control-group">
	<div class="control-label"><?php echo $this->form->getLabel('recweb_by'); ?></div>
	<div class="controls"><?php echo $this->form->getInput('recweb_by'); ?></div>
</div>
<div class="control-group">
	<div class="control-label"><?php echo $this->form->getLabel('recommend_site_intro'); ?></div>
	<div class="controls"><?php echo $this->form->getInput('recommend_site_intro'); ?></div>
</div>

<h4><?php echo JText::_('COM_JVLE_MISC_CAPTCHA_TITLE'); ?></h4>
<div class="control-group">
	<div class="control-label"><?php echo $this->form->getLabel('enable_captcha'); ?></div>
	<div class="controls"><?php echo $this->form->getInput('enable_captcha'); ?></div>
</div>

<h4><?php echo JText::_('COM_JVLE_MISC_ADS_TITLE'); ?></h4>
<div class="control-group">
	<div class="control-label"><?php echo $this->form->getLabel('enable_ads'); ?></div>
	<div class="controls"><?php echo $this->form->getInput('enable_ads'); ?></div>
</div>
<div class="control-group">
	<div class="control-label"><?php echo $this->form->getLabel('ad_top'); ?></div>
	<div class="controls"><?php echo $this->form->getInput('ad_top'); ?></div>
</div>
<div class="control-group">
	<div class="control-label"><?php echo $this->form->getLabel('ad_bottom'); ?></div>
	<div class="controls"><?php echo $this->form->getInput('ad_bottom'); ?></div>
</div>