<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');
?>
<script type="text/javascript">
function toggle_reject_reason(obj)
{
	if (obj.value == 'REJECTED')
		document.getElementById('rejreason').style.display = '';
	else
		document.getElementById('rejreason').style.display = 'none';
}
</script>
<div class="modal hide fade" id="collapseModal2">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">x</button>
		<h3><?php echo JText::_('COM_JVLE_LINKS_CHANGE_STATUS');?></h3>
	</div>
	<div class="modal-body">
		<div class="control-group">
			<div class="control-label"><?php echo JText::_('COM_JVLE_LINKS_CHGSTS_NEWSTS'); ?></div>
			<div class="controls">
				<select name="chgsts_newsts" id="chgsts_newsts" class="inputbox" onchange="javascript:toggle_reject_reason(this);">
					<option selected="selected" value="ESTABLISHED">ESTABLISHED</option>
					<option value="PENDING">PENDING</option>
					<option value="REJECTED">REJECTED</option>
					<option value="UNKNOWN">UNKNOWN</option>
				</select>
			</div>
		</div>
		<div class="control-group" id="rejreason" style="display:none">
			<div class="control-label"><?php echo JText::_('COM_JVLE_LINKS_CHGSTS_REJREASON'); ?></div>
			<div class="controls">
				<textarea rows="3" cols="60" name="chgsts_rejreason" id="chgsts_rejreason" class="inputbox"></textarea>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn" type="button" data-dismiss="modal">
			<?php echo JText::_('JCANCEL'); ?>
		</button>
		<button class="btn btn-primary" type="submit" onclick="Joomla.submitbutton('link.chgsts');">
			<?php echo JText::_('COM_JVLE_LINKS_CHANGE_STATUS'); ?>
		</button>
	</div>
</div>