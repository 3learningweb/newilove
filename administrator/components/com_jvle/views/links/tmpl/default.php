<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$sortFields = $this->getSortFields();
?>
<style type="text/css">
#jvleloading {
	background:#fff url(components/com_jvle/assets/images/jvle_loading.GIF) no-repeat center center;
	height: 100px;
	width: 100px;
	position: fixed;
	z-index: 1000;
	left: 50%;
	top: 50%;
	margin: -25px 0 0 -25px;
}
</style>
<script type="text/javascript">
Joomla.orderTable = function()
{
	table = document.getElementById("sortTable");
	direction = document.getElementById("directionTable");
	order = table.options[table.selectedIndex].value;
	if (order != '<?php echo $listOrder; ?>')
	{
		dirn = 'asc';
	}
	else
	{
		dirn = direction.options[direction.selectedIndex].value;
	}
	Joomla.tableOrdering(order, dirn, '');
}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_jvle&view=links'); ?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
		<div id="filter-bar" class="btn-toolbar">
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>			
			<div class="btn-group pull-right hidden-phone">
				<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC');?></label>
				<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC');?></option>
					<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING');?></option>
					<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING');?></option>
				</select>
			</div>
			<div class="btn-group pull-right">
				<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY');?></label>
				<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JGLOBAL_SORT_BY');?></option>
					<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder);?>
				</select>
			</div>
		</div>
		<div class="clearfix"> </div>
	
		<p id="blcarea" style="display:none;" class="alert alert-warn"></p>
		<table class="table table-striped" id="articleList">
			<thead>
				<tr>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
					</th>
					<th width="10%" class="nowrap center">
						<?php echo $this->escape(JText::_('COM_JVLE_LINK_LTYPE')); ?>
					</th>
					<th width="25%" class="nowrap center">
						<?php echo $this->escape(JText::_('COM_JVLE_LINK_URL')); ?>
					</th>
					<th width="10%" class="nowrap center">
						<?php echo $this->escape(JText::_('COM_JVLE_LINK_CATEGORY')); ?>
					</th>
					<th width="10%" class="nowrap center">
						<?php echo $this->escape(JText::_('COM_JVLE_LINK_STATUS')); ?>
					</th>
					<th width="10%" class="nowrap center">
						<?php echo $this->escape(JText::_('COM_JVLE_LINK_PUBON')); ?>
					</th>					
					<th width="10%" class="nowrap center">
						<?php echo $this->escape(JText::_('COM_JVLE_LINK_LBLCHECK')); ?>
					</th>
					<th width="10%" class="nowrap center">
						<?php echo $this->escape(JText::_('COM_JVLE_LINK_FEATURED')); ?>
					</th>
					<th width="10%" class="nowrap center">
						<?php echo $this->escape(JText::_('COM_JVLE_LINK_EXTYPE')); ?>
					</th>
					<th class="nowrap center hidden-phone">
						<?php echo $this->escape(JText::_('JGRID_HEADING_ID')); ?>
					</th>
				</tr>
			</thead>
			<tfoot>
			</tfoot>
			<tbody>
			<?php
					foreach ($this->items as $i => $link) :
					
						$lrco = ($link->rlc_last_checked_on == '0000-00-00 00:00:00') ? JText::_('COM_JVLE_NEVER') : $link->rlc_last_checked_on;
						$link_or_banner = ($link->exchange_link) ? JText::_('COM_JVLE_LINK') : JText::_('COM_JVLE_BANNER');

						if ($link->link_status == 'ESTABLISHED')
							$status_class = "btn-success";
						else if ($link->link_status == 'PENDING')
							$status_class = "btn-warning";
						else if ($link->link_status == 'REJECTED')
							$status_class = "btn-danger";
						else 
							$status_class = "btn-inverse";	

						if ($link->link_type == 'ONE-WAY')
						{
							$link_type = 1;
							$type_class = "badge-success";
						}
						else
						{
							$link_type = 2;
							$type_class = "badge-info";
						}
						 
			?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center hidden-phone">
						<?php echo JHtml::_('grid.id', $i, $link->id); ?>
					</td>
					<td class="center hidden-phone">
						<span class="badge <?php echo $type_class; ?> hasTip" title="<?php echo $link->link_type; ?>"><?php echo $link_type; ?></span>
					</td>					
					<td class="left hidden-phone">
						<a href="index.php?option=com_jvle&task=link.edit&id=<?php echo $link->id; ?>"><?php echo $this->escape($link->partner_url); ?></a>
					</td>
					<td class="left hidden-phone">
						<?php echo $this->escape(JvleUtil::getCategoryName($link->link_category)); ?>
					</td>
					<td class="center hidden-phone">
						<span class="hasTip" title="<?php echo $link->link_status; ?>">
							<button type="button" class="btn btn-mini <?php echo $status_class; ?>"><?php echo $link->link_status; ?></button>
						</span>
					</td>					
					<td class="center hidden-phone">
						<?php echo $this->escape($link->link_published_on); ?>
					</td>					
					<td class="center hidden-phone nowrap">
						<?php echo $this->escape($lrco); ?>
						<?php if ($link->link_type == 'TWO-WAY') :
								JvleUtil::makeAjaxCall('link'.$link->id, $link->id, "jvleloading", "blcarea");
						?>
								<p><button class="btn btn-mini" id="link<?php echo $link->id; ?>"><?php echo JText::_('COM_JVLE_CHECK_NOW'); ?></button></p>
						<?php endif; ?>
					</td>					
					<td class="center hidden-phone">
						<?php echo JHtml::_('jgrid.published', $link->featured, $i, 'links.', 0, 'cb1'); ?>
					</td>
					<td class="center hidden-phone">
						<span class="hasTip" title="<?php echo $link_or_banner; ?>">
							<button type="button" class="btn btn-mini"><?php echo $link_or_banner; ?></button>
						</span>					
					</td>						
					<td class="center hidden-phone">
						<?php echo $this->escape($link->id); ?>
					</td>
				</tr>				
			<?php endforeach; ?>
			</tbody>
		</table>
		
		<?php echo $this->loadTemplate('chgcat'); ?>
		<?php echo $this->loadTemplate('chgsts'); ?>
	</div>
	<div id="jvleloading" style="display:none;"></div>

	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />	
	<?php echo JHtml::_('form.token'); ?>
</form>