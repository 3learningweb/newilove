<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');
?>
<div class="modal hide fade" id="collapseModal1">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">x</button>
		<h3><?php echo JText::_('COM_JVLE_LINKS_CHANGE_CATEGORY');?></h3>
	</div>
	<div class="modal-body">
		<div class="control-group">
			<div class="control-label"><?php echo JText::_('COM_JVLE_LINKS_NEWCAT'); ?></div>
			<div class="controls">
				<?php echo JvleUtil::getCategoryDropdown('chgcat_newcat'); ?>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn" type="button" data-dismiss="modal">
			<?php echo JText::_('JCANCEL'); ?>
		</button>
		<button class="btn btn-primary" type="submit" onclick="Joomla.submitbutton('link.chgcat');">
			<?php echo JText::_('COM_JVLE_LINKS_CHANGE_CATEGORY'); ?>
		</button>
	</div>
</div>