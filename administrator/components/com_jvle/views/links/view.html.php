<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewLinks extends JViewLegacy
{
	protected $items = array();
	
	function display($tpl = null)
	{
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');
		
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		$this->cfg = JvleCfg::getInstance();
		
		$this->addToolBar();
		$this->sidebar = JHtmlSidebar::render();
		$this->setDocument();
		
		parent::display($tpl);				
	}

	protected function addToolBar()
	{
		$bar = JToolBar::getInstance('toolbar');		
		JToolBarHelper::title(JText::_('COM_JVLE_INFO').' '.JText::_('COM_JVLE_LINKS'), 'links');		
		JHtmlSidebar::setAction('index.php?option=com_jvle');
		
		JToolbarHelper::addNew('link.add');
		
		JHtml::_('bootstrap.modal', 'collapseModal');
				
		$title = JText::_('COM_JVLE_LINKS_CHANGE_CATEGORY');
		$dhtml = "<button data-toggle=\"modal\" data-target=\"#collapseModal1\" class=\"btn btn-small\"><i class=\"icon-checkbox-partial\" title=\"".$title."\"></i> ".$title."</button>";
		$bar->appendButton('Custom', $dhtml, 'chgcat');
		
		$title = JText::_('COM_JVLE_LINKS_CHANGE_STATUS');
		$dhtml = "<button data-toggle=\"modal\" data-target=\"#collapseModal2\" class=\"btn btn-small\"><i class=\"icon-checkbox-partial\" title=\"".$title."\"></i> ".$title."</button>";
		$bar->appendButton('Custom', $dhtml, 'chgsts');
		
		JToolBarHelper::deleteList(JText::_('COM_JVLE_LINKS_DEL_CONFIRM'), 'links.delete');

        $canDo = JvleAdminUtil::getActions();
        if ($canDo->get('core.admin'))
            JToolBarHelper::preferences('com_jvle');
        
        // Link types
        $ltypes = array();
        $ltypes[] = JHtml::_('select.option', 'ONE-WAY', JText::_('COM_JVLE_OWAY_LINK'));
        $ltypes[] = JHtml::_('select.option', 'TWO-WAY', JText::_('COM_JVLE_TWAY_LINK'));
        
        JHtmlSidebar::addFilter(
        	'- '.JText::_('COM_JVLE_LINK_LTYPE').' -',
        	'filter_ltypes',
	        JHtml::_('select.options', $ltypes, 'value', 'text', $this->state->get('filter.ltypes'))
        );
        
        // Link Status
        $lsts = array();
        $lsts[] = JHtml::_('select.option', 'ESTABLISHED', 'Established');
        $lsts[] = JHtml::_('select.option', 'PENDING', 'Pending');
        $lsts[] = JHtml::_('select.option', 'REJECTED', 'Rejected');
        $lsts[] = JHtml::_('select.option', 'UNKNOWN', 'Unknown');
        
        JHtmlSidebar::addFilter(
        	'- '.JText::_('COM_JVLE_LINK_STATUS').' -',
        	'filter_lsts',
        	JHtml::_('select.options', $lsts, 'value', 'text', $this->state->get('filter.lsts'))
        );
        
        // Featured
		$lfeatured = array();
		$lfeatured[] = JHtml::_('select.option', 'Y', JText::_('COM_JVLE_LINK_FEATURED_YES'));
		$lfeatured[] = JHtml::_('select.option', 'N', JText::_('COM_JVLE_LINK_FEATURED_NO'));

		JHtmlSidebar::addFilter(
			'- '.JText::_('COM_JVLE_LINK_FEATURED').' -',
			'filter_lfeatured',
			JHtml::_('select.options', $lfeatured, 'value', 'text', $this->state->get('filter.lfeatured'))
		);

		// Category
		$lcategory = $cats = array();
		JvleUtil::getCategories(0, $cats);		 
		if (count($cats))
		{
			foreach($cats as $catg)
			{
				$prefix = ($catg->cpid) ? '..... ' : '';
				$lcategory[] = JHtml::_('select.option', $catg->id, $prefix.$catg->name);
			}
		}
		
		JHtmlSidebar::addFilter(
			'- '.JText::_('COM_JVLE_LINK_CATEGORY').' -',
			'filter_lcategory',
			JHtml::_('select.options', $lcategory, 'value', 'text', $this->state->get('filter.lcategory'))
		);		
	}

	protected function setDocument()
	{
		JHtml::_('behavior.framework');
		
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('COM_JVLE_INFO').' '.JText::_('COM_JVLE_LINKS'));
	}

	protected function getSortFields()
	{
		return array(
					"link_published_on" => JText::_("COM_JVLE_LINK_PUBON"),
					"link_rating" => JText::_("COM_JVLE_LINK_RATING"),
					"num_votes" => JText::_("COM_JVLE_LINK_VOTES"),
					"hits" => JText::_("COM_JVLE_LINK_HITS"),
					"id" => JText::_("JGRID_HEADING_ID"),
				);
	}
}