<?php defined('_JEXEC') or die('Restricted access'); ?>

<div class="row-fluid">
	<div class="span3">
		<table class="table table-striped">
		<tr class="error" id="start">
			<td><strong><?php echo JText::_('COM_JVLE_INSTALL_INITIATION'); ?></strong></td>
		</tr>
<?php for ($i=0;$i<count($this->versions_to_upgrade);$i++) : ?>
		<tr id="<?php echo $this->versions_to_upgrade[$i]; ?>">
			<td><strong><?php echo JText::_('COM_JVLE_INSTALL_STEP'); ?>-<?php echo $i+2; ?>: <?php echo JText::_('COM_JVLE_INSTALL_UPGRADE_TO'); ?> <?php echo $this->versions_to_upgrade[$i]; ?></strong></td>
		</tr>
<?php endfor; ?>
		</table>
	</div>
	<div class="span9">
		<div class="row-fluid">
    		<div class="progress">
    			<div id="pbar" class="bar" style="width: 1%;"></div>
    		</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<p class="alert alert-info"><?php echo $this->desc; ?></p>
				<p class="text-center"><button type="button" id="install" name="install" class="btn btn-primary"><?php echo JText::_('COM_JVLE_INSTALL_BUTTON'); ?></button></p>
				<div id="iarea"></div>
			</div>
		</div>
	</div>
</div>

<form name="jvextins" id="jvextins">
	<input type="hidden" name="finstall" value="0" />
</form>