<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewInstall extends JViewLegacy
{
	protected $desc;
	protected $versions_to_upgrade = array();

	function display($tpl = null)
	{
		JHtmlBehavior::framework(); 
		
		$title = "";

		try
		{
			$ma_version = JvleInstallerHelper::getVersionFromManifest();
			JvleInstallerDebug::log("Version from manifest: ".$ma_version);

			$db_version = JvleInstallerHelper::getDbVersion();
			JvleInstallerDebug::log("Version from db: ".$db_version);

			if (0 == $db_version)
			{
				JvleInstallerDebug::log("Initiating a fresh installation...");
				$title = "JV-LinkExchanger Suite - New Installation";
				$this->desc = "Welcome to New Installation of JV-LinkExchanger Component. This utility will install JV-LinkExchanger component version ".$ma_version." on your Joomla website. Please click on the 'Install/Upgrade' button to start the installation.";
			}
			else
			{
				JvleInstallerDebug::log("Initiating an upgrade installation...");
				
				$sindex = $tindex = 0;
				$this->versions_to_upgrade = JvleInstallerHelper::getVersionsUpgradeList($db_version, $sindex, $tindex);
				
				$title = "JV-LinkExchanger Suite - Upgrade";
				$this->desc = "Welcome to Upgrade Installation of JV-LinkExchanger Component. This utility will upgrade JV-LinkExchanger component to version ".$ma_version." on your Joomla website. Please click on the 'Install/Upgrade' button to start the upgrade.";
	
				JvleInstallerDebug::log("Versions to upgrade: <pre>".print_r($this->versions_to_upgrade, true)."</pre>");
			}

			$ajaxQuery = '  	var Element = Element.implement ({
									appendHTML: function(html) {
										var existing_html = this.get("html");
										this.set("html", existing_html + html);
									}
								});

            					window.addEvent("domready", function() {
            						document.id("install").addEvent("click", function() {
            							document.id("install").disabled = true;

            							var url = "index.php?option=com_jvle";
            							var req = new Request({
            								url: url,
            								method: "post",
            								onRequest: function() {
                                            },
            								onSuccess: function(response) {
            									var parsed = response.split("|");
            									var nstep = parsed[0];
            									var errflag = parsed[1];
            									var percent = parsed[2];
            									var msg = parsed[3];

            									document.id("iarea").appendHTML("<p>"+msg+"</p>");

            									document.id("pbar").setStyle("width", percent+"%");

            									if (nstep == "0") {
            										if (errflag == "0") {
            											document.id("iarea").appendHTML("'.JText::_("COM_JVLE_INSTALLUPGRADE_OK").'");
            										}
            										else {
            											document.id("pbar").addClass("bar-danger");
            											document.id("iarea").appendHTML("'.JText::_("COM_JVLE_INSTALLUPGRADE_ERR").'");
            										}
            									}
												else {
													$$("tr").removeClass("error");
													document.id(nstep).addClass("error");
													document.id("install").fireEvent("click");
												}
            								},
            								onFailure: function() {
            									alert("Error encountered during installation/upgrade");
            								}
            							}).send("view=ajax&layout=install&format=raw");
            						});
            					});';
			$document = JFactory::getDocument();
			$document->addScriptDeclaration($ajaxQuery);
		}
		catch (Exception $ex)
		{
			$title = "JV-LinkExchanger Installation";
			$this->desc = $ex->getMessage();
		}

		JToolBarHelper::title($title, 'generic.png');
		parent::display($tpl);
	}
}

