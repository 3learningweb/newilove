<?php
/**
 * @version		$Id: edit.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.formvalidation');

$script = "
	jQuery(document).ready(function ($){
		$('#jform_link_type').change(function(){
			if($(this).val() == 'ONE-WAY') {
				$('#xchange').css('display', 'none');
			} else {
				$('#xchange').css('display', 'block');
			}
		}).trigger('change');
		$('#jform_exchange_link').change(function(){
			if($(this).val() == 1) {
				$('#plink').css('display', 'block');
				$('#pbanner').css('display', 'none');
			} else if($(this).val() == 0) {
				$('#plink').css('display', 'none');
				$('#pbanner').css('display', 'block');
			} else {
				$('#plink').css('display', 'none');
				$('#pbanner').css('display', 'none');
			}		
		}).trigger('change');		
	});";
JFactory::getDocument()->addScriptDeclaration($script);
?>

<form action="<?php echo JRoute::_('index.php?option=com_jvle&layout=edit&id='.(int)$this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-horizontal" enctype="multipart/form-data">
<fieldset>
	<legend><?php echo $this->escape($this->title); ?></legend>
	<div class="span6">
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('link_type'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('link_type'); ?></div>
		</div>		
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('partner_url'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('partner_url'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('partner_email'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('partner_email'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('featured'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('featured'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('link_category'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('link_category'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('exchange_link'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('exchange_link'); ?></div>
		</div>		
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('link_rating'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('link_rating'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('num_votes'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('num_votes'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('hits'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('hits'); ?></div>
		</div>
		<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('id'); ?></div>
		</div>									
	</div>
	<div class="span5">
		<div class="row-fluid" id="xchange" style="display:none;">
			<div class="span12">
				<h4>Reciprocal Link/Banner Exchange Options</h4>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('reciprocal_link_url'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('reciprocal_link_url'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('enable_scheduler'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('enable_scheduler'); ?></div>
				</div>										
			</div>
		</div>
		<div class="row-fluid" id="plink" style="display:none;">
			<div class="span12">
				<h4>Partner Link Options</h4>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('partner_title'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('partner_title'); ?></div>
				</div>		
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('partner_desc'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('partner_desc'); ?></div>
				</div>									
			</div>
		</div>
		<div class="row-fluid" id="pbanner" style="display:none;">
			<div class="span12">
				<h4>Partner Banner Options</h4>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('banner_loc'); ?></div>
					<div class="controls">
						<?php if ($this->item->id) : ?>
						<p><img src="<?php echo _JVLE_BANNERS.$this->item->banner_loc; ?>" /></p>
						<?php endif; ?>
						
						<?php echo $this->form->getInput('banner_loc'); ?>
						<p><small><?php echo JText::sprintf('COM_JVLE_LINK_BANNER_USAGE', $this->cfg->banner_extns, $this->cfg->banner_kbsize_max, $this->cfg->banner_width_max, $this->cfg->banner_height_max); ?></small></p>
					</div>
				</div>						
			</div>
		</div>		
	</div>
</fieldset>

<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>

<?php echo JvleAdminUtil::showFooter(); ?>
