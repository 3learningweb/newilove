<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewAjax extends JViewLegacy
{
    function display($tpl = null)
    {
    	$layout = JFactory::getApplication()->input->getString('layout', '');
    	switch ($layout)
    	{
    		case 'install': $this->installJvle(); break;
    		case 'blcheck': $this->checkBackLink($tpl); break;
    		default: echo ''; break;
    	}

    	JFactory::getApplication()->close();
    }
    
    private function checkBackLink($tpl)
    {
    	try
    	{
    		$cfg = JvleCfg::getInstance();
    		
	    	$id = JFactory::getApplication()->input->getInt('id', 0);
	    	if (!$id)
	    		throw new Exception(JText::_('COM_JVLE_INVALIDREQ'));
	    	
	    	$link = JvleDb::getRow("select * from #__jvle_links where id = ".(int)$id);
	    	if (!$link)
	    		throw new Exception(JText::_('COM_JVLE_INVALIDREQ'));
	
	    	if ($link->link_type == 'ONE-WAY')
	    		throw new Exception(JText::_('COM_JVLE_BLINK_CHECK_ERR_1'));
	    	
	    	if ($link->reciprocal_link_url == '')
	    		throw new Exception(JText::_('COM_JVLE_BLINK_CHECK_ERR_2'));
	    	
	    	$ret = JvleUtil::checkReciprocalLink($link->reciprocal_link_url, $cfg->self_url);
	    	switch ($ret)
	    	{
				case 0: 
                {
                	$msg = JText::_('COM_JVLE_BLINK_CHECK_ERR_3'); 
                    $new_status = 'ESTABLISHED';
                    break;
                }
                case 1: 
                {
                	$msg = JText::_('COM_JVLE_BLINK_CHECK_ERR_4');
                	$new_status = $link->link_status;
                	break;
                }
				case 2: 
				{
					$msg = JText::_('COM_JVLE_BLINK_CHECK_ERR_5');
 					$new_status = 'PENDING';
                    break;
				}
                case 3: 
                {
                	$msg = JText::_('COM_JVLE_BLINK_CHECK_ERR_6');
                	$new_status = 'PENDING';
                	break;
                }
                default: 
                {
                	$msg = JText::_('COM_JVLE_BLINK_CHECK_ERR_7');
                	$new_status = $link->link_status;
                	break;
                }
	    	}
	    	
	    	if ($cfg->rlc_status_update)
	    		JvleUtil::changeLinkStatus($id, $new_status);
	    	
	    	JvleUtil::updateLastRecipDate($id);
	    	
			echo $msg;	    	
    	}
    	catch (Exception $ex)
    	{
    		echo $ex->getMessage();
    	}
	}

	private function installJvle()
	{
		$percent_complete = 0;
		$out = '';
	
		$ma_version = JvleInstallerHelper::getVersionFromManifest();
		$db_version = JvleInstallerHelper::getDbVersion();
	
		try
		{
			if (0 == $db_version)
			{
				$installer = JvleInstallerHelper::getInstallerObject("0.0");
				$installer->execute();
				if ($installer->getErrorFlag())
					throw new Exception($installer->getMessage());
	
				JvleInstallerHelper::updateVersionInDb($ma_version);
	
				$percent_complete = 100;
				$out .= '0|0|'.$percent_complete.'|'.$installer->getMessage();
			}
			else
			{
				$sindex = $tindex =  0;
				$versions_to_upgrade = JvleInstallerHelper::getVersionsUpgradeList($db_version, $sindex, $tindex);
	
				if (!count($versions_to_upgrade))
				{
					$percent_complete = 100;
					$out .= '0|0|'.$percent_complete.'|Upgrade complete';
				}
				else
				{
					$installer = JvleInstallerHelper::getInstallerObject($versions_to_upgrade[0]);
					$retval = $installer->execute();
					if ($installer->getErrorFlag())
						throw new Exception($installer->getMessage());
	
					JvleInstallerHelper::updateVersionInDb($versions_to_upgrade[0]);
	
					if (count($versions_to_upgrade) == 1)
					{
						$percent_complete = 100;
						$out .= '0|0|'.$percent_complete.'|'.$installer->getMessage();
					}
					else
					{
						$percent_complete = intval((1 + $sindex)*100/(1 + $tindex + 1));
						$percent_complete = ($percent_complete > 100) ? 100 : $percent_complete;
	
						$out .= $versions_to_upgrade[1].'|0|'.$percent_complete.'|'.$installer->getMessage();
					}
				}
			}
		}
		catch (Exception $ex)
		{
			$percent_complete = 100;
			$out .= '0|1|'.$percent_complete.'|<p class="alert alert-error">'.$ex->getMessage().'</p>';
		}
	
		$out = JString::trim($out, "<br />");
		$out .= "<br />";
		echo $out; // {done(1)}|{errorflag(1)}|{percent complete}|{message}
	}
}