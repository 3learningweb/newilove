<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');
?>

<script type="text/javascript">
Joomla.orderTable = function()
{
	table = document.getElementById("sortTable");
	direction = document.getElementById("directionTable");
	order = table.options[table.selectedIndex].value;
	if (order != '<?php echo $listOrder; ?>')
	{
		dirn = 'asc';
	}
	else
	{
		dirn = direction.options[direction.selectedIndex].value;
	}
	Joomla.tableOrdering(order, dirn, '');
}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_jvle&view=categories'); ?>" method="post" name="adminForm" id="adminForm">

<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
<?php 	echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
		<table class="table table-striped" id="categoryList">
			<thead>
				<tr>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
					</th>
					<th width="25%" class="nowrap left">
						<?php echo $this->escape(JText::_('COM_JVLE_CATS_NAME')); ?>
					</th>
					<th width="38%" class="nowrap center">
						<?php echo $this->escape(JText::_('COM_JVLE_CATS_DESC')); ?>
					</th>
					<th width="8%" class="nowrap center">
						<?php echo $this->escape(JText::_('COM_JVLE_CATS_NLINKS')); ?>
					</th>
					<th width="8%" class="nowrap center">
						<?php echo $this->escape(JText::_('COM_JVLE_CATS_MLINKS')); ?>
					</th>
					<th width="8%" class="nowrap center">
						<?php echo $this->escape(JText::_('COM_JVLE_CATS_ALINKS')); ?>
					</th>
					<th width="8%" class="nowrap center">
						<?php echo $this->escape(JText::_('COM_JVLE_CATS_VIS')); ?>
					</th>
					<th class="nowrap center hidden-phone">
						<?php echo $this->escape(JText::_('JGRID_HEADING_ID')); ?>
					</th>
				</tr>
			</thead>
			<tfoot>
			</tfoot>
			<tbody>
			<?php
					foreach ($this->items as $i => $category) :
						$numlinks = JvleUtil::getNumlinks($category->id);
						$addimg = ($category->cpid) ? "&nbsp;&nbsp;&nbsp;&nbsp;<img align='center' src='".JUri::root()."administrator/components/com_jvle/assets/images/rdir.png' border='0' />&nbsp;&nbsp;" : "";
						$maxlinks = ($category->maxlinks) ? $category->maxlinks : JText::_('COM_JVLE_UNLIMITED');					
			?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center hidden-phone">
						<?php echo JHtml::_('grid.id', $i, $category->id); ?>
					</td>
					<td class="hidden-phone">
						<?php echo $addimg; ?><a href="index.php?option=com_jvle&task=category.edit&id=<?php echo $category->id; ?>"><?php echo $this->escape($category->name); ?></a>
					</td>
					<td class="left hidden-phone">
						<?php echo $this->escape($category->description); ?>
					</td>
					<td class="center hidden-phone">
						<?php echo $this->escape($numlinks); ?>
					</td>
					<td class="center hidden-phone">
						<?php echo $this->escape($maxlinks); ?>
					</td>					
					<td class="center hidden-phone">
						<?php echo JHtml::_('jgrid.published', $category->accept_links, $i, 'categories.', 0, 'cb1'); ?>
					</td>
					<td class="center hidden-phone">
						<?php echo JHtml::_('jgrid.published', $category->visibility, $i, 'categories.', 0, 'cb3'); ?>
					</td>
					<td class="center hidden-phone">
						<?php echo $this->escape($category->id); ?>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		
		<br />
		<div class="well well-small"><?php echo JText::_('COM_JVLE_CATS_NOTE1'); ?></div>

	</div>

	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo JHtml::_('form.token'); ?>
</form>