<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewCategories extends JViewLegacy
{
	protected $items = array();
	
	function display($tpl = null)
	{
		JvleUtil::getCategories(0, $this->items);
		
		$this->cfg = JvleCfg::getInstance();
		
		$this->addToolBar();
		$this->sidebar = JHtmlSidebar::render();
		$this->setDocument();

		parent::display($tpl);
	}

	protected function addToolBar()
	{
		JToolBarHelper::title(JText::_('COM_JVLE_INFO').' '.JText::_('COM_JVLE_CATEGORIES'), 'categories');
		
		JToolbarHelper::addNew('category.add');
		JToolBarHelper::deleteList(JText::_('COM_JVLE_CATS_DEL_CONFIRM'), 'categories.delete');

        $canDo = JvleAdminUtil::getActions();
        if ($canDo->get('core.admin'))
            JToolBarHelper::preferences('com_jvle');
	}

	protected function setDocument()
	{
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('COM_JVLE_INFO').' '.JText::_('COM_JVLE_CATEGORIES'));
	}
}