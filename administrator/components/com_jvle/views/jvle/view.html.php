<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewJvle extends JViewLegacy
{
    protected $installed_version;
    protected $latest_version;
    protected $arrFeeds = array();

    function display($tpl = null)
    {
        $this->installed_version = JvleInstallerHelper::getVersionFromManifest();
        $this->latest_version = JvleUtil::getLatestVersion();

        // Get live news feed for JV-LE
        $arrFeeds = array();
        if (class_exists("DOMDocument"))
        {
        	$doc = new DOMDocument();
        	$doc->load(JvleUtil::NEWS_FEED_URL);
        	$cnt=0;
        	foreach ($doc->getElementsByTagName('item') as $node) {
        		$itemRSS = array (
        				'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
        				'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
        				'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
        				'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue
        		);
        		array_push($arrFeeds, $itemRSS);
        		$cnt++;
        		if ($cnt == 5)
        			break;
        	}
        }
        $this->arrFeeds = $arrFeeds;

        $this->addToolBar();
        $this->sidebar = JHtmlSidebar::render();
        $this->setDocument();

        parent::display($tpl);
    }

    protected function addToolBar()
    {
        JToolBarHelper::title(JText::_('COM_JVLE_INFO').' '.JText::_('COM_JVLE_DASHBOARD'), 'jvle');

        $canDo = JvleAdminUtil::getActions();
        if ($canDo->get('core.admin'))
            JToolBarHelper::preferences('com_jvle');
    }

    protected function setDocument()
    {
    	JHtmlBehavior::framework();
    	JHtml::_('behavior.modal');

        $document = JFactory::getDocument();
        $document->setTitle(JText::_('COM_JVLE_INFO').' '.JText::_('COM_JVLE_DASHBOARD')); 
        $document->addStyleSheet("components/com_jvle/assets/css/jvle.css");
    }
}