<?php
/**
 * @version		$Id: default.php 128 2011-07-09 09:25:34Z sniranjan $
 * @package		JV-Relatives
 * @subpackage	com_jvrelatives
 * @copyright	Copyright 2008-2013 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

// No direct access
defined('_JEXEC') or die('Restricted access');
?>
<?php if (!empty( $this->sidebar)) : ?>
<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
<?php else : ?>
<div id="j-main-container">
<?php endif;?>

	<div class="row-fluid">
		<div class="span6">
			<div class="row-fluid center">
				<a href="http://www.jv-extensions.com" target="_blank"><img src="<?php echo JUri::root(); ?>administrator/components/com_jvle/assets/images/jvextensions.png" align="middle" alt="JV-LinkExchanger from JV-Extensions" style="border: none; margin: 8px;" /></a>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<h3><?php echo JText::_("COM_JVLE_QUICK_LINKS"); ?></h3>
					<ul class="nav nav-list">
						<li><a href="http://www.jv-extensions.com/my-account/my-downloads" target="_blank"><i class="icon-download"></i> <?php echo JText::_('COM_JVLE_LABEL_DOWNLOADS'); ?></a></li>
						<li><a href="http://www.jv-extensions.com/documentation/jv-linkexchanger" target="_blank"><i class="icon-briefcase"></i> <?php echo JText::_('COM_JVLE_LABEL_DOCUMENTATION'); ?></a></li>
						<li><a href="http://www.jv-extensions.com/support/community-forum/categories/listings/jv-linkexchanger" target="_blank"><i class="icon-user"></i> <?php echo JText::_('COM_JVLE_LABEL_SUPPORT'); ?></a></li>
					</ul>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<h3><?php echo JText::_("COM_JVLE_MORE_PRODUCTS"); ?></h3>
					<table class="table table-hover">
					<tbody>
						<tr><td><img src="<?php echo _JVLE_LIVESITE.'administrator/components/com_jvle/assets/images/jvld.png'; ?>" style="margin-right:8px;" align="left" alt="JV-LinkDirectory"/><strong>JV-LinkDirectory</strong><br /><?php echo JText::_('COM_JVLE_JVLD_DESC'); ?> - <a href="http://www.jv-extensions.com/products/jv-linkdirectory" target="_blank">Read more</a></td></tr>
						<tr><td><img src="<?php echo _JVLE_LIVESITE.'administrator/components/com_jvle/assets/images/jvcl.png'; ?>" style="margin-right:8px;" align="left" alt="JV-ContentLinks"/><strong>JV-ContentLinks</strong><br /><?php echo JText::_('COM_JVLE_JVCL_DESC'); ?> - <a href="http://www.jv-extensions.com/products/jv-contentlinks" target="_blank">Read more</a></td></tr>
						<tr><td><img src="<?php echo _JVLE_LIVESITE.'administrator/components/com_jvle/assets/images/jvrel.png'; ?>" style="margin-right:8px;" align="left" alt="JV-Relatives"/><strong>JV-Relatives</strong><br /><?php echo JText::_('COM_JVLE_JVREL_DESC'); ?> - <a href="http://www.jv-extensions.com/products/jv-relatives" target="_blank">Read more</a></td></tr>
						<tr><td><img src="<?php echo _JVLE_LIVESITE.'administrator/components/com_jvle/assets/images/jvpm.png'; ?>" style="margin-right:8px;" align="left" alt="JV-PostMaster"/><strong>JV-PostMaster</strong><br /><?php echo JText::_('COM_JVLE_JVPM_DESC'); ?> - <a href="http://www.jv-extensions.com/products/jv-postmaster" target="_blank">Read more</a></td></tr>
						<tr><td><img src="<?php echo _JVLE_LIVESITE.'administrator/components/com_jvle/assets/images/jvstw.png'; ?>" style="margin-right:8px;" align="left" alt="JV-ShrinkTheWeb"/><strong>JV-ShrinkTheWeb</strong><br /><?php echo JText::_('COM_JVLE_JVSTW_DESC'); ?> - <a href="http://www.jv-extensions.com/products/jv-shrinktheweb" target="_blank">Read more</a></td></tr>
					</tbody>
				   	</table>
				</div>
			</div>
		</div>
		<div class="offset1 span5 pull-right">
			<h3><?php echo JText::_("COM_JVLE_INFO"); ?></h3>
			<table class="table table-striped">
			<tbody>
				<tr>
		            <td><?php echo JText::_("COM_JVLE_INSTALLED_VERSION"); ?>:</td>
		            <td><?php echo $this->installed_version; ?></td>
		        </tr>
		        <tr>
		            <td><?php echo JText::_("COM_JVLE_LATEST_VERSION"); ?>:</td>
		            <td><?php echo $this->latest_version; ?></td>
		        </tr>
		        <tr>
		            <td><?php echo JText::_("COM_JVLE_COPYRIGHT"); ?>:</td>
		             <td>&copy; 2007-2015, JV-Extensions</td>
		        </tr>
		        <tr>
		            <td><?php echo JText::_("COM_JVLE_AUTHOR"); ?>:</td>
		            <td><a href="http://www.jv-extensions.com" target="_blank">JV-Extensions</a>, <a href="mailto:sales@jv-extensions.com">sales@jv-extensions.com</a></td>
		        </tr>
		        <tr>
		            <td><?php echo JText::_("COM_JVLE_DESCRIPTION"); ?>:</td>
		            <td><?php echo JText::_("COM_JVLE_DESCVAL"); ?></td>
		        </tr>
		        <tr>
		            <td><?php echo JText::_("COM_JVLE_LICENSE"); ?>:</td>
		            <td>GNU GPLv3 - <a class="modal" rel="{handler: 'iframe',size: {x: 700, y: 500}}" target="_blank" href="<?php echo JUri::root(); ?>components/com_jvle/LICENSE.txt"><?php echo JText::_("COM_JVLE_READLICENSE"); ?></a></td>
		        </tr>
			</tbody>
			</table>

			<h3><?php echo JText::_('COM_JVLE_LABEL_NEWSFEED'); ?></h3>
        	<ul class="nav nav-tabs nav-stacked">
			<?php for ($i=0;$i<count($this->arrFeeds);$i++) : ?>
        		<li>
        			<a href="<?php echo $this->arrFeeds[$i]['link']; ?>" target="_blank">
        				<small><?php echo $this->arrFeeds[$i]['date']; ?></small><br />
        				<?php echo $this->arrFeeds[$i]['title']; ?>
        			</a>
        		</li>
        		<?php endfor; ?>
        	</ul>
		</div>
	</div>
	<?php echo JvleAdminUtil::showFooter(); ?>
</div>