<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class JvleViewCategory extends JViewLegacy
{
	protected $form;
	protected $item;

    function display($tpl = null)
    {
    	$this->form = $this->get('Form');
        $this->item = $this->get('Item');

		if (count($errors = $this->get('Errors')))
        {
			JError::raiseError(500, implode('\n', $errors));
            return false;
		}

		$this->cfg = JvleCfg::getInstance();
		$this->title = ($this->item->id) ? JText::_('COM_JVLE_CATEGORY_TITLE_EDIT') : JText::_('COM_JVLE_CATEGORY_TITLE_ADD');

        $this->addToolBar();
        $this->setDocument();

        parent::display($tpl);
    }

    protected function addToolBar()
    {
    	JFactory::getApplication()->input->set('hidemainmenu', true);
    
    	JToolBarHelper::title(JText::_('COM_JVLE_INFO').' - '.$this->title, 'jvle');    	
    	JToolBarHelper::apply('category.apply');
    	JToolBarHelper::save('category.save');
    	JToolBarHelper::save2new('category.save2new');
    	JToolBarHelper::cancel('category.cancel');     
    }
    
    protected function setDocument()
    {
    	$document = JFactory::getDocument();
    	$document->setTitle(JText::_('COM_JVLE_INFO').' - '.$this->title);
    }
}