<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

if (!JFactory::getUser()->authorise('core.manage', 'com_jvle'))
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));

if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'common'.DS.'jvle.resources.php');

define("_JVLE_ITEMID", JvleBase::getItemid());

require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'common'.DS.'jvle.utilities.php');

define("_JVLE_ABSPATH", JPATH_COMPONENT_ADMINISTRATOR);
define("_JVLE_LIVESITE", JvleUtil::getLiveSiteURL());
define("_JVLE_IMGDIR", _JVLE_LIVESITE."components/com_jvle/assets/images/");
define("_JVLE_ABS_BANNERS", JPATH_SITE.DS.'images'.DS.'com_jvle'.DS.'banners');
define("_JVLE_BANNERS", _JVLE_LIVESITE.'images/com_jvle/banners/');

require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'libraries'.DS.'jvle.adminutilities.php');

require_once(_JVLE_ABSPATH.DS."libraries".DS."class.installer.php");

jimport('joomla.application.component.controller');

$controller = JControllerLegacy::getInstance('Jvle');

$controller->execute(JFactory::getApplication()->input->getCmd('task'));
$controller->redirect();