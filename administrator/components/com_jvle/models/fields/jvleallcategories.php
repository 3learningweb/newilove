<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

JFormHelper::loadFieldClass('list');

class JFormFieldJvleallcategories extends JFormFieldList
{
	protected $type = 'Jvleallcategories';

	protected function getOptions()
	{
		$options = array();
		
		$cats = array();
		JvleUtil::getCategories(0, $cats);
		
		if (count($cats))
		{
			foreach($cats as $catg)
			{
				$prefix = ($catg->cpid) ? '..... ' : '';
				$suffix = ($catg->accept_links) ? '' : ' ***';
				$options[] = array('text'=>$prefix.$catg->name.$suffix, 'value'=>$catg->id);
			}				
		}
		
		$options = array_merge(parent::getOptions(), $options);
		return $options;
	}
}
