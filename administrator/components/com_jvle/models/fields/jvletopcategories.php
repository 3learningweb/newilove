<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

JFormHelper::loadFieldClass('list');

class JFormFieldJvletopcategories extends JFormFieldList
{
	protected $type = 'Jvletopcategories';

	protected function getOptions()
	{
		$options = array();
		
		$rows = JvleDb::getRows("select id, name from `#__jvle_categories` where `cpid` = '0' order by `name` asc");
		if (count($rows))
		{
			foreach($rows as $catg)
				$options[] = array('text'=>$catg->name, 'value'=>$catg->id);
		}
		
		$options = array_merge(parent::getOptions(), $options);
		return $options;
	}
}
