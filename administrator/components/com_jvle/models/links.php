<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

class JvleModelLinks extends JModelList
{
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
					'id',
					'link_published_on',
					'link_rating',
					'num_votes',
					'hits',
			);
		}
	
		parent::__construct($config);
	}
	
	protected function getListQuery()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
	
		$query->select(array("id", "partner_url", "link_type", "link_category", "link_status", "rlc_last_checked_on", "featured", "exchange_link", "link_published_on"));
		$query->from("#__jvle_links");
		
		$ltype = $this->state->get('filter.ltypes');
		if ($ltype == 'ONE-WAY')
			$query->where("link_type = 'ONE-WAY'");
		else if ($ltype == 'TWO-WAY')
			$query->where("link_type = 'TWO-WAY'");		
	
		$lsts = $this->state->get('filter.lsts');
		if ($lsts != '')
			$query->where("link_status = '".$lsts."'");
			
		$lfeatured = $this->state->get('filter.lfeatured');
		if ($lfeatured == 'Y')
			$query->where("featured = 1");
		else if ($lfeatured == 'N')
			$query->where("featured = 0");

		$lcategory = (int)$this->state->get('filter.lcategory');
		if ((is_numeric($lcategory)) && ($lcategory))
			$query->where("link_category = ".(int)$lcategory);
		
		$orderCol = $this->getState('list.ordering', 'link_published_on');
		$orderDirn	= $this->getState('list.direction', 'desc');
		$query->order($db->escape($orderCol.' '.$orderDirn));

		return $query;
	}
	
	protected function getStoreId($id = '')
	{
		$id	.= ':'.$this->getState('filter.ltypes');
		$id	.= ':'.$this->getState('filter.lsts');
		$id	.= ':'.$this->getState('filter.lfeatured');
		$id	.= ':'.$this->getState('filter.lcategory');
		$id	.= ':'.$this->getState('filter.state');
	
		return parent::getStoreId($id);
	}
	
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication('administrator');

		$ltype = $this->getUserStateFromRequest($this->context.'.filter.ltypes', 'filter_ltypes', '', 'string');
		$this->setState('filter.ltypes', $ltype);
		
		$lsts = $this->getUserStateFromRequest($this->context.'.filter.lsts', 'filter_lsts', '', 'string');
		$this->setState('filter.lsts', $lsts);

		$lfeatured = $this->getUserStateFromRequest($this->context.'.filter.lfeatured', 'filter_lfeatured', '', 'string');
		$this->setState('filter.lfeatured', $lfeatured);
		
		$lcategory = $this->getUserStateFromRequest($this->context.'.filter.lcategory', 'filter_lcategory', '', '');
		$this->setState('filter.lcategory', $lcategory);
		
		$state = $this->getUserStateFromRequest($this->context.'.filter.state', 'filter_state', '', 'string');
		$this->setState('filter.state', $state);
	
		parent::populateState('link_published_on', 'desc');
	}
}