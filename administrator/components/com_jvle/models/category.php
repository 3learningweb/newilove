<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modeladmin');;

class JvleModelCategory extends JModelAdmin
{
	public function getTable($type = 'Category', $prefix = 'JvleTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_jvle.category', 'category', array('control' => 'jform', 'load_data' => $loadData), true);
		if (empty($form))
			return false;

		return $form;
	}

	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState('com_jvle.edit.category.data', array());
		if (empty($data))
			$data = $this->getItem();

		return $data;
	}
}