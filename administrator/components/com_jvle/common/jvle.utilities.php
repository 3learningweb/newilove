<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

abstract class JvleUtil
{
    const NEWS_FEED_URL = "http://www.jv-extensions.com/blog/feed";
    const VERSIONCHECK_URL = "http://www.jv-extensions.com/versioncheck.php?pid=jvle&j=30x";

    public static function getLatestVersion() 
    {    
    	if (!function_exists('curl_init'))
    		return;

    	$ch = @curl_init(self::VERSIONCHECK_URL."&j=".JVERSION);
    	if ($ch == FALSE) return 2;
    	@curl_setopt($ch, CURLOPT_HEADER, 0);
    	@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	@curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
    	@curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    	@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    	@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    	$resp = @curl_exec($ch);
    	@curl_close($ch);

    	$tokens = explode("|",$resp);
    	return JString::trim($tokens[1]);
    }
	
	public static function getLiveSiteURL()
    {
        return JString::rtrim(JUri::root(), "/")."/";
    }
    
    public static function getCategories($sel_cid, &$cats)
    {
    	$rows = JvleDb::getRows("select * from `#__jvle_categories` where `cpid` = '".(int)$sel_cid."' order by `name` asc");
    	if (!count($rows))
    		return;
    
    	foreach ($rows as $cat)
    	{
    		array_push($cats, $cat);
    		JvleUtil::getCategories($cat->id, $cats);
    	}
    
    	return;
    }

    public static function getNumlinks($cid, $anystatus=1)
    {
    	$sql = ($anystatus) ? "select count(*) as CNT from #__jvle_links where link_category = '".(int)$cid."'" : "select count(*) as CNT from #__jvle_links where link_category = '".(int)$cid."' and link_status = 'ESTABLISHED'";
    	return JvleDb::getCount($sql);
    }
    
    public static function getTopLevelCategories(&$c_id, &$c_name, &$c_cpid, &$c_alink)
    {
        $rows = JvleDb::getRows("select * from `#__jvle_categories` where `cpid` = '0' order by `name` asc");
        if (count($rows))
        {
            foreach($rows as $catg)
            {
                array_push($c_id, $catg->id);
                array_push($c_name, $catg->name);
                array_push($c_cpid, $catg->cpid);
                array_push($c_alink, $catg->accept_links);
            }
        }
    }
    
    public static function isNewLink($url)
    {
    	$parts = parse_url($url);
    
    	$host_in_url = $parts['host'];
    	$host_in_url = (substr($host_in_url, 0, 4) == 'www.') ? substr($host_in_url, 4) : $host_in_url;
    
    	$curl_1 = '://'.$host_in_url;
    	$curl_2 = 'www.'.$host_in_url;
    
    	$cnt = JvleDb::getCount("select count(id) as CNT from #__jvle_links where (partner_url like '%".JvleSecure::defendSQL($curl_1)."%' or partner_url like '%".JvleSecure::defendSQL($curl_2)."%')");
    	return ($cnt) ? 0 : 1;
    }
    
    public static function getCategoryName($cid)
    {
        $obj = JvleDb::getRow("select id, name from #__jvle_categories where id = '".(int)$cid."'");
        return $obj->name;
    }
    
    public static function getCategoryDropdown($elem_name)
    {
    	$options = $cats = array();
    	JvleUtil::getCategories(0, $cats);
    	
    	if (count($cats))
    	{
    		foreach($cats as $catg)
    		{
    			$prefix = ($catg->cpid) ? '..... ' : '';
    			$suffix = ($catg->accept_links) ? '' : ' ***';
    			$options[] = array('text'=>$prefix.$catg->name.$suffix, 'value'=>$catg->id);
    		}
    	}
    	
    	$out = '<select name="'.$elem_name.'" class="inputbox" id="'.$elem_name.'">
					<option value="">'.JText::_('JSELECT').'</option>';
    	
    	for ($i=0;$i<count($options);$i++)
    		$out .= '<option value="'.$options[$i]['value'].'">'.$options[$i]['text'].'</option>';

    	$out .= '</select>';
    	return $out;
    }

    public static function sendEmail($toaddr, $subject, $content, $mode=0) 
    {    
    	try
    	{
	    	$cfg = JvleCfg::getInstance();
	    
	    	$mailer = JFactory::getMailer();
	    	$mailer->setSender(array($cfg->self_email, $cfg->self_email));
	    	$mailer->addRecipient($toaddr);
	    
	    	if ($mode)
	    	{
	    		$mailer->isHTML(true);
	    		$mailer->Encoding = 'base64';
	    	}
	    	else
	    	{
	    		$content = JFilterInput::getInstance()->clean($content, 'string');
	    	}
	    
	    	$mailer->setSubject($subject);
	    	$mailer->setBody($content);
	    
	    	if ($mailer->Send() !== true)
	    		throw new Exception("Error while sending email");
    	}
    	catch (Exception $ex)
    	{
    		echo $ex->getMessage();	
    	}   
    	 
    	return;
    }

    public static function makeAjaxCall($selector, $id, $loading_area, $result_area)
    {
    	$js = '	window.addEvent("domready", function() {
						document.id("'.$selector.'").addEvent("click", function(e) {
							e.stop();				
							var id = '.$id.';
							var req = new Request({
	            				url: "index.php?option=com_jvle",
	            				method: "post",
	            				onRequest: function() {
	            					document.id("'.$selector.'").disabled = true;
	            					document.id("'.$result_area.'").empty();
	            					document.id("'.$result_area.'").setStyle("display", "none");
	            					document.id("'.$loading_area.'").setStyle("display", "block");
	            				},
	            				onSuccess: function(response) {
	            					document.id("'.$loading_area.'").setStyle("display", "none");
	            					document.id("'.$result_area.'").set("html", response);
	            					document.id("'.$result_area.'").setStyle("display", "block");
	            					document.id("'.$selector.'").disabled = false;
	            				},
	            				onFailure: function() {
	            					document.id("'.$loading_area.'").setStyle("display", "none");
	            					document.id("'.$result_area.'").set("text", "Error encountered");
	            					document.id("'.$result_area.'").setStyle("display", "block");
	            					document.id("'.$selector.'").disabled = false;
	            				}
        				}).send("view=ajax&layout=blcheck&format=raw&id="+id);
				});
		});';
    	JFactory::getDocument()->addScriptDeclaration($js);
    }
    
    public static function getRemoteSitePageByCurl($partner_page)
    {
    	$ch = @curl_init($partner_page);
    	if ($ch == FALSE)
    		return null;
    
    	@curl_setopt($ch, CURLOPT_HEADER, 1);
    	@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	@curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    	@curl_setopt($ch, CURLOPT_USERAGENT, 'PHP Client');
    	@curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    	@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
    	@curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
    	$resp = @curl_exec($ch);
    
    	if (@curl_error($ch) != '') { @curl_close($ch); return null; }
    	@curl_close($ch);
    
    	return ($resp != '') ? $resp : null;
    }
    
    public static function getRemoteSitePageBySocket($partner_page)
    {
    	if (function_exists("fsockopen"))
    	{
    		preg_match("/^(http:\/\/)?([^\/]+)/i", $partner_page, $hostname);
    		$host = $hostname[2];
    		$tail = str_replace($hostname[0], "", $partner_page);
    		
    		for($x=0;$x<2;$x++)
    		{
    			$resp = "";
    			$startfile = @fsockopen($host, '80');
    			if ($startfile)
    			{
    				$page = (JString::trim($tail) == "") ? "/" : $tail;
    				fputs($startfile, "GET ".$page." HTTP/1.0\r\nHost: ".$host."\r\nUser-Agent: PHP Client\r\n\r\n");
    				while(!feof($startfile))
    					$resp .= fgets($startfile, 10240);
                    fclose($startfile);
    				return $resp;
    			}
    		}
    	}
    
    	return null;
    }
    
    public static function getRemoteSitePage($partner_page)
    {
    	if (function_exists("curl_init"))
    	{
    		$resp = JvleUtil::getRemoteSitePageByCurl($partner_page);
    		return (($resp == null) ? JvleUtil::getRemoteSitePageBySocket($partner_page) : $resp);
    	}
    	else
    		return JvleUtil::getRemoteSitePageBySocket($partner_page);
    }
    
    public static function checkReciprocalLink($siteurl, $recip)
    {
    	$recip = JString::strtolower(JString::trim($recip, "/ "));
    	$indexfile = JvleUtil::getRemoteSitePage($siteurl);
    	return ($indexfile != null) ? JvleUtil::validateBackLink($indexfile, $recip) : 1;
    }
    
    public static function validateBackLink($content, $url_to_find)
    {
    	$search_pattern = "/<a(.*?)>(.*?)<\/a>/is";
    	preg_match_all($search_pattern, $content, $matches);
    
    	$url_to_find_alt1 = (JString::substr($url_to_find, 7, 4) == 'www.') ? 'http://'.JString::substr($url_to_find, 11) : 'http://www.'.JString::substr($url_to_find, 7);
    
    	for ($url_in=0,$i=0;$i<count($matches[1]);$i++)
    	{
    		$atag = '<a '.$matches[1][$i].'>';
    		$url_in_partner_page = JString::strtolower(JString::trim(JvleUtil::getATagParam(" href", $atag), "/ "));
    
    		if ((JString::substr($url_in_partner_page, 0, JString::strlen($url_to_find)) == $url_to_find) ||
    			(JString::substr($url_in_partner_page, 0, JString::strlen($url_to_find_alt1)) == $url_to_find_alt1))
    		{
    			$url_in = 1;
    			break;
    		}
    	}
    
    	return ($url_in) ? ((JString::strtolower(JString::trim(JvleUtil::getATagParam(" rel", $atag))) == 'nofollow') ? 2 : 0) : 3;
    }
    
    public static function getATagParam($param, $tag)
    {
    	return (preg_match('/'.preg_quote($param).'=([\'"])?((?(1).+?|[^\s>]+))(?(1)\1)/is', $tag, $match)) ? urldecode($match[2]) : '';
    }

    public static function updateLastRecipDate($id) 
    {
    	JvleDb::update("update #__jvle_links set rlc_last_checked_on = '".JFactory::getDate()->toSql()."' where id = ".(int)$id);
    }

    public static function getTargetString()
    {
    	$cfg = JvleCfg::getInstance();
    	return ($cfg->window_new) ? ' target="_blank" ' : '';
    }
    
    public static function showSelectTag($tagname, $fname, $optvar, $optval, $jsfunc='')
    {
    	$ret = "";
    	$ret .= '<select class="inputbox" name="'.$tagname.'" id="'.$tagname.'" '.$jsfunc.'>';
    	for ($i=0;$i<count($optvar);$i++)
    	{
    		$t = ($fname == $optval[$i]) ? 'selected="selected"' : '';
    		$ret .= '<option value="'.$optval[$i].'" '.$t.'>'.$optvar[$i].'</option>';
    	}
    	$ret .= '</select>';
    	
    	return $ret;
    }
    
    public static function uploadFile($file_id, $dest_path, $allowed_extns='', $width=0, $height=0, $maxsize=0, $err_on_noupload=1)
    {
    	$files = JFactory::getApplication()->input->files->get('jform');
    	$file = $files[$file_id];
    	
    	if ((!is_array($file)) || ($file['error'] == 4) || ($file['name'] == ''))
    	{
    		if ($err_on_noupload)
   				throw new Exception(JText::_("COM_JVLE_FILE_NONE"));
    		
    		return '';
    	}
    	
    	$file['name'] = JFile::makeSafe($file['name']);
    	
    	if ($file['error'] || $file['size'] < 1)
    		throw new Exception(JText::_("COM_JVLE_FILE_UPLOAD_ERR"));
    	
    	// Check for file extension
    	if ($allowed_extns != '')
    	{
    		if (!in_array(JFile::getExt($file['name']), explode(",", $allowed_extns)))
    			throw new Exception(JText::_("COM_JVLE_ERR_UPLOAD_EXTN_NOT_ALLOWED"));
    	}
    		
    	// Check for file dimension
    	if ($width && $height)
    	{
    		list ($w1, $h1, $typ, $w2) = getimagesize($file['tmp_name']);
    		if (($w1 > $width) || ($h1 > $height))
    			throw new Exception(JText::_('COM_JVLE_ERR_UPLOAD_IMG_EXCEEDS_DIMN'));
    	}
    		
    	// Check the size of the file
    	if ($maxsize)
    	{
    		$fsize = round($file['size']/1024, 2);
    		if ($fsize > $maxsize)
    			throw new Exception(JText::_('COM_JVLE_ERR_UPLOAD_IMG_EXCEEDS_SIZE'));
    	}
    	 
    	$out_file = "j".time().rand(1, 999)."_".$file['name'];
    	if (JFile::upload($file['tmp_name'], $dest_path.DS.$out_file) == false)
    		throw new Exception(JText::sprintf("COM_JVLE_ERR_UPLOAD_NOMOVE", $dest_path));
    		
    	return $out_file;
    }

    public static function changeLinkStatus($id, $new_status, $reject_reason='')
    {
    	$link = JvleDb::getRow("select * from #__jvle_links where id = ".(int)$id);
    	if (!$link)
    		return;
    
    	if ($link->link_status == $new_status)
    		return;
    
    	$cfg = JvleCfg::getInstance();
    	switch ($new_status)
    	{
    		case 'ESTABLISHED':
    		{
    			JvleDb::update("update #__jvle_links set link_status = 'ESTABLISHED', link_published_on = '".JFactory::getDate()->toSql()."' where id = ".(int)$id);
    			if ($link->partner_email != '')
    			{
    				$lnk = _JVLE_LIVESITE.'index.php?option=com_jvle&Itemid='._JVLE_ITEMID.'&view=all_category_links&cid='.$link->link_category;
    				if ($link->exchange_link)
    				{
    					$msg = JText::sprintf('COM_JVLE_EM_LINK_ACCEPTED_B', $link->partner_url, $cfg->self_url, $lnk, $cfg->self_email, $cfg->self_url);
    					JvleUtil::sendEmail($link->partner_email, JText::_("COM_JVLE_EM_LINK_ACCEPTED_S"), $msg);
    				}
    				else
    				{
    					$msg = JText::sprintf('COM_JVLE_EM_BANNER_ACCEPTED_B', $link->partner_url, $cfg->self_url, $lnk, $cfg->self_email, $cfg->self_url);
    					JvleUtil::sendEmail($link->partner_email, JText::_("COM_JVLE_EM_BANNER_ACCEPTED_S"), $msg);
    				}
    			}
    			break;
    		}
    		case 'REJECTED':
    		{
    			$rreason = ($reject_reason != '') ? $reject_reason : $cfg->generic_reject_reason;
    
    			JvleDb::update("update #__jvle_links set link_status = 'REJECTED' where id = ".(int)$id);
    			if ($link->partner_email != '')
    			{
    				if ($link->exchange_link)
    				{
    					$msg = JText::_("COM_JVLE_EM_LINK_REJECTED_B");
    					if ($rreason != '')
    						$msg .= JText::sprintf('COM_JVLE_EM_LINK_REJECTED_B_REASON', htmlspecialchars($rreason));
    
    					$msg .= JText::sprintf('COM_JVLE_EM_QUERIES', $cfg->self_url, $cfg->self_email);  					
    					JvleUtil::sendEmail($link->partner_email, JText::_("COM_JVLE_EM_LINK_REJECTED_S"), $msg);
    				}
    				else
    				{
    					$msg = JText::_("COM_JVLE_EM_BANNER_REJECTED_B");
    					if ($rreason != '')
    						$msg .= JText::sprintf('COM_JVLE_EM_LINK_REJECTED_B_REASON', htmlspecialchars($rreason));
    
    					$msg .= JText::sprintf('COM_JVLE_EM_QUERIES', $cfg->self_url, $cfg->self_email);
    					JvleUtil::sendEmail($link->partner_email, JText::_("COM_JVLE_EM_BANNER_REJECTED_S"), $msg);
    				}
    			}
    			break;
    		}
    		default:
    		{
    			JvleDb::update("update #__jvle_links set link_status = '".JvleSecure::defendSQL($new_status)."', link_published_on = '0000-00-00 00:00:00' where id = ".(int)$id);
    			break;
    		}
    	}
    
    	return;
    }    
}