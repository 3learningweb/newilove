<?php
/**
 * @version		$Id: controller.php 112 2011-06-13 18:52:28Z sniranjan $
 * @package		JV-LinkExchanger
 * @subpackage	com_jvle
 * @copyright	Copyright 2008-2015 JV-Extensions. All rights reserved
 * @license		GNU General Public License version 3 or later
 * @author		JV-Extensions
 * @link		http://www.jv-extensions.com
 */

defined('_JEXEC') or die('Restricted access');

define("_JVLE_FEATUREDLINK_PAGE", 0);
define("_JVLE_GENCATEGORY_PAGE", 1);
define("_JVLE_LATLINKS_PAGE", 2);
define("_JVLE_SEARCHRESULTS_PAGE", 3); 

jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

abstract class JvleDb
{
    public static function getRows($sql)
    {
        $database = JFactory::getDbo();
        $database->setQuery($sql);
        return $database->loadObjectList();
    }

    public static function getRow($sql)
    {
        $database = JFactory::getDbo();
        $database->setQuery($sql);
        return $database->loadObject();
    }

    public static function getCount($sql)
    {
       $database = JFactory::getDbo();
        $database->setQuery($sql);
        return $database->loadResult();
    }

    public static function update($sql)
    {
        $database = JFactory::getDbo();
        $database->setQuery($sql);
        $database->execute();
    }

    public static function getValue($sql)
    {
        $database = JFactory::getDbo();
        $database->setQuery($sql);
        return $database->loadResult();
    }

    public static function Quote($text) {
    	$database = JFactory::getDbo();
    	return $database->quote($text);
    }
}

abstract class JvleSecure
{
	public static function defendCSRF($type="post")
	{
		if ($type == "post")
			JSession::checkToken() or jexit('Invalid Token');
		else
			JSession::checkToken('get') or jexit('Invalid Token');
	}

	public static function defendSQL($param, $dos_attack=0)
	{
		$database = JFactory::getDbo();
		return ($dos_attack) ? $database->escape($param, true) : $database->escape($param);
	}
}

class JvleCfg
{
	private static $_instance = null;
	private $_config;

	private function __construct() {
		$this->_config = JvleDb::getRow("select * from #__jvle_settings");
	}

	public static function getInstance() {
		if (!self::$_instance) {
			self::$_instance = new JvleCfg;
		}

		return self::$_instance;
	}

	public function __get($var) {
		return (isset($this->_config->$var)) ? $this->_config->$var : "";
	}
}

abstract class JvleBase
{
    public static function getItemid()
    {
        return JvleDb::getValue("select id from #__menu where link = 'index.php?option=com_jvle&view=jvle' and type = 'component' and published = '1'");
    }
}