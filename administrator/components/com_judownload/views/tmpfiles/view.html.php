<?php
/**
 * ------------------------------------------------------------------------
 * JUDownload for Joomla 2.5, 3.x
 * ------------------------------------------------------------------------
 *
 * @copyright      Copyright (C) 2010-2015 JoomUltra Co., Ltd. All Rights Reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @author         JoomUltra Co., Ltd
 * @website        http://www.joomultra.com
 * @----------------------------------------------------------------------@
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');


class JUDownloadViewTmpFiles extends JUDLViewAdmin
{
	
	public function display($tpl = null)
	{
		
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		
		$this->items            = $this->get('Items');
		$this->pagination       = $this->get('Pagination');
		$this->state            = $this->get('State');
		$this->canDo            = JUDownloadHelper::getActions('com_judownload');
		$this->groupCanDoDelete = JUDownloadHelper::checkGroupPermission("tmpfiles.delete");

		
		$this->addToolBar();

		if (JUDownloadHelper::isJoomla3x())
		{
			$this->filterForm    = $this->get('FilterForm');
			$this->activeFilters = $this->get('ActiveFilters');
		}
		
		parent::display($tpl);

		
		$this->setDocument();
	}

	
	protected function addToolBar()
	{
		JToolBarHelper::title(JText::_('COM_JUDOWNLOAD_MANAGER_TEMPORARY_FILES'), 'temporary-files');

		if ($this->canDo->get('core.delete') && $this->groupCanDoDelete)
		{
			JToolBarHelper::deleteList('COM_JUDOWNLOAD_ARE_YOU_SURE_YOU_WANT_TO_DELETE_THESE_FILES', 'tmpfiles.delete', 'JTOOLBAR_DELETE');
			$bar = JToolBar::getInstance('toolbar');
			
			$bar->appendButton('Confirm', JText::_('COM_JUDOWNLOAD_ARE_YOU_SURE_TO_PRUNE_FILES'), 'remove', JText::_('COM_JUDOWNLOAD_PRUNE_BTN'), 'tmpfiles.prunefiles', false);
			$bar->appendButton('Confirm', JText::_('COM_JUDOWNLOAD_ARE_YOU_SURE_TO_RESET_DOWNLOAD_FOLDER'), 'remove', JText::_('COM_JUDOWNLOAD_RESET_DOWNLOAD_FOLDER_BTN'), 'tmpfiles.emptydownloadfolder', false);
		}

		JToolBarHelper::divider();
		$bar = JToolBar::getInstance('toolbar');
		$bar->addButtonPath(JPATH_ADMINISTRATOR . "/components/com_judownload/helpers/button");
		$bar->appendButton('JUHelp', 'help', JText::_('JTOOLBAR_HELP'));
	}

	
	protected function setDocument()
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_JUDOWNLOAD_MANAGER_TEMPORARY_FILES'));
	}

	
	protected function getSortFields()
	{
		return array(
			'ftmp.id'               => JText::_('COM_JUDOWNLOAD_FIELD_ID'),
			'ua.username'           => JText::_('COM_JUDOWNLOAD_FIELD_USERNAME'),
			'ftmp.included_fileids' => JText::_('COM_JUDOWNLOAD_FIELD_DOWNLOAD_FILES'),
			'ftmp.file_path'        => JText::_('COM_JUDOWNLOAD_FIELD_FILE_PATH'),
			'ftmp.file_size'        => JText::_('COM_JUDOWNLOAD_FIELD_FILE_SIZE'),
			'ftmp.created'          => JText::_('COM_JUDOWNLOAD_FIELD_CREATED'),
			'ftmp.removed'          => JText::_('COM_JUDOWNLOAD_FIELD_REMOVED')
		);
	}
}
