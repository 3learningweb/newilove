<?php
/**
 * ------------------------------------------------------------------------
 * JUDownload for Joomla 2.5, 3.x
 * ------------------------------------------------------------------------
 *
 * @copyright      Copyright (C) 2010-2015 JoomUltra Co., Ltd. All Rights Reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @author         JoomUltra Co., Ltd
 * @website        http://www.joomultra.com
 * @----------------------------------------------------------------------@
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.controlleradmin');
jimport('joomla.application.component.view');


class JUDownloadControllerListCats extends JControllerAdmin
{
	
	protected $text_prefix = 'COM_JUDOWNLOAD_LISTCATS';

	
	public function getModel($name = 'ListCats', $prefix = 'JUDownloadModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

    public function saveOrderAjax()
    {
        
        $pks = $this->input->post->get('cid', array(), 'array');
        $order = $this->input->post->get('order', array(), 'array');

        
        JArrayHelper::toInteger($pks);
        JArrayHelper::toInteger($order);

        
        $model = $this->getModel('Category', 'JUDownloadModel');

        
        $return = $model->saveorder($pks, $order);

        if ($return)
        {
            echo "1";
        }

        
        JFactory::getApplication()->close();
    }
}
