<?php
/**
 * ------------------------------------------------------------------------
 * JUDownload for Joomla 2.5, 3.x
 * ------------------------------------------------------------------------
 *
 * @copyright      Copyright (C) 2010-2015 JoomUltra Co., Ltd. All Rights Reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @author         JoomUltra Co., Ltd
 * @website        http://www.joomultra.com
 * @----------------------------------------------------------------------@
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.controlleradmin');


class JUDownloadControllerTmpFiles extends JControllerAdmin
{
	
	protected $text_prefix = 'COM_JUDOWNLOAD_TMP_FILES';

	
	public function getModel($name = 'TmpFile', $prefix = 'JUDownloadModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	
	public function pruneFiles()
	{
		$model            = $this->getModel();
		$totalPrunedFiles = $model->prunefiles();
		$this->setRedirect('index.php?option=com_judownload&view=tmpfiles', JText::sprintf($this->text_prefix . '_N_ITEMS_DELETED', $totalPrunedFiles));
	}

	
	public function emptyDownloadFolder()
	{
		$model = $this->getModel();
		$model->emptyDownloadFolder();
		$this->setRedirect('index.php?option=com_judownload&view=tmpfiles', JText::_('COM_JUDOWNLOAD_DOWNLOAD_FOLDER_HAS_BEEN_RESETED'));
	}
}
