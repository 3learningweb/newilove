CREATE TABLE `#__judownload_documents_ordering` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_catid_documentid` (`cat_id`,`doc_id`),
  KEY `idx_documentid` (`doc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;