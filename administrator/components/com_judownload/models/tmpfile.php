<?php
/**
 * ------------------------------------------------------------------------
 * JUDownload for Joomla 2.5, 3.x
 * ------------------------------------------------------------------------
 *
 * @copyright      Copyright (C) 2010-2015 JoomUltra Co., Ltd. All Rights Reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @author         JoomUltra Co., Ltd
 * @website        http://www.joomultra.com
 * @----------------------------------------------------------------------@
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.modeladmin');


class JUDownloadModelTmpFile extends JModelAdmin
{
	
	public function getTable($type = 'TmpFile', $prefix = 'JUDownloadTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	
	public function getForm($data = array(), $loadData = true)
	{
		
		$form = $this->loadForm('com_judownload.tmpfile', 'tmpfile', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	
	public function getScript()
	{
		return 'administrator/components/com_judownload/models/forms/tmpfile.js';
	}

	
	protected function loadFormData()
	{
		
		$data = JFactory::getApplication()->getUserState('com_judownload.edit.tmpfile.data', array());
		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}

	
	public function pruneFiles()
	{
		$db    = JFactory::getDbo();
		$now   = JFactory::getDate()->toSql();
		$query = "SELECT file_path FROM #__judownload_files_tmp WHERE removed <= '$now'";
		$db->setQuery($query);
		$filePaths = $db->loadColumn();
		foreach ($filePaths AS $filePath)
		{
			if (!empty($filePath))
			{
				$fileDir = dirname($filePath) . "/";
				JFolder::delete(JPATH_SITE . "/" . JUDownloadFrontHelper::getDirectory('download_directory', 'judownload/') . $fileDir);
			}
		}

		$query = "DELETE FROM #__judownload_files_tmp WHERE removed <= '$now'";
		$db->setQuery($query);
		$db->execute();

		return count($filePaths);
	}

	
	public function emptyDownloadFolder()
	{
		$downloadFolder = JPATH_SITE . "/" . JUDownloadFrontHelper::getDirectory('download_directory', 'judownload/');
		
		JFolder::delete($downloadFolder);

		
		$file_index = $downloadFolder . 'index.html';
		$buffer     = "<!DOCTYPE html><title></title>";
		JFile::write($file_index, $buffer);

		
		$fileHtaccessInMedia = JPATH_SITE . "/media/com_judownload/.htaccess";
		if (JFile::exists($fileHtaccessInMedia))
		{
			$fileHtaccess = JPATH_SITE . "/" . JUDownloadFrontHelper::getDirectory('download_directory', 'judownload/') . ".htaccess";
			JFile::copy($fileHtaccessInMedia, $fileHtaccess);
		}

		
		$db    = JFactory::getDbo();
		$query = "DELETE FROM #__judownload_files_tmp";
		$db->setQuery($query);
		$db->execute();
	}

	
	protected function deletePhysicalFiles($id)
	{
		$db    = JFactory::getDbo();
		$query = "SELECT file_path FROM #__judownload_files_tmp WHERE id = $id";
		$db->setQuery($query);
		$filePath = $db->loadResult();
		if (!empty($filePath))
		{
			$fileDir = dirname($filePath) . "/";
			JFolder::delete(JPATH_SITE . "/" . JUDownloadFrontHelper::getDirectory('download_directory', 'judownload/') . $fileDir);
		}
	}

	
	public function delete(&$pks)
	{
		
		$dispatcher = JDispatcher::getInstance();
		$pks        = (array) $pks;
		$table      = $this->getTable();

		
		JPluginHelper::importPlugin('content');

		
		foreach ($pks AS $i => $pk)
		{
			if ($table->load($pk))
			{
				if ($this->canDelete($table))
				{

					$context = $this->option . '.' . $this->name;

					
					$result = $dispatcher->trigger($this->event_before_delete, array($context, $table));
					if (in_array(false, $result, true))
					{
						$this->setError($table->getError());

						return false;
					}

					
					$this->deletePhysicalFiles($table->id);

					if (!$table->delete($pk))
					{
						$this->setError($table->getError());

						return false;
					}

					
					$dispatcher->trigger($this->event_after_delete, array($context, $table));
				}
				else
				{
					
					unset($pks[$i]);
					$error = $this->getError();
					if ($error)
					{
						JError::raiseWarning(500, $error);

						return false;
					}
					else
					{
						JError::raiseWarning(403, JText::_('JLIB_APPLICATION_ERROR_DELETE_NOT_PERMITTED'));

						return false;
					}
				}
			}
			else
			{
				$this->setError($table->getError());

				return false;
			}
		}

		
		$this->cleanCache();

		return true;
	}
}