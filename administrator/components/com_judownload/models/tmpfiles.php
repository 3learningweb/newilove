<?php
/**
 * ------------------------------------------------------------------------
 * JUDownload for Joomla 2.5, 3.x
 * ------------------------------------------------------------------------
 *
 * @copyright      Copyright (C) 2010-2015 JoomUltra Co., Ltd. All Rights Reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 * @author         JoomUltra Co., Ltd
 * @website        http://www.joomultra.com
 * @----------------------------------------------------------------------@
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.modellist');


class JUDownloadModelTmpFiles extends JModelList
{
	
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'ftmp.id',
				'ua.username',
				'ftmp.included_fileids',
				'ftmp.file_path',
				'ftmp.file_size',
				'ftmp.created',
				'ftmp.removed'
			);
		}

		parent::__construct($config);
	}

	
	protected function populateState($ordering = null, $direction = null)
	{
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		parent::populateState('ftmp.created', 'desc');
	}

	
	protected function getStoreId($id = '')
	{
		
		$id .= ':' . $this->getState('list.search');

		return parent::getStoreId($id);
	}

	
	protected function getListQuery()
	{
		
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('ftmp.*');
		$query->select('ua.username');
		$query->from('#__judownload_files_tmp AS ftmp LEFT JOIN #__users AS ua ON ftmp.user_id = ua.id');

		$search = $this->getState('filter.search');

		
		if (!empty($search))
		{
			$search = '%' . $db->escape($search, true) . '%';
			$query->where("ua.username LIKE '{$search}'");
		}

		
		$orderCol  = $this->getState('list.ordering');
		$orderDirn = $this->getState('list.direction');

		if ($orderCol != '')
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	public function getItems()
	{
		$items = parent::getItems();

		if ($items)
		{
			foreach ($items AS $item)
			{
				$item->actionlink = 'index.php?option=com_judownload&amp;task=tmpfile.edit&amp;id=' . $item->id;
			}
		}

		return $items;
	}

}
