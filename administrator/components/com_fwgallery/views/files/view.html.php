<?php
/**
 * FW Gallery 3.1.0
 * @copyright (C) 2014 Fastw3b
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.fastw3b.net/ Official website
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );

class fwGalleryViewFiles extends fwgView {
    function display($tmpl=null) {
        $model = $this->getModel();

		$this->getMenu('files');
        $this->assign('types', JFHelper :: loadTypes($published_only = false));
        $this->assign('search', $model->getUserState('search', '', 'string'));
        $this->assign('project_id', $model->getUserState('project_id'));
        $this->assign('projects', $model->getProjects());
        $this->assign('files', $model->getFiles());
        $this->assign('pagination', $model->getPagination());
        parent::display($tmpl);
    }

    function batch($tmpl=null) {
        $model = $this->getModel();
        $this->assign('projects', $model->getProjects());
        if (!$this->projects) {
			$app = JFactory :: getApplication();
			$app->redirect(JRoute :: _('index.php?option=com_fwgallery&view=fwgallery', JText :: _('FWG_CREATE_A_GALLERY_FIRST')));
        }

		$this->getMenu('files');
        $this->assign('user', JFactory :: getUser());
        $this->assign('clients', $model->getClients());
        $this->assign('params', JComponentHelper :: getParams('com_fwgallery'));
        parent::display($tmpl);
    }
}
