<?php
/**
 * FW Gallery 3.1.0
 * @copyright (C) 2014 Fastw3b
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.fastw3b.net/ Official website
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );
$qty_images = count($this->images);
?>
jQuery('#fwgallery-manualresize-step-1').html(' - <?php echo JText :: sprintf('FWG_FOUND_IMAGES', $qty_images); ?>');
<?php
if ($qty_images) {
?>
jQuery('#fwgallery-manualresize-step-2').html(' - <img src="<?php echo JURI :: root(true); ?>/administrator/components/com_fwgallery/assets/images/pleasewait.gif" alt="<?php echo JText :: _('FWG_PLEASE_WAIT', true); ?>" />');
var form = jQuery('#fw-manualresize-form');
jQuery('input[name="last_id"]', form).val('<?php echo $this->images[0]->id; ?>');
jQuery('input[name="step"]', form).val('2');
jQuery.ajax({
	url: form.attr('src'),
	method: 'post',
	data: form.serialize(),
	dataType: 'script'
});
<?php
}
?>
