<?php
/**
 * FW Gallery 3.1.0
 * @copyright (C) 2014 Fastw3b
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.fastw3b.net/ Official website
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );
$qty_images = count($this->images);
?>
var form = jQuery('#fw-manualresize-form');
<?php
if ($this->result->next_id) {
?>
jQuery('#fwgallery-manualresize-step-2').html(' - <?php echo JText :: sprintf('FWG_PROCESSING_IMAGE', $this->result->pos, $qty_images); ?> <img src="<?php echo JURI :: root(true); ?>/administrator/components/com_fwgallery/assets/images/pleasewait.gif" alt="<?php echo JText :: _('FWG_PLEASE_WAIT', true); ?>" />');
jQuery('input[name="last_id"]', form).val('<?php echo $this->result->next_id; ?>');
jQuery.ajax({
	url: form.attr('src'),
	method: 'post',
	data: form.serialize(),
	dataType: 'script'
});
<?php
} else {
?>
jQuery('#fwgallery-manualresize-step-2').html(' - <?php echo JText :: _('FWG_FINISHED'); ?>');
jQuery('button[name="resize_button"]', form).attr('disabled', false);
<?php
}
?>
