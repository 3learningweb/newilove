<?php
/**
 * FW Gallery 3.1.0
 * @copyright (C) 2014 Fastw3b
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.fastw3b.net/ Official website
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );

class fwGalleryViewPlugins extends fwgView {
    function display($tmpl=null) {
        $model = $this->getModel();

		$this->getMenu('plugins');
        $this->assign('plugins', $model->getPlugins());
        $this->assign('installed_plugins', $model->getInstalledPlugins());
        $this->assign('fwgallery_plugins_installed', $model->checkGalleryPlugins($this->installed_plugins));
        parent::display($tmpl);
    }

    function edit($tmpl=null) {
        $model = $this->getModel();
        if ($plugin = $model->getPlugin()) {
	        if (!headers_sent()) {
				header("Expires: Tue, 1 Jul 2003 05:00:00 GMT");
				header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
				header("Cache-Control: no-store, no-cache, must-revalidate");
				header("Pragma: no-cache");
	        }
	        ob_implicit_flush();
	        $model->processPlugin($plugin);
        }
    }
}
