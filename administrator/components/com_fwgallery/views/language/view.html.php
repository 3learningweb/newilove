<?php
/**
 * FW Real Estate 3.1.0 - Joomla! Property Manager
 * @copyright (C) 2011 Fastw3b
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.fastw3b.net/ Official website
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );

class fwGalleryViewLanguage extends fwgView {
    function display($tmpl=null) {
        $model = $this->getModel();

		$this->getMenu('language');

		$this->assignRef('type', $model->getUserState('type', 0, 'int'));
        $this->assignRef('search', $model->getUserState('search', '', 'string'));
        $this->assignRef('language', JFHelper :: getLanguage());
        $this->assignRef('languages', JFHelper :: getInstalledLanguages());
        $this->assignRef('data', $model->getLanguageData());
        parent::display($tmpl);
    }
}
