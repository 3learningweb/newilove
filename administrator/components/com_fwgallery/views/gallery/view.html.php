<?php
/**
 * FW Gallery 3.1.0
 * @copyright (C) 2014 Fastw3b
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.fastw3b.net/ Official website
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );

class fwGalleryViewGallery extends fwgView {
    function display($tmpl=null) {
        $model = $this->getModel();

		$this->getMenu('fwgallery');
        $this->assign('user', JFactory :: getUser());
        $this->assign('groups', JFHelper :: getGroups());
        $this->assign('clients', $model->getClients());
        $this->assign('obj', $model->getProject());
        parent::display($tmpl);
    }
}
