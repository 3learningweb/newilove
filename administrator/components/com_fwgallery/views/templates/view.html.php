<?php
/**
 * FW Gallery 3.1.0
 * @copyright (C) 2014 Fastw3b
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.fastw3b.net/ Official website
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );

class fwGalleryViewTemplates extends fwgView {
    function display($tmpl=null) {
        $model = $this->getModel();

		$this->getMenu('templates');
        $this->assign('template', JFHelper :: getCurrentTemplate());
        $this->assign('templates', JFHelper :: getTemplates());
        parent::display($tmpl);
    }
}
