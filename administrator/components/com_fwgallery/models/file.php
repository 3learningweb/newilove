<?php
/**
 * FW Gallery 3.1.0
 * @copyright (C) 2014 Fastw3b
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.fastw3b.net/ Official website
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );

class fwGalleryModelFile extends JModelAdmin {
	function getForm($data = array(), $loadData = true) {
	}
    function getUserState($name, $def='', $type='cmd') {
        $app = JFactory::getApplication();
        return $app->getUserStateFromRequest('com_fwgallery.files.'.$name, $name, $def, $type);
    }
    function getProjects() {
		$model = JModelLegacy :: getInstance('files', 'fwGalleryModel');
		return $model->getProjects();
	}
    function getFile() {
        $file = $this->getTable('file');
        if (($ids = (array)JRequest::getVar('cid') and $id = JArrayHelper::getValue($ids, 0)) or $id = JRequest::getInt('id', 0)) {
            $file->load($id);
        } else if ($project_id = JRequest :: getInt('project_id')) {
        	$file->project_id = $project_id;
        }
        return $file;
    }

    function save() {
        $image = $this->getTable('file');
        if ($id = JRequest::getInt('id') and !$image->load($id)) JRequest :: setVar('id', 0);

        if ($image->bind(JRequest::get('default', JREQUEST_ALLOWHTML)) and $image->check() and $image->store()) {
            $this->setError(JText::_('FWG_THE_IMAGE_DATA').' '.JText::_('FWG_STORED_SUCCESSFULLY'));
            return $image->id;
        } else
        	$this->setError(JText::_('FWG_THE_IMAGE_DATA').' '.JText::_('FWG_STORING_ERROR').':'.$image->getError());
    }

    function getClients() {
        $db = JFactory::getDBO();
        $db->setQuery('SELECT u.id, u.name FROM #__users AS u ORDER BY u.name');
        return $db->loadObjectList();
    }

	function getMedias() {
		$medias = array();
		$types = array('flv'=>'flv','youtube'=>'youtube','vimeo'=>'vimeo','blip.tv'=>'blip.tv', 'mov'=>'mov', 'mp4'=>'mp4', 'divx'=>'divx', 'avi'=>'avi');
		foreach ($types as $key=>$media) {
			$medias[] = JHTML :: _('select.option', $key, $media, 'id', 'name');
		}
		return $medias;
	}
}
