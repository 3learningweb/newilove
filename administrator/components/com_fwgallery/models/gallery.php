<?php
/**
 * FW Gallery 3.1.0
 * @copyright (C) 2014 Fastw3b
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.fastw3b.net/ Official website
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );

class fwGalleryModelGallery extends JModelAdmin {
	function getForm($data = array(), $loadData = true) {
	}
    function getUserState($name, $def='', $type='cmd') {
        $app = JFactory::getApplication();
        $context = 'com_fwgallery.projects.';
        return $app->getUserStateFromRequest($context.$name, $name, $def, $type);
    }

    function getProject() {
        $project = $this->getTable('project');
        if (($ids = (array)JRequest::getVar('cid') and $id = JArrayHelper::getValue($ids, 0)) or $id = JRequest::getInt('id', 0)) {
            $project->load($id);
        }
        return $project;
    }

    function save() {
        $project = $this->getTable('project');
        if ($id = JRequest::getInt('id') and !$project->load($id)) JRequest :: setVar('id', 0);

        if ($project->bind(JRequest::get('default', JREQUEST_ALLOWHTML)) and $project->check() and $project->store()) {
            $this->setError(JText::_('FWG_THE_GALLERY_DATA').' '.JText::_('stored successfully'));
            return $project->id;
        } else
            $this->setError(JText::_('FWG_THE_GALLERY_DATA').' '.JText::_('FWG_STORING_ERROR').':'.$project->getError());
    }

    function getClients() {
        $db = JFactory::getDBO();
        $db->setQuery('SELECT u.id, u.name FROM #__users AS u ORDER BY u.name');
        return $db->loadObjectList();
    }

    function getGroups() {
		$acl = JFactory::getACL();
		return array_merge(
			array(JHTML :: _('select.option', '0', JText :: _('FWG_PUBLIC'))),
			(array)$acl->get_group_children_tree(null, 'Public Frontend', false)
		);
    }
}
