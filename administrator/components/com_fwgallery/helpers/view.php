<?php
/**
 * FW Gallery 3.1.0
 * @copyright (C) 2014 Fastw3b
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.fastw3b.net/ Official website
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );

class fwgView extends JViewLegacy {
    function getMenu($menu) {
		$items = array(
			'FWG_GALLERIES' => 'fwgallery',
			'FWG_IMAGES' => 'files',
			'FWG_PLUGINS' => 'plugins',
			'FWG_TEMPLATES' => 'templates',
			'FWG_LANGUAGE' => 'language',
			'FWG_CONFIGURATION' => 'config',
			'FWG_IPHONE_APP' => 'iphone'
		);
		foreach ($items as $title=>$option) {
			JSubMenuHelper::addEntry(
				JText::_($title),
				'index.php?option=com_fwgallery&view='.$option,
				$option == $menu
			);
		}
    }
}