<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_postcards
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Postcards helper.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_postcards
 * @since       1.6
 */
class PostcardsHelper
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string	The name of the active view.
	 * @since   1.6
	 */
	public static function addSubmenu($vName = 'items')
	{

		JHtmlSidebar::addEntry(
			JText::_('COM_POSTCARDS_SUBMENU_ITEMS'),
			'index.php?option=com_postcards&view=items',
			$vName == 'items'
		);
		
		JHtmlSidebar::addEntry(
			JText::_('COM_POSTCARDS_SUBMENU_CATEGORIES'),
			'index.php?option=com_categories&extension=com_postcards',
			$vName == 'categories'
		);
		if ($vName == 'categories')
		{
			JToolbarHelper::title(
				JText::sprintf('COM_CATEGORIES_CATEGORIES_TITLE', JText::_('com_postcards')),
				'postcards-categories');
		}
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param   integer  The category ID.
	 * @return  JObject
	 * @since   1.6
	 */
	public static function getActions($categoryId = 0)
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($categoryId))
		{
			$assetName = 'com_postcards';
			$level = 'component';
		}
		else
		{
			$assetName = 'com_postcards.category.'.(int) $categoryId;
			$level = 'category';
		}

		$actions = JAccess::getActions('com_postcards', $level);

		foreach ($actions as $action)
		{
			$result->set($action->name,	$user->authorise($action->name, $assetName));
		}

		return $result;
	}
}
