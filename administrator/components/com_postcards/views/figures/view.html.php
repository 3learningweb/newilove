<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_postcards
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of figures.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_postcards
 * @since       1.5
 */
class PostcardsViewFigures extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');

		PostcardsHelper::addSubmenu('figures');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/postcards.php';

		$state	= $this->get('State');
		$canDo	= PostcardsHelper::getActions();
		$user	= JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_POSTCARDS_MANAGER_FIGURES'), 'generic.png');
		if (count($user->getAuthorisedCategories('com_postcards', 'core.create')) > 0)
		{
			JToolbarHelper::addNew('figure.add');
		}
		if ($canDo->get('core.edit'))
		{
			JToolbarHelper::editList('figure.edit');
		}
		if ($canDo->get('core.edit.state')) {

			JToolbarHelper::publish('figures.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('figures.unpublish', 'JTOOLBAR_UNPUBLISH', true);

			JToolbarHelper::archiveList('figures.archive');
			JToolbarHelper::checkin('figures.checkin');
		}
		if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
		{
			JToolbarHelper::deleteList('', 'figures.delete', 'JTOOLBAR_EMPTY_TRASH');
		} elseif ($canDo->get('core.edit.state'))
		{
			JToolbarHelper::trash('figures.trash');
		}
		
		if ($canDo->get('core.admin'))
		{
//			JToolbarHelper::preferences('com_postcards');
		}

		JHtmlSidebar::setAction('index.php?option=com_postcards&view=figures');

		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_PUBLISHED'),
			'filter_state',
			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.state'), true)
		);

	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
			'a.state' => JText::_('JSTATUS'),
			'a.title' => JText::_('JGLOBAL_TITLE'),
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}
}
